﻿using Nami.Web.Core;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Nami.Web.HubControl
{
    public class ThongBaoHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }

        public void init(long idUser, string connectId, string type)
        {
            RepositoryConnectUser.Save(idUser, type, connectId);
        }

        public override System.Threading.Tasks.Task OnConnected()
        {

            //Clients.Others.userConnected(Context.ConnectionId);

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool ak)
        {

            //Clients.Others.userLeft(Context.ConnectionId);
            RepositoryConnectUser.Remove(Context.ConnectionId);
            return base.OnDisconnected(false);
        }
    }
}