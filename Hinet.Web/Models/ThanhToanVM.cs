﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.Entities;

namespace Nami.Web.Models
{
    public class ThanhToanVM
    {
        public QLHopDongItem HopDongItemInfo { get; set; }
        public string OrderType { get; set; }
        public long IdItem { get; set; }
        public string ItemType { get; set; }
        public int Amount { get; set; }
        public string OrderDescription { get; set; }
        public string BankCode { get; set; }
        public string Locale { get; set; }
    }
}