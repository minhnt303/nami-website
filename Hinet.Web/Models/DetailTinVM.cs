﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.AppUserService.Dto;
using Nami.Service.BuildingsInfoService.Dto;
using Nami.Service.RoomInfoService.Dto;

namespace Nami.Web.Models
{
    public class DetailTinVM
    {
        public List<BuildingsInfoDto> listBuilding { get; set; }
        public List<RoomInfoDto> listRoom { get; set; }

        public BuildingsInfoDto objBuilding { get; set; }
        public RoomInfoDto objRoom { get; set; }

        public string ItemType { get; set; }
        public long ItemId { get; set; }
        public UserDto CurrentUserInfo { get; set; }
    }
}