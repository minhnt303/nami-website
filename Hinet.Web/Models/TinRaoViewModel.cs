﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.BuildingsInfoService.Dto;
using Nami.Service.Common;
using Nami.Service.RoomInfoService.Dto;

namespace Nami.Web.Models
{
    public class TinRaoViewModel
    {
        public PageListResultBO<BuildingsInfoDto> TinRaoBuilding { get; set; }
        public PageListResultBO<RoomInfoDto> TinRaoRoom { get; set; }
        public string TinhCode { get; set; }
    }
}