﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.Common;
using Nami.Service.QLTinTucService.Dto;
using Nami.Service.QLTuVanService.Dto;

namespace Nami.Web.Models
{
    public class TinTucViewModel
    {
        public List<QLTuVanDto> listTuVanTheoChuDe { get; set; }

        public List<QLTinTucDto> listTinTucByChuDe { get; set; }

        public QLTinTucDto objInfo { get; set; }
        public PageListResultBO<QLTinTucDto> TinTucs { get; set; }
        public QLTuVanDto TuVanObj { get; set; }
        public PageListResultBO<QLTuVanDto> TuVans { get; set; }
    }
}