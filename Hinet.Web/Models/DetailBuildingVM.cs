﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.Entities;
using Nami.Service.BuildingsInfoService.Dto;

namespace Nami.Web.Models
{
    public class DetailBuildingVM
    {
        public List<FileDinhKem> imageList { get; set; }
        public BuildingsInfoDto objInfo { get; set; }

        public List<RoomInfo> ListRoom { get; set; }
    }
}