﻿using Nami.Model.Entities;
using Nami.Service.CommentByAccountService.Dto;
using System.Collections.Generic;

namespace Nami.Web.Models
{
    public class BuildingInfoViewModel
    {
        public List<FileDinhKem> imageList { get; set; }
        public List<CommentByAccountDto> listComment { get; set; }

        public BuildingsInfo objInfo { get; set; }

        public CommentByAccount objComment { get; set; }
        public long? CurrentUserId { get; set; }
    }
}