﻿using Nami.Service.BuildingsInfoService.Dto;
using Nami.Service.RoomInfoService.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nami.Web.Models
{
    public class EndUserPersonalCreateAgreementViewModel
    {
        public string TinhId { get; set; }
        public string HuyenId { get; set; }
        public string XaId { get; set; }
        public decimal? MoneyFrom { get; set; }
        public decimal? MoneyTo { get; set; }
        public string HomeTypeId { get; set; }
        public List<BuildingsInfoDto> ListBuilding { get; set; }
        public List<RoomInfoDto> ListRoom { get; set; }
        public string TrongSoTinh { get; set; }
        public string TrongSoHuyen { get; set; }
        public string TrongSoXa { get; set; }
        public string TrongSoGia { get; set; }
        public bool HuyenArea { get; set; }
        public bool XaArea { get; set; }
        public long? CurrentUserId { get; set; }
    }
}