﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.AgreementsInfoService.Dto;

namespace Nami.Web.Models
{
    public class EndUserPersonalAgreementViewModel
    {
        public List<AgreementsInfoDto> objInfo { get; set; }
    }
}