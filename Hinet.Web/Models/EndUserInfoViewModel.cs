﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.Entities;
using Nami.Model.IdentityEntities;
using Nami.Service.AppUserService.Dto;

namespace Nami.Web.Models
{
    public class EndUserInfoViewModel
    {
        public  AppUser AppUser { get; set; }
    }
}