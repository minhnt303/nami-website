﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.Common;
using Nami.Service.OrderInfoService.Dto;

namespace Nami.Web.Models
{
    public class IndexOrderVM
    {
        public PageListResultBO<OrderInfoDto> ListOrder { get; set; }
    }
}