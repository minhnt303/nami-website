﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Models
{
    public class EndUserAccountViewModel
    {
        //[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        //public string Email { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string PhoneNumber { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Address { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]

        public string FullName { get; set; }
        public string Avatar { get; set; }

    }
}