﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.GiaiTrinhPhanAnhService.Dto;
using Nami.Service.OrderInfoService.Dto;
using Nami.Service.QLHopDongItemService.Dto;

namespace Nami.Web.Models
{
    public class DetailOrderVM
    {
        public OrderInfoDto objInfo { get; set; }
        public List<GiaiTrinhPhanAnhDto> GiaiTrinhList { get; set; }
        //public List<PhanAnhFileAttach> file { get; set; }
    }
}