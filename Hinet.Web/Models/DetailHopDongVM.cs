﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.HistoryService.Dto;
using Nami.Service.QLHopDongItemService.Dto;
using Nami.Service.QLHopDongService.Dto;

namespace Nami.Web.Models
{
    public class DetailHopDongVM
    {
        public QLHopDongDto objInfo { get; set; }
        public List<QLHopDongItemDto> listItem { get; set; }
        public List<HistoryDto> historys { get; set; }
    }
}