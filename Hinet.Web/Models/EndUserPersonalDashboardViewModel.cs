﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.AgreementsInfoService.Dto;

namespace Nami.Web.Models
{
    public class EndUserPersonalDashboardViewModel
    {
        public List<AgreementsInfoDto> ListAgreement { get; set; }
    }
}