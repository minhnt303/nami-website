﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.AgreementsInfoService.Dto;
using Nami.Service.QLHopDongDangKyService.Dto;

namespace Nami.Web.Models
{
    public class EndUserPersonalRoomAndBuildingViewModel
    {
        public List<QLHopDongDangKyDto> listHopDongDangKy { get; set; }
    }
}