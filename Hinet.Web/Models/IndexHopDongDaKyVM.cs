﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.QLHopDongService.Dto;

namespace Nami.Web.Models
{
    public class IndexHopDongDaKyVM
    {
        public List<QLHopDongDto> listHopDongDaKy { get; set; }
    }
}