﻿using Nami.Model.Entities;
using Nami.Service.CommentByAccountService.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nami.Web.Models
{
    public class RoomInfoViewModel
    {
        public List<FileDinhKem> imageList { get; set; }
        public List<CommentByAccountDto> listComment { get; set; }

        public RoomInfo objInfo { get; set; }
    }
}