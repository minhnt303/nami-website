﻿using Nami.Service.Common;
using Nami.Service.NotificationService.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nami.Web.Models
{
    public class EndUserNotificationViewModel
    {
        public PageListResultBO<NotificationDto> NotiList { get; set; }
    }
}