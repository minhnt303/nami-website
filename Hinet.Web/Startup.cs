﻿using Autofac;
using Autofac.Integration.Mvc;
using Hangfire;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using System.Reflection;
using System.Web.Configuration;
using Nami.Web.Common;
using System;
using Nami.Service.CommonConfigurationService;

[assembly: OwinStartupAttribute(typeof(Nami.Web.Startup))]
namespace Nami.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();

            var configDB = WebConfigurationManager.AppSettings["HangFireDB"];
            if (!string.IsNullOrEmpty(configDB))
            {
                GlobalConfiguration.Configuration.UseSqlServerStorage(configDB);
                app.UseHangfireDashboard();
                app.UseHangfireServer();
            }


            //RecurringJob.AddOrUpdate<MakeNewPlanProvider>("MakeNewPlan", x => x.MakePlan(), "0 0 1 * * ?", TimeZoneInfo.Local);
        }
    }
}
