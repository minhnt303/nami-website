﻿function NotiSuccess(title, message) {
    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: title,
        time: 2000,
        sticky: false,
        // (string | mandatory) the text inside the notification
        text: message,
        class_name: 'gritter-success'
    });
}
function NotiError(title, message) {
    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: title,
        time: 2000,
        sticky: false,
        // (string | mandatory) the text inside the notification
        text: message,
        class_name: 'gritter-error'
    });
}

function AfterSussessActionAjaxform() {
    location.reload();
}

function AjaxSearchSuccess(rs) {
    location.reload();
}

function AjaxFormSuccess(rs) {
    if (rs.Status) {

        $("#MasterModal").modal("hide");
        $("#MasterModal").empty();
        NotiSuccess("Thành công", "Cập nhật dữ liệu thành công");
        AfterSussessActionAjaxform();
    } else {
        NotiError("Lỗi xử lý", rs.Message);
    }
}
function AjaxFormError() {
    NotiError("Có lỗi xảy ra", "Vui lòng kiểm tra lại thông tin");
}

function AjaxCall(url, type, data, callback, callbackError) {
    var isfunction = callback && typeof (callback) == "function";
    if (!isfunction) {
        callback = function () {
            console.log("Chưa cài đặt sự kiện thành công");
        }
    }
    var isfunction = callbackError && typeof (callbackError) == "function";
    if (!isfunction) {
        callbackError = function () {
            NotiError("Thao tác không thể thực hiện");
        }
    }
    $.ajax({
        url: url,
        type: type,
        data: data,
        success: callback,
        error: callbackError
    })

}
function EditAction(url) {
    AjaxCall(url, 'get', null, function (rs) {
        $("#MasterModal").html(rs);
        $("#MasterModal").modal("show");

    })
}
function CreateAction(url) {
    AjaxCall(url, 'get', null, function (rs) {
        $("#MasterModal").html(rs);
        $("#MasterModal").modal("show");
        if ($('#MasterModal .modal-dialog.fixed-right').length > 0) {
            $(".modal-backdrop").css('display', 'none');
            $(".modal").css('pointer-events', 'none');
            $("button[data-dismiss=modal]").on('click', function () {
                $(".modal-backdrop").css('display', 'none');
                $(".modal").css('pointer-events', 'auto');
            });
        } else {

        }

    })
}

//<div id="MasterModal" class="modal fade aside aside-right aside-vc no-backdrop aside-hidden" role="dialog" data-placement="right" data-backdrop="false" overflow-y="scroll" style="display: none;">
function closeFRModal() {
    if ($("#MasterModal .modal-dialog").length) {
        //$("#MasterModal").modal("hide");
        //$("#MasterModal .modal-dialog .modal-content .modal-header .close").first().trigger("click");
    }
}
function FixedRightActionModalMode(rs, width) {
    $("#MasterModalFixed").html(rs);
    $("#MasterModalFixed").addClass("aside");
    $("#MasterModalFixed").attr({ "data-placement": "right", "data-backdrop": "false", "tabindex": "-1" });
    $("#MasterModalFixed .modal-dialog").addClass("fixed-modal-right", "scroll-disable");
    $("#MasterModalFixed .modal-dialog").addClass("fmr-width-" + width);
    $('.modal.aside').ace_aside();
    $("#MasterModalFixed").modal("show");
    $(".modal.aside.in").css("z-index", 1045);
    $("#MasterModalFixed").on("shown.bs.modal", function () {
        $("#MasterModalFixed").removeClass("aside-hidden");
        //$("#MasterModalFixed").css({ 'display': 'block', 'position': 'fixed' });
    });

}
function FixedRightAction(url) {
    closeFRModal();
    AjaxCall(url, 'get', null, function (rs) {
        FixedRightActionModalMode(rs, 20);
    })
    affterFRModal();
}
function FixedRightAction_2(url) {
    closeFRModal();
    AjaxCall(url, 'get', null, function (rs) {
        FixedRightActionModalMode(rs, 20);
    })
}
function FixedRightAction_4(url) {
    closeFRModal();
    AjaxCall(url, 'get', null, function (rs) {
        FixedRightActionModalMode(rs, 40);
    })
}
function FixedRightAction_6(url) {
    closeFRModal();
    AjaxCall(url, 'get', null, function (rs) {
        FixedRightActionModalMode(rs, 60);
    })
}
function FixedRightAction_8(url) {
    closeFRModal();
    AjaxCall(url, 'get', null, function (rs) {
        FixedRightActionModalMode(rs, 80);
    })
}


//fixedActionCreate begin
function OpenModalAction(url) {
    AjaxCall(url, 'get', null, function (rs) {
        $("#MasterModal").html(rs);
        $("#MasterModal").modal("show");

    })
}

function DeleteAction(url, mesage) {
    if (mesage == null || mesage == '') {
        mesage = "Bạn xác nhận thực hiện thao tác này ?";
    }
    $.confirm({
        title: 'Xác nhận xóa',
        content: mesage,
        draggable: false,
        theme: 'material',
        buttons: {
            confirm: {
                btnClass: 'btn-primary',
                text: "Xác nhận",
                action: function () {
                    AjaxCall(url, 'post', null, function (rs) {
                        if (rs.Status) {
                            NotiSuccess("Thành công", rs.Message);
                            AfterSussessActionAjaxform();
                        } else {
                            NotiError("Lỗi xử lý", rs.Message);
                        }
                    })
                }
            },
            cancel: {
                text: "Đóng",
                action: function () {

                }
            }
        }
    });
    //bootbox.confirm(mesage, function (result) {
    //    if (result) {
    //        AjaxCall(url, 'post', null, function (rs) {
    //            if (rs.Status) {
    //                NotiSuccess("Thành công", rs.Message);
    //                AfterSussessActionAjaxform();
    //            } else {
    //                NotiError("Lỗi xử lý", rs.Message);
    //            }
    //        })
    //    }
    //});
}

function ConfirmAction(url, type, data, mesage) {
    if (mesage == null || mesage == '') {
        mesage = "Bạn xác nhận thực hiện thao tác này ?";
    }
    bootbox.confirm(mesage, function (result) {
        if (result) {
            AjaxCall(url, type, data, function (rs) {
                if (rs.Status) {
                    NotiSuccess("Thành công", rs.Message);
                    AfterSussessActionAjaxform();
                } else {
                    NotiError("Lỗi xử lý", rs.Message);
                }
            })
        }
    });
}

/**
 * @author:duynn
 * @create_date: 19/04/2019
 * @param {any} url
 * @param {any} mesage
 */
function onDelete(url, mesage) {
    if (mesage == null || mesage == '') {
        mesage = "Bạn xác nhận thực hiện thao tác này ?";
    }
    bootbox.confirm(mesage, function (result) {
        if (result) {
            AjaxCall(url, 'delete', null, function (result) {
                if (result.Status) {
                    NotiSuccess("Thành công", result.Message);
                    AfterSussessActionAjaxform();
                } else {
                    NotiError("Lỗi xử lý", result.Message);
                }
            })
        }
    });
}


function ToDate(obj) {
    if (obj == null) {
        return "";
    } else {

        if (obj.indexOf('Date') >= 0) {
            var dateint = parseInt(obj.match(/\d+/)[0]);

            obj = new Date(dateint);
        } else {
            obj = new Date(obj);
        }
        var mon = '';
        if ((obj.getMonth() + 1) < 10) {
            mon = "0" + (obj.getMonth() + 1);
        } else {
            mon = (obj.getMonth() + 1);
        }
        var day = "";
        if (obj.getDate() < 10) {
            day = '0' + obj.getDate();
        } else {
            day = obj.getDate();
        }
        var date_string = day + "/" + mon + "/" + obj.getFullYear();
        return date_string;

    }
}


function ToDateTime(obj) {
    if (obj == null) {
        return "";
    } else {

        if (obj.indexOf('Date') >= 0) {
            var dateint = parseInt(obj.match(/\d+/)[0]);
            obj = new Date(dateint);
        } else {
            obj = new Date(obj);
        }
        var mon = '';
        if ((obj.getMonth() + 1) < 10) {
            mon = "0" + (obj.getMonth() + 1);
        } else {
            mon = (obj.getMonth() + 1);
        }
        var day = "";
        if (obj.getDate() < 10) {
            day = '0' + obj.getDate();
        } else {
            day = obj.getDate();
        }

        var hour = obj.getHours();
        if (hour < 10) {
            hour = "0" + hour;
        }
        var minute = obj.getMinutes()
        if (minute < 10) {
            minute = "0" + minute;
        }
        var date_string = day + "/" + mon + "/" + obj.getFullYear() + " " + hour + ":" + minute;
        return date_string;

    }
}


function onAddNewRow(element) {
    var parent = $(element).closest('table');
    if (!parent) {
        return;
    }
    var request = $(element).data('request');
    if (!request) {
        return;
    }

    $.get(request, function (result) {
        parent.find('tbody').append(result);
    })
}

function onRemoveRow(element) {
    var parent = $(element).closest('tr');
    if (!parent) {
        return;
    }
    $(parent).remove();
}

function validateRequired(container) {
    var isValid = true;
    $("#" + container + " .required").each(function () {
        var parent = $(this).parents(".form-group").first();
        if (parent.length == 0) {
            parent = $(this).parent();
        }
        var errorText = parent.find(".error");
        if ($(this).val() == null || $(this).val().length == 0 || $(this).val().toString().trim() == "") {
            errorText.addClass('error-required');
            isValid = false;
        } else {
            errorText.removeClass('error-required');
        }
    });
    return isValid;
}

function validateDate(container) {
    var isValid = true;
    var pattern = /^[0-3][0-9]\/[01][0-9]\/[12][0-9][0-9][0-9]$/;
    var inputs = $("#" + container + " .checkDateValid");

    inputs.each(function () {
        var parent = $(this).parents(".form-group").first();
        var errorText = parent.find(".error");
        if ($(this).val() != null &&
            $(this).val().length != 0 &&
            $(this).val().toString().trim() != "") {
            if (!pattern.test($(this).val())) {
                errorText.addClass("error-date-format");
                isValid = false;
            }
            else {
                errorText.removeClass("error-date-format");
            }
        }
    })
    return isValid;
}

function validateSelectOption(container) {
    var isValid = true;
    var inputs = $("#" + container + " select.requiredDropDownList");
    inputs.each(function () {
        var parent = $(this).parents(" .form-group").first();
        var errorText = parent.find(".error");
        if ($(this).val() == null || $(this).val().length == 0) {
            errorText.addClass('error-required-dropdown');
            isValid = false;
        } else {
            errorText.removeClass('error-required-dropdown');
        }
    })
    return isValid;
}

function validateTextArea(container) {
    var isValid = true;
    $("#" + container + " .requiredTextArea").each(function () {
        var parent = $(this).parents(" .form-group").first();
        var errorText = parent.find(".error");
        if ($(this).html() == null || $(this).html().trim() == "") {
            errText.addClass('error-required');
            isValid = false;
        } else {
            errorText.removeClass('error-required');
        }
    });
    return isValid;
}

function validateNumber(container) {
    var isValid = true;
    var inputs = $('#' + container + ' .checkIsNumeric');
    if (inputs.length > 0) {
        inputs.each(function () {
            var parent = $(this).parents('.form-group').first();
            var errorDOM = parent.find('.error');

            if ($(this).val() != null &&
                $(this).val().length != 0 &&
                $(this).val().toString().trim() != "") {

                if (!$.isNumeric($(this).val())) {
                    errorDOM.addClass("error-number-format");
                    isValid = false;
                } else {
                    errorDOM.removeClass("error-number-format");
                }
            }
        })
    }
    return isValid;
}

function validateHTMLInjection(container) {
    var isValid = true;
    var pattern = /<[a-z][\s\S]*>/i;
    var inputs = $('#' + container + ' .checkHTMLInjection');
    if (inputs.length > 0) {
        inputs.each(function () {
            var parent = $(this).parents('.form-group').first();
            var errorDOM = parent.find('.error');

            if ($(this).val() != null &&
                $(this).val().length != 0 &&
                $(this).val().toString().trim() != "") {

                if (pattern.test($(this).val())) {
                    errorDOM.addClass('error-html-format');
                    isValid = false;
                }
                else {
                    errorDOM.removeClass('error-html-format');
                }
            }
        })
    }
    return isValid;
}

function validateSpecialCharacter(container) {
    var isValid = true;
    var pattern = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
    var inputs = $('#' + container + ' .checkSpecialCharacter');
    if (inputs.length > 0) {
        inputs.each(function () {
            var parent = $(this).parents('.form-group').first();
            var errorDOM = parent.find('.error');

            if ($(this).val() != null &&
                $(this).val().length != 0 &&
                $(this).val().toString().trim() != "") {

                if (pattern.test($(this).val())) {
                    errorDOM.addClass('error-special-character');
                    isValid = false;
                }
                else {
                    errorDOM.removeClass('error-special-character');
                }
            }
        })
    }
    return isValid;
}

function validateForm(formId) {
    var error = 0;
    var isValid = true;
    error += this.validateRequired(formId) ? 0 : 1;
    error += this.validateDate(formId) ? 0 : 1;
    error += this.validateSelectOption(formId) ? 0 : 1;
    error += this.validateTextArea(formId) ? 0 : 1;
    error += this.validateNumber(formId) ? 0 : 1;
    error += this.validateHTMLInjection(formId) ? 0 : 1;
    error += this.validateSpecialCharacter(formId) ? 0 : 1;

    $('#' + formId).find('.error').each(function () {
        var message = '';
        if ($(this).hasClass('error-required')) {
            message = 'Vui lòng nhập thông tin';
        } else if ($(this).hasClass('error-date-format')) {
            message = 'Vui lòng nhập theo định dạng "ngày/tháng/năm"';
        } else if ($(this).hasClass('error-number-format')) {
            message = 'Vui lòng đúng định dạng số';
        } else if ($(this).hasClass('error-html-format')) {
            message = 'Vui lòng không nhập ký tự HTML';
        } else if ($(this).hasClass('error-required-dropdown')) {
            message = 'Vui lòng chọn thông tin';
        } else if ($(this).hasClass('error-special-character')) {
            message = 'Vui lòng không nhập ký tự đặc biệt';
        }

        if (message !== '') {
            $(this).html(message);
            $(this).css('display', 'inline');
        } else {
            $(this).css('display', 'none');
        }
    });

    if (error > 0) {
        isValid = false;
    } else {
        isValid = true;
    }
    return isValid;
}

//Tạo action dropdown
// listAction [{
//  icon:""
//  text: "",
//  clickAction: "",
//  linkAction:"",
//  title:""
//}]
function RenderDropdownAction(name, listAction) {
    var result = '<div class="btn-group"><button data-toggle="dropdown" class="btn btn-xs btn-primary btn-white dropdown-toggle" aria-expanded="false">' + name + '<i class="ace-icon fa fa-angle-down icon-on-right"></i></button><ul class="dropdown-menu">';

    for (var ac of listAction) {

        if (ac != null) {
            var itemResult = "<li>";
            if (ac.clickAction != null) {
                itemResult += "<a href='javascript:void(0)' onclick='" + ac.clickAction + "'   title = '" + ac.title + "'><i class='" + ac.icon + "'> </i> " + ac.text + "</a>";
            } else {
                itemResult += "<a href='" + ac.linkAction + "' title = '" + ac.title + "'><i class='" + ac.icon + "'> </i> " + ac.text + "</a>";
            }
            itemResult += "</li>";
            result += itemResult;
        }

    }
    result += "</ul></div>";
    return result;
}

function renderDropdownList(urlRe, elementId, defaultMess) {
    console.log(urlRe);
    $.ajax({
        url: urlRe,
        type: 'Post',
        success: function (rs) {
            var content = "<option value=''>" + defaultMess + "</option>";
            if (rs != null) {

                for (var i = 0; i < rs.length; i++) {
                    content += "<option value='" + rs[i].Value + "'>" + rs[i].Text + "</option>";
                }


            }
            $("#" + elementId).html(content);
        },
        error: function (e) {
            NotiError("Lỗi xử lý");
        }
    });
};
//$('#MasterModal').on('hide.bs.modal', function (e) {

//        $(".modal").css('pointer-events', 'auto');

//})


function PersonalAndCompanyChangeStatus(status, id, area, view, action, currentUserId, stickid) {
    var result = `<div class="btn-group">
                               <button data-toggle="dropdown" class="btn btn-xs btn-primary btn-white dropdown-toggle" aria-expanded="false">Thao tác<i class="ace-icon fa fa-angle-down icon-on-right"></i>
                               </button>
						   <ul class="dropdown-menu dropdown-menu-right">`;
    if ((stickid == null || stickid == 0) || (stickid != currentUserId)) {
        result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
    }
    else {
        switch (status) {
            case 0: result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=2&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'><i class='glyphicon glyphicon-plus-sign'style='color: #4fff4f'> </i> Yêu cầu thay đổi thông tin</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='" + action + "(\"/" + area + "/" + view + "/" + action + "?status=1&id=" + id + "\",1)' title='Duyệt thông tin trực tuyến'><i class='glyphicon glyphicon-ok-sign'style='color: #045dd9'> </i> Duyệt thông tin hồ sơ</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=4&id=" + id + "\")' title='Từ chối hồ sơ'><i class='glyphicon glyphicon-minus-sign'style='color: #ff4242'> </i> Từ chối hồ sơ</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'><i class='glyphicon glyphicon-lock'style='color:#ffd663'> </i> Khóa hồ sơ</a> </li>";
                result += "<li><a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> </li>";
                break;
            case 1: result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=2&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'><i class='glyphicon glyphicon-plus-sign'style='color: #4fff4f'> </i> Yêu cầu thay đổi thông tin</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'><i class='glyphicon glyphicon-lock'style='color:#ffd663'> </i> Khóa hồ sơ</a> </li>";
                result += "<li><a href='/ProductInfoArea/ProductInfo/Index/" + id + "' title='Chỉnh sửa'><i class='glyphicon glyphicon-shopping-cart'style='color:#f70ab0'> </i> Quản lý sản phẩm</a> </li>";
                result += "<li><a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> </li>";
                break;
            case 2: result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'><i class='glyphicon glyphicon-lock'style='color:#ffd663'> </i> Khóa hồ sơ</a> </li>";
                result += "<li><a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> </li>";
                break;
            case 3: result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=2&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'><i class='glyphicon glyphicon-plus-sign'style='color: #4fff4f'> </i> Yêu cầu thay đổi thông tin</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='" + action + "(\"/" + area + "/" + view + "/" + action + "?status=1&id=" + id + "\",1)' title='Duyệt thông tin trực tuyến'><i class='glyphicon glyphicon-ok-sign'style='color: #045dd9'> </i> Duyệt thông tin hồ sơ</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=4&id=" + id + "\")' title='Từ chối hồ sơ'><i class='glyphicon glyphicon-minus-sign'style='color: #ff4242'> </i> Từ chối hồ sơ</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'><i class='glyphicon glyphicon-lock'style='color:#ffd663'> </i> Khóa hồ sơ</a> </li>";
                result += "<li><a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> </li>";
                break;
            case 4: result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=2&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'><i class='glyphicon glyphicon-plus-sign'style='color: #4fff4f'> </i> Yêu cầu thay đổi thông tin</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'><i class='glyphicon glyphicon-lock'style='color:#ffd663'> </i> Khóa hồ sơ</a> </li>";
                result += "<li><a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> </li>";
                break;
            case 5: result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='DeleteAction(\"/" + area + "/" + view + "/Delete/" + id + "\")' title='Xóa'><i class=' glyphicon glyphicon-remove' style='color:red'> </i> Xóa hồ sơ</a></li>";
                break;
        }
    }
    result += "</ul></div > ";
    return result;
}

function PersonalAndCompanyChangeStatusDetail(status, id, area, view, action, currentUserId, stickid) {
    var result = "";
    if ((stickid == null || stickid == 0) || (stickid != currentUserId)) {
    }
    if (stickid == 0 && (currentUserId != stickid)) {
        result += "<a class='btn btn-info btn-xs floatRightStatusButton' title='Nhận xử lý hồ sơ này' href='javascript:void(0)' onclick='GetProcess(\"/" + area + "/" + view + "/GetProcess/" + id + "\")'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-ok-sign'> </i> Nhận xử lý</a>";
    }
    else if (stickid == currentUserId) {
        switch (status) {
            case 0:
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=2&id=" + id + "\")' title='Thay đổi thông tin hồ sơ' class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-plus-sign'> </i> Yêu cầu thay đổi thông tin</a> ";
                result += "<a href='javascript:void(0)' onclick='" + action + "(\"/" + area + "/" + view + "/" + action + "?status=1&id=" + id + "\",1)' title='Duyệt thông tin trực tuyến'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-ok-sign'> </i> Duyệt thông tin hồ sơ</a> ";
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=4&id=" + id + "\")' title='Từ chối hồ sơ'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-minus-sign'> </i> Từ chối hồ sơ</a> ";
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-lock'> </i> Khóa hồ sơ</a> ";
                result += "<a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> ";
                break;
            case 1:
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=2&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-plus-sign'> </i> Yêu cầu thay đổi thông tin</a> ";
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-lock'> </i> Khóa hồ sơ</a> ";
                result += "<a href='/ProductInfoArea/ProductInfo/Index/" + id + "' title='Chỉnh sửa'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-shopping-cart'> </i> Quản lý sản phẩm</a> ";
                result += "<a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> ";
                break;
            case 2:
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-lock'> </i> Khóa hồ sơ</a> ";
                result += "<a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> ";
                break;
            case 3:
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=2&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-plus-sign'> </i> Yêu cầu thay đổi thông tin</a> ";
                result += "<a href='javascript:void(0)' onclick='" + action + "(\"/" + area + "/" + view + "/" + action + "?status=1&id=" + id + "\",1)' title='Duyệt thông tin trực tuyến'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-ok-sign'> </i> Duyệt thông tin hồ sơ</a> ";
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=4&id=" + id + "\")' title='Từ chối hồ sơ'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-minus-sign'> </i> Từ chối hồ sơ</a> ";
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-lock'> </i> Khóa hồ sơ</a> ";
                result += "<a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> ";
                break;
            case 4:
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=2&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-plus-sign'> </i> Yêu cầu thay đổi thông tin</a> ";
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-lock'> </i> Khóa hồ sơ</a> ";
                result += "<a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'class='btn btn-primary btn-sm floatRightStatusButton'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> ";
                break;
            case 5:
                result += "<a href='javascript:void(0)' onclick='DeleteAction(\"/" + area + "/" + view + "/Delete/" + id + "\")' title='Xóa'class='btn btn-primary btn-sm floatRightStatusButton'><i class=' glyphicon glyphicon-remove' > </i> Xóa hồ sơ</a> </li>";
                break;
        }
    }
    return result;
}

function PersonalAndCompanyChangeStatusPopUpDetail(status, id, area, view, action, currentUserId, stickid) {
    var result = "";
    if ((stickid == null || stickid == 0) || (stickid != currentUserId)) {

    }
    if (stickid == 0 && (currentUserId != stickid)) {
        result += "<a class='btn btn-info btn-sm' title='Nhận xử lý hồ sơ này' href='javascript:void(0)' onclick='GetProcess(\"/" + area + "/" + view + "/GetProcess/" + id + "\")'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-ok-sign'> </i> Nhận xử lý</a>";

    }
    else if (stickid == currentUserId) {
        switch (status) {
            case 0:
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=2&id=" + id + "\")' title='Thay đổi thông tin hồ sơ' class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-plus-sign'> </i> Yêu cầu thay đổi thông tin</a> ";
                result += "<a href='javascript:void(0)' onclick='" + action + "(\"/" + area + "/" + view + "/" + action + "?status=1&id=" + id + "\",1)' title='Duyệt thông tin trực tuyến'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-ok-sign'> </i> Duyệt thông tin hồ sơ</a> ";
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=4&id=" + id + "\")' title='Từ chối hồ sơ'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-minus-sign'> </i> Từ chối hồ sơ</a> ";
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-lock'> </i> Khóa hồ sơ</a> ";
                result += "<a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> ";

                break;
            case 1:
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=2&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-plus-sign'> </i> Yêu cầu thay đổi thông tin</a> ";
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-lock'> </i> Khóa hồ sơ</a> ";
                result += "<a href='/ProductInfoArea/ProductInfo/Index/" + id + "' title='Chỉnh sửa'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-shopping-cart'> </i> Quản lý sản phẩm</a> ";
                result += "<a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> ";

                break;
            case 2:
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-lock'> </i> Khóa hồ sơ</a> ";
                result += "<a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> ";

                break;
            case 3:
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=2&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-plus-sign'> </i> Yêu cầu thay đổi thông tin</a> ";
                result += "<a href='javascript:void(0)' onclick='" + action + "(\"/" + area + "/" + view + "/" + action + "?status=1&id=" + id + "\",1)' title='Duyệt thông tin trực tuyến'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-ok-sign'> </i> Duyệt thông tin hồ sơ</a> ";
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=4&id=" + id + "\")' title='Từ chối hồ sơ'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-minus-sign'> </i> Từ chối hồ sơ</a> ";
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-lock'> </i> Khóa hồ sơ</a> ";
                result += "<a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> ";

                break;
            case 4:
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=2&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-plus-sign'> </i> Yêu cầu thay đổi thông tin</a> ";
                result += "<a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=5&id=" + id + "\")'title='Khóa hồ sơ'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-lock'> </i> Khóa hồ sơ</a> ";
                result += "<a href='/" + area + "/" + view + "/Edit/" + id + "' title='Chỉnh sửa'class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-edit'> </i> Sửa thông tin</a> ";

                break;
            case 5:
                result += "<a href='javascript:void(0)' onclick='DeleteAction(\"/" + area + "/" + view + "/Delete/" + id + "\")' title='Xóa'class='btn btn-primary btn-sm'><i class=' glyphicon glyphicon-remove' > </i> Xóa hồ sơ</a> </li>";

                break;
        }
    }
    result += "<a class='btn btn-default btn-sm' data-dismiss='modal'>Đóng</a> ";
    return result;
}

function PersonalAndCompanyStatus(status) {
    switch (status) {
        case 0: return '<b class="label label-primary"style="height:28px;">Tài khoản</br>đăng ký mới</b>';
        case 1: return '<b class="label label-success" style="height:28px;">Tài khoản</br>đã duyệt</b>';
        case 2: return '<b class="label label-info"style="height:42px;">Tài khoản cần</br>bổ sung</br>thông tin</b>';
        case 3: return '<b class="label label-info"style="height:42px;">Tài khoản</br>đề nghị</br>chỉnh sửa</b>';
        case 4: return '<b class="label label-danger"style="height:28px;">Tài khoản</br>bị từ chối</b>';
        case 5: return '<b class="label label-warning"style="height:28px;">Tài khoản</br>bị khóa</b>';
    }
}

function EndUserStatus(status) {
    switch (status) {
        case 0: return '<b class="label label-primary" style="font-size: 11px">Tài khoản đăng ký mới</b>';
        case 1: return '<b class="label label-success" style="font-size: 11px">Tài khoản đã duyệt</b>';
        case 2: return '<b class="label label-info" style="font-size: 11px">Tài khoản cần bổ sung thông tin</b>';
        case 3: return '<b class="label label-info" style="font-size: 11px">Tài khoản đề nghị chỉnh sửa</b>';
        case 4: return '<b class="label label-danger" style="font-size: 11px">Tài khoản bị từ chối</b>';
        case 5: return '<b class="label label-warning" style="font-size: 11px">Tài khoản bị khóa</b>';
    }
}

function DauMoi(stickid, currentUserId, id, CurrentUserName, area, view) {
    if (stickid == currentUserId) {
        return (stickid == null || stickid == 0) ? "<a class='btn btn-primary btn-xs' title='Nhận là đầu mối xử lý hồ sơ này' href='javascript:void(0)' onclick='GetProcess(\"/" + area + "/" + view + "/GetProcess/" + id + "\")'>Nhận xử lý</a>" : CurrentUserName + "<br/><a class='btn btn-info btn-xs' title='Chuyển đầu mối xử lý hồ sơ này' href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/PhanDauMoiIndex?id=" + currentUserId + "&item=" + id + "\")'>Chuyển đầu mỗi xử lý</a>";
    } else {
        return (stickid == null || stickid == 0) ? "<a class='btn btn-primary btn-xs' title='Nhận là đầu mối xử lý hồ sơ này' href='javascript:void(0)' onclick='GetProcess(\"/" + area + "/" + view + "/GetProcess/" + id + "\")'>Nhận xử lý</a>" : CurrentUserName
    }
}



function WebsiteAndAppChangeStatus(status, id, area, view, action, currentUserId, stickid) {
    var result = `<div class="btn-group">
							   <button data-toggle="dropdown" class="btn btn-xs btn-primary btn-white dropdown-toggle" aria-expanded="false">Thao tác<i class="ace-icon fa fa-angle-down icon-on-right"></i>
							   </button>
    						   <ul class="dropdown-menu dropdown-menu-right">`; if ((stickid == null || stickid == 0) || (stickid != currentUserId)) {
        result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
    }
    else {
        switch (status) {
            case 0:
                result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=1&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'><i class='glyphicon glyphicon glyphicon-plus-sign'style='color: #4fff4f'> </i> Yêu cầu bổ sung thông tin</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='" + action + "(\"/" + area + "/" + view + "/" + action + "?status=2&id=" + id + "\",2)' title='Duyệt thông tin điện tử'><i class='glyphicon glyphicon-ok-sign'style='color: #045dd9'> </i> Duyệt đăng ký</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=4&id=" + id + "\")' title='Từ chối website'><i class='glyphicon glyphicon-minus-sign'style='color: #ff4242'> </i> Từ chối</a> </li>";
                result += "<li><a href='/" + area + "/" + view + "/Edit/" + id + "'   title = 'Chỉnh sửa'><i class='glyphicon glyphicon-edit'> </i> Sửa đăng ký</a> </li>";
                break;
            case 1:
                result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=7&id=" + id + "\")' title='Chấm dứt đăng ký website'><i class='glyphicon glyphicon glyphicon-download' style='color: #146dfc'> </i> Chấm dứt đăng ký</a> </li>";
                result += "<li><a href='/" + area + "/" + view + "/Edit/" + id + "'   title = 'Chỉnh sửa'><i class='glyphicon glyphicon-edit'> </i> Sửa đăng ký</a> </li>";
                break;
            case 2:
                result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=1&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'><i class='glyphicon glyphicon glyphicon-plus-sign'style='color: #4fff4f'> </i> Yêu cầu bổ sung thông tin</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='" + action + "(\"/" + area + "/" + view + "/" + action + "?status=3&id=" + id + "\",3)' title='Xác nhận website'><i class='glyphicon glyphicon-ok-circle' style='color: #045dd9'> </i> Xác nhận đăng ký</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=4&id=" + id + "\")' title='Từ chối website'><i class='glyphicon glyphicon-minus-sign'style='color: #ff4242'> </i> Từ chối</a> </li>";
                result += "<li><a href='/" + area + "/" + view + "/Edit/" + id + "'   title = 'Chỉnh sửa'><i class='glyphicon glyphicon-edit'> </i> Sửa đăng ký</a> </li>";
                break;
            case 3:
                result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=1&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'><i class='glyphicon glyphicon glyphicon-plus-sign'style='color: #4fff4f'> </i> Yêu cầu bổ sung thông tin</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=7&id=" + id + "\")' title='Chấm dứt đăng ký website'><i class='glyphicon glyphicon glyphicon-download' style='color: #146dfc'> </i> Chấm dứt đăng ký</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='" + action + "(\"/" + area + "/" + view + "/" + action + "?status=8&id=" + id + "\",8)' title='Hủy đăng ký website'><i class='glyphicon glyphicon glyphicon-minus-sign'style='color: #ff4242'> </i> Hủy đăng ký</a> </li>";
                break;
            case 4:
                result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=7&id=" + id + "\")' title='Chấm dứt đăng ký website'><i class='glyphicon glyphicon glyphicon-download' style='color: #146dfc'> </i> Chấm dứt đăng ký</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='" + action + "(\"/" + area + "/" + view + "/" + action + "?status=8&id=" + id + "\",8)' title='Hủy đăng ký website'><i class='glyphicon glyphicon glyphicon-minus-sign'style='color: #ff4242'> </i> Hủy đăng ký</a> </li>";
                break;
            case 5:
                result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=1&id=" + id + "\")' title='Thay đổi thông tin hồ sơ'><i class='glyphicon glyphicon glyphicon-plus-sign'style='color: #4fff4f'> </i> Yêu cầu bổ sung thông tin</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='" + action + "(\"/" + area + "/" + view + "/" + action + "?status=2&id=" + id + "\",2)' title='Duyệt thông tin điện tử'><i class='glyphicon glyphicon-ok-sign'style='color: #045dd9'> </i> Duyệt đăng ký</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='CreateAction(\"/" + area + "/" + view + "/RequestStatus?status=4&id=" + id + "\")' title='Từ chối website'><i class='glyphicon glyphicon-minus-sign'style='color: #ff4242'> </i> Từ chối</a> </li>";
                result += "<li><a href='/" + area + "/" + view + "/Edit/" + id + "'   title = 'Chỉnh sửa'><i class='glyphicon glyphicon-edit'> </i> Sửa đăng ký</a> </li>";
                break;
            case 6:
                result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='" + action + "(\"/" + area + "/" + view + "/" + action + "?status=8&id=" + id + "\",8)' title='Hủy đăng ký website'><i class='glyphicon glyphicon glyphicon-minus-sign'style='color: #ff4242'> </i> Hủy đăng ký</a> </li>";
                break;
            case 7: result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='javascript:void(0)' onclick='DeleteAction(\"/" + area + "/" + view + "/Delete/" + id + "\")'  title = 'Xóa'><i class=' glyphicon glyphicon-remove' style='color:red'> </i> Xóa</a></li>";
                break;
            case 8: result += "<li><a href='/" + area + "/" + view + "/Detail/" + id + "' title='Thông tin chi tiết'><i class='glyphicon glyphicon-user'> </i> Thông tin chi tiết</a> </li>";
                result += "<li><a href='/" + area + "/" + view + "/Edit/" + id + "'   title = 'Chỉnh sửa'><i class='glyphicon glyphicon-edit'> </i> Sửa đăng ký</a> </li>";
                break;
        }
    }

    result += "</ul></div>";
    return result;
}

function LoaiThuongNhanToChuc(typeOrganization) {
    switch (typeOrganization) {
        case 'Company': return '<span class="label label-info">Tài khoản thương nhân</span>'
        case 'Organization': return '<span class="label label-default">Tài khoản tổ chức</span>'
    }
}

function addBroadcastNoti(mess) {
    $("#boxRunText").append("<span class='textItem'>" + mess + "</span>");
}

function showBroadcastNoti() {
    
    if ($("#boxRunText .textItem").length > 0) {
        var firstItemNoti = $("#boxRunText .textItem").first();
        firstItemNoti.addClass("active");
        
    }
}



function displayWebsiteStatus(status) {
    switch (status) {
        case 0: return '<b class="label label-primary"style="height:28px;">Website chờ</br>duyệt</b>';
        case 1: return '<b class="label label-info"style="height:28px;">Website cần bổ</br>sung thông tin</b>';
        case 2: return '<b class="label label-default"style="height:28px;">Website đã</br>duyệt điện tử</b>';
        case 3: return '<b class="label label-success"style="height:28px;">Website đã</br>xác nhận</b>';
        case 4: return '<b class="label label-info"style="height:28px;">Website bị</br>từ chối</b>';
        case 5: return '<b class="label label-grey"style="height:28px;">Website đề nghị</br>chỉnh sửa</b>';
        case 6: return '<b class="label label-warning"style="height:42px;">Website đề nghị</br>chấm dứt</br>đăng ký</b>';
        case 7: return '<b class="label label-danger"style="height:42px;">Website đã</br>chấm dứt</br>đăng ký</b>';
        case 8: return '<b class="label label-inverse"style="height:28px;">Website đã</br>hủy đăng ký</b>';
        case 9: return '<b class="label label-warning"style="height:28px;">Website đề nghị</br>chấm dứt thông báo</b>';
        case 10: return '<b class="label label-inverse"style="height:28px;">Website đã</br>chấm dứt thông báo</b>';
        case 11: return '<b class="label label-danger"style="height:28px;">Website đã</br>hủy thông báo</b>';
        case 12: return '<b class="label label-warning"style="height:28px;">Website cần</br>gia hạn</b>';
        case 13: return '<b class="label label-warning"style="height:28px;">Website đã yêu cầu gia hạn</br></b>';
    }
}

function displayAppStatus(status) {
    switch (status) {
        case 0: return '<b class="label label-primary"style="height:28px;">Ứng dụng</br>chờ duyệt</b>';
        case 1: return '<b class="label label-info"style="height:42px;">Ứng dụng cần</br>bổ sung</br>thông tin</b>';
        case 2: return '<b class="label label-default"style="height:28px;">Ứng dụng đã</br>duyệt điện tử</b>';
        case 3: return '<b class="label label-success"style="height:28px;">Ứng dụng đã</br>xác nhận</b>';
        case 4: return '<b class="label label-info"style="height:28px;">Ứng dụng bị</br>từ chối</b>';
        case 5: return '<b class="label label-grey"style="height:42px;">Ứng dụng</br>đề nghị</br>chỉnh sửa</b>';
        case 6: return '<b class="label label-warning"style="height:42px;">Ứng dụng đề</br>nghị chấm dứt</br>đăng ký</b>';
        case 7: return '<b class="label label-danger"style="height:42px;">Ứng dụng đã</br>chấm dứt</br>đăng ký</b>';
        case 8: return '<b class="label label-inverse"style="height:28px;">Ứng dụng đã</br>hủy đăng ký</b>';
        case 9: return '<b class="label label-warning"style="height:28px;">Ứng dụng đề nghị</br>chấm dứt thông báo</b>';
        case 10: return '<b class="label label-inverse"style="height:28px;">Ứng dụng đã</br>chấm dứt thông báo</b>';
        case 11: return '<b class="label label-danger"style="height:28px;">Ứng dụng đã</br>hủy thông báo</b>';
        case 12: return '<b class="label label-warning"style="height:28px;">Ứng dụng cần</br>gia hạn</b>';
        case 13: return '<b class="label label-warning"style="height:28px;">Ứng dụng đã yêu cầu gia hạn</br></b>';

    }
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
        if (color == '#ffffff' || color == '#FFFFFF') {
            color = '#7ed6df';
        }
    }
    return color;
}
function formatNumberCurrentcy(num) {
    if (num != null) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    } else {
        return num;
    }
}

function OpenDropdownMenu() {
    if ($(".dropdownItem").is(":visible")) {
        $(".dropdownItem").hide();
        $(".dropdownBar").css("background-color", "rgba(255,255,255,0)");
    } else {
        $(".dropdownItem").show();
        $(".dropdownBar").css("background-color", "rgba(255,255,255,0.5)");
        $(".dropdownItem").css("display", "grid");
        $(".dropdownItem").css("z-index", "10");
        $(".dropdownItem").css("left", "-121px");
        $(".dropdownItem").css("right", "10px");
        $(".dropdownItem").css("top", "41px");
        $(".dropdownItem").css("margin-right", "10px");
        $(".dropdownItem").css("padding", "10px");
        $(".dropdownItem").css("background-color", "#bdc3c7");
        $(".dropdownItem").css("border-radius", "4px");
        $(".dropdownItem").css("position", "absolute");
    }
}
function GetActionOfStatus(listFunc, status) {
    let lstData = listFunc.filter((item) => {
        return item.Status == status
    });
    return lstData[0];
}

function getLink(configBtn, cl, prams) {

    var url = configBtn.LinkPattern;
    if (prams != null) {
        for (var i = 0; i < prams.length; i++) {
            url = url.replace("{" + i + "}", prams[i]);
        }
    }
    var icon = "";
    if (configBtn.Icon != null) {
        icon = "<i class='" + configBtn.Icon + "'> </i> ";
    }
    switch (configBtn.Type) {
        case 1:
            return "<a class='" + cl + "' href='" + url + "'>" + icon + configBtn.Name + "</a>";
        case 2:
            return "<a class='" + cl + "' href='javascripts:void(0)' onclick='EditAction(\"" + url + "\")'>" + icon + configBtn.Name + "</a>";
        case 3:
            return "<a class='" + cl + "' href='javascripts:void(0)' onclick='confirmLink(\"" + url + "\")'>" + icon + configBtn.Name + "</a>";
    }
}

function confirmLink(url, message) {
    var mss = "Xác nhận thao tác ?";
    if (message != null) {
        mss = message;
    }

    $.confirm({
        title: mss,
        content: "Bạn chắc chắn muốn thực hiện thao tác này",
        draggable: false,
        theme: 'material',
        buttons: {
            confirm: {
                btnClass: 'btn-primary',
                text: "Xác nhận",
                action: function () {
                    AjaxCall(url, 'post', null, function (rs) {
                        if (rs.Status) {
                            NotiSuccess("Thành công", rs.Message);
                            AfterSussessActionAjaxform();
                        } else {
                            NotiError("Lỗi xử lý", rs.Message);
                        }
                    })
                }
            },
            cancel: {
                text: "Đóng",
                action: function () {

                }
            }
        }
    });
}
function chuyendoidata(str) {
    return decodeURIComponent(escape(window.atob(str)));
}
//hàm confirm để gọi ajax
function onConfirmCallAjax(url, type, params, callBack, title, content) {
    if (!title) {
        title = 'Xác nhận thao tác';
    }

    if (!content) {
        content = 'Bạn chắc chắn muốn thực hiện thao tác này';
    }

    $.confirm({
        title: title,
        content: content,
        draggable: false,
        theme: 'material',
        buttons: {
            confirm: {
                btnClass: 'btn-primary',
                text: "Xác nhận",
                action: function () {
                    AjaxCall(url, type, params, callBack);
                }
            },
            cancel: {
                text: "Đóng",
                action: function () {

                }
            }
        }
    });
}

//hàm confirm để gọi một hàm khác
function onConfirmCall(callBack, title, content) {
    if (!title) {
        title = 'Xác nhận thao tác';
    }

    if (!content) {
        content = 'Bạn chắc chắn muốn thực hiện thao tác này';
    }

    $.confirm({
        title: title,
        content: content,
        draggable: false,
        theme: 'material',
        buttons: {
            confirm: {
                btnClass: 'btn-primary',
                text: "Xác nhận",
                action: function () {
                    callBack();
                }
            },
            cancel: {
                text: "Đóng",
                action: function () {

                }
            }
        }
    });
}


function formatMoney(num, currency = "", splitter = ",") {
    if (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + " " + currency;
    }
    return num;
}
function BackButtonByParentUrl() {
    var ref = document.referrer;
    location.href = ref;
}