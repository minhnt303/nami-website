﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Nami.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            // BotDetect requests must not be routed 
            routes.IgnoreRoute("{*botdetect}",
            new { botdetect = @"(.*)BotDetectCaptcha\.ashx" });
            routes.MapRoute(
               name: "AdminRoute",
               url: "admin",
               defaults: new { controller = "Dashboard", action = "Index", id = UrlParameter.Optional }
           ).DataTokens = new RouteValueDictionary(new { area = "DashboardArea" }); ;

            routes.MapRoute(
              name: "XemTinTuc",
              url: "Tin-Tuc/{slug}",
              defaults: new { controller = "TinTuc", action = "Index", slug = UrlParameter.Optional }
           );

            routes.MapRoute(
              name: "XemDSTinTuc",
              url: "Tin-Tuc-DS/{page}",
              defaults: new { controller = "TinTuc", action = "PageIndex", page = UrlParameter.Optional }
           );

            routes.MapRoute(
              name: "XemDSTinTucNoiBat",
              url: "Tin-Tuc-Noi-Bat-DS/{page}",
              defaults: new { controller = "TinTuc", action = "PageIndexNoiBat", page = UrlParameter.Optional }
           );

            routes.MapRoute(
              name: "XemTuVan",
              url: "Tu-Van/{slug}",
              defaults: new { controller = "TinTuc", action = "IndexTuVan", slug = UrlParameter.Optional }
           );
            routes.MapRoute(
              name: "XemDSTuVan",
              url: "Tu-Van-DS/{page}",
              defaults: new { controller = "TinTuc", action = "PageIndexTuVan", page = UrlParameter.Optional }
           );

           // routes.MapRoute(
           //   name: "XemDSTinRaoTheoDiaDiem",
           //   url: "Tin-Rao-Theo-Dia-Diem?tinhCode={tinhCode}&page={page}",
           //   defaults: new { controller = "NhaChoThueTheoDiaDiemVaDuAn", action = "PageIndexTinRaoTheoDiaDiem", tinhCode = UrlParameter.Optional, page = UrlParameter.Optional }
           //);

            routes.MapRoute(
               name: "Default",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
           );
        }
    }
}
