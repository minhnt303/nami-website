﻿using CommonHelper.String;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;

namespace Nami.Web.Common
{
    public class EmailConfigProvider
    {
        public static List<EmailAccountEntry> emailAccountEntries;
        static EmailConfigProvider()
        {
            emailAccountEntries = new List<EmailAccountEntry>();
        }
        public static int GetAmountMailSent()
        {
            var result = 0;
            if (emailAccountEntries!=null)
            {
                foreach (var item in emailAccountEntries)
                {
                    if (item.date.GetValueOrDefault().Date<DateTime.Now.Date)
                    {
                        item.date = DateTime.Now;
                        item.Amount = 0;
                    }
                }
                result = emailAccountEntries.Sum(x => x.Amount);
            }
            return result;
        }
        public static EmailAccountEntry GetEmailAccountSend()
        {
            checkDate();
            var maxEmails = WebConfigurationManager.AppSettings["MaxEmailSendEachDay"].ToIntOrZero();
            var email = emailAccountEntries.Where(x => x.Amount < maxEmails).FirstOrDefault();
            return email;
        }

        public static void checkDate()
        {
            if (emailAccountEntries != null)
            {
                foreach (var item in emailAccountEntries)
                {
                    if (item.date.Value.Date < DateTime.Now.Date)
                    {
                        item.date = DateTime.Now;
                        item.Amount = 0;
                    }
                }
            }
        }
        public static void LoadConfig(string path)
        {
            emailAccountEntries = JsonConvert.DeserializeObject<List<EmailAccountEntry>>(File.ReadAllText(path));
        }

        public static void WriteData(string path)
        {
            var jsonData = JsonConvert.SerializeObject(emailAccountEntries);
            File.WriteAllText(path,jsonData);
        }
        public static void UpAmount(string user)
        {
            var obj = emailAccountEntries.Where(x => x.UserName == user).FirstOrDefault();
            if (obj!=null)
            {
                obj.Amount += 1;
            }
            //var pathJson = HostingEnvironment.MapPath("~/App_Data/EmailConfig.json");
            //WriteData(pathJson);
        }
    }
    public class EmailAccountEntry
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Amount { get; set; }
        public DateTime? date { get; set; }
    }
}