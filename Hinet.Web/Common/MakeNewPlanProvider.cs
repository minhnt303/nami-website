﻿using Autofac;
using Nami.Model;
using Nami.Model.Entities;
using Nami.Service.AppUserService;
using Nami.Service.Constant;
using Nami.Service.NotificationService;
using Nami.Web.Core;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Nami.Web.Common
{
    public class MakeNewPlanProvider : IMakeNewPlanProvider
    {
        public static DateTime GetStartOfDay(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 1, 0, 0, 0);
        }
        public static DateTime GetEndOfDay(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59, 0);
        }

        public bool MakePlan()
        {
            return true;
        }
    }
}