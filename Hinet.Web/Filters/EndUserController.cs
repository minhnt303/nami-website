﻿using log4net;
using Microsoft.AspNet.Identity;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nami.Service.AppUserService.Dto;
using Nami.Service.Constant;
using System.Net;
using Nami.Service.OperationService;
using Nami.Service.AppUserService;

namespace Nami.Web.Filters
{
    public class EndUserController : Controller
    {
        ILog _loger;
        protected UserDto CurrentUserInfo;
        private readonly IAppUserService _appUserService;
        private readonly IOperationService _operationService;
        protected long? CurrentUserId = null;
        public EndUserController()
        {

            _loger = LogManager.GetLogger("RollingLogFileAppender");
            _appUserService = DependencyResolver.Current.GetService<IAppUserService>();
            _operationService = DependencyResolver.Current.GetService<IOperationService>();
            CurrentUserInfo = SessionManager.GetUserInfo() as UserDto;
            if (CurrentUserInfo != null)
            {
                CurrentUserId = CurrentUserInfo.Id;
                
            }
        }


        /// <summary>
        /// Kiểm tra xem user hiện tại có quyền không
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public bool HasPermission(string permission)
        {
            if (CurrentUserInfo != null && CurrentUserInfo.ListActions != null)
            {
                foreach (var item in CurrentUserInfo.ListActions)
                {
                    if (item.ListOperation.Any(x => x.Code == permission))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        protected override void OnAuthentication(System.Web.Mvc.Filters.AuthenticationContext filterContext)
        {

            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
                 || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);
            if (!skipAuthorization)
            {
                var userinfo = SessionManager.GetUserInfo() as UserDto;
                if (userinfo == null)
                {
                    filterContext.Result = RedirectToAction("login", "Account", new { Area = "" });
                }


            }

            base.OnAuthentication(filterContext);
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Session != null)
            {
                bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
               || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);
                if (!skipAuthorization)
                {

                    if (filterContext.HttpContext.Session.IsNewSession)
                    {
                        HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                        if (filterContext.HttpContext.Request.IsAjaxRequest())
                        {

                            if (((ReflectedActionDescriptor)filterContext.ActionDescriptor).MethodInfo.ReturnType == typeof(JsonResult))
                            {
                                var rs = new JsonResultBO(false);
                                rs.Message = "Phiên làm việc của bạn đã hết";
                                filterContext.Result = Json(rs);
                            }
                            else if (((ReflectedActionDescriptor)filterContext.ActionDescriptor).MethodInfo.ReturnType == typeof(PartialViewResult))
                            {
                                filterContext.Result =
                                RedirectToAction("TimeOutSession", "Error", new { area = "" });
                            }

                        }
                        else
                        {
                            filterContext.Result =
                           RedirectToAction("login", "account", new { area = "" });
                        }

                        return;

                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            _loger.Error("Lỗi hệ thống", filterContext.Exception);
            TempData["filterContext"] = filterContext;
            //filterContext.ExceptionHandled = true;

            //// Redirect on error:
            //filterContext.Result = RedirectToAction("Index", "Errors", filterContext.Exception);

            // OR set the result without redirection:
            //filterContext.Result = new ViewResult
            //{
            //    ViewName = "~/Views/Errors/Index.cshtml"
            //};
        }
    }
}