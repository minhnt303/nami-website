using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.MapLocationInfoArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		public long? BuildingsId { get; set; }
		public long? RoomsId { get; set; }
		public string ItemTypeCode { get; set; }
		public string Xlocation { get; set; }
		public string Ylocation { get; set; }

        
    }
}