using System.Web.Mvc;

namespace Nami.Web.Areas.MapLocationInfoArea
{
    public class MapLocationInfoAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "MapLocationInfoArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MapLocationInfoArea_default",
                "MapLocationInfoArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}