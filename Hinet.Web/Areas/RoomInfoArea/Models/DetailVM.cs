using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;

namespace Nami.Web.Areas.RoomInfoArea.Models
{
    public class DetailVM
    {
        public List<FileDinhKem> imageList;

        public RoomInfo objInfo { get; set; }
    }
}