﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.RoomInfoArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		public long? BuildingsId { get; set; }
		public string RoomName { get; set; }
		public int? Floor { get; set; }
		public int? NumberOfPersonPerRoom { get; set; }
		public int? Price { get; set; }
		public string Description { get; set; }
		public string TinhCode { get; set; }
		public string XaCode { get; set; }
		public string HuyenCode { get; set; }
		public string QuocGiaCode { get; set; }
		public string Address { get; set; }
		public string ServicesCode { get; set; }
		public int? InputWaterElectricFrom { get; set; }
		public int? InputWaterElectricTo { get; set; }
		public int? BillsFrom { get; set; }
		public int? BillsTo { get; set; }
		public int? CollectionMoneyFrom { get; set; }
		public int? CollectionMoneyTo { get; set; }
		public int? EndOfConstractFrom { get; set; }
		public int? EndOfConstractTo { get; set; }

        [DisplayName("Chiều dài phòng")]
        public int? ChieuDaiRoom { get; set; }

        [DisplayName("Chiều rộng phòng")]
        public int? ChieuRongRoom { get; set; }

        [DisplayName("Số lượng người lớn có thể chứa trong 1 phòng")]
        public int? NumberOfPeoperPerRoomAdult { get; set; }

        [DisplayName("Số lượng trẻ con có thể chứa trong 1 phòng")]
        public int? NumberOfPeoperPerRoomKid { get; set; }

        [DisplayName("Kinh độ")]
        public string Longitude { get; set; }

        [DisplayName("Vĩ độ")]
        public string Latitude { get; set; }
    }
}