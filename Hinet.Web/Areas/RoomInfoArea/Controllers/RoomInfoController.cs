using AutoMapper;
using Nami.Model.Entities;
using Nami.Service.BuildingsInfoService;
using Nami.Service.Common;
using Nami.Service.RoomInfoService;
using Nami.Service.RoomInfoService.Dto;
using Nami.Web.Areas.RoomInfoArea.Models;
using Nami.Web.Filters;
using log4net;
using System;
using System.Web;
using System.Web.Mvc;
using Nami.Service.FileDinhKemService;
using Nami.Service.Constant;


namespace Nami.Web.Areas.RoomInfoArea.Controllers
{
    public class RoomInfoController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "RoomInfo_index";
        public const string permissionCreate = "RoomInfo_create";
        public const string permissionEdit = "RoomInfo_edit";
        public const string permissionDelete = "RoomInfo_delete";
        public const string permissionImport = "RoomInfo_Inport";
        public const string permissionExport = "RoomInfo_export";
        public const string searchKey = "RoomInfoPageSearchModel";
        private readonly IRoomInfoService _RoomInfoService;
        private readonly IBuildingsInfoService _buildingsInfoService;
        private readonly IFileDinhKemService _fileDinhKemService;

        public RoomInfoController(IRoomInfoService RoomInfoService, ILog Ilog,
            IBuildingsInfoService buildingsInfoService,
            IMapper mapper,
            IFileDinhKemService fileDinhKemService
            )
        {
            _RoomInfoService = RoomInfoService;
            _Ilog = Ilog;
            _mapper = mapper;
            _buildingsInfoService = buildingsInfoService;
            _fileDinhKemService = fileDinhKemService;
            var dropdownlistBuilding = _buildingsInfoService.GetDropDownListBulding(CurrentUserInfo.UserName);
            SessionManager.SetValue("dropdownlistBuilding", dropdownlistBuilding);
        }
        // GET: RoomInfoArea/RoomInfo
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var listData = _RoomInfoService.GetDaTaByPageByCreatedBy(CurrentUserInfo.UserName,null);
            ViewBag.dropdownlistBuilding = _buildingsInfoService.GetDropDownListBulding(CurrentUserInfo.UserName);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as RoomInfoSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new RoomInfoSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _RoomInfoService.GetDaTaByPageByCreatedBy(CurrentUserInfo.UserName,searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();
            ViewBag.dropdownlistBuilding = _buildingsInfoService.GetDropDownListBulding(CurrentUserInfo.UserName);

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model, string longtitude, string latitude)
        {
            var result = new JsonResultBO(true, "Tạo Phòng thành công");
            ViewBag.dropdownlistBuilding = _buildingsInfoService.GetDropDownListBulding(CurrentUserInfo.UserName);
            try
            {
                if (ModelState.IsValid)
                {
                    if (string.IsNullOrEmpty(longtitude) && string.IsNullOrEmpty(latitude))
                    {
                        throw new Exception("Bạn chưa chọn địa chỉ trên bản đồ");
                    }
                    var EntityModel = _mapper.Map<RoomInfo>(model);
                    EntityModel.Longitude = longtitude;
                    EntityModel.Latitude = latitude;
                    EntityModel.NumberOfPersonPerRoom = model.NumberOfPeoperPerRoomAdult + model.NumberOfPeoperPerRoomKid;
                    _RoomInfoService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Phòng", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj = _RoomInfoService.GetById(id);
            ViewBag.dropdownlistBuilding = _buildingsInfoService.GetDropDownListBulding(CurrentUserInfo.UserName);
            if (obj == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model, string longtitude, string latitude)
        {
            var result = new JsonResultBO(true);
            ViewBag.dropdownlistBuilding = _buildingsInfoService.GetDropDownListBulding(CurrentUserInfo.UserName);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _RoomInfoService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }
                    var lastlng = obj.Longitude;
                    var lastlat = obj.Latitude;
                    obj = _mapper.Map(model, obj);
                    if (!string.IsNullOrEmpty(longtitude) && !string.IsNullOrEmpty(latitude))
                    {
                        obj.Longitude = longtitude;
                        obj.Latitude = latitude;
                    }
                    else if (!string.IsNullOrEmpty(lastlng) && !string.IsNullOrEmpty(lastlat))
                    {
                        obj.Longitude = lastlng;
                        obj.Latitude = lastlat;
                    }
                    obj.NumberOfPersonPerRoom = obj.NumberOfPeoperPerRoomKid + obj.NumberOfPeoperPerRoomAdult;
                    _RoomInfoService.Update(obj);

                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Phòng", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(RoomInfoSearchDto form)
        { 
            if (form.pageSize == 0)
            {
                form.pageSize = 20;
            }
            SessionManager.SetValue((searchKey), form);

            var data = _RoomInfoService.GetDaTaByPageByCreatedBy(CurrentUserInfo.UserName, form, 1, form.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Phòng thành công");
            try
            {
                var user = _RoomInfoService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _RoomInfoService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }


        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _RoomInfoService.GetById(id);
            model.imageList = _fileDinhKemService.GetListImageFile(id, ItemTypeConstant.Room);
            return View(model);
        }
        public PartialViewResult DetailPopup(long id)
        {
            var model = new DetailVM();
            model.objInfo = _RoomInfoService.GetById(id);
            model.imageList = _fileDinhKemService.GetListImageFile(id, ItemTypeConstant.Room);
            return PartialView("_detailPartial", model);
        }
    }
}