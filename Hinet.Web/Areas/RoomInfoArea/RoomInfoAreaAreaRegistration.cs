using System.Web.Mvc;

namespace Nami.Web.Areas.RoomInfoArea
{
    public class RoomInfoAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "RoomInfoArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "RoomInfoArea_default",
                "RoomInfoArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}