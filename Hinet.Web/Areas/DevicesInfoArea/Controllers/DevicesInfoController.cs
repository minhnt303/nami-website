using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.DevicesInfoArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.DevicesInfoService;
using Nami.Service.DevicesInfoService.Dto;



namespace Nami.Web.Areas.DevicesInfoArea.Controllers
{
    public class DevicesInfoController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "DevicesInfo_index";
        public const string permissionCreate = "DevicesInfo_create";
        public const string permissionEdit = "DevicesInfo_edit";
        public const string permissionDelete = "DevicesInfo_delete";
        public const string permissionImport = "DevicesInfo_Inport";
        public const string permissionExport = "DevicesInfo_export";
        public const string searchKey = "DevicesInfoPageSearchModel";
        private readonly IDevicesInfoService _DevicesInfoService;


        public DevicesInfoController(IDevicesInfoService DevicesInfoService, ILog Ilog,

            IMapper mapper
            )
        {
            _DevicesInfoService = DevicesInfoService;
            _Ilog = Ilog;
            _mapper = mapper;

        }
        // GET: DevicesInfoArea/DevicesInfo
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var listData = _DevicesInfoService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as DevicesInfoSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new DevicesInfoSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _DevicesInfoService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Thiết bị thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<DevicesInfo>(model);
                    _DevicesInfoService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Thiết bị", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _DevicesInfoService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _DevicesInfoService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _DevicesInfoService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Thiết bị", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(DevicesInfoSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as DevicesInfoSearchDto;

            if (searchModel == null)
            {
                searchModel = new DevicesInfoSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.DeviceNameFilter = form.DeviceNameFilter;
			searchModel.PriceFilter = form.PriceFilter;
			searchModel.CompensationPriceFilter = form.CompensationPriceFilter;
			searchModel.StatusFilter = form.StatusFilter;
			searchModel.UnitsCodeFilter = form.UnitsCodeFilter;
			searchModel.QuantityFilter = form.QuantityFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _DevicesInfoService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Thiết bị thành công");
            try
            {
                var user = _DevicesInfoService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _DevicesInfoService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _DevicesInfoService.GetById(id);
            return View(model);
        }

        
    }
}