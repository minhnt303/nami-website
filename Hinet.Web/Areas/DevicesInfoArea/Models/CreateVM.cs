using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.DevicesInfoArea.Models
{
    public class CreateVM
    {
		public long? BuildingsId { get; set; }
		public long? RoomsId { get; set; }
		public string DeviceName { get; set; }
		public int? Price { get; set; }
		public int? CompensationPrice { get; set; }
		public string Status { get; set; }
		public string UnitsCode { get; set; }
		public string Description { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public int Quantity { get; set; }

        
    }
}