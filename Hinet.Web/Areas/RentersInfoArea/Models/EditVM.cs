using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.RentersInfoArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		public long? BuildingsId { get; set; }
		public string RentersName { get; set; }
		public long? Phone { get; set; }
		public string Email { get; set; }
		public DateTime? DateOfBirth { get; set; }
		public string SexCode { get; set; }
		public int? NumberId { get; set; }
		public DateTime? NumberDate { get; set; }
		public string NumberAddress { get; set; }
		public string HoKhau { get; set; }
		public string JobCode { get; set; }
		public string JobAddress { get; set; }
		public bool? DangKyTamTru { get; set; }
		public string Note { get; set; }
		public string ParentName { get; set; }
		public int? ParentPhone { get; set; }

        
    }
}