using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.RentersInfoArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.RentersInfoService;
using Nami.Service.RentersInfoService.Dto;



namespace Nami.Web.Areas.RentersInfoArea.Controllers
{
    public class RentersInfoController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "RentersInfo_index";
        public const string permissionCreate = "RentersInfo_create";
        public const string permissionEdit = "RentersInfo_edit";
        public const string permissionDelete = "RentersInfo_delete";
        public const string permissionImport = "RentersInfo_Inport";
        public const string permissionExport = "RentersInfo_export";
        public const string searchKey = "RentersInfoPageSearchModel";
        private readonly IRentersInfoService _RentersInfoService;


        public RentersInfoController(IRentersInfoService RentersInfoService, ILog Ilog,

            IMapper mapper
            )
        {
            _RentersInfoService = RentersInfoService;
            _Ilog = Ilog;
            _mapper = mapper;

        }
        // GET: RentersInfoArea/RentersInfo
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var listData = _RentersInfoService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as RentersInfoSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new RentersInfoSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _RentersInfoService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Khách thuê thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<RentersInfo>(model);
                    _RentersInfoService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Khách thuê", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _RentersInfoService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _RentersInfoService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _RentersInfoService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Khách thuê", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(RentersInfoSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as RentersInfoSearchDto;

            if (searchModel == null)
            {
                searchModel = new RentersInfoSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.RentersNameFilter = form.RentersNameFilter;
			searchModel.SexCodeFilter = form.SexCodeFilter;
			searchModel.JobCodeFilter = form.JobCodeFilter;
			searchModel.DangKyTamTruFilter = form.DangKyTamTruFilter;
			searchModel.ParentNameFilter = form.ParentNameFilter;
			searchModel.ParentPhoneFilter = form.ParentPhoneFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _RentersInfoService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Khách thuê thành công");
            try
            {
                var user = _RentersInfoService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _RentersInfoService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _RentersInfoService.GetById(id);
            return View(model);
        }

        
    }
}