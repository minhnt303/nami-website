using System.Web.Mvc;

namespace Nami.Web.Areas.RentersInfoArea
{
    public class RentersInfoAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "RentersInfoArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "RentersInfoArea_default",
                "RentersInfoArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}