using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.BDBieuDoArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.BDBieuDoService;
using Nami.Service.BDBieuDoService.Dto;
using CommonHelper.Excel;
using CommonHelper.ObjectExtention;
using Nami.Web.Common;
using System.IO;
using System.Web.Configuration;
using CommonHelper;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.BDDataProvinceService;
using Nami.Service.BDGraphsService;
using Nami.Service.BDSetUpService;
using Nami.Service.BDTitleService;
using Nami.Service.BDValueAxesService;


namespace Nami.Web.Areas.BDBieuDoArea.Controllers
{
    public class BDBieuDoController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "BDBieuDo_index";
        public const string permissionCreate = "BDBieuDo_create";
        public const string permissionEdit = "BDBieuDo_edit";
        public const string permissionDelete = "BDBieuDo_delete";
        public const string permissionImport = "BDBieuDo_Inport";
        public const string permissionExport = "BDBieuDo_export";
        public const string searchKey = "BDBieuDoPageSearchModel";
        private readonly IBDBieuDoService _BDBieuDoService;
	    private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        private readonly IBDDataProvinceService _bDDataProvinceService;
        private readonly IBDGraphsService _bDGraphsService;
        private readonly IBDSetUpService _bDSetUpService;
        private readonly IBDTitleService _bDTitleService;
        private readonly IBDValueAxesService _bDValueAxesService;

        public BDBieuDoController(IBDBieuDoService BDBieuDoService, ILog Ilog,
            IBDDataProvinceService bDDataProvinceService,
            IBDGraphsService bDGraphsService,
            IBDSetUpService bDSetUpService,
            IBDTitleService bDTitleService,
            IBDValueAxesService bDValueAxesService,
        IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IMapper mapper
            )
        {
            _BDBieuDoService = BDBieuDoService;
            _Ilog = Ilog;
            _mapper = mapper;
		_dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _bDDataProvinceService = bDDataProvinceService;
            _bDGraphsService = bDGraphsService;
            _bDSetUpService = bDSetUpService;
            _bDTitleService = bDTitleService;
            _bDValueAxesService = bDValueAxesService;
        }
        // GET: BDBieuDoArea/BDBieuDo
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {

            var listData = _BDBieuDoService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as BDBieuDoSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new BDBieuDoSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _BDBieuDoService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Quản lý biểu đồ thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<BDBieuDo>(model);
                    _BDBieuDoService.Create(EntityModel);
                    var bdSetUpModel = new BDSetUp();
                    bdSetUpModel.IdBieuDo = EntityModel.Id;
                    bdSetUpModel.StartDuration = 1;
                    bdSetUpModel.TextColor = "#000000";
                    bdSetUpModel.Theme = BDThemeConstant.Default;
                    bdSetUpModel.FontSize = 12;
                    bdSetUpModel.FontFamily = "Verdana";
                    _bDSetUpService.Create(bdSetUpModel);
                    var bdTitleModel = new BDTitle();
                    bdTitleModel.IdBieuDo = EntityModel.Id;
                    bdTitleModel.IdTitle = "Title-1";
                    bdTitleModel.Size = 15;
                    bdTitleModel.Text = EntityModel.Name;
                    bdTitleModel.IsBold = false;
                    bdTitleModel.Color = "#000000";
                    _bDTitleService.Create(bdTitleModel);
                    var bdValueAxesModel = new BDValueAxes();
                    bdValueAxesModel.IdBieuDo = EntityModel.Id;
                    bdValueAxesModel.IdValue = "ValueAxis-1";
                    bdValueAxesModel.StackType = StackTypeConstant.Regular;
                    bdValueAxesModel.Title = "Axis title";
                    _bDValueAxesService.Create(bdValueAxesModel);
                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý biểu đồ", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _BDBieuDoService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _BDBieuDoService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _BDBieuDoService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý biểu đồ", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(BDBieuDoSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as BDBieuDoSearchDto;

            if (searchModel == null)
            {
                searchModel = new BDBieuDoSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.NameFilter = form.NameFilter;
			searchModel.TypeFilter = form.TypeFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _BDBieuDoService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý biểu đồ thành công");
            try
            {
                var user = _BDBieuDoService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _BDBieuDoService.SoftDelete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _BDBieuDoService.GetById(id);
            //model.listTitle = _bDTitleService.GetDaTaByPage
            return View(model);
        }


        
    }
}