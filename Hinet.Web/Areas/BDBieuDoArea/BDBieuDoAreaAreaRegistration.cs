using System.Web.Mvc;

namespace Nami.Web.Areas.BDBieuDoArea
{
    public class BDBieuDoAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BDBieuDoArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BDBieuDoArea_default",
                "BDBieuDoArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}