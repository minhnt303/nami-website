using System.Web.Mvc;

namespace Nami.Web.Areas.CauHinhCongThucArea
{
    public class CauHinhCongThucAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "CauHinhCongThucArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "CauHinhCongThucArea_default",
                "CauHinhCongThucArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}