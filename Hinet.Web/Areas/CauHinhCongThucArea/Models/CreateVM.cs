using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.CauHinhCongThucArea.Models
{
    public class CreateVM
    {
		public long? CongThucId { get; set; }
		public string DinhMucName { get; set; }
		public int? SoDau { get; set; }
		public int? SoCuoi { get; set; }
		public int? Price { get; set; }
		public string Description { get; set; }

        
    }
}