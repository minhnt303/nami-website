using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.CauHinhCongThucArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.CauHinhCongThucService;
using Nami.Service.CauHinhCongThucService.Dto;



namespace Nami.Web.Areas.CauHinhCongThucArea.Controllers
{
    public class CauHinhCongThucController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "CauHinhCongThuc_index";
        public const string permissionCreate = "CauHinhCongThuc_create";
        public const string permissionEdit = "CauHinhCongThuc_edit";
        public const string permissionDelete = "CauHinhCongThuc_delete";
        public const string permissionImport = "CauHinhCongThuc_Inport";
        public const string permissionExport = "CauHinhCongThuc_export";
        public const string searchKey = "CauHinhCongThucPageSearchModel";
        private readonly ICauHinhCongThucService _CauHinhCongThucService;


        public CauHinhCongThucController(ICauHinhCongThucService CauHinhCongThucService, ILog Ilog,

            IMapper mapper
            )
        {
            _CauHinhCongThucService = CauHinhCongThucService;
            _Ilog = Ilog;
            _mapper = mapper;

        }
        // GET: CauHinhCongThucArea/CauHinhCongThuc
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var listData = _CauHinhCongThucService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as CauHinhCongThucSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new CauHinhCongThucSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _CauHinhCongThucService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Cấu hình công thức thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<CauHinhCongThuc>(model);
                    _CauHinhCongThucService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Cấu hình công thức", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _CauHinhCongThucService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _CauHinhCongThucService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _CauHinhCongThucService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Cấu hình công thức", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(CauHinhCongThucSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as CauHinhCongThucSearchDto;

            if (searchModel == null)
            {
                searchModel = new CauHinhCongThucSearchDto();
                searchModel.pageSize = 20;
            }

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _CauHinhCongThucService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Cấu hình công thức thành công");
            try
            {
                var user = _CauHinhCongThucService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _CauHinhCongThucService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _CauHinhCongThucService.GetById(id);
            return View(model);
        }

        
    }
}