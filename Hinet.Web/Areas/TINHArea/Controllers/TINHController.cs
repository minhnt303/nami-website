using AutoMapper;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.HUYEN2Service;
using Nami.Service.HUYENService;
using Nami.Service.TINH2Service;
using Nami.Service.TINHService;
using Nami.Service.TINHService.Dto;
using Nami.Service.XA2Service;
using Nami.Service.XAService;
using Nami.Web.Areas.TINHArea.Models;
using Nami.Web.Filters;
using log4net;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Nami.Web.Areas.TINHArea.Controllers
{
    public class TINHController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "TINH_index";
        public const string permissionCreate = "TINH_create";
        public const string permissionEdit = "TINH_edit";
        public const string permissionDelete = "TINH_delete";
        public const string permissionImport = "TINH_Inport";
        public const string permissionExport = "TINH_export";
        public const string searchKey = "TINHPageSearchModel";
        private readonly ITINHService _TINHService;
        private readonly IHUYENService _hUYENService;
        private readonly IXAService _xAService;
        private readonly ITINH2Service _tINH2Service;
        private readonly IHUYEN2Service _hUYEN2Service;
        private readonly IXA2Service _xA2Service;


        public TINHController(ITINHService TINHService, ILog Ilog,
            IHUYENService hUYENService,
            IXAService xAService,
            ITINH2Service tINH2Service,
            IHUYEN2Service hUYEN2Service,
            IXA2Service xA2Service,
            IMapper mapper
            )
        {
            _TINHService = TINHService;
            _Ilog = Ilog;
            _mapper = mapper;
            _hUYENService = hUYENService;
            _xAService = xAService;
            _hUYEN2Service = hUYEN2Service;
            _tINH2Service = tINH2Service;
            _xA2Service = xA2Service;
        }
        // GET: TINHArea/TINH
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var listData = _TINHService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as TINHSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new TINHSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _TINHService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Tỉnh/Thành phố thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<TINH>(model);
                    _TINHService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Tỉnh/Thành phố", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(int id)
        {
            var myModel = new EditVM();

            var obj = _TINHService.GetById(id);
            if (obj == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _TINHService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj = _mapper.Map(model, obj);
                    _TINHService.Update(obj);

                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Tỉnh/Thành phố", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(TINHSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as TINHSearchDto;

            if (searchModel == null)
            {
                searchModel = new TINHSearchDto();
                searchModel.pageSize = 20;
            }
            searchModel.MaTinhFilter = form.MaTinhFilter;
            searchModel.TenTinhFilter = form.TenTinhFilter;

            SessionManager.SetValue((searchKey), searchModel);

            var data = _TINHService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            var result = new JsonResultBO(true, "Xóa Tỉnh/Thành phố thành công");
            try
            {
                var user = _TINHService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _TINHService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }


        public ActionResult Detail(int id)
        {
            var model = new DetailVM();
            model.objInfo = _TINHService.GetById(id);
            return View(model);
        }


        [HttpPost]
        public JsonResult UpdateDateTinh2()
        {
            var result = new JsonResultBO(true, "Cập nhật dữ liệu tỉnh 2 thành công");
            try
            {
                var listTinh = _TINHService.GetAll().ToList();
                var listTinh2 = _tINH2Service.GetAll().ToList();

                foreach (var item in listTinh2)
                {
                    var tinh2Info = _tINH2Service.GetById(item.Id);
                    if (tinh2Info != null && !string.IsNullOrEmpty(tinh2Info.TenTinh))
                    {
                        var tinhInfo = listTinh.Where(x => x.TenTinh.ToLower().Contains(tinh2Info.TenTinh.ToLower())).FirstOrDefault();
                        if (tinhInfo != null)
                        {
                            tinh2Info.Loai = tinhInfo.Loai;
                            tinh2Info.Longitude = tinhInfo.Longitude;
                            tinh2Info.Latitude = tinhInfo.Latitude;
                            tinh2Info.LastMaTinh = tinhInfo.MaTinh;
                            _tINH2Service.Update(tinh2Info);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi Cập nhật dữ liệu tỉnh 2", ex);
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult UpdateHuyen2()
        {
            var result = new JsonResultBO(true, "Cập nhật dữ liệu huyện 2 thành công");
            try
            {
                //var listHuyen = _hUYENService.GetAll().ToList();
                //var listHuyen2 = _hUYEN2Service.GetAll().ToList();
                var listTinh2 = _tINH2Service.GetAll().ToList();

                foreach (var item in listTinh2)
                {
                    var listHuyen2 = _hUYEN2Service.GetAll().Where(x=>x.TinhId == item.MaTinh).ToList();
                    var listHuyen = _hUYENService.GetAll().Where(x=>x.TinhId == item.LastMaTinh).ToList();
                    foreach (var item2 in listHuyen2)
                    {
                        var huyenInfo = listHuyen.Where(x => x.TenHuyen.ToLower().Contains(item2.TenHuyen.ToLower())).FirstOrDefault();
                        if (huyenInfo != null && item2 != null) {
                            item2.Longitude = huyenInfo.Longitude;
                            item2.Latitude = huyenInfo.Latitude;
                            item2.Location = huyenInfo.Location;
                            item2.LastMaHuyen = huyenInfo.MaHuyen;
                            _hUYEN2Service.Update(item2);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi Cập nhật dữ liệu huyện 2", ex);
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult UpdateXa2()
        {
            var result = new JsonResultBO(true, "Cập nhật dữ liệu xã 2 thành công");
            try
            {
                //var listHuyen = _hUYENService.GetAll().ToList();
                //var listHuyen2 = _hUYEN2Service.GetAll().ToList();
                var listHuyen2 = _hUYEN2Service.GetAll().ToList();

                foreach (var item in listHuyen2)
                {
                    var listXa2 = _xA2Service.GetAll().Where(x => x.HuyenId == item.MaHuyen).ToList();
                    var listXa = _xAService.GetAll().Where(x => x.HuyenId == item.LastMaHuyen).ToList();
                    foreach (var item2 in listXa2)
                    {
                        var xaInfo = listXa.Where(x => x.TenXa.Trim().ToLower().Contains(item2.TenXa.Trim().ToLower())).FirstOrDefault();
                        if (xaInfo != null && item2 != null)
                        {
                            item2.Longitude = xaInfo.Longitude;
                            item2.Latitude = xaInfo.Latitude;
                            item2.Location = xaInfo.Location;
                            item2.LastMaXa = xaInfo.MaXa;
                            _xA2Service.Update(item2);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi Cập nhật dữ liệu xã 2", ex);
            }
            return Json(result);
        }
    }
}