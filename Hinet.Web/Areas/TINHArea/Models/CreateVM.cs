using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.TINHArea.Models
{
    public class CreateVM
    {
		public string MaTinh { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public string TenTinh { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public string Loai { get; set; }

        
    }
}