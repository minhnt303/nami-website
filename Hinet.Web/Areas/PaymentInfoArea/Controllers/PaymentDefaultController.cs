﻿using AutoMapper;
using CommonHelper.Upload;
using Nami.Model.Entities;
using Nami.Service.AgreementsInfoService;
using Nami.Service.AppUserService;
using Nami.Service.BuildingsInfoService;
using Nami.Service.BuildingsInfoService.Dto;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.DM_NhomDanhmucService;
using Nami.Service.FileDinhKemService;
using Nami.Service.HistoryService;
using Nami.Service.HUYENService;
using Nami.Service.QuanLyDuAnService;
using Nami.Service.QuanLyDuongPhoService;
using Nami.Service.RentersInfoService;
using Nami.Service.RentersInfoService.Dto;
using Nami.Service.RoomInfoService;
using Nami.Service.TaiLieuDinhKemService;
using Nami.Service.TINHService;
using Nami.Service.XAService;
using Nami.Web.Areas.BuildingsInfoArea.Models;
using Nami.Web.Filters;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Nami.Web.Areas.PaymentInfoArea.Controllers
{
    public class PaymentDefaultController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "BuildingsInfo_index";
        public const string permissionCreate = "BuildingsInfo_create";
        public const string permissionEdit = "BuildingsInfo_edit";
        public const string permissionDelete = "BuildingsInfo_delete";
        public const string permissionImport = "BuildingsInfo_Inport";
        public const string permissionExport = "BuildingsInfo_export";
        public const string searchKey = "BuildingsInfoPageSearchModel";
        private readonly IBuildingsInfoService _BuildingsInfoService;
        private readonly IRoomInfoService _roomInfoService;
        private readonly IFileDinhKemService _fileDinhKemService;
        private readonly IRentersInfoService _rentersInfoService;
        private readonly IAgreementsInfoService _agreementsInfoService;
        private readonly IAppUserService _appUserService;
        private readonly ITINHService _tINHService;
        private readonly IHUYENService _hUYENService;
        private readonly IXAService _xAService;
        private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        private readonly IDM_NhomDanhmucService _dM_NhomDanhmucService;
        private readonly IQuanLyDuAnService _QuanLyDuAnService;
        private readonly IQuanLyDuongPhoService _QuanLyDuongPhoService;
        private readonly IHistoryService _historyService;
        private readonly ITaiLieuDinhKemService _taiLieuDinhKemService;


        public PaymentDefaultController(IBuildingsInfoService BuildingsInfoService, ILog Ilog,
            IRoomInfoService roomInfoService,
            IMapper mapper,
            IFileDinhKemService fileDinhKemService,
            IRentersInfoService rentersInfoService,
            IAppUserService appUserService,
            IAgreementsInfoService agreementsInfoService,
            ITINHService tINHService,
            IHUYENService hUYENService,
            IXAService xAService,
            IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IDM_NhomDanhmucService dM_NhomDanhmucService,
            IQuanLyDuAnService QuanLyDuAnService,
            IHistoryService historyService,
            IQuanLyDuongPhoService QuanLyDuongPhoService,
            ITaiLieuDinhKemService taiLieuDinhKemService
            )
        {
            _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _dM_NhomDanhmucService = dM_NhomDanhmucService;
            _tINHService = tINHService;
            _hUYENService = hUYENService;
            _xAService = xAService;
            _QuanLyDuAnService = QuanLyDuAnService;
            _QuanLyDuongPhoService = QuanLyDuongPhoService;
            _BuildingsInfoService = BuildingsInfoService;
            _Ilog = Ilog;
            _mapper = mapper;
            _roomInfoService = roomInfoService;
            _fileDinhKemService = fileDinhKemService;
            _rentersInfoService = rentersInfoService;
            _agreementsInfoService = agreementsInfoService;
            _appUserService = appUserService;
            _historyService = historyService;
            _taiLieuDinhKemService = taiLieuDinhKemService;
        }

        // GET: PaymentInfoArea/PaymentDefault
        public ActionResult Index()
        {
            return View();
        }
    }
}