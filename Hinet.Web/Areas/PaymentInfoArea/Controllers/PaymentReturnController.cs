﻿using AutoMapper;
using Nami.Model.Entities;
using Nami.Service.AgreementsInfoService;
using Nami.Service.AppUserService;
using Nami.Service.BuildingsInfoService;
using Nami.Service.Constant;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.DM_NhomDanhmucService;
using Nami.Service.FileDinhKemService;
using Nami.Service.HistoryService;
using Nami.Service.HUYENService;
using Nami.Service.OrderInfoService;
using Nami.Service.OrderInfoService.Dto;
using Nami.Service.QuanLyDuAnService;
using Nami.Service.QuanLyDuongPhoService;
using Nami.Service.RentersInfoService;
using Nami.Service.RoomInfoService;
using Nami.Service.TaiLieuDinhKemService;
using Nami.Service.TINHService;
using Nami.Service.XAService;
using Nami.Web.Areas.PaymentInfoArea.Models;
using Nami.Web.Filters;
using log4net;
using System;
using System.Configuration;
using System.Web.Mvc;

namespace Nami.Web.Areas.PaymentInfoArea.Controllers
{
    public class PaymentReturnController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "BuildingsInfo_index";
        public const string permissionCreate = "BuildingsInfo_create";
        public const string permissionEdit = "BuildingsInfo_edit";
        public const string permissionDelete = "BuildingsInfo_delete";
        public const string permissionImport = "BuildingsInfo_Inport";
        public const string permissionExport = "BuildingsInfo_export";
        public const string searchKey = "BuildingsInfoPageSearchModel";
        private readonly IBuildingsInfoService _BuildingsInfoService;
        private readonly IRoomInfoService _roomInfoService;
        private readonly IFileDinhKemService _fileDinhKemService;
        private readonly IRentersInfoService _rentersInfoService;
        private readonly IAgreementsInfoService _agreementsInfoService;
        private readonly IAppUserService _appUserService;
        private readonly ITINHService _tINHService;
        private readonly IHUYENService _hUYENService;
        private readonly IXAService _xAService;
        private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        private readonly IDM_NhomDanhmucService _dM_NhomDanhmucService;
        private readonly IQuanLyDuAnService _QuanLyDuAnService;
        private readonly IQuanLyDuongPhoService _QuanLyDuongPhoService;
        private readonly IHistoryService _historyService;
        private readonly ITaiLieuDinhKemService _taiLieuDinhKemService;
        private readonly IOrderInfoService _orderInfoService;

        public PaymentReturnController(IBuildingsInfoService BuildingsInfoService, ILog Ilog,
            IRoomInfoService roomInfoService,
            IMapper mapper,
            IFileDinhKemService fileDinhKemService,
            IRentersInfoService rentersInfoService,
            IAppUserService appUserService,
            IAgreementsInfoService agreementsInfoService,
            ITINHService tINHService,
            IHUYENService hUYENService,
            IXAService xAService,
            IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IDM_NhomDanhmucService dM_NhomDanhmucService,
            IQuanLyDuAnService QuanLyDuAnService,
            IHistoryService historyService,
            IQuanLyDuongPhoService QuanLyDuongPhoService,
            ITaiLieuDinhKemService taiLieuDinhKemService,
            IOrderInfoService orderInfoService
            )
        {
            _orderInfoService = orderInfoService;
            _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _dM_NhomDanhmucService = dM_NhomDanhmucService;
            _tINHService = tINHService;
            _hUYENService = hUYENService;
            _xAService = xAService;
            _QuanLyDuAnService = QuanLyDuAnService;
            _QuanLyDuongPhoService = QuanLyDuongPhoService;
            _BuildingsInfoService = BuildingsInfoService;
            _Ilog = Ilog;
            _mapper = mapper;
            _roomInfoService = roomInfoService;
            _fileDinhKemService = fileDinhKemService;
            _rentersInfoService = rentersInfoService;
            _agreementsInfoService = agreementsInfoService;
            _appUserService = appUserService;
            _historyService = historyService;
            _taiLieuDinhKemService = taiLieuDinhKemService;
        }
        // GET: PaymentInfoArea/PaymentReturn
        public ActionResult Index(decimal vnp_Amount, string vnp_BankCode, string vnp_BankTranNo, string vnp_CardType, string vnp_OrderInfo, string vnp_PayDate, string vnp_ResponseCode, string vnp_TmnCode, string vnp_TransactionNo, string vnp_TxnRef, string vnp_SecureHashType, string vnp_SecureHash)
        {
            var model = new ReturnVM();
            //log.InfoFormat("Begin VNPAY Return, URL={0}", Request.RawUrl);
            if (Request.QueryString.Count > 0)
            {
                string vnp_HashSecret = ConfigurationManager.AppSettings["vnp_HashSecret"]; //Chuoi bi mat
                var vnpayData = Request.QueryString;
                VnPayLibrary vnpay = new VnPayLibrary();
                //if (vnpayData.Count > 0)
                //{
                foreach (string s in vnpayData)
                {
                    //get all querystring data
                    if (!string.IsNullOrEmpty(s) && s.StartsWith("vnp_"))
                    {
                        vnpay.AddResponseData(s, vnpayData[s]);
                    }
                }
                // }
                var OrderInfoId = long.Parse(vnp_TxnRef);
                var OrderInfoModel = new OrderInfo();
                var OrderInfoDtoModel = new OrderInfoDto();
                if (OrderInfoId != 0)
                {
                    OrderInfoModel = _orderInfoService.GetById(OrderInfoId);
                    OrderInfoDtoModel = _orderInfoService.GetDtoById(OrderInfoId);
                    if (OrderInfoDtoModel != null)
                    {
                        model.OrderInfoDtoModel = OrderInfoDtoModel;
                    }
                }

                //vnp_TxnRef: Ma don hang merchant gui VNPAY tai command=pay    
                long orderId = Convert.ToInt64(vnpay.GetResponseData("vnp_TxnRef"));
                //vnp_TransactionNo: Ma GD tai he thong VNPAY
                long vnpayTranId = Convert.ToInt64(vnpay.GetResponseData("vnp_TransactionNo"));
                ////vnp_ResponseCode:Response code from VNPAY: 00: Thanh cong, Khac 00: Xem tai lieu
                //string vnp_ResponseCode = vnpay.GetResponseData("vnp_ResponseCode");
                ////vnp_SecureHash: MD5 cua du lieu tra ve
                //String vnp_SecureHash = Request.QueryString["vnp_SecureHash"];
                bool checkSignature = vnpay.ValidateSignature(vnp_SecureHash, vnp_HashSecret);
                model.checkSignature = checkSignature;
                model.vnp_ResponseCode = vnp_ResponseCode;
                if (checkSignature)
                {
                    if (vnp_ResponseCode == "00")
                    {
                        //Thanh toan thanh cong
                        model.DisplayMsg = "Thanh toán thành công";
                        //displayMsg.InnerText = "Thanh toán thành công";
                        //log.InfoFormat("Thanh toan thanh cong, OrderId={0}, VNPAY TranId={1}", orderId, vnpayTranId);
                        if (OrderInfoModel != null)
                        {
                            OrderInfoModel.TrangThaiThanhToan = true;
                            OrderInfoModel.BankCode = vnp_BankCode;
                            _orderInfoService.Update(OrderInfoModel);
                            if (OrderInfoModel.ItemType == ItemTypeConstant.Building)
                            {
                                var BuildingInfo = _BuildingsInfoService.GetById(OrderInfoModel.IdItem);
                                BuildingInfo.StatusTin = true;
                                _BuildingsInfoService.Update(BuildingInfo);
                                var listRoom = _roomInfoService.GetListRoomByCurrentCreateAndBuildingId(CurrentUserInfo.FullName, BuildingInfo.Id);
                                foreach (var item in listRoom)
                                {
                                    var roomInfo = _roomInfoService.GetById(item.Id);
                                    roomInfo.StartDate = BuildingInfo.StartDate;
                                    roomInfo.EndDate = BuildingInfo.EndDate;
                                    roomInfo.StatusTin = true;
                                    _roomInfoService.Update(roomInfo);
                                }
                            }
                            else if (OrderInfoModel.ItemType == ItemTypeConstant.Room)
                            {
                                var RoomInfo = _roomInfoService.GetById(OrderInfoModel.IdItem);
                                RoomInfo.StatusTin = true;
                                _roomInfoService.Update(RoomInfo);
                            }
                        }
                    }
                    else
                    {
                        //Thanh toan khong thanh cong. Ma loi: vnp_ResponseCode
                        model.DisplayMsg = "Có lỗi xảy ra trong quá trình xử lý.Mã lỗi: " + vnp_ResponseCode;
                        //displayMsg.InnerText = "Có lỗi xảy ra trong quá trình xử lý.Mã lỗi: " + vnp_ResponseCode;
                        //log.InfoFormat("Thanh toan loi, OrderId={0}, VNPAY TranId={1},ResponseCode={2}", orderId, vnpayTranId, vnp_ResponseCode);
                    }
                }
                else
                {
                    //log.InfoFormat("Invalid signature, InputData={0}", Request.RawUrl);
                    //displayMsg.InnerText = "Có lỗi xảy ra trong quá trình xử lý";
                    model.DisplayMsg = "Có lỗi xảy ra trong quá trình xử lý";
                }
            }
            return View(model);
        }
    }
}