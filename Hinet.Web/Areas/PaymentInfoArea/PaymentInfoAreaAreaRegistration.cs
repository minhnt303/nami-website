﻿using System.Web.Mvc;

namespace Nami.Web.Areas.PaymentInfoArea
{
    public class PaymentInfoAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PaymentInfoArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PaymentInfoArea_default",
                "PaymentInfoArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}