﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.OrderInfoService.Dto;

namespace Nami.Web.Areas.PaymentInfoArea.Models
{
    public class ReturnVM
    {
        public string vnp_ResponseCode { get; set; }

        public bool checkSignature { get; set; }

        public string DisplayMsg { get; set; }
        public OrderInfoDto OrderInfoDtoModel { get; set; }
    }
}