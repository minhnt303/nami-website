using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.QuanLyDuAnArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.QuanLyDuAnService;
using Nami.Service.QuanLyDuAnService.Dto;
using CommonHelper.Excel;
using CommonHelper.ObjectExtention;
using Nami.Web.Common;
using System.IO;
using System.Web.Configuration;
using CommonHelper;
using Nami.Service.DM_DulieuDanhmucService;



namespace Nami.Web.Areas.QuanLyDuAnArea.Controllers
{
    public class QuanLyDuAnController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "QuanLyDuAn_index";
        public const string permissionCreate = "QuanLyDuAn_create";
        public const string permissionEdit = "QuanLyDuAn_edit";
        public const string permissionDelete = "QuanLyDuAn_delete";
        public const string permissionImport = "QuanLyDuAn_Inport";
        public const string permissionExport = "QuanLyDuAn_export";
        public const string searchKey = "QuanLyDuAnPageSearchModel";
        private readonly IQuanLyDuAnService _QuanLyDuAnService;
	private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;


        public QuanLyDuAnController(IQuanLyDuAnService QuanLyDuAnService, ILog Ilog,

		IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IMapper mapper
            )
        {
            _QuanLyDuAnService = QuanLyDuAnService;
            _Ilog = Ilog;
            _mapper = mapper;
		_dM_DulieuDanhmucService = dM_DulieuDanhmucService;

        }
        // GET: QuanLyDuAnArea/QuanLyDuAn
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {

            var listData = _QuanLyDuAnService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QuanLyDuAnSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new QuanLyDuAnSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _QuanLyDuAnService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Quản lý dự án thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<QuanLyDuAn>(model);
                    _QuanLyDuAnService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý dự án", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _QuanLyDuAnService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _QuanLyDuAnService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _QuanLyDuAnService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý dự án", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(QuanLyDuAnSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QuanLyDuAnSearchDto;

            if (searchModel == null)
            {
                searchModel = new QuanLyDuAnSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.TenDuAnFilter = form.TenDuAnFilter;
			searchModel.DuAnIdFilter = form.DuAnIdFilter;
			searchModel.TinhIdFilter = form.TinhIdFilter;
			searchModel.HuyenIdFilter = form.HuyenIdFilter;
			searchModel.LocationFilter = form.LocationFilter;
			searchModel.LongitudeFilter = form.LongitudeFilter;
			searchModel.LatitudeFilter = form.LatitudeFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _QuanLyDuAnService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý dự án thành công");
            try
            {
                var user = _QuanLyDuAnService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _QuanLyDuAnService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _QuanLyDuAnService.GetById(id);
            return View(model);
        }


        
    }
}