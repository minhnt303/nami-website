using System.Web.Mvc;

namespace Nami.Web.Areas.QuanLyDuAnArea
{
    public class QuanLyDuAnAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "QuanLyDuAnArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "QuanLyDuAnArea_default",
                "QuanLyDuAnArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}