using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.QLHopDongItemArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.QLHopDongItemService;
using Nami.Service.QLHopDongItemService.Dto;
using CommonHelper.Excel;
using CommonHelper.ObjectExtention;
using Nami.Web.Common;
using System.IO;
using System.Web.Configuration;
using CommonHelper;
using Nami.Service.DM_DulieuDanhmucService;



namespace Nami.Web.Areas.QLHopDongItemArea.Controllers
{
    public class QLHopDongItemController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "QLHopDongItem_index";
        public const string permissionCreate = "QLHopDongItem_create";
        public const string permissionEdit = "QLHopDongItem_edit";
        public const string permissionDelete = "QLHopDongItem_delete";
        public const string permissionImport = "QLHopDongItem_Inport";
        public const string permissionExport = "QLHopDongItem_export";
        public const string searchKey = "QLHopDongItemPageSearchModel";
        private readonly IQLHopDongItemService _QLHopDongItemService;
	private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;


        public QLHopDongItemController(IQLHopDongItemService QLHopDongItemService, ILog Ilog,

		IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IMapper mapper
            )
        {
            _QLHopDongItemService = QLHopDongItemService;
            _Ilog = Ilog;
            _mapper = mapper;
		_dM_DulieuDanhmucService = dM_DulieuDanhmucService;

        }
        // GET: QLHopDongItemArea/QLHopDongItem
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {

            var listData = _QLHopDongItemService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLHopDongItemSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new QLHopDongItemSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _QLHopDongItemService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Quản lý chi tiết hợp đồng thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<QLHopDongItem>(model);
                    _QLHopDongItemService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý chi tiết hợp đồng", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _QLHopDongItemService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _QLHopDongItemService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _QLHopDongItemService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý chi tiết hợp đồng", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(QLHopDongItemSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLHopDongItemSearchDto;

            if (searchModel == null)
            {
                searchModel = new QLHopDongItemSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.IdQLHopDongFilter = form.IdQLHopDongFilter;
			searchModel.ThangFilter = form.ThangFilter;
			searchModel.StartDateFilter = form.StartDateFilter;
			searchModel.EndDateFilter = form.EndDateFilter;
			searchModel.DepositsMoneyFilter = form.DepositsMoneyFilter;
			searchModel.TienPhaiTraHangThangFilter = form.TienPhaiTraHangThangFilter;
			searchModel.TienThangNayFilter = form.TienThangNayFilter;
			searchModel.PayDateFilter = form.PayDateFilter;
			searchModel.NoteFilter = form.NoteFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _QLHopDongItemService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý chi tiết hợp đồng thành công");
            try
            {
                var user = _QLHopDongItemService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _QLHopDongItemService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _QLHopDongItemService.GetById(id);
            return View(model);
        }


        
    }
}