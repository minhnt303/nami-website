using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.QLHopDongItemArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		public long? IdQLHopDong { get; set; }
		public int? Thang { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public int? DepositsMoney { get; set; }
		public int? TienPhaiTraHangThang { get; set; }
		public int? TienThangNay { get; set; }
		public DateTime? PayDate { get; set; }
		public string Note { get; set; }

        
    }
}