using System.Web.Mvc;

namespace Nami.Web.Areas.QLHopDongItemArea
{
    public class QLHopDongItemAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "QLHopDongItemArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "QLHopDongItemArea_default",
                "QLHopDongItemArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}