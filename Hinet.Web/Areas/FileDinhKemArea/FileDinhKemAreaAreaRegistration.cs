using System.Web.Mvc;

namespace Nami.Web.Areas.FileDinhKemArea
{
    public class FileDinhKemAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "FileDinhKemArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "FileDinhKemArea_default",
                "FileDinhKemArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}