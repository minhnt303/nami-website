using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.FileDinhKemArea.Models
{
    public class CreateVM
    {
		public long? IdItem { get; set; }
		public string TypeItem { get; set; }
		public string ImageUrl { get; set; }

        
    }
}