using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.FileDinhKemArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.FileDinhKemService;
using Nami.Service.FileDinhKemService.Dto;



namespace Nami.Web.Areas.FileDinhKemArea.Controllers
{
    public class FileDinhKemController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "FileDinhKem_index";
        public const string permissionCreate = "FileDinhKem_create";
        public const string permissionEdit = "FileDinhKem_edit";
        public const string permissionDelete = "FileDinhKem_delete";
        public const string permissionImport = "FileDinhKem_Inport";
        public const string permissionExport = "FileDinhKem_export";
        public const string searchKey = "FileDinhKemPageSearchModel";
        private readonly IFileDinhKemService _FileDinhKemService;


        public FileDinhKemController(IFileDinhKemService FileDinhKemService, ILog Ilog,

            IMapper mapper
            )
        {
            _FileDinhKemService = FileDinhKemService;
            _Ilog = Ilog;
            _mapper = mapper;

        }
        // GET: FileDinhKemArea/FileDinhKem
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var listData = _FileDinhKemService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as FileDinhKemSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new FileDinhKemSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _FileDinhKemService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo File đính kèm thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<FileDinhKem>(model);
                    _FileDinhKemService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới File đính kèm", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _FileDinhKemService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _FileDinhKemService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _FileDinhKemService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin File đính kèm", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(FileDinhKemSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as FileDinhKemSearchDto;

            if (searchModel == null)
            {
                searchModel = new FileDinhKemSearchDto();
                searchModel.pageSize = 20;
            }

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _FileDinhKemService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa File đính kèm thành công");
            try
            {
                var user = _FileDinhKemService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _FileDinhKemService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _FileDinhKemService.GetById(id);
            return View(model);
        }

        
    }
}