using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.CongThucArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		public string CongThucName { get; set; }
		public string DichVuCode { get; set; }
		public string Description { get; set; }

        
    }
}