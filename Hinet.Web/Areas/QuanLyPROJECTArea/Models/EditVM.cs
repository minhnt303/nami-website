using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.QuanLyPROJECTArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public string TenDuAn { get; set; }
		public string DuAnId { get; set; }
		public string TinhId { get; set; }
		public string HuyenId { get; set; }
		public string Location { get; set; }
		public string Longitude { get; set; }
		public string Latitude { get; set; }

        
    }
}