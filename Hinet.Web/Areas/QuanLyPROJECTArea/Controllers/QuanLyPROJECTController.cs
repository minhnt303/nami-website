using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.QuanLyPROJECTArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.QuanLyPROJECTService;
using Nami.Service.QuanLyPROJECTService.Dto;
using CommonHelper.Excel;
using CommonHelper.ObjectExtention;
using Nami.Web.Common;
using System.IO;
using System.Web.Configuration;
using CommonHelper;
using Nami.Service.DM_DulieuDanhmucService;



namespace Nami.Web.Areas.QuanLyPROJECTArea.Controllers
{
    public class QuanLyPROJECTController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "QuanLyPROJECT_index";
        public const string permissionCreate = "QuanLyPROJECT_create";
        public const string permissionEdit = "QuanLyPROJECT_edit";
        public const string permissionDelete = "QuanLyPROJECT_delete";
        public const string permissionImport = "QuanLyPROJECT_Inport";
        public const string permissionExport = "QuanLyPROJECT_export";
        public const string searchKey = "QuanLyPROJECTPageSearchModel";
        private readonly IQuanLyPROJECTService _QuanLyPROJECTService;
	private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;


        public QuanLyPROJECTController(IQuanLyPROJECTService QuanLyPROJECTService, ILog Ilog,

		IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IMapper mapper
            )
        {
            _QuanLyPROJECTService = QuanLyPROJECTService;
            _Ilog = Ilog;
            _mapper = mapper;
		_dM_DulieuDanhmucService = dM_DulieuDanhmucService;

        }
        // GET: QuanLyPROJECTArea/QuanLyPROJECT
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {

            var listData = _QuanLyPROJECTService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QuanLyPROJECTSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new QuanLyPROJECTSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _QuanLyPROJECTService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Quản lý dự án thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<QuanLyPROJECT>(model);
                    _QuanLyPROJECTService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý dự án", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _QuanLyPROJECTService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _QuanLyPROJECTService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _QuanLyPROJECTService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý dự án", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(QuanLyPROJECTSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QuanLyPROJECTSearchDto;

            if (searchModel == null)
            {
                searchModel = new QuanLyPROJECTSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.TenDuAnFilter = form.TenDuAnFilter;
			searchModel.DuAnIdFilter = form.DuAnIdFilter;
			searchModel.TinhIdFilter = form.TinhIdFilter;
			searchModel.HuyenIdFilter = form.HuyenIdFilter;
			searchModel.LocationFilter = form.LocationFilter;
			searchModel.LongitudeFilter = form.LongitudeFilter;
			searchModel.LatitudeFilter = form.LatitudeFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _QuanLyPROJECTService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý dự án thành công");
            try
            {
                var user = _QuanLyPROJECTService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _QuanLyPROJECTService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _QuanLyPROJECTService.GetById(id);
            return View(model);
        }


        
    }
}