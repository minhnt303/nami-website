using System.Web.Mvc;

namespace Nami.Web.Areas.QuanLyPROJECTArea
{
    public class QuanLyPROJECTAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "QuanLyPROJECTArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "QuanLyPROJECTArea_default",
                "QuanLyPROJECTArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}