using AutoMapper;
using CommonHelper.Upload;
using Nami.Model.Entities;
using Nami.Service.AgreementsInfoService;
using Nami.Service.AgreementsInfoService.Dto;
using Nami.Service.BuildingsInfoService;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.FileDinhKemService;
using Nami.Service.RoomInfoService;
using Nami.Web.Areas.AgreementsInfoArea.Models;
using Nami.Web.Filters;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;


namespace Nami.Web.Areas.AgreementsInfoArea.Controllers
{
    public class AgreementsInfoController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "AgreementsInfo_index";
        public const string permissionCreate = "AgreementsInfo_create";
        public const string permissionEdit = "AgreementsInfo_edit";
        public const string permissionDelete = "AgreementsInfo_delete";
        public const string permissionImport = "AgreementsInfo_Inport";
        public const string permissionExport = "AgreementsInfo_export";
        public const string searchKey = "AgreementsInfoPageSearchModel";
        private readonly IAgreementsInfoService _AgreementsInfoService;
        private readonly IBuildingsInfoService _buildingsInfoService;
        private readonly IRoomInfoService _roomInfoService;
        private readonly IFileDinhKemService _fileDinhKemService;


        public AgreementsInfoController(IAgreementsInfoService AgreementsInfoService, ILog Ilog,
            IBuildingsInfoService buildingsInfoService,
            IRoomInfoService roomInfoService,
            IMapper mapper,
            IFileDinhKemService fileDinhKemService
            )
        {
            _AgreementsInfoService = AgreementsInfoService;
            _Ilog = Ilog;
            _mapper = mapper;
            _buildingsInfoService = buildingsInfoService;
            _roomInfoService = roomInfoService;
            _fileDinhKemService = fileDinhKemService;

        }
        // GET: AgreementsInfoArea/AgreementsInfo
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var listData = _AgreementsInfoService.GetDaTaByPageCreatedBy(CurrentUserInfo.UserName, null);
            var NumberOfAgreement = _AgreementsInfoService.CountNumberOfAgreementCreatedBy(CurrentUserInfo.UserName);
            ViewBag.NumberOfAgreement = NumberOfAgreement;
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as AgreementsInfoSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new AgreementsInfoSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _AgreementsInfoService.GetDaTaByPageCreatedBy(CurrentUserInfo.UserName, searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();
            ViewBag.dropdownlistBuilding = _buildingsInfoService.GetDropDownListBuldingForAggrement(CurrentUserId, null);
            ViewBag.dropdownlistRoom = _roomInfoService.GetDropDownListRoom(CurrentUserInfo.UserName);
            ViewBag.dropdownlistAgreementType = ConstantExtension.GetDropdownData<AgreementTypeConstant>(null);
            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult Create(CreateVM model, List<HttpPostedFileBase> file)
        {
            ViewBag.dropdownlistBuilding = _buildingsInfoService.GetDropDownListBuldingForAggrement(CurrentUserId, null);
            ViewBag.dropdownlistRoom = _roomInfoService.GetDropDownListRoom(CurrentUserInfo.UserName);
            ViewBag.dropdownlistAgreementType = ConstantExtension.GetDropdownData<AgreementTypeConstant>(null);
            var result = new JsonResultBO(true, "Tạo Hợp đồng thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<AgreementsInfo>(model);
                    _AgreementsInfoService.Create(EntityModel);
                    if (file != null && file.Any())
                    {
                        foreach (var item in file)
                        {
                            var FileDinhKemModel = new FileDinhKem();
                            FileDinhKemModel.IdItem = EntityModel.Id;
                            FileDinhKemModel.TypeItem = ItemTypeConstant.Agreement;
                            if (item != null && item.ContentLength > 0)
                            {
                                var resultUpload = UploadProvider.SaveFile(item, null, ".png,.jpge,.jpg,.doc,.docx,.pdf,.elsx", null, "Uploads/HopDong/", HostingEnvironment.MapPath("/"));

                                if (resultUpload.status == true)
                                {
                                    FileDinhKemModel.ImageUrl = resultUpload.path;
                                }
                                else
                                {
                                    ModelState.AddModelError("file", "Vui lòng chọn đúng định dạng .png,.jpge,.jpg,.doc,.docx,.pdf,.elsx");
                                }

                            }
                            _fileDinhKemService.Create(FileDinhKemModel);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Hợp đồng", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj = _AgreementsInfoService.GetById(id);
            if (obj == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }
            ViewBag.dropdownlistBuilding = _buildingsInfoService.GetDropDownListBuldingForAggrement(CurrentUserId, obj.BuildingsId.ToString());
            ViewBag.dropdownlistRoom = _roomInfoService.GetDropDownListRoom(CurrentUserInfo.UserName);
            ViewBag.dropdownlistAgreementType = ConstantExtension.GetDropdownData<AgreementTypeConstant>(obj.AgreementCode);
            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model, List<HttpPostedFileBase> file)
        {
            var obj = _AgreementsInfoService.GetById(model.Id);
            if (obj == null)
            {
                throw new Exception("Không tìm thấy thông tin");
            }
            ViewBag.dropdownlistBuilding = _buildingsInfoService.GetDropDownListBuldingForAggrement(CurrentUserId, obj.BuildingsId.ToString());
            ViewBag.dropdownlistRoom = _roomInfoService.GetDropDownListRoom(CurrentUserInfo.UserName);
            ViewBag.dropdownlistAgreementType = ConstantExtension.GetDropdownData<AgreementTypeConstant>(null);
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {


                    obj = _mapper.Map(model, obj);
                    _AgreementsInfoService.Update(obj);
                    if (file != null && file.Any())
                    {
                        foreach (var item in file)
                        {
                            var FileDinhKemModel = new FileDinhKem();
                            FileDinhKemModel.IdItem = obj.Id;
                            FileDinhKemModel.TypeItem = ItemTypeConstant.Agreement;
                            if (item != null && item.ContentLength > 0)
                            {
                                var resultUpload = UploadProvider.SaveFile(item, null, ".png,.jpge,.jpg,.doc,.docx,.pdf,.elsx", null, "Uploads/HopDong/", HostingEnvironment.MapPath("/"));

                                if (resultUpload.status == true)
                                {
                                    FileDinhKemModel.ImageUrl = resultUpload.path;
                                }
                                else
                                {
                                    ModelState.AddModelError("file", "Vui lòng chọn đúng định dạng .png,.jpge,.jpg,.doc,.docx,.pdf,.elsx");
                                }

                            }
                            _fileDinhKemService.Create(FileDinhKemModel);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Hợp đồng", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(AgreementsInfoSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as AgreementsInfoSearchDto;

            if (searchModel == null)
            {
                searchModel = new AgreementsInfoSearchDto();
                searchModel.pageSize = 20;
            }
            searchModel.AgreementCodeFilter = form.AgreementCodeFilter;
            searchModel.DepositsMoneyFilter = form.DepositsMoneyFilter;
            searchModel.MonthFilter = form.MonthFilter;
            searchModel.DateStartFilter = form.DateStartFilter;
            searchModel.DateEndFilter = form.DateEndFilter;
            searchModel.PayPerMonthFilter = form.PayPerMonthFilter;

            SessionManager.SetValue((searchKey), searchModel);

            var data = _AgreementsInfoService.GetDaTaByPageCreatedBy(CurrentUserInfo.UserName, searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Hợp đồng thành công");
            try
            {
                var user = _AgreementsInfoService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _AgreementsInfoService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }


        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _AgreementsInfoService.GetDtoById(id);
            model.filedinhkem = _fileDinhKemService.GetListImageFile(id, ItemTypeConstant.Agreement);
            return View(model);
        }
        public PartialViewResult DetailPopup(long id)
        {
            var model = new DetailVM();
            model.objInfo = _AgreementsInfoService.GetDtoById(id);
            model.filedinhkem = _fileDinhKemService.GetListImageFile(id, ItemTypeConstant.Agreement);
            return PartialView("_detailPartial", model);
        }

        public PartialViewResult GiaHanHopDong(long id)
        {
            var model = new EditVM();
            model.objInfo = _AgreementsInfoService.GetDtoById(id);
            model.RepresenterName = model.objInfo.RepresenterName;
            model.DateEnd = model.objInfo.DateEnd;
            model.DateStart = model.objInfo.DateStart;
            model.Month = model.objInfo.Month;
            model.PayPerMonth = model.objInfo.PayPerMonth;
            model.Note = model.objInfo.Note;
            return PartialView("_GiaHanHopDongPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult GiaHanHopDong(EditVM model, List<HttpPostedFileBase> file)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _AgreementsInfoService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }
                    if (!string.IsNullOrEmpty(model.RepresenterName))
                    {
                        obj.RepresenterName = model.RepresenterName;
                    }
                    else
                    {
                        throw new Exception("Bạn chưa nhập tên người đặt phòng!");
                    }
                    if (model.DateEnd != null)
                    {
                        obj.DateEnd = model.DateEnd;
                    }
                    else
                    {
                        throw new Exception("Bạn chưa nhập ngày kết thúc hợp đồng!");
                    }
                    if (model.DateStart != null)
                    {
                        obj.DateStart = model.DateStart;
                    }
                    else
                    {
                        throw new Exception("Bạn chưa nhập ngày bắt đầu hợp đồng!");
                    }
                    if (model.PayPerMonth != null && model.PayPerMonth != 0)
                    {
                        obj.PayPerMonth = model.PayPerMonth;
                    }
                    else
                    {
                        throw new Exception("Bạn chưa nhập chi phí nhà hàng tháng!");
                    }
                    if (model.Month != null && model.Month != 0)
                    {
                        obj.Month = model.Month;
                    }
                    else
                    {
                        throw new Exception("Bạn chưa nhập thời hạn hợp đồng!");
                    }
                    if (!string.IsNullOrEmpty(model.Note))
                    {
                        obj.Note = model.Note;
                    }
                    _AgreementsInfoService.Update(obj);
                    if (file != null && file.Any())
                    {
                        foreach (var item in file)
                        {
                            var FileDinhKemModel = new FileDinhKem();
                            FileDinhKemModel.IdItem = obj.Id;
                            FileDinhKemModel.TypeItem = ItemTypeConstant.Agreement;
                            if (item != null && item.ContentLength > 0)
                            {
                                var resultUpload = UploadProvider.SaveFile(item, null, ".png,.jpge,.jpg,.doc,.docx,.pdf,.elsx", null, "Uploads/HopDong/", HostingEnvironment.MapPath("/"));

                                if (resultUpload.status == true)
                                {
                                    FileDinhKemModel.ImageUrl = resultUpload.path;
                                }
                                else
                                {
                                    ModelState.AddModelError("file", "Vui lòng chọn đúng định dạng .png,.jpge,.jpg,.doc,.docx,.pdf,.elsx");
                                }

                            }
                            _fileDinhKemService.Create(FileDinhKemModel);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Hợp đồng", ex);
            }
            return Json(result);
        }
    }
}