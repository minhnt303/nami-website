﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.AgreementsInfoArea.Models
{
    public class GiaHanHopDongVM
    {
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public int? Month { get; set; }
        public string Note { get; set; }
    }
}