using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Nami.Service.AgreementsInfoService.Dto;

namespace Nami.Web.Areas.AgreementsInfoArea.Models
{
    public class EditVM
    {
        public AgreementsInfoDto objInfo;

        public long Id { get; set; }
        public long? BuildingsId { get; set; }
        public long? RoomsId { get; set; }
        public string AgreementCode { get; set; }
        public string RentersGroupId { get; set; }
        public string ServicesGroupId { get; set; }
        public string RepresenterName { get; set; }
        public int? DepositsMoney { get; set; }
        public int? Month { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public int? PayPerMonth { get; set; }
        public string Note { get; set; }


    }
}