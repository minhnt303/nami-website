using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.AgreementsInfoService.Dto;

namespace Nami.Web.Areas.AgreementsInfoArea.Models
{
    public class DetailVM
    {
        public List<FileDinhKem> filedinhkem;

        public AgreementsInfoDto objInfo { get; set; }
    }
}