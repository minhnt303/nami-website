using System.Web.Mvc;

namespace Nami.Web.Areas.AgreementsInfoArea
{
    public class AgreementsInfoAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AgreementsInfoArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AgreementsInfoArea_default",
                "AgreementsInfoArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}