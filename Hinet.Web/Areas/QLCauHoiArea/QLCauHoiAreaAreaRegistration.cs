using System.Web.Mvc;

namespace Nami.Web.Areas.QLCauHoiArea
{
    public class QLCauHoiAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "QLCauHoiArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "QLCauHoiArea_default",
                "QLCauHoiArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}