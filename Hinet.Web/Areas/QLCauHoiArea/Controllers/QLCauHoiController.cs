using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.QLCauHoiArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.QLCauHoiService;
using Nami.Service.QLCauHoiService.Dto;
using CommonHelper.Excel;
using CommonHelper.ObjectExtention;
using Nami.Web.Common;
using System.IO;
using System.Web.Configuration;
using CommonHelper;
using Nami.Service.DM_DulieuDanhmucService;



namespace Nami.Web.Areas.QLCauHoiArea.Controllers
{
    public class QLCauHoiController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "QLCauHoi_index";
        public const string permissionCreate = "QLCauHoi_create";
        public const string permissionEdit = "QLCauHoi_edit";
        public const string permissionDelete = "QLCauHoi_delete";
        public const string permissionImport = "QLCauHoi_Inport";
        public const string permissionExport = "QLCauHoi_export";
        public const string searchKey = "QLCauHoiPageSearchModel";
        private readonly IQLCauHoiService _QLCauHoiService;
	private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;


        public QLCauHoiController(IQLCauHoiService QLCauHoiService, ILog Ilog,

		IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IMapper mapper
            )
        {
            _QLCauHoiService = QLCauHoiService;
            _Ilog = Ilog;
            _mapper = mapper;
		_dM_DulieuDanhmucService = dM_DulieuDanhmucService;

        }
        // GET: QLCauHoiArea/QLCauHoi
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {

            var listData = _QLCauHoiService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLCauHoiSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new QLCauHoiSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _QLCauHoiService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Quản lý câu hỏi thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<QLCauHoi>(model);
                    _QLCauHoiService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý câu hỏi", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _QLCauHoiService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _QLCauHoiService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _QLCauHoiService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý câu hỏi", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(QLCauHoiSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLCauHoiSearchDto;

            if (searchModel == null)
            {
                searchModel = new QLCauHoiSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.NameFilter = form.NameFilter;
			searchModel.EmailFilter = form.EmailFilter;
			searchModel.PhoneNumberFilter = form.PhoneNumberFilter;
			searchModel.CauHoiFilter = form.CauHoiFilter;
			searchModel.IdNguoiXuLyFilter = form.IdNguoiXuLyFilter;
			searchModel.StatusFilter = form.StatusFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _QLCauHoiService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý câu hỏi thành công");
            try
            {
                var user = _QLCauHoiService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _QLCauHoiService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _QLCauHoiService.GetById(id);
            return View(model);
        }
	//[PermissionAccess(Code = permissionImport)]
        public FileResult ExportExcel()
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLCauHoiSearchDto;
            var data = _QLCauHoiService.GetDaTaByPage(searchModel).ListItem;
		var dataExport = _mapper.Map<List<QLCauHoiExportDto>>(data);
            var fileExcel = ExportExcelV2Helper.Export<QLCauHoiExportDto>(dataExport);
            return File(fileExcel, "application/octet-stream", "QLCauHoi.xlsx");
        }

        
    }
}