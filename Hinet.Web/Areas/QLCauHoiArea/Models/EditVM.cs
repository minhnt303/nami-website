using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.QLCauHoiArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public string CauHoi { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public long IdNguoiXuLy { get; set; }
		public string Status { get; set; }

        
    }
}