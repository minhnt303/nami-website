﻿using Nami.Model.IdentityEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.UserArea.Models
{
    public class ResetPasswordVM
    {
        public AppUser UserInfo {get;set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string NewPassword { get; set; }
    }
}