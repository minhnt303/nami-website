﻿using AutoMapper;
using CommonHelper;
using CommonHelper.Excel;
using CommonHelper.String;
using CommonHelper.Upload;
using Nami.Model.IdentityEntities;
using Nami.Service.AppUserService;
using Nami.Service.AppUserService.Dto;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.DepartmentService;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.RoleService;
using Nami.Service.TINHService;
using Nami.Service.UserRoleService;
using Nami.Web.Areas.UserArea.Models;
using Nami.Web.Common;
using Nami.Web.Filters;
using Nami.Web.Models;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Nami.Web.Areas.UserArea.Controllers
{
    public class UserController : BaseController
    {
        private readonly IAppUserService _appUserService;
        private readonly IRoleService _roleService;
        private readonly IUserRoleService _userRoleService;
        private readonly IDepartmentService _deparmentService;
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "QLTaiKhoan";
        IDM_DulieuDanhmucService _dm_DulieuDanhmucService;
        ITINHService _TINHService;

        public UserController(IAppUserService appUserService, ILog Ilog,
            IRoleService roleService,
            IUserRoleService userRoleService,
            IDepartmentService deparmentService,
            IDM_DulieuDanhmucService dm_DulieuDanhmucService,
            IMapper mapper,
            ITINHService TINHService
            )
        {
            _userRoleService = userRoleService;
            _appUserService = appUserService;
            _Ilog = Ilog;
            _roleService = roleService;
            _mapper = mapper;
            _deparmentService = deparmentService;
            _dm_DulieuDanhmucService = dm_DulieuDanhmucService;
            _TINHService = TINHService;
        }
        // GET: UserArea/User
        [PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var userData = _appUserService.GetDaTaByPage(null);
            foreach (var userItem in userData.ListItem)
            {
                var Province = !string.IsNullOrEmpty(userItem.ProvinceManagement) ? userItem.ProvinceManagement.Split(',').ToList() : new List<string>();
                var ProvinceList = new List<string>();
                foreach (var item in Province)
                {
                    var tinhInfo = _TINHService.GetNameByMaTinh(item);
                    if (tinhInfo != null && !string.IsNullOrEmpty(tinhInfo.TenTinh))
                    {
                        var tinhName = tinhInfo.TenTinh;
                        ProvinceList.Add(tinhName);
                    }
                }
                userItem.ProvinceManagement = string.Join(",", ProvinceList);
            }
            return View(userData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue("UserPageSearchModel") as AppUserSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new AppUserSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue("UserPageSearchModel", searchModel);
            }
            var data = _appUserService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }

        public PartialViewResult Create()
        {
            var myModel = new CreateVM();
            var listDeparment = _deparmentService.GetDropdown("Name", "Id");
            ViewBag.listDeparment = listDeparment;
            var listJobPosition = _dm_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.ChucVu, "Id");
            ViewBag.listJobPosition = listJobPosition;
            ViewBag.ListTinh = _TINHService.GetDropdown("TenTinh", "MaTinh", string.Empty);
            return PartialView("_CreatePartial", myModel);
        }

        public ActionResult MyProfile()
        {
            var model = new DetailVM();
            model.users = _appUserService.GetDtoById((long)CurrentUserId);
            string departmentName = "Chưa cập nhật";
            string job = "Chưa cập nhật";
            if (model.users.IdDepartment != null)
            {
                if (_deparmentService.GetById(model.users.IdDepartment).Name != null)
                {
                    departmentName = _deparmentService.GetById(model.users.IdDepartment).Name;
                }
            }
            if (model.users.IdChucVu != null)
            {
                if (_dm_DulieuDanhmucService.GetByIdName("CHUCVU", model.users.IdChucVu)?.Name != null)
                {
                    job = _dm_DulieuDanhmucService.GetByIdName("CHUCVU", model.users.IdChucVu)?.Name;
                }
            }
            ViewBag.departmentName = departmentName;
            ViewBag.job = job;
            return View(model);
        }

        public PartialViewResult Edit(long id)
        {
            var user = _appUserService.GetById(id);
            if (user == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }
            var myModel = new EditVM()
            {
                Id = user.Id,                
                FullName = user.FullName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                BirthDay = user.BirthDay,
                Gender = user.Gender,
                Address = user.Address,
                IdDepartment = user.IdDepartment,
                IdChucVu = user.IdChucVu,
                Province = !string.IsNullOrEmpty(user.ProvinceManagement) ? user.ProvinceManagement.Split(',').ToList() : new List<string>()
            };
            var listJobPosition = _dm_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.ChucVu, "Id");
            ViewBag.listJobPosition = listJobPosition;
            var listDeparment = _deparmentService.GetDropdown("Name", "Id", user.IdDepartment);
            ViewBag.listDeparment = listDeparment;
            myModel = _mapper.Map(user, myModel);
            var ListTinh = _TINHService.GetDropdown("TenTinh", "MaTinh", string.Empty);
            if (myModel.Province.Any())
            {
                foreach (var item in ListTinh)
                {
                    if (myModel.Province.Contains(item.Value))
                    {
                        item.Selected = true;
                    }
                }
            }
            ViewBag.ListTinh = ListTinh;

            return PartialView("_EditPartial", myModel);
        }       

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(EditVM model)
        {
            
            
            var ListTinh = _TINHService.GetDropdown("TenTinh", "MaTinh", string.Empty);
            var listJobPosition = _dm_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.ChucVu, "Id");
            foreach (var item in ListTinh)
            {
                if (model.Province != null && model.Province.Contains(item.Value))
                {
                    item.Selected = true;
                }
            }
            
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var user = _appUserService.GetById(model.Id);
                    if (user == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }
                    else
                    {                        
                        if (!string.IsNullOrEmpty(model.Email) && _appUserService.CheckExistEmail(model.Email, user.Id))
                        {
                            throw new Exception(string.Format("Email {0} đã được sử dụng", model.Email));
                        }
                        user.ProvinceManagement = model.Province != null ? string.Join(",", model.Province) : string.Empty;
                        model.IdChucVu = model.IdChucVu == null ? user.IdChucVu : model.IdChucVu;
                        model.IdDepartment = model.IdDepartment == null ? user.IdDepartment : model.IdDepartment;
                        user = _mapper.Map(model, user);
                        _appUserService.Update(user);
                    }
                }
            }
            catch (Exception ex)
            {

                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin User", ex);
                /*if (ex is DbEntityValidationException)
                {
                    var err = (DbEntityValidationException)ex;
                    foreach (var eve in err.EntityValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                }*/
            }
            return Json(result);
        }

        public PartialViewResult UpdateEmailEnduser(long id)
        {
            var model = new UpdateEnduserVM();
            var userData = _appUserService.GetById(id);
            if (userData != null && userData.TypeAccount == AccountTypeConstant.NguoiDung)
            {
                ViewBag.userData = userData;
                model.Id = userData.Id;
            }
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateEmailEnduser(UpdateEnduserVM model)
        {
            var result = new JsonResultBO(true, "Cập nhật email thành công");
            var userData = _appUserService.GetById(model.Id);
            if (userData == null || userData.TypeAccount != AccountTypeConstant.NguoiDung)
            {
                result.MessageFail("Không tìm thấy tài khoản");
                return Json(result);
            }
            userData.Email = model.Email;
            _appUserService.Update(userData);
            return Json(result);
        }

        public PartialViewResult UpdatePassEnduser(long id)
        {
            var model = new UpdateEnduserPasswordVM();
            var userData = _appUserService.GetById(id);
            if (userData != null && userData.TypeAccount == AccountTypeConstant.NguoiDung)
            {
                ViewBag.userData = userData;
                model.Id = userData.Id;
            }
            return PartialView(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdatePassEnduser(UpdateEnduserPasswordVM model)
        {
            var result = new JsonResultBO(true, "Cập nhật mật khẩu thành công");
            try
            {
                var user = _appUserService.GetById(model.Id);
                if (user == null || user.TypeAccount != AccountTypeConstant.NguoiDung)
                {
                    result.MessageFail("Không tìm thấy tài khoản");
                    return Json(result);
                }
                user.IsUpdateNewPass = true;
                _appUserService.Update(user);
                var UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                if (string.IsNullOrEmpty(user.SecurityStamp))
                {
                    var rsUpdateStamp = UserManager.UpdateSecurityStamp(user.Id);
                }
                var userObj = UserManager.FindById(user.Id);

                var tokenUpdatePass = UserManager.GeneratePasswordResetToken(user.Id);

                var rs = UserManager.ResetPassword(user.Id, tokenUpdatePass, model.Password);
                if (!rs.Succeeded)
                {
                    result.MessageFail(rs.Errors.FirstOrDefault());
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                _Ilog.Error(ex.Message, ex);
                result.MessageFail(ex.Message);
                return Json(result);
            }
            return Json(result);
        }

        public PartialViewResult EditProfile(long id)
        {
            var user = _appUserService.GetById(id);
            if (user == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }
            var myModel = new EditVM()
            {
                Id = user.Id,
                FullName = user.FullName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                BirthDay = user.BirthDay,
                Gender = user.Gender,
                Address = user.Address
            };
            return PartialView("_EditProfilePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditProfile(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var user = _appUserService.GetById(model.Id);
                    if (user == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(model.Email) && _appUserService.CheckExistEmail(model.Email, user.Id))
                        {
                            throw new Exception(string.Format("Email {0} đã được sử dụng", model.Email));
                        }

                        user.FullName = model.FullName;
                        user.Email = model.Email;
                        user.PhoneNumber = model.PhoneNumber;
                        user.BirthDay = model.BirthDay;
                        user.Gender = (int)model.Gender;
                        user.Address = model.Address;
                        _appUserService.Update(user);
                    }
                }
            }
            catch (Exception ex)
            {

                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin User", ex);                
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(CreateVM model)
        {
            ViewBag.ListTinh = _TINHService.GetDropdown("TenTinh", "MaTinh", string.Empty);
            var listJobPosition = _dm_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.ChucVu, "Id");
            ViewBag.listJobPosition = listJobPosition;
            var result = new JsonResultBO(true, "Tạo tài khoản thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    if (_appUserService.CheckExistUserName(model.UserName))
                    {
                        throw new Exception(string.Format("Tài khoản {0} đã tồn tại", model.UserName));
                    }
                    if (_appUserService.CheckExistEmail(model.Email))
                    {
                        throw new Exception(string.Format("Email {0} đã được sửa dụng", model.Email));
                    }
                    var user = new AppUser();
                    user.UserName = model.UserName;
                    user.FullName = model.FullName;
                    user.PhoneNumber = model.PhoneNumber;
                    user.BirthDay = model.BirthDay;
                    user.Address = model.Address;
                    user.Gender = model.Gender;
                    user.Email = model.Email;
                    user.IdChucVu = model.IdChucVu;
                    user.IdDepartment = model.IdDepartment;
                    user.Avatar = "images/avatars/profile-pic.jpg";
                    user.TypeAccount = AccountTypeConstant.BussinessUser;
                    user.ProvinceManagement = string.Join(",", model.Province);
                    var UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    //Kiểm tra thông tin tài khoản
                    var resultUser = UserManager.CreateAsync(user, UserConst.DefaultPassword).Result;
                    if (!resultUser.Succeeded)
                    {
                        throw new Exception(getErrorString(resultUser));
                    }

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới tài khoản", ex);
            }
            return Json(result);
        }
        private string getErrorString(IdentityResult identityResult)
        {
            var strMessage = string.Empty;
            foreach (var item in identityResult.Errors)
            {
                strMessage += item;
            }
            return strMessage;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(AppUserSearchDto form)
        {
            var searchModel = SessionManager.GetValue("UserPageSearchModel") as AppUserSearchDto;

            if (searchModel == null)
            {
                searchModel = new AppUserSearchDto();
                searchModel.pageSize = 20;
            }

            searchModel.UserNameFilter = form.UserNameFilter;
            searchModel.AddressFilter = form.AddressFilter;
            searchModel.EmailFilter = form.EmailFilter;
            searchModel.FullNameFilter = form.FullNameFilter;
            SessionManager.SetValue("UserPageSearchModel", searchModel);

            var data = _appUserService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa tài khoản thành công");
            try
            {
                var user = _appUserService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _appUserService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        public ActionResult SetupRole(long id)
        {
            var user = _appUserService.GetById(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserInfo = user;
            ViewBag.ListRole = _roleService.GetAll().ToList();
            ViewBag.UserListRole = _userRoleService.GetRoleOfUser(user.Id);
            return PartialView("_SetupRolePartial");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SaveSetupRole(List<int> ListRoleUser, long userId)
        {
            var result = new JsonResultBO(true, "Thiết lập vai trò thành công");
            var userData = _appUserService.GetById(userId);
            if (userData == null)
            {
                return HttpNotFound();
            }
            var isSuccess = _userRoleService.SaveRole(ListRoleUser, userId);
            if (!isSuccess)
            {
                result.MessageFail("Lỗi khi thiết lập vai trò người dùng");
            }
            return Json(result);
        }
        public ActionResult Detail(int id)
        {
            var model = new DetailVM();
            model.users = _appUserService.GetDtoById(id);
            if (model.users.IdDepartment != null)
            {
                ViewBag.departmentName = _deparmentService.GetById(model.users.IdDepartment).Name;
            }
            else ViewBag.departmentName = "";

            if (model.users.IdChucVu != null)
            {
                ViewBag.job = _dm_DulieuDanhmucService.GetByIdName("CHUCVU", model.users.IdChucVu)?.Name;
            }
            else ViewBag.job = "";
            return View(model);
        }
        public PartialViewResult EditAvatar(long id)
        {
            var model = new DetailVM();
            model.users = _appUserService.GetDtoById(id);
            return PartialView("_EditAvatarPartial", model);
        }
        [HttpPost]
        public ActionResult EditAvatar(FormCollection collection, HttpPostedFileBase File)
        {
            var id = collection["ID"].ToIntOrZero();
            var myModel = _appUserService.GetById(id);
            var result = new JsonResultBO(true);
            try
            {
                if (File != null && File.ContentLength > 0)
                {
                    var resultUpload = UploadProvider.SaveFile(File, null, ".jpg,.png", null, "Uploads/Avatars/", HostingEnvironment.MapPath("/"));

                    if (resultUpload.status == true)
                    {
                        myModel.Avatar = resultUpload.path;
                    }
                }
                _appUserService.Update(myModel);
                var currentUser = (UserDto)SessionManager.GetUserInfo();
                currentUser.Avatar = myModel.Avatar;

            }
            catch
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
            }

            return RedirectToAction("MyProfile");
        }

        [HttpPost]
        public JsonResult ResetPassword(long id)
        {
            var result = new JsonResultBO(true);
            var user = _appUserService.GetById(id);
            if (user == null)
            {
                result.MessageFail("Không tìm thấy người dùng");
                return Json(result);
            }
            user.IsUpdateNewPass = true;

            _appUserService.Update(user);
            var UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (string.IsNullOrEmpty(user.SecurityStamp))
            {
                UserManager.UpdateSecurityStamp(user.Id);
            }

            var tokenUpdatePass = UserManager.GeneratePasswordResetToken(user.Id);
            var defaultPass = "102030Aa@";
            var rs = UserManager.ResetPassword(user.Id, tokenUpdatePass, defaultPass);
            if (!rs.Succeeded)
            {
                result.MessageFail(rs.Errors.FirstOrDefault());
                return Json(result);
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult LockUser(long? id, bool islock)
        {
            var result = new JsonResultBO(true);
            var obj = _appUserService.GetById(id);
            if (obj == null)
            {
                result.MessageFail("Không tìm thấy thông tin");
                return Json(result);
            }
            try
            {
                if (islock == true)
                {
                    obj.LockoutEnabled = true;
                    obj.LockoutEndDateUtc = DateTime.Now.AddYears(1000);
                }
                else if (islock == false)
                {
                    obj.LockoutEnabled = false;
                    obj.LockoutEndDateUtc = null;
                }
                _appUserService.Update(obj);
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không khóa/mở khóa được tài khoản";
                _Ilog.Error("Lỗi khóa/mở khóa tài khoản", ex);
                result.MessageFail(ex.Message);
                return Json(result);
            }
            return Json(result);
        }
        public PartialViewResult ResetPasswordPartial(long id)
        {
            var myModel = new ResetPasswordVM();
            var obj = _appUserService.GetById(id);
            if (obj == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }
            myModel.UserInfo = obj;
            return PartialView("_ResetPasswordPartial", myModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public JsonResult ResetPasswordPartial(ResetPasswordVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _appUserService.GetById(model.UserInfo.Id);
                    if (user == null)
                    {
                        result.MessageFail("Không tìm thấy người dùng");
                        return Json(result);
                    }
                    user.IsUpdateNewPass = true;

                    _appUserService.Update(user);
                    var UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    if (string.IsNullOrEmpty(user.SecurityStamp))
                    {
                        UserManager.UpdateSecurityStamp(user.Id);
                    }

                    var tokenUpdatePass = UserManager.GeneratePasswordResetToken(user.Id);
                    var defaultPass = "102030Aa@";
                    if (!string.IsNullOrEmpty(model.NewPassword))
                    {
                        defaultPass = model.NewPassword;
                    }
                    var rs = UserManager.ResetPassword(user.Id, tokenUpdatePass, defaultPass);
                    if (!rs.Succeeded)
                    {
                        result.MessageFail(rs.Errors.FirstOrDefault());
                        return Json(result);
                    }

                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = ex.Message;
                _Ilog.Error("Lỗi reset mật khẩu", ex);
            }
            return Json(result);
        }


        //[PermissionAccess(Code = permissionImport)]
        public ActionResult Import()
        {
            var model = new ImportVM();
            model.PathTemplate = Path.Combine(@"/Uploads", WebConfigurationManager.AppSettings["IMPORT_AppUser"]);

            return View(model);
        }

        [HttpPost]
        public ActionResult CheckImport(FormCollection collection, HttpPostedFileBase fileImport)
        {
            JsonResultImportBO<AppUserImportDto> result = new JsonResultImportBO<AppUserImportDto>(true);
            //Kiểm tra file có tồn tại k?
            if (fileImport == null)
            {
                result.Status = false;
                result.Message = "Không có file đọc dữ liệu";
                return View(result);
            }

            //Lưu file upload để đọc
            var saveFileResult = UploadProvider.SaveFile(fileImport, null, ".xls,.xlsx", null, "TempImportFile", HostingEnvironment.MapPath("/Uploads"));
            if (!saveFileResult.status)
            {
                result.Status = false;
                result.Message = saveFileResult.message;
                return View(result);
            }
            else
            {

                #region Config để import dữ liệu
                var importHelper = new ImportExcelHelper<AppUserImportDto>();
                importHelper.PathTemplate = saveFileResult.fullPath;
                //importHelper.StartCol = 2;
                importHelper.StartRow = collection["ROWSTART"].ToIntOrZero();
                importHelper.ConfigColumn = new List<ConfigModule>();
                importHelper.ConfigColumn = ExcelImportExtention.GetConfigCol<AppUserImportDto>(collection);
                #endregion
                var rsl = importHelper.ImportCustomRow();
                if (rsl.ListTrue != null && rsl.ListTrue.Any())
                {
                    var lstTrueCheckTrung = rsl.ListTrue.ToList();
                    rsl.ListTrue = new List<AppUserImportDto>();
                    foreach (var item in lstTrueCheckTrung)
                    {
                        if (!rsl.ListTrue.Any(x => x.UserName.Equals(item.UserName)) && _appUserService.CheckExistUserName(item.UserName) != true)
                        {
                            item.UserName = item.UserName;
                            rsl.ListTrue.Add(item);
                        }
                        //else if (!rsl.ListTrue.Any(x => x.UserName.Equals(item.UserName)) && _appUserService.CheckExistUserName(item.UserName) == true)
                        //{
                        //    item.IsExist = true;
                        //    rsl.ListTrue.Add(item);
                        //}
                        else
                        {
                            var stringErr = "+ Tên đăng nhập trùng lặp \n";
                            rsl.lstFalse.Add(ExcelImportExtention.GetErrMess<AppUserImportDto>(item, stringErr));
                        }
                    }
                }
                if (rsl.Status)
                {
                    result.Status = true;
                    result.Message = rsl.Message;

                    result.ListData = rsl.ListTrue;
                    result.ListFalse = rsl.lstFalse;
                }
                else
                {
                    result.Status = false;
                    result.Message = rsl.Message;
                }

            }
            return View(result);
        }


        [HttpPost]
        public JsonResult GetExportError(List<List<string>> lstData)
        {
            ExportExcelHelper<AppUserImportDto> exPro = new ExportExcelHelper<AppUserImportDto>();
            exPro.PathStore = Path.Combine(HostingEnvironment.MapPath("/Uploads"), "ErrorExport");
            exPro.PathTemplate = Path.Combine(HostingEnvironment.MapPath("/Uploads"), WebConfigurationManager.AppSettings["IMPORT_AppUser"]);
            exPro.StartRow = 5;
            exPro.StartCol = 2;
            exPro.FileName = "ErrorImportAppUser";
            var result = exPro.ExportText(lstData);
            if (result.Status)
            {
                result.PathStore = Path.Combine(@"/Uploads/ErrorExport", result.FileName);
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult SaveImportData(List<AppUserImportDto> Data)
        {
            var result = new JsonResultBO(true);

            var lstObjSave = new List<AppUser>();
            try
            {
                foreach (var item in Data)
                {
                    var obj = _mapper.Map<AppUser>(item);
                    obj.TypeAccount = AccountTypeConstant.BussinessUser;
                    if (!string.IsNullOrEmpty(obj.UserName) && _appUserService.CheckExistUserName(obj.UserName) != true)
                    {
                        var UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                        //Kiểm tra thông tin tài khoản
                        var resultUser = UserManager.CreateAsync(obj, UserConst.DefaultPassword).Result;
                        if (!resultUser.Succeeded)
                        {
                            throw new Exception(getErrorString(resultUser));
                        }
                        if (!string.IsNullOrEmpty(item.IdRole))
                        {
                            var ListRoleUser = item.IdRole.Split(',').ToList();
                            var ListRoleInt = new List<int>();
                            foreach (var item2 in ListRoleUser)
                            {
                                if (item2.ToIntOrZero() > 0)
                                {
                                    ListRoleInt.Add(item2.ToIntOrZero());
                                }
                            }
                            if (ListRoleInt != null && ListRoleInt.Any())
                            {
                                var isSuccess = _userRoleService.SaveRole(ListRoleInt, obj.Id);

                                //if (!isSuccess)
                                //{
                                //    result.MessageFail("Lỗi khi thiết lập vai trò người dùng");
                                //}
                            }
                        }
                    }
                    //else if (!string.IsNullOrEmpty(obj.UserName) && _appUserService.CheckExistUserName(obj.UserName) == true)
                    //{
                    //    var UserInfo = _appUserService.GetUserByUsername(obj.UserName);
                    //    UserInfo.FullName = obj.FullName;
                    //    UserInfo.IdDepartment = obj.IdDepartment;
                    //    UserInfo.IdChucVu = obj.IdChucVu;
                    //    UserInfo.PhoneNumber = obj.PhoneNumber;
                    //    UserInfo.Email = obj.Email;
                    //    UserInfo.Address = obj.Address;
                    //    UserInfo.ProvinceManagement = obj.ProvinceManagement;
                    //    _appUserService.Update(UserInfo);

                    //    if (!string.IsNullOrEmpty(item.IdRole))
                    //    {
                    //        var ListRoleUser = item.IdRole.Split(',').ToList();
                    //        var ListRoleInt = new List<int>();
                    //        foreach (var item2 in ListRoleUser)
                    //        {
                    //            if (item2.ToIntOrZero() > 0)
                    //            {
                    //                ListRoleInt.Add(item2.ToIntOrZero());
                    //            }
                    //        }
                    //        if (ListRoleInt != null && ListRoleInt.Any())
                    //        {
                    //            var isSuccess = _userRoleService.SaveRole(ListRoleInt, UserInfo.Id);

                    //            //if (!isSuccess)
                    //            //{
                    //            //    result.MessageFail("Lỗi khi thiết lập vai trò người dùng");
                    //            //}
                    //        }
                    //    }
                    //}
                }

            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Lỗi dữ liệu, không thể import";
                _Ilog.Error("Lỗi Import", ex);
            }

            return Json(result);
        }
    }
}