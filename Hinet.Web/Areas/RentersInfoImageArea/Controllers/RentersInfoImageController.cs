using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.RentersInfoImageArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.RentersInfoImageService;
using Nami.Service.RentersInfoImageService.Dto;



namespace Nami.Web.Areas.RentersInfoImageArea.Controllers
{
    public class RentersInfoImageController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "RentersInfoImage_index";
        public const string permissionCreate = "RentersInfoImage_create";
        public const string permissionEdit = "RentersInfoImage_edit";
        public const string permissionDelete = "RentersInfoImage_delete";
        public const string permissionImport = "RentersInfoImage_Inport";
        public const string permissionExport = "RentersInfoImage_export";
        public const string searchKey = "RentersInfoImagePageSearchModel";
        private readonly IRentersInfoImageService _RentersInfoImageService;


        public RentersInfoImageController(IRentersInfoImageService RentersInfoImageService, ILog Ilog,

            IMapper mapper
            )
        {
            _RentersInfoImageService = RentersInfoImageService;
            _Ilog = Ilog;
            _mapper = mapper;

        }
        // GET: RentersInfoImageArea/RentersInfoImage
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var listData = _RentersInfoImageService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as RentersInfoImageSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new RentersInfoImageSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _RentersInfoImageService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo File ảnh khách thuê thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<RentersInfoImage>(model);
                    _RentersInfoImageService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới File ảnh khách thuê", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _RentersInfoImageService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _RentersInfoImageService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _RentersInfoImageService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin File ảnh khách thuê", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(RentersInfoImageSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as RentersInfoImageSearchDto;

            if (searchModel == null)
            {
                searchModel = new RentersInfoImageSearchDto();
                searchModel.pageSize = 20;
            }

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _RentersInfoImageService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa File ảnh khách thuê thành công");
            try
            {
                var user = _RentersInfoImageService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _RentersInfoImageService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _RentersInfoImageService.GetById(id);
            return View(model);
        }

        
    }
}