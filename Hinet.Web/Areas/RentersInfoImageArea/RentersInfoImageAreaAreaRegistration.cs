using System.Web.Mvc;

namespace Nami.Web.Areas.RentersInfoImageArea
{
    public class RentersInfoImageAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "RentersInfoImageArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "RentersInfoImageArea_default",
                "RentersInfoImageArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}