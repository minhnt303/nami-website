using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.RentersInfoImageArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		public long? RentersId { get; set; }
		public string RentersType { get; set; }
		public string ImageUrl { get; set; }

        
    }
}