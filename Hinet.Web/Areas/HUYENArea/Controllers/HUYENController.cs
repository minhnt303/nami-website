using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.HUYENArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.HUYENService;
using Nami.Service.HUYENService.Dto;
using Nami.Service.TINHService;

namespace Nami.Web.Areas.HUYENArea.Controllers
{
    public class HUYENController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "HUYEN_index";
        public const string permissionCreate = "HUYEN_create";
        public const string permissionEdit = "HUYEN_edit";
        public const string permissionDelete = "HUYEN_delete";
        public const string permissionImport = "HUYEN_Inport";
        public const string permissionExport = "HUYEN_export";
        public const string searchKey = "HUYENPageSearchModel";
        private readonly IHUYENService _HUYENService;
        private readonly ITINHService _TINHService;


        public HUYENController(IHUYENService HUYENService, ILog Ilog,
            ITINHService TINHService,
            IMapper mapper
            )
        {
            _HUYENService = HUYENService;
            _Ilog = Ilog;
            _mapper = mapper;
            _TINHService = TINHService;

        }
        // GET: HUYENArea/HUYEN
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index(string id)
        {
            
            SessionManager.SetValue(searchKey, null);
            ViewBag.Tinh = _TINHService.GetById(id.ToLongOrZero());
            if (ViewBag.Tinh == null)
            {
                return new HttpNotFoundResult();
            }
            var listData = _HUYENService.GetDaTaByPage(ViewBag.Tinh.MaTinh, null);
            return View(listData);
        }


        [HttpPost]
        public JsonResult getData(string tinhid, int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as HUYENSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new HUYENSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var tinh= _TINHService.GetById(tinhid.ToLongOrZero());
            var data = _HUYENService.GetDaTaByPage(tinh.MaTinh, searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create(string id)
        {
            var myModel = new CreateVM();
            myModel.TinhId = id;
            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Quận/Huyện thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<HUYEN>(model);
                    _HUYENService.Create(EntityModel);

                }
                else
                {
                    result.MessageFail(ModelState.GetErrors());
                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quận/Huyện", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(int id)
        {
            var myModel = new EditVM();

            var obj = _HUYENService.GetById(id);
            if (obj == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _HUYENService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj = _mapper.Map(model, obj);
                    _HUYENService.Update(obj);

                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quận/Huyện", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(string tinhid, HUYENSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as HUYENSearchDto;

            if (searchModel == null)
            {
                searchModel = new HUYENSearchDto();
                searchModel.pageSize = 20;
            }
            searchModel.MaHuyenFilter = form.MaHuyenFilter;
            searchModel.TenHuyenFilter = form.TenHuyenFilter;
            searchModel.LoaiFilter = form.LoaiFilter;

            SessionManager.SetValue((searchKey), searchModel);
            var tinh = _TINHService.GetById(tinhid.ToLongOrZero());
            var data = _HUYENService.GetDaTaByPage(tinh.MaTinh, searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            var result = new JsonResultBO(true, "Xóa Quận/Huyện thành công");
            try
            {
                var user = _HUYENService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _HUYENService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }


        public ActionResult Detail(int id)
        {
            var model = new DetailVM();
            model.objInfo = _HUYENService.GetById(id);
            return View(model);
        }


    }
}