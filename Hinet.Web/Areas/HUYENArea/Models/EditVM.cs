using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.HUYENArea.Models
{
    public class EditVM
    {
		public int Id { get; set; }
		public string MaHuyen { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public string TenHuyen { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public string Loai { get; set; }
		public string Location { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public string TinhId { get; set; }

        
    }
}