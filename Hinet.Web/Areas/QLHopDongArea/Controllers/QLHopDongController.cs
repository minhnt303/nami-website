using AutoMapper;
using CommonHelper.Upload;
using Nami.Model.Entities;
using Nami.Service.BuildingsInfoService;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.FileDinhKemService;
using Nami.Service.HistoryService;
using Nami.Service.QLHopDongItemService;
using Nami.Service.QLHopDongService;
using Nami.Service.QLHopDongService.Dto;
using Nami.Service.RoomInfoService;
using Nami.Web.Areas.QLHopDongArea.Models;
using Nami.Web.Filters;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Service.QLHopDongDangKyService;
namespace Nami.Web.Areas.QLHopDongArea.Controllers
{
    public class QLHopDongController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "QLHopDong_index";
        public const string permissionCreate = "QLHopDong_create";
        public const string permissionEdit = "QLHopDong_edit";
        public const string permissionDelete = "QLHopDong_delete";
        public const string permissionImport = "QLHopDong_Inport";
        public const string permissionExport = "QLHopDong_export";
        public const string searchKey = "QLHopDongPageSearchModel";
        private readonly IQLHopDongService _QLHopDongService;
        private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        private readonly IBuildingsInfoService _BuildingsInfoService;
        private readonly IRoomInfoService _RoomInfoService;
        private readonly IFileDinhKemService _FileDinhKemService;
        private readonly IHistoryService _HistoryService;
        private readonly IQLHopDongItemService _QLHopDongItemService;
        private readonly IQLHopDongDangKyService _QLHopDongDangKyService;


        public QLHopDongController(IQLHopDongService QLHopDongService, ILog Ilog,
            IBuildingsInfoService BuildingsInfoService,
            IRoomInfoService RoomInfoService,
            IFileDinhKemService FileDinhKemService,
            IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IHistoryService HistoryService,
            IQLHopDongItemService QLHopDongItemService,
            IQLHopDongDangKyService QLHopDongDangKyService,
            IMapper mapper
            )
        {
            _BuildingsInfoService = BuildingsInfoService;
            _QLHopDongService = QLHopDongService;
            _Ilog = Ilog;
            _mapper = mapper;
            _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _RoomInfoService = RoomInfoService;
            _FileDinhKemService = FileDinhKemService;
            _HistoryService = HistoryService;
            _QLHopDongItemService = QLHopDongItemService;
            _QLHopDongDangKyService = QLHopDongDangKyService;
        }
        // GET: QLHopDongArea/QLHopDong
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var PermissionWatchAll = HasPermission(PermissionCodeConst.Super_Admin);
            var listData = _QLHopDongService.GetDaTaByPageByCurrentUserId(PermissionWatchAll,CurrentUserId,null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var PermissionWatchAll = HasPermission(PermissionCodeConst.Super_Admin);
            var searchModel = SessionManager.GetValue(searchKey) as QLHopDongSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new QLHopDongSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _QLHopDongService.GetDaTaByPageByCurrentUserId(PermissionWatchAll,CurrentUserId,searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();
            ViewBag.dropdownlistAgreementType = ConstantExtension.GetDropdownData<AgreementTypeConstant>(null);
            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult Create(CreateVM model, List<HttpPostedFileBase> file)
        {
            ViewBag.dropdownlistAgreementType = ConstantExtension.GetDropdownData<AgreementTypeConstant>(null);
            var result = new JsonResultBO(true, "Tạo Quản lý hợp đồng thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.DateStart == null && model.DateEnd == null)
                    {
                        result.MessageFail("Vui chọn cả ngày bắt đầu và ngày kết thúc");
                        return Json(result);
                    }
                    if (model.DateStart > model.DateEnd)
                    {
                        result.MessageFail("Vui chọn cả ngày bắt đầu nhỏ hơn ngày kết thúc");
                        return Json(result);
                    }

                    if (file != null && file.Any())
                    {
                        foreach (var item in file)
                        {
                            if (item != null && item.ContentLength > 0)
                            {
                                string fileDinhKem = Path.GetExtension(item.FileName);
                                List<string> ExtensionSupport = new List<string>() { ".pdf" };
                                if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                                {
                                    result.MessageFail("Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonPDF);
                                    return Json(result);
                                }
                                var resultUpload = UploadProvider.SaveFile(item, null, UploadProvider.ListExtensionCommonPDF, UploadProvider.MaxSizeCommon, "Uploads/HopDong/", HostingEnvironment.MapPath("/"));
                            }
                        }
                    }

                    var EntityModel = _mapper.Map<QLHopDong>(model);
                    EntityModel.Month = model.NumberMonth;
                    EntityModel.Ngay = model.NumberDay;
                    _QLHopDongService.Create(EntityModel);

                    if (file != null && file.Any())
                    {
                        foreach (var item in file)
                        {
                            if (item != null && item.ContentLength > 0)
                            {
                                string fileDinhKem = Path.GetExtension(item.FileName);
                                List<string> ExtensionSupport = new List<string>() { ".doc", ".docx", ".pdf", ".xlsx", ".xls", ".ppt", ".png", ".jpg", ".jpeg", ".rar", ".rip" };
                                if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                                {
                                    result.MessageFail("Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonPDF);
                                    return Json(result);
                                }
                                var resultUpload = UploadProvider.SaveFile(item, null, UploadProvider.ListExtensionCommonPDF, UploadProvider.MaxSizeCommon, "Uploads/TaiLieuKeHoachTinBai/", HostingEnvironment.MapPath("/"));
                                if (resultUpload.status)
                                {
                                    var fileDinhKemNewModel = new FileDinhKem();
                                    fileDinhKemNewModel.ImageUrl = resultUpload.path;
                                    fileDinhKemNewModel.TypeItem = ItemTypeConstant.Agreement;
                                    fileDinhKemNewModel.IdItem = EntityModel.Id;
                                    _FileDinhKemService.Create(fileDinhKemNewModel);
                                }
                                else
                                {
                                    result.MessageFail(resultUpload.message);
                                    return Json(result);
                                }
                            }
                        }
                    }

                    if (model.NumberYear != null && model.NumberMonth != null && model.NumberDay != null && model.NumberYear == 0 && model.DateStart != null && model.DateEnd != null)
                    {
                        if (model.NumberMonth == 0)
                        {
                            var QLHopDongItemNew = new QLHopDongItem();
                            QLHopDongItemNew.EndDate = EntityModel.DateEnd;
                            QLHopDongItemNew.IdQLHopDong = EntityModel.Id;
                            QLHopDongItemNew.Note = EntityModel.Note;
                            if (model.NgayThuTienHangThang != null)
                            {
                                var NgayThuTien = new DateTime(model.DateStart.Value.Year, model.DateStart.Value.Month, model.NgayThuTienHangThang.Value);
                                QLHopDongItemNew.PayDate = NgayThuTien;
                            }
                            QLHopDongItemNew.StartDate = EntityModel.DateStart;
                            QLHopDongItemNew.Thang = 1;
                            QLHopDongItemNew.TienPhaiTraHangThang = EntityModel.PayPerMonth;
                            QLHopDongItemNew.DepositsMoney = EntityModel.DepositsMoney;
                            if (model.DepositsMoney != null && model.PayPerMonth != null)
                            {
                                QLHopDongItemNew.TienThangNay = model.PayPerMonth - model.DepositsMoney;
                            }
                            else if (model.DepositsMoney == null && model.PayPerMonth != null)
                            {
                                QLHopDongItemNew.TienThangNay = model.PayPerMonth;
                            }
                            _QLHopDongItemService.Create(QLHopDongItemNew);
                        }
                        else if (model.NumberMonth > 0 && model.NumberMonth <= 12)
                        {
                            if (model.NumberDay == 0 || model.NumberDay == null)
                            {
                                for (int i = 1; i <= model.NumberMonth; i++)
                                {
                                    var QLHopDongItemNew = new QLHopDongItem();
                                    if (i == 1)
                                    {
                                        QLHopDongItemNew.StartDate = EntityModel.DateStart;
                                        QLHopDongItemNew.EndDate = EntityModel.DateStart.Value.AddMonths(1);
                                    }
                                    else
                                    {
                                        QLHopDongItemNew.StartDate = EntityModel.DateStart.Value.AddMonths(i);
                                        QLHopDongItemNew.EndDate = QLHopDongItemNew.StartDate.Value.AddMonths(1);
                                    }
                                    QLHopDongItemNew.IdQLHopDong = EntityModel.Id;
                                    QLHopDongItemNew.Note = EntityModel.Note;
                                    if (model.NgayThuTienHangThang != null)
                                    {
                                        var NgayThuTien = new DateTime(QLHopDongItemNew.StartDate.Value.Year, QLHopDongItemNew.StartDate.Value.Month, model.NgayThuTienHangThang.Value);
                                        QLHopDongItemNew.PayDate = NgayThuTien;
                                    }
                                    QLHopDongItemNew.Thang = i;
                                    QLHopDongItemNew.TienPhaiTraHangThang = EntityModel.PayPerMonth;
                                    if (i == 1)
                                    {
                                        QLHopDongItemNew.DepositsMoney = EntityModel.DepositsMoney;
                                        if (model.DepositsMoney != null && model.PayPerMonth != null)
                                        {
                                            QLHopDongItemNew.TienThangNay = model.PayPerMonth - model.DepositsMoney;
                                        }
                                        else if (model.DepositsMoney == null && model.PayPerMonth != null)
                                        {
                                            QLHopDongItemNew.TienThangNay = model.PayPerMonth;
                                        }
                                    }
                                    else
                                    {
                                        QLHopDongItemNew.TienThangNay = model.PayPerMonth;
                                    }
                                    _QLHopDongItemService.Create(QLHopDongItemNew);
                                }
                            }
                            else
                            {
                                var QLHopDongItemNew = new QLHopDongItem();
                                QLHopDongItemNew.StartDate = model.DateStart.Value.AddMonths(model.NumberMonth.Value + 1);
                                QLHopDongItemNew.EndDate = model.DateEnd;
                                QLHopDongItemNew.IdQLHopDong = EntityModel.Id;
                                QLHopDongItemNew.Note = EntityModel.Note;
                                if (model.NgayThuTienHangThang != null)
                                {
                                    var NgayThuTien = new DateTime(QLHopDongItemNew.StartDate.Value.Year, QLHopDongItemNew.StartDate.Value.Month, model.NgayThuTienHangThang.Value);
                                    QLHopDongItemNew.PayDate = NgayThuTien;
                                }
                                QLHopDongItemNew.Thang = model.NumberMonth + 1;
                                QLHopDongItemNew.TienPhaiTraHangThang = EntityModel.PayPerMonth;
                                QLHopDongItemNew.TienThangNay = model.PayPerMonth;
                                _QLHopDongItemService.Create(QLHopDongItemNew);
                            }
                        }
                    }
                    else if (model.NumberYear != null && model.NumberMonth != null && model.NumberDay != null && model.NumberYear > 0 && model.DateStart != null && model.DateEnd != null)
                    {
                        if (model.NumberYear > 0)
                        {
                            for (int a = 1; a <= model.NumberYear; a++)
                            {
                                for (int i = 1; i <= 12; i++)
                                {
                                    var QLHopDongItemNew = new QLHopDongItem();
                                    if (i == 1)
                                    {
                                        QLHopDongItemNew.StartDate = EntityModel.DateStart;
                                        QLHopDongItemNew.EndDate = EntityModel.DateStart.Value.AddMonths(1);
                                    }
                                    else
                                    {
                                        QLHopDongItemNew.StartDate = EntityModel.DateStart.Value.AddMonths(i);
                                        QLHopDongItemNew.EndDate = QLHopDongItemNew.StartDate.Value.AddMonths(1);
                                    }
                                    QLHopDongItemNew.IdQLHopDong = EntityModel.Id;
                                    QLHopDongItemNew.Note = EntityModel.Note;
                                    if (model.NgayThuTienHangThang != null)
                                    {
                                        var NgayThuTien = new DateTime(QLHopDongItemNew.StartDate.Value.Year, QLHopDongItemNew.StartDate.Value.Month, model.NgayThuTienHangThang.Value);
                                        QLHopDongItemNew.PayDate = NgayThuTien;
                                    }
                                    QLHopDongItemNew.Thang = i;
                                    QLHopDongItemNew.TienPhaiTraHangThang = EntityModel.PayPerMonth;
                                    if (i == 1)
                                    {
                                        QLHopDongItemNew.DepositsMoney = EntityModel.DepositsMoney;
                                        if (model.DepositsMoney != null && model.PayPerMonth != null)
                                        {
                                            QLHopDongItemNew.TienThangNay = model.PayPerMonth - model.DepositsMoney;
                                        }
                                        else if (model.DepositsMoney == null && model.PayPerMonth != null)
                                        {
                                            QLHopDongItemNew.TienThangNay = model.PayPerMonth;
                                        }
                                    }
                                    else
                                    {
                                        QLHopDongItemNew.TienThangNay = model.PayPerMonth;
                                    }
                                    _QLHopDongItemService.Create(QLHopDongItemNew);
                                }
                            }
                        }
                        if (model.NumberMonth == 0)
                        {
                            var QLHopDongItemNew = new QLHopDongItem();
                            QLHopDongItemNew.StartDate = EntityModel.DateStart.Value.AddYears(model.NumberYear.Value + 1);
                            QLHopDongItemNew.EndDate = model.DateEnd;
                            QLHopDongItemNew.IdQLHopDong = EntityModel.Id;
                            QLHopDongItemNew.Note = EntityModel.Note;
                            if (model.NgayThuTienHangThang != null)
                            {
                                var NgayThuTien = new DateTime(model.DateStart.Value.Year, model.DateStart.Value.Month, model.NgayThuTienHangThang.Value);
                                QLHopDongItemNew.PayDate = NgayThuTien;
                            }
                            QLHopDongItemNew.Thang = 1;
                            QLHopDongItemNew.TienPhaiTraHangThang = EntityModel.PayPerMonth;
                            QLHopDongItemNew.DepositsMoney = EntityModel.DepositsMoney;
                            if (model.DepositsMoney != null && model.PayPerMonth != null)
                            {
                                QLHopDongItemNew.TienThangNay = model.PayPerMonth - model.DepositsMoney;
                            }
                            else if (model.DepositsMoney == null && model.PayPerMonth != null)
                            {
                                QLHopDongItemNew.TienThangNay = model.PayPerMonth;
                            }
                            _QLHopDongItemService.Create(QLHopDongItemNew);
                        }
                        else if (model.NumberMonth > 0 && model.NumberMonth <= 12)
                        {
                            if (model.NumberDay == 0 || model.NumberDay == null)
                            {
                                for (int i = 1; i <= model.NumberMonth; i++)
                                {
                                    var QLHopDongItemNew = new QLHopDongItem();
                                    if (i == 1)
                                    {
                                        QLHopDongItemNew.StartDate = EntityModel.DateStart.Value.AddYears(model.NumberYear.Value + 1);
                                        QLHopDongItemNew.EndDate = QLHopDongItemNew.StartDate.Value.AddMonths(1);
                                    }
                                    else
                                    {
                                        QLHopDongItemNew.StartDate = EntityModel.DateStart.Value.AddYears(model.NumberYear.Value + 1).AddMonths(i);
                                        QLHopDongItemNew.EndDate = QLHopDongItemNew.StartDate.Value.AddMonths(1);
                                    }
                                    QLHopDongItemNew.IdQLHopDong = EntityModel.Id;
                                    QLHopDongItemNew.Note = EntityModel.Note;
                                    if (model.NgayThuTienHangThang != null)
                                    {
                                        var NgayThuTien = new DateTime(QLHopDongItemNew.StartDate.Value.Year, QLHopDongItemNew.StartDate.Value.Month, model.NgayThuTienHangThang.Value);
                                        QLHopDongItemNew.PayDate = NgayThuTien;
                                    }
                                    QLHopDongItemNew.Thang = i;
                                    QLHopDongItemNew.TienPhaiTraHangThang = EntityModel.PayPerMonth;
                                    if (i == 1)
                                    {
                                        QLHopDongItemNew.DepositsMoney = EntityModel.DepositsMoney;
                                        if (model.DepositsMoney != null && model.PayPerMonth != null)
                                        {
                                            QLHopDongItemNew.TienThangNay = model.PayPerMonth - model.DepositsMoney;
                                        }
                                        else if (model.DepositsMoney == null && model.PayPerMonth != null)
                                        {
                                            QLHopDongItemNew.TienThangNay = model.PayPerMonth;
                                        }
                                    }
                                    else
                                    {
                                        QLHopDongItemNew.TienThangNay = model.PayPerMonth;
                                    }
                                    _QLHopDongItemService.Create(QLHopDongItemNew);
                                }
                            }
                            else
                            {
                                var QLHopDongItemNew = new QLHopDongItem();
                                QLHopDongItemNew.StartDate = model.DateStart.Value.AddYears(model.NumberYear.Value + 1).AddMonths(model.NumberMonth.Value + 1);
                                QLHopDongItemNew.EndDate = model.DateEnd;
                                QLHopDongItemNew.IdQLHopDong = EntityModel.Id;
                                QLHopDongItemNew.Note = EntityModel.Note;
                                if (model.NgayThuTienHangThang != null)
                                {
                                    var NgayThuTien = new DateTime(QLHopDongItemNew.StartDate.Value.Year, QLHopDongItemNew.StartDate.Value.Month, model.NgayThuTienHangThang.Value);
                                    QLHopDongItemNew.PayDate = NgayThuTien;
                                }
                                QLHopDongItemNew.Thang = model.NumberMonth + 1;
                                QLHopDongItemNew.TienPhaiTraHangThang = EntityModel.PayPerMonth;
                                QLHopDongItemNew.TienThangNay = model.PayPerMonth;
                                _QLHopDongItemService.Create(QLHopDongItemNew);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý hợp đồng", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj = _QLHopDongService.GetById(id);
            if (obj == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _QLHopDongService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj = _mapper.Map(model, obj);
                    _QLHopDongService.Update(obj);

                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý hợp đồng", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(QLHopDongSearchDto form)
        {
            var PermissionWatchAll = HasPermission(PermissionCodeConst.Super_Admin);
            var searchModel = SessionManager.GetValue(searchKey) as QLHopDongSearchDto;

            if (searchModel == null)
            {
                searchModel = new QLHopDongSearchDto();
                searchModel.pageSize = 20;
            }
            searchModel.ItemIdFilter = form.ItemIdFilter;
            searchModel.ItemTypeFilter = form.ItemTypeFilter;
            searchModel.AgreementCodeFilter = form.AgreementCodeFilter;
            searchModel.RepresenterNameFilter = form.RepresenterNameFilter;
            searchModel.DepositsMoneyFilter = form.DepositsMoneyFilter;
            searchModel.MonthFilter = form.MonthFilter;
            searchModel.NgayFilter = form.NgayFilter;
            searchModel.DateStartFilter = form.DateStartFilter;
            searchModel.DateEndFilter = form.DateEndFilter;
            searchModel.PayPerMonthFilter = form.PayPerMonthFilter;
            searchModel.NoteFilter = form.NoteFilter;

            SessionManager.SetValue((searchKey), searchModel);

            var data = _QLHopDongService.GetDaTaByPageByCurrentUserId(PermissionWatchAll,CurrentUserId,searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý hợp đồng thành công");
            try
            {
                var user = _QLHopDongService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _QLHopDongService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }


        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _QLHopDongService.GetDtoById(id);
            model.listItem = _QLHopDongItemService.GetListQLHopDongItemByIdHopDong(id);
            model.historys = _HistoryService.GetDaTaByIdHistory(id, ItemTypeConstant.Agreement);
            return View(model);
        }



        [HttpPost]
        public JsonResult GetDropDownItemType(string ItemType)
        {
            var listItemType = new List<SelectListItem>();
            if (!string.IsNullOrEmpty(ItemType))
            {
                if (ItemType == ItemTypeConstant.Building)
                {
                    listItemType = _BuildingsInfoService.GetDropDownListBuldingForAggrement(CurrentUserId, null);
                    if (listItemType != null && listItemType.Any())
                    {
                        return Json(listItemType);
                    }
                    else
                    {
                        return Json(new List<SelectListItem>());
                    }
                }
                else if (ItemType == ItemTypeConstant.Room)
                {
                    listItemType = _RoomInfoService.GetDropDownListRoomForAggreement(CurrentUserId, null);
                    if (listItemType != null && listItemType.Any())
                    {
                        return Json(listItemType);
                    }
                    else
                    {
                        return Json(new List<SelectListItem>());
                    }
                }
                else
                {
                    return Json(new List<SelectListItem>());
                }
            }
            else
            {
                return Json(new List<SelectListItem>());
            }
        }


        [HttpGet]
        public JsonResult GetPriceDetail(long? ItemId, string ItemType)
        {
            var price = 0;
            //return Json(price, JsonRequestBehavior.AllowGet);
            if (!string.IsNullOrEmpty(ItemType))
            {
                if (ItemType == ItemTypeConstant.Building)
                {
                    var BuilingInfo = _BuildingsInfoService.GetById(ItemId);
                    if (BuilingInfo != null && BuilingInfo.Price != null)
                    {
                        return Json(BuilingInfo.Price, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(price, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (ItemType == ItemTypeConstant.Room)
                {
                    var RoomInfo = _RoomInfoService.GetById(ItemId);
                    if (RoomInfo != null && RoomInfo.Price != null)
                    {
                        return Json(RoomInfo.Price, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(price, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(price, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(price, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetDropDownDKLamHopDong(long id)
        {
            var result = _QLHopDongDangKyService.GetDropDownDangKyHopDongById(id,ItemTypeConstant.Building, null);
            return Json(result);
        }
    }
}