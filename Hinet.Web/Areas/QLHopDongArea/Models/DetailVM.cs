using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.QLHopDongService.Dto;
using Nami.Service.QLHopDongItemService.Dto;
using Nami.Service.HistoryService.Dto;

namespace Nami.Web.Areas.QLHopDongArea.Models
{
    public class DetailVM
    {
        public List<HistoryDto> historys { get; set; }

        public List<QLHopDongItemDto> listItem { get; set; }

        public QLHopDongDto objInfo { get; set; }
    }
}