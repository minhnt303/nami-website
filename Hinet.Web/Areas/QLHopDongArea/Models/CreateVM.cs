﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.QLHopDongArea.Models
{
    public class CreateVM
    {
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public long? ItemId { get; set; }
		public string ItemType { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string AgreementCode { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string RepresenterName { get; set; }
		public int? DepositsMoney { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public DateTime? DateStart { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public DateTime? DateEnd { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public int? NgayThuTienHangThang { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public int? PayPerMonth { get; set; }
		public string Note { get; set; }
        public string file { get; set; }
        public int? NumberYear { get; set; }
        public int? NumberMonth { get; set; }
        public int? NumberDay { get; set; }
        public int? SoNgayTinhLaMotThang { get; set; }
        public long? UserId { get; set; }
    }
}