using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.QLHopDongArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		public long? ItemId { get; set; }
		public string ItemType { get; set; }
		public string AgreementCode { get; set; }
		public string RepresenterName { get; set; }
		public int? DepositsMoney { get; set; }
		public int? Month { get; set; }
		public int? Ngay { get; set; }
		public DateTime? DateStart { get; set; }
		public DateTime? DateEnd { get; set; }
		public int? PayPerMonth { get; set; }
		public string Note { get; set; }

        
    }
}