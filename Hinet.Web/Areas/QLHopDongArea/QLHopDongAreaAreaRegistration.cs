using System.Web.Mvc;

namespace Nami.Web.Areas.QLHopDongArea
{
    public class QLHopDongAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "QLHopDongArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "QLHopDongArea_default",
                "QLHopDongArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}