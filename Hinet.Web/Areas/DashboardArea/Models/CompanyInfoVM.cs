﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.DashboardArea.Models
{
    public class CompanyInfoVM
    {
        [DisplayName("Tài khoản mới đăng ký")]
        public int MoiTao { get; set; }
        [DisplayName("Tài khoản cần bổ sung thông tin")]
        public int BoSung { get; set; }
        [DisplayName("Tài khoản đã duyệt")]
        public int DaDuyet { get; set; }
        [DisplayName("Tài khoản bị từ chối")]
        public int TuChoi { get; set; }
        [DisplayName("Tài khoản đề nghị chỉnh sửa")]
        public int ChinhSua { get; set; }
        [DisplayName("Tài khoản bị khóa")]
        public int BijKhoa { get; set; }
        [DisplayName("Tổng số tài khoản")]
        public int TongSo { get; set; }
    }
}