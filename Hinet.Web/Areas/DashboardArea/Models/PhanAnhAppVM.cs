﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.DashboardArea.Models
{
    public class PhanAnhAppVM
    {
        [DisplayName("Mới đăng")]
        public int MoiDang { get; set; }
        [DisplayName("Đang xử lý")]
        public int DangXuLy { get; set; }
        [DisplayName("Đã xử lý")]
        public int DaXuLy { get; set; }
        [DisplayName("Bị từ chối")]
        public int BiTuChoi { get; set; }
        [DisplayName("Tổng số")]
        public int TongSo { get; set; }
    }
}