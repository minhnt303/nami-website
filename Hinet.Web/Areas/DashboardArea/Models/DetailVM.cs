﻿using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.NotificationService.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.DashboardArea.Models
{
    public class DetailVM
    {        
        public AppInfoVM AppVMInfo { get; set; }
        public AppInfoVM AppVMRegister { get; set; }
        public PersonalInfoVM PersonalVM { get; set; }
        public CompanyInfoVM CompanyVM { get; set; }
        public WebInfoVM WebVMInfo { get; set; }
        public WebInfoVM WebVMRegister { get; set; }
        public PhanAnhAppVM AppReport { get; set; }
        public PhanAnhWebVM WebReport { get; set; }
        public PageListResultBO<NotificationDto> ObjNoti { get; set; }

    }
}