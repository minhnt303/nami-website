﻿using AutoMapper;
using Hangfire;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.NotificationService;
using Nami.Web.Areas.DashboardArea.Models;
using Nami.Web.Core;
using Nami.Web.Filters;
using log4net;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AuthorizeAttribute = System.Web.Mvc.AuthorizeAttribute;

namespace Nami.Web.Areas.DashboardArea.Controllers
{
    public class DashboardController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly ILog _log;
        private readonly INotificationService _notificationService;

        //IConnectionMultiplexer _connectionMultiplexer;
        //IDatabase _cacheDatabase;
        public DashboardController(
            IMapper mapper,
            ILog log,
            INotificationService notificationService
            )
        {
            _log = log;
            _mapper = mapper;
            _notificationService = notificationService;
            //_connectionMultiplexer = connectionMultiplexer;
            //_cacheDatabase = cacheDatabase;
        }
        // GET: DashboardArea/Dashboard
        [Authorize]
        public ActionResult Index()
        {
            //var noti = new Notification();
            //noti.FromUser = null;
            //noti.ToUser = null;
            //noti.Message =  "Hoàn vũ vừa tạo hồ sơ cá nhân";
            //noti.Type = NotificationTypeConstant.Global;
            //noti.IsRead = false;


            //Task.Run(() => NotificationProvider.SendMessage(noti));

            DetailVM detail = new DetailVM();
            detail.WebVMInfo = new WebInfoVM();
            detail.AppVMInfo = new AppInfoVM();
            detail.WebVMRegister = new WebInfoVM();
            detail.AppVMRegister = new AppInfoVM();
            detail.CompanyVM = new CompanyInfoVM();
            detail.PersonalVM = new PersonalInfoVM();
            detail.WebReport = new PhanAnhWebVM();
            detail.AppReport = new PhanAnhAppVM();
            detail.ObjNoti = new Service.Common.PageListResultBO<Service.NotificationService.Dto.NotificationDto>();



            //Bảng thông báo
            detail.ObjNoti = _notificationService.GetAllByUserId(CurrentUserId, 10);

            return View(detail);
        }

        public ActionResult TestNoti(long Id, string mess)
        {
            var noti = new Notification();
            noti.FromUser = null;
            noti.ToUser = Id;
            noti.Message = mess;
            noti.Type = NotificationTypeConstant.System;
            noti.IsRead = false;


            Task.Run(() => NotificationProvider.SendMessage(noti));
            return View();
        }
        [AllowAnonymous]
        public ActionResult UpdateLucene()
        {
            //var listPersonal = _personalInfo.GetAll();

            //foreach (var personInfo in listPersonal)
            //{
            //    LuceneSearch.AddUpdateLuceneIndex(new SearchLuceneDto
            //    {
            //        Id = ModuleTypeConstant.HoSoTaiKhoanCaNhan + "_" + personInfo.Id,
            //        Title = personInfo.Name != null ? personInfo.Name : "Unknow",
            //        Description = string.Join(", ", new string[] { personInfo.Name, personInfo.Phone, personInfo.NumberId, personInfo.Address, personInfo.Email }),
            //        Type = ModuleTypeConstant.HoSoTaiKhoanCaNhan,
            //        IdItem = personInfo.Id,
            //        Link = "/personalInfoarea/personalInfo/detail/" + personInfo.Id
            //    });
            //}

            //var listCompany = _companyInfo.GetAll();
            //foreach (var companyInfo in listCompany)
            //{
            //    LuceneSearch.AddUpdateLuceneIndex(new SearchLuceneDto
            //    {
            //        Id = ModuleTypeConstant.HoSoTaiKhoanThuongNhan + "_" + companyInfo.Id,
            //        Title = companyInfo.Name != null ? companyInfo.Name : "Unknow",
            //        Description = string.Join(", ", new string[] { companyInfo.Name, companyInfo.Phone, companyInfo.Certificate, companyInfo.Address, companyInfo.Email }),
            //        Type = ModuleTypeConstant.HoSoTaiKhoanCaNhan,
            //        IdItem = companyInfo.Id,
            //        Link = "/companyInfoarea/companyInfo/detail/" + companyInfo.Id
            //    });
            //}
            //var listWebsite = _websiteInfoService.GetAll();
            //foreach (var websiteInfo in listWebsite)
            //{
            //    var typestr = string.Empty;
            //    var link = "/WebsiteInfoArea/WebsiteSaleNoti/Detail/";
            //    if (websiteInfo.WebsiteManageTypeId==WebsiteManageTypeConstant.DKWebCCDV)
            //    {
            //        typestr = ModuleTypeConstant.HoSoWebsiteCCDV;
            //        link = "/WebsiteInfoArea/WebsiteInfo/Detail/";
            //    }
            //    else
            //    {
            //        typestr = ModuleTypeConstant.HoSoWebsiteBanHang;

            //    }
            //    LuceneSearch.AddUpdateLuceneIndex(new SearchLuceneDto
            //    {
            //        Id = typestr + "_" + websiteInfo.Id,
            //        Title = websiteInfo.Name != null ? websiteInfo.Name : "Unknow",
            //        Description = string.Join(", ", new string[] { websiteInfo.CompanyName, websiteInfo.Domain, websiteInfo.DomainAdd, websiteInfo.RepresenterName, websiteInfo.RepresenterEmail }),
            //        Type = typestr,
            //        IdItem = websiteInfo.Id,
            //        Link = link + websiteInfo.Id
            //    });
            //}

            //var listapp = _appInfoService.GetAll();
            //foreach (var appInfo in listapp)
            //{
            //    var typestr = string.Empty;
            //    var link = "/AppInfoArea/AppInfo/Detail/";
            //    if (appInfo.AppManageTypeId == AppManageTypeConstant.DKWebCCDV)
            //    {
            //        typestr = ModuleTypeConstant.HoSoAppCCDV;
            //        link = "/AppInfoArea/AppSaleNoti/Detail/";
            //    }
            //    else
            //    {
            //        typestr = ModuleTypeConstant.HoSoWebsiteBanHang;

            //    }
            //    LuceneSearch.AddUpdateLuceneIndex(new SearchLuceneDto
            //    {
            //        Id = typestr + "_" + appInfo.Id,
            //        Title = appInfo.Name != null ? appInfo.Name : "Unknow",
            //        Description = string.Join(", ", new string[] { appInfo.CompanyName, appInfo.Domain, appInfo.DomainAdd, appInfo.RepresenterName, appInfo.RepresenterEmail }),
            //        Type = typestr,
            //        IdItem = appInfo.Id,
            //        Link = link + appInfo.Id
            //    });
            //}

            return View();
        }

        public PartialViewResult SearchCommon(string key)
        {
            var listdata = LuceneSearch.Search(key).Take(10).ToList();
            return PartialView(listdata);
        }

        public string FirstHangfire(string id)
        {
            try
            {
                BackgroundJob.Enqueue<HangFireCommon.SayHelloAll>(x => x.hello(id));
                BackgroundJob.Schedule<HangFireCommon.SayHelloAll>(x => x.hello(id), new System.TimeSpan(0, 1, 0));
                return "OK";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost]
        public JsonResult TestNoti()
        {
            var result = new JsonResultBO(true);
            var notification = new Notification();
            notification.Message = "Test Noti";
            notification.IsRead = false;
            notification.Type = NotificationTypeConstant.System;
            notification.ToUser = CurrentUserId;
            notification.FromUser = CurrentUserId;
            notification.Link = "/DashboardArea/Dashboard";
            _notificationService.Create(notification);
            Task.Run(() => NotificationProvider.SendMessage(notification));
            return Json(result);
        }
    }
}