﻿using Nami.Service.BuildingsInfoService.Dto;
using Nami.Service.RoomInfoService.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.QuanLyThongTinChungHeThongArea.Models
{
    public class DataBuildingRoom
    {
        public List<BuildingsInfoDto> listBuilding { get; set; }
        public List<RoomInfoDto> listRoom { get; set; }
    }
}