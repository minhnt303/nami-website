﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.BuildingsInfoService.Dto;
using Nami.Service.RoomInfoService.Dto;

namespace Nami.Web.Areas.QuanLyThongTinChungHeThongArea.Models
{
    public class ThongTinVM
    {
        public BuildingsInfoDto BuildingInfo { get; set; }
        public bool IsBuilding { get; set; }
        public RoomInfoDto RoomInfo { get; set; }
        public bool IsRoom { get; set; }
        public decimal TongThuNhapThangHienTai { get; set; }
    }
}