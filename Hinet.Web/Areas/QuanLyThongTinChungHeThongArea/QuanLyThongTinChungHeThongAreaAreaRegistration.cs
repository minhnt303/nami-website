﻿using System.Web.Mvc;

namespace Nami.Web.Areas.QuanLyThongTinChungHeThongArea
{
    public class QuanLyThongTinChungHeThongAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "QuanLyThongTinChungHeThongArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "QuanLyThongTinChungHeThongArea_default",
                "QuanLyThongTinChungHeThongArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}