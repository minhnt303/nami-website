﻿using Nami.Service.BuildingsInfoService;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.RoomInfoService;
using Nami.Web.Areas.QuanLyThongTinChungHeThongArea.Models;
using Nami.Web.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Nami.Web.Areas.QuanLyThongTinChungHeThongArea.Controllers
{
    public class QuanLyThongTinChungHeThongController : BaseController
    {
        private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        private readonly IBuildingsInfoService _buildingsInfoService;
        private readonly IRoomInfoService _roomInfoService;

        public QuanLyThongTinChungHeThongController(
        IDM_DulieuDanhmucService dM_DulieuDanhmucService,
        IBuildingsInfoService buildingsInfoService,
        IRoomInfoService roomInfoService
            )
        {
            _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _buildingsInfoService = buildingsInfoService;
            _roomInfoService = roomInfoService;
        }
        public ActionResult Index()
        {
            var model = new ThongTinVM();
            ViewBag.dropdownListBuilding = ConstantExtension.GetDropdownData<BuildingTypeConstant>(null);
            model.TongThuNhapThangHienTai = _buildingsInfoService.TongThuNhapThangHienTai(CurrentUserId);
            return View(model);
        }


        public JsonResult GetDropListHouseByType(string houseType)
        {
            if (houseType == BuildingTypeConstant.Building)
            {
                var listBuilding = _buildingsInfoService.GetDropDownListBulding(CurrentUserInfo.UserName);
                if (listBuilding != null && listBuilding.Any())
                {
                    return Json(listBuilding);
                }
                else
                {
                    return Json(new List<SelectListItem>());
                }
            }
            else if (houseType == BuildingTypeConstant.Room)
            {
                var listRoom = _roomInfoService.GetDropDownListRoom(CurrentUserInfo.UserName);
                if (listRoom != null && listRoom.Any())
                {
                    return Json(listRoom);
                }
                else
                {
                    return Json(new List<SelectListItem>());
                }
            }
            else
            {
                return Json(new List<SelectListItem>());
            }
        }

        [HttpPost]
        public JsonResult GetDataSearch(string HouseType, long? IdHouse)
        {
            if (!string.IsNullOrEmpty(HouseType))
            {
                if (IdHouse != null)
                {
                    if (HouseType == BuildingTypeConstant.Building)
                    {
                        var houseInfo = new ThongTinVM();
                        houseInfo.BuildingInfo = _buildingsInfoService.GetDtoById(IdHouse);
                        houseInfo.IsBuilding = true;
                        return Json(houseInfo);
                    }
                    else if (HouseType == BuildingTypeConstant.Room)
                    {
                        var houseInfo = new ThongTinVM();
                        houseInfo.RoomInfo = _roomInfoService.GetDtoById(IdHouse.HasValue ? IdHouse.Value : 0);
                        houseInfo.IsRoom = true;
                        return Json(houseInfo);
                    }
                    else
                    {
                        return Json(null);
                    }
                }
                else
                {
                    if (HouseType == BuildingTypeConstant.Building)
                    {
                        var listBuilding = _buildingsInfoService.GetListBuildingByCreatedId(CurrentUserId);
                        return Json(listBuilding);
                    }
                    else if (HouseType == BuildingTypeConstant.Room)
                    {
                        var listRoom = _roomInfoService.GetListRoomByCreatedID(CurrentUserId);
                        return Json(listRoom);
                    }
                    else
                    {
                        return Json(null);
                    }
                }
            }
            else
            {
                var listJson = new DataBuildingRoom();
                listJson.listBuilding = _buildingsInfoService.GetListBuildingByCreatedId(CurrentUserId);
                listJson.listRoom = _roomInfoService.GetListRoomByCreatedID(CurrentUserId);
                return Json(listJson);
            }
        }
    }
}