using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.QLTinTucArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.QLTinTucService;
using Nami.Service.QLTinTucService.Dto;
using CommonHelper.Excel;
using CommonHelper.ObjectExtention;
using Nami.Web.Common;
using System.IO;
using System.Web.Configuration;
using CommonHelper;
using Nami.Service.DM_DulieuDanhmucService;



namespace Nami.Web.Areas.QLTinTucArea.Controllers
{
    public class QLTinTucController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "QLTinTuc_index";
        public const string permissionCreate = "QLTinTuc_create";
        public const string permissionEdit = "QLTinTuc_edit";
        public const string permissionDelete = "QLTinTuc_delete";
        public const string permissionImport = "QLTinTuc_Inport";
        public const string permissionExport = "QLTinTuc_export";
        public const string searchKey = "QLTinTucPageSearchModel";
        private readonly IQLTinTucService _QLTinTucService;
	private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;


        public QLTinTucController(IQLTinTucService QLTinTucService, ILog Ilog,

		IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IMapper mapper
            )
        {
            _QLTinTucService = QLTinTucService;
            _Ilog = Ilog;
            _mapper = mapper;
		_dM_DulieuDanhmucService = dM_DulieuDanhmucService;

        }
        // GET: QLTinTucArea/QLTinTuc
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {

            var listData = _QLTinTucService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLTinTucSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new QLTinTucSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _QLTinTucService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();
            ViewBag.dropdownlistChuDe = _dM_DulieuDanhmucService.GetDropdownlistId(DanhMucConstant.CHUDETINTUC, null);
            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult Create(CreateVM model, HttpPostedFileBase file)
        {
            ViewBag.dropdownlistChuDe = _dM_DulieuDanhmucService.GetDropdownlistId(DanhMucConstant.CHUDETINTUC, null);
            var result = new JsonResultBO(true, "Tạo Quản lý tin tức thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<QLTinTuc>(model);
                    if (file != null && file.ContentLength > 0)
                    {
                        string fileDinhKem = Path.GetExtension(file.FileName);
                        List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg", ".gif" };
                        if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                        {
                            result.MessageFail("Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                            return Json(result);
                        }
                        var resultUpload = UploadProvider.SaveFile(file, null, UploadProvider.ListExtensionCommonImage, UploadProvider.MaxSizeCommon, "Uploads/LinkAnhTinTuc/", HostingEnvironment.MapPath("/"));
                        if (resultUpload.status)
                        {
                            EntityModel.ImageLink = resultUpload.path;
                        }
                        else
                        {
                            result.MessageFail(resultUpload.message);
                            return Json(result);
                        }
                    }
                    EntityModel.SlugTitle = EntityModel.Name.SlugTitleName();
                    _QLTinTucService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý tin tức", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _QLTinTucService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }
            ViewBag.dropdownlistChuDe = _dM_DulieuDanhmucService.GetDropdownlistId(DanhMucConstant.CHUDETINTUC, obj.ChuDeId.ToString());
            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult Edit(EditVM model, HttpPostedFileBase file)
        {
            var obj = _QLTinTucService.GetById(model.Id);
            if (obj == null)
            {
                throw new Exception("Không tìm thấy thông tin");
            }

            ViewBag.dropdownlistChuDe = _dM_DulieuDanhmucService.GetDropdownlistId(DanhMucConstant.CHUDETINTUC, obj.ChuDeId.ToString());
            var result = new JsonResultBO(true);
            try
            {
                #region Kiểm tra valid tài liệu đính kèm
                if (file != null && file.ContentLength > 0)
                {
                    string fileDinhKem = Path.GetExtension(file.FileName);
                    List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg", ".gif" };
                    if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                    {
                        result.MessageFail("Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                        return Json(result);
                    }
                }
                #endregion
                if (ModelState.IsValid)
                {
                    obj= _mapper.Map(model, obj);

                    if (file != null && file.ContentLength > 0)
                    {
                        string fileDinhKem = Path.GetExtension(file.FileName);
                        List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg" };
                        if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                        {
                            result.MessageFail("Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                            return Json(result);
                        }
                        var resultUpload = UploadProvider.SaveFile(file, null, UploadProvider.ListExtensionCommon, UploadProvider.MaxSizeCommon, "Uploads/LinkAnhTinTuc/", HostingEnvironment.MapPath("/"));
                        if (resultUpload.status)
                        {
                            obj.ImageLink = resultUpload.path;
                        }
                        else
                        {
                            result.MessageFail(resultUpload.message);
                            return Json(result);
                        }
                    }
                    if (string.IsNullOrEmpty(obj.SlugTitle))
                    {
                        obj.SlugTitle = model.Name.SlugTitleName();
                    }
                    _QLTinTucService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý tin tức", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(QLTinTucSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLTinTucSearchDto;

            if (searchModel == null)
            {
                searchModel = new QLTinTucSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.ImageLinkFilter = form.ImageLinkFilter;
			searchModel.NameFilter = form.NameFilter;
			searchModel.NamePhuFilter = form.NamePhuFilter;
			searchModel.DescriptionFilter = form.DescriptionFilter;
			searchModel.StatusFilter = form.StatusFilter;
			searchModel.AuthorFilter = form.AuthorFilter;
			searchModel.ChuDeIdFilter = form.ChuDeIdFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _QLTinTucService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý tin tức thành công");
            try
            {
                var user = _QLTinTucService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _QLTinTucService.SoftDelete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _QLTinTucService.GetDtoById(id);
            return View(model);
        }
	//[PermissionAccess(Code = permissionImport)]
        public FileResult ExportExcel()
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLTinTucSearchDto;
            var data = _QLTinTucService.GetDaTaByPage(searchModel).ListItem;
		var dataExport = _mapper.Map<List<QLTinTucExportDto>>(data);
            var fileExcel = ExportExcelV2Helper.Export<QLTinTucExportDto>(dataExport);
            return File(fileExcel, "application/octet-stream", "QLTinTuc.xlsx");
        }

        
    }
}