using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.QLTinTucService.Dto;

namespace Nami.Web.Areas.QLTinTucArea.Models
{
    public class DetailVM
    {
       public QLTinTucDto objInfo { get; set; }
    }
}