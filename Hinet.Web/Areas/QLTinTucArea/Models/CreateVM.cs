using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.QLTinTucArea.Models
{
    public class CreateVM
    {
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string Name { get; set; }
		public string NamePhu { get; set; }
        public string Description { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public bool Status { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string Author { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public long ChuDeId { get; set; }
		public bool IsHot { get; set; }
        public string file { get; set; }
    }
}