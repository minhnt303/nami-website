using System.Web.Mvc;

namespace Nami.Web.Areas.QLTinTucArea
{
    public class QLTinTucAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "QLTinTucArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "QLTinTucArea_default",
                "QLTinTucArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}