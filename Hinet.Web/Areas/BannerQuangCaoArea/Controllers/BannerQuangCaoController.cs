using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.BannerQuangCaoArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.BannerQuangCaoService;
using Nami.Service.BannerQuangCaoService.Dto;
using CommonHelper.Excel;
using CommonHelper.ObjectExtention;
using Nami.Web.Common;
using System.IO;
using System.Web.Configuration;
using CommonHelper;
using Nami.Service.DM_DulieuDanhmucService;



namespace Nami.Web.Areas.BannerQuangCaoArea.Controllers
{
    public class BannerQuangCaoController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "BannerQuangCao_index";
        public const string permissionCreate = "BannerQuangCao_create";
        public const string permissionEdit = "BannerQuangCao_edit";
        public const string permissionDelete = "BannerQuangCao_delete";
        public const string permissionImport = "BannerQuangCao_Inport";
        public const string permissionExport = "BannerQuangCao_export";
        public const string searchKey = "BannerQuangCaoPageSearchModel";
        private readonly IBannerQuangCaoService _BannerQuangCaoService;
	    private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;


        public BannerQuangCaoController(IBannerQuangCaoService BannerQuangCaoService, ILog Ilog,

		IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IMapper mapper
            )
        {
            _BannerQuangCaoService = BannerQuangCaoService;
            _Ilog = Ilog;
            _mapper = mapper;
		    _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            var droplistViTriHienThi = ConstantExtension.GetDropdownData<ViTriHienThiQuangCaoConstant>(null);
            SessionManager.SetValue("droplistViTriHienThi", droplistViTriHienThi);

        }
        // GET: BannerQuangCaoArea/BannerQuangCao
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var listData = _BannerQuangCaoService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as BannerQuangCaoSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new BannerQuangCaoSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _BannerQuangCaoService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();
            ViewBag.dropViTriHienThi = ConstantExtension.GetDropdownData<ViTriHienThiQuangCaoConstant>(null);
            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult Create(CreateVM model, HttpPostedFileBase file)
        {
            ViewBag.dropViTriHienThi = ConstantExtension.GetDropdownData<ViTriHienThiQuangCaoConstant>(null);
            var result = new JsonResultBO(true, "Tạo Quản lý quảng cáo thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<BannerQuangCao>(model);
                    if (file != null && file.ContentLength > 0)
                    {
                        string fileDinhKem = Path.GetExtension(file.FileName);
                        List<string> ExtensionSupport = new List<string>() {".png", ".jpg", ".jpeg", ".gif" };
                        if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                        {
                            result.MessageFail("Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                            return Json(result);
                        }
                        var resultUpload = UploadProvider.SaveFile(file, null, UploadProvider.ListExtensionCommonImage, UploadProvider.MaxSizeCommon, "Uploads/LinkAnhBanner/", HostingEnvironment.MapPath("/"));
                        if (resultUpload.status)
                        {
                            EntityModel.ImageLink = resultUpload.path;
                        }
                        else
                        {
                            result.MessageFail(resultUpload.message);
                            return Json(result);
                        }
                    }
                    _BannerQuangCaoService.Create(EntityModel);
                }
            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý quảng cáo", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _BannerQuangCaoService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }
            ViewBag.dropViTriHienThi = ConstantExtension.GetDropdownData<ViTriHienThiQuangCaoConstant>(obj.ViTriHienThi);
            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult Edit(EditVM model, HttpPostedFileBase file)
        {
            var obj = _BannerQuangCaoService.GetById(model.Id);
            if (obj == null)
            {
                throw new Exception("Không tìm thấy thông tin");
            }
            ViewBag.dropViTriHienThi = ConstantExtension.GetDropdownData<ViTriHienThiQuangCaoConstant>(obj.ViTriHienThi);
            var result = new JsonResultBO(true);
            try
            {
                #region Kiểm tra valid tài liệu đính kèm
                if (file != null && file.ContentLength > 0)
                {
                    string fileDinhKem = Path.GetExtension(file.FileName);
                    List<string> ExtensionSupport = new List<string>() {".png", ".jpg", ".jpeg", ".gif" };
                    if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                    {
                        result.MessageFail("Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                        return Json(result);
                    }
                }
                #endregion
                if (ModelState.IsValid)
                {
                    obj= _mapper.Map(model, obj);

                    if (file != null && file.ContentLength > 0)
                    {
                        string fileDinhKem = Path.GetExtension(file.FileName);
                        List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg" };
                        if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                        {
                            result.MessageFail("Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                            return Json(result);
                        }
                        var resultUpload = UploadProvider.SaveFile(file, null, UploadProvider.ListExtensionCommon, UploadProvider.MaxSizeCommon, "Uploads/LinkAnhBanner/", HostingEnvironment.MapPath("/"));
                        if (resultUpload.status)
                        {
                            obj.ImageLink = resultUpload.path;
                        }
                        else
                        {
                            result.MessageFail(resultUpload.message);
                            return Json(result);
                        }
                    }
                    _BannerQuangCaoService.Update(obj);
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý quảng cáo", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(BannerQuangCaoSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as BannerQuangCaoSearchDto;

            if (searchModel == null)
            {
                searchModel = new BannerQuangCaoSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.StatusFilter = form.StatusFilter;
			searchModel.NameFilter = form.NameFilter;
			searchModel.DescriptionFilter = form.DescriptionFilter;
			searchModel.ViTriHienThiFilter = form.ViTriHienThiFilter;
            SessionManager.SetValue((searchKey) , searchModel);

            var data = _BannerQuangCaoService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý quảng cáo thành công");
            try
            {
                var user = _BannerQuangCaoService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _BannerQuangCaoService.SoftDelete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _BannerQuangCaoService.GetDtoById(id);
            return View(model);
        }
	//[PermissionAccess(Code = permissionImport)]
        public FileResult ExportExcel()
        {
            var searchModel = SessionManager.GetValue(searchKey) as BannerQuangCaoSearchDto;
            var data = _BannerQuangCaoService.GetDaTaByPage(searchModel).ListItem;
		var dataExport = _mapper.Map<List<BannerQuangCaoExportDto>>(data);
            var fileExcel = ExportExcelV2Helper.Export<BannerQuangCaoExportDto>(dataExport);
            return File(fileExcel, "application/octet-stream", "BannerQuangCao.xlsx");
        }

        
    }
}