using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.BannerQuangCaoArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		public string LinkUrl { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public bool Status { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string ViTriHienThi { get; set; }
        public string file { get; set; }
    }
}