using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.BannerQuangCaoService.Dto;

namespace Nami.Web.Areas.BannerQuangCaoArea.Models
{
    public class DetailVM
    {
       public BannerQuangCaoDto objInfo { get; set; }
    }
}