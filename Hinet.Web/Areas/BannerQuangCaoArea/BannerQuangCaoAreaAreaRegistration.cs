using System.Web.Mvc;

namespace Nami.Web.Areas.BannerQuangCaoArea
{
    public class BannerQuangCaoAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BannerQuangCaoArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BannerQuangCaoArea_default",
                "BannerQuangCaoArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}