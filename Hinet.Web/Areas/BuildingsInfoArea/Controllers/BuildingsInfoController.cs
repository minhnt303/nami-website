using AutoMapper;
using CommonHelper.Upload;
using Nami.Model.Entities;
using Nami.Service.AgreementsInfoService;
using Nami.Service.AppUserService;
using Nami.Service.BuildingsInfoService;
using Nami.Service.BuildingsInfoService.Dto;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.DM_NhomDanhmucService;
using Nami.Service.FileDinhKemService;
using Nami.Service.HistoryService;
using Nami.Service.HUYENService;
using Nami.Service.QuanLyDuAnService;
using Nami.Service.QuanLyDuongPhoService;
using Nami.Service.RentersInfoService;
using Nami.Service.RentersInfoService.Dto;
using Nami.Service.RoomInfoService;
using Nami.Service.TaiLieuDinhKemService;
using Nami.Service.TINHService;
using Nami.Service.XAService;
using Nami.Web.Areas.BuildingsInfoArea.Models;
using Nami.Web.Filters;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Service.OrderInfoService;
using Nami.Service.CommonConfigurationService;
using System.Threading.Tasks;
using Nami.Web.Core;
using Nami.Service.NotificationService;

namespace Nami.Web.Areas.BuildingsInfoArea.Controllers
{
    public class BuildingsInfoController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "BuildingsInfo_index";
        public const string permissionCreate = "BuildingsInfo_create";
        public const string permissionEdit = "BuildingsInfo_edit";
        public const string permissionDelete = "BuildingsInfo_delete";
        public const string permissionImport = "BuildingsInfo_Inport";
        public const string permissionExport = "BuildingsInfo_export";
        public const string searchKey = "BuildingsInfoPageSearchModel";
        private readonly IBuildingsInfoService _BuildingsInfoService;
        private readonly IRoomInfoService _roomInfoService;
        private readonly IFileDinhKemService _fileDinhKemService;
        private readonly IRentersInfoService _rentersInfoService;
        private readonly IAgreementsInfoService _agreementsInfoService;
        private readonly IAppUserService _appUserService;
        private readonly ITINHService _tINHService;
        private readonly IHUYENService _hUYENService;
        private readonly IXAService _xAService;
        private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        private readonly IDM_NhomDanhmucService _dM_NhomDanhmucService;
        private readonly IQuanLyDuAnService _QuanLyDuAnService;
        private readonly IQuanLyDuongPhoService _QuanLyDuongPhoService;
        private readonly IHistoryService _historyService;
        private readonly ITaiLieuDinhKemService _taiLieuDinhKemService;
        private readonly IOrderInfoService _orderInfoService;
        private readonly ICommonConfigurationService _commonConfigurationService;
        private readonly INotificationService _NotificationService;

        public BuildingsInfoController(IBuildingsInfoService BuildingsInfoService, ILog Ilog,
            IRoomInfoService roomInfoService,
            IMapper mapper,
            IFileDinhKemService fileDinhKemService,
            IRentersInfoService rentersInfoService,
            IAppUserService appUserService,
            IAgreementsInfoService agreementsInfoService,
            ITINHService tINHService,
            IHUYENService hUYENService,
            IXAService xAService,
            IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IDM_NhomDanhmucService dM_NhomDanhmucService,
            IQuanLyDuAnService QuanLyDuAnService,
            IHistoryService historyService,
            IQuanLyDuongPhoService QuanLyDuongPhoService,
            ITaiLieuDinhKemService taiLieuDinhKemService,
            IOrderInfoService orderInfoService,
            ICommonConfigurationService commonConfigurationService,
            INotificationService NotificationService
            )
        {
            _commonConfigurationService = commonConfigurationService;
            _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _dM_NhomDanhmucService = dM_NhomDanhmucService;
            _tINHService = tINHService;
            _hUYENService = hUYENService;
            _xAService = xAService;
            _QuanLyDuAnService = QuanLyDuAnService;
            _QuanLyDuongPhoService = QuanLyDuongPhoService;
            _BuildingsInfoService = BuildingsInfoService;
            _Ilog = Ilog;
            _mapper = mapper;
            _roomInfoService = roomInfoService;
            _fileDinhKemService = fileDinhKemService;
            _rentersInfoService = rentersInfoService;
            _agreementsInfoService = agreementsInfoService;
            _appUserService = appUserService;
            _historyService = historyService;
            _taiLieuDinhKemService = taiLieuDinhKemService;
            _orderInfoService = orderInfoService;
            _NotificationService = NotificationService;

            var droplistStatus = ConstantExtension.GetDropdownData<ItemStatusConstant>(null);
            SessionManager.SetValue("droplistStatus", droplistStatus);
        }
        // GET: BuildingsInfoArea/BuildingsInfo
        //[PermissionAccess(Code = permissionIndex)]IndexXemNha
        public ActionResult Index()
        {
            var PermissionWatchAll = HasPermission(PermissionCodeConst.Super_Admin);
            var data = new IndexVM();
            data.objInfo = _BuildingsInfoService.GetDaTaByPageByCreatedId(PermissionWatchAll, CurrentUserId, null);
            data.firstBuldings = _BuildingsInfoService.GetFirstBulidingData(CurrentUserId);
            SessionManager.SetValue(searchKey, null);
            return View(data);
        }

        public ActionResult IndexXemNha()
        {
            var data = new IndexVM();
            data.objInfoXemNha = _rentersInfoService.GetDaTaByPageByCreatedID(CurrentUserId, null);
            SessionManager.SetValue(searchKey, null);
            return View(data);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var PermissionWatchAll = HasPermission(PermissionCodeConst.Super_Admin);
            var searchModel = SessionManager.GetValue(searchKey) as BuildingsInfoSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new BuildingsInfoSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _BuildingsInfoService.GetDaTaByPageByCreatedId(PermissionWatchAll, CurrentUserId, searchModel, indexPage, pageSize);
            return Json(data);
        }
        [HttpPost]
        public JsonResult getDataXemNha(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as RentersInfoSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new RentersInfoSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _rentersInfoService.GetDaTaByPageByCreatedID(CurrentUserId, searchModel, indexPage, pageSize);
            return Json(data);
        }

        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        public ActionResult CreateRedirect()
        {
            var myModel = new CreateVM();
            var UserInfo = _appUserService.GetDtoById(CurrentUserId.HasValue ? CurrentUserId.Value : 0);
            if (UserInfo != null)
            {
                myModel.TenLienHe = UserInfo.FullName;
                myModel.DienThoai = UserInfo.PhoneNumber;
                myModel.DiDong = UserInfo.PhoneNumber;
                myModel.Email = UserInfo.Email;
                myModel.DiaChiLienHe = UserInfo.Address;
            }
            myModel.IsCreateRoom = false;
            ViewBag.dropdownListQuocGia = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.QuocGia, DanhMucConstant.VietNam).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListTinh = _tINHService.GetDropdown("TenTinh", "MaTinh", string.Empty).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListHuyen = new List<SelectListItem>();
            ViewBag.dropdownListXa = new List<SelectListItem>();
            ViewBag.dropdownListStreet = new List<SelectListItem>();
            ViewBag.dropdownListDuAn = new List<SelectListItem>();
            ViewBag.dropdownListHuongNha = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.HUONGNHA, null).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListMucDichSuDung = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.MUCDICHSUDUNG, null).OrderBy(x => x.Text).ToList();
            return View(myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult CreateRedirect(CreateVM model, string longtitude, string latitude, List<HttpPostedFileBase> file)
        {
            ViewBag.dropdownListQuocGia = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.QuocGia, DanhMucConstant.VietNam).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListTinh = _tINHService.GetDropdown("TenTinh", "MaTinh", string.Empty).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListHuyen = new List<SelectListItem>();
            ViewBag.dropdownListXa = new List<SelectListItem>();
            ViewBag.dropdownListStreet = new List<SelectListItem>();
            ViewBag.dropdownListDuAn = new List<SelectListItem>();
            ViewBag.dropdownListHuongNha = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.HUONGNHA, null).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListMucDichSuDung = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.MUCDICHSUDUNG, null).OrderBy(x => x.Text).ToList();

            var result = new JsonResultBO(true, "Tạo Nhà trọ thành công");
            try
            {
                if (string.IsNullOrEmpty(longtitude) && string.IsNullOrEmpty(latitude))
                {
                    ModelState.AddModelError("ToaDoViTri", "Bạn chưa chọn địa chỉ trên bản đồ");
                    return View(model);
                }
                if (file != null && file.Any())
                {
                    foreach (var item in file)
                    {
                        if (item != null && item.ContentLength > 0)
                        {
                            string fileDinhKem = Path.GetExtension(item.FileName);
                            List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg", ".gif" };
                            if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                            {
                                ModelState.AddModelError("file", "Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                                return View(model);
                            }
                        }
                    }
                }
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<BuildingsInfo>(model);
                    EntityModel.QuocGiaCode = DanhMucConstant.VietNam;
                    EntityModel.Longitude = longtitude;
                    EntityModel.Latitude = latitude;
                    EntityModel.Status = ItemStatusConstant.MoiTao;
                    _BuildingsInfoService.Create(EntityModel);

                    if (file != null && file.Any())
                    {
                        foreach (var item in file)
                        {
                            if (item != null && item.ContentLength > 0)
                            {
                                string fileDinhKem = Path.GetExtension(item.FileName);
                                List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg", ".gif" };
                                if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                                {
                                    ModelState.AddModelError("file", "Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                                    return View(model);
                                }
                                var resultUpload = UploadProvider.SaveFile(item, null, UploadProvider.ListExtensionCommonImage, UploadProvider.MaxSizeCommon, "Uploads/TaiLieuToaNha/", HostingEnvironment.MapPath("/"));
                                if (resultUpload.status)
                                {
                                    var fileDinhKemNewModel = new FileDinhKem();
                                    fileDinhKemNewModel.ImageUrl = resultUpload.path;
                                    fileDinhKemNewModel.TypeItem = ItemTypeConstant.Building;
                                    fileDinhKemNewModel.IdItem = EntityModel.Id;
                                    _fileDinhKemService.Create(fileDinhKemNewModel);
                                }
                                else
                                {
                                    ModelState.AddModelError("file", resultUpload.message);
                                    return View(model);
                                }
                            }
                        }
                    }

                    if (model.IsCreateRoom == true)
                    {
                        for (int floor = 1; floor <= EntityModel.Floor; floor++)
                        {
                            for (int room = 1; room <= EntityModel.NumberOfRoom; room++)
                            {
                                var RoomModel = new RoomInfo();
                                RoomModel.Address = EntityModel.Address;
                                RoomModel.BuildingsId = EntityModel.Id;
                                RoomModel.Description = EntityModel.Description;
                                RoomModel.Floor = floor;
                                RoomModel.RoomName = "Phòng-" + room;
                                RoomModel.NumberOfPersonPerRoom = EntityModel.NumberOfPeoperPerRoomAdult + EntityModel.NumberOfPeoperPerRoomKid;
                                RoomModel.Price = EntityModel.Price;
                                RoomModel.Address = EntityModel.Address;
                                RoomModel.Longitude = longtitude;
                                RoomModel.Latitude = latitude;
                                RoomModel.TinhCode = EntityModel.TinhCode;
                                RoomModel.HuyenCode = EntityModel.HuyenCode;
                                RoomModel.XaCode = EntityModel.XaCode;
                                RoomModel.StreetCode = EntityModel.StreetCode;
                                RoomModel.DuAnId = EntityModel.DuAnId;
                                RoomModel.MucDichSuDung = EntityModel.MucDichSuDung;
                                RoomModel.HuongBanCong = EntityModel.HuongBanCong;
                                RoomModel.HuongNha = EntityModel.HuongNha;
                                RoomModel.QuocGiaCode = EntityModel.QuocGiaCode;
                                //RoomModel.ChieuDaiRoom = EntityModel.ChieuDaiRoom;
                                //RoomModel.ChieuRongRoom = EntityModel.ChieuRongRoom;
                                RoomModel.DienTich = model.DienTichMoiPhong;
                                RoomModel.NumberOfPeoperPerRoomAdult = EntityModel.NumberOfPeoperPerRoomAdult;
                                RoomModel.NumberOfPeoperPerRoomKid = EntityModel.NumberOfPeoperPerRoomKid;

                                RoomModel.SoPhongNgu = EntityModel.SoPhongNgu;
                                RoomModel.SoToiLet = EntityModel.SoToiLet;
                                RoomModel.NoiThat = EntityModel.NoiThat;
                                RoomModel.ThongTinPhapLy = EntityModel.ThongTinPhapLy;
                                RoomModel.TenLienHe = EntityModel.TenLienHe;
                                RoomModel.DiaChiLienHe = EntityModel.DiaChiLienHe;
                                RoomModel.DienThoai = EntityModel.DienThoai;
                                RoomModel.DiDong = EntityModel.DiDong;
                                RoomModel.Email = EntityModel.Email;
                                RoomModel.Status = EntityModel.Status;
                                RoomModel.StatusTin = EntityModel.StatusTin;
                                _roomInfoService.Create(RoomModel);
                            }
                        }
                    }

                    var logHistory = new History();
                    logHistory.IdItem = EntityModel.Id;
                    logHistory.TypeItem = ItemTypeConstant.Building;
                    logHistory.LogId = CurrentUserId;
                    logHistory.HistoryContent = CurrentUserInfo.FullName + " đã tạo mới thông tin tòa nhà/khu nhà";
                    _historyService.Create(logHistory);

                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Nhà trọ", ex);
            }
            return View(model);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj = _BuildingsInfoService.GetById(id);
            if (obj == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        public ActionResult EditRedirect(long id)
        {
            var myModel = new EditVM();
            var obj = _BuildingsInfoService.GetById(id);
            if (obj == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }
            myModel = _mapper.Map(obj, myModel);
            ViewBag.dropdownListQuocGia = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.QuocGia, DanhMucConstant.VietNam).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListTinh = _tINHService.GetDropdown("TenTinh", "MaTinh", obj.TinhCode).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListHuyen = _hUYENService.GetDropdownOfTinh(obj.TinhCode, obj.HuyenCode);
            ViewBag.dropdownListXa = _xAService.GetDropdownOfHuyen(obj.HuyenCode, obj.XaCode);
            ViewBag.dropdownListStreet = _QuanLyDuongPhoService.GetDropdownOfStreetByHuyen(obj.HuyenCode, obj.StreetCode);
            ViewBag.dropdownListDuAn = _QuanLyDuAnService.GetDropdownOfDuAnByHuyen(obj.HuyenCode, obj.DuAnId.ToString());
            ViewBag.dropdownListHuongNha = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.HUONGNHA, obj.HuongNha).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListHuongBanCong = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.HUONGNHA, obj.HuongBanCong).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListMucDichSuDung = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.MUCDICHSUDUNG, obj.MucDichSuDung).OrderBy(x => x.Text).ToList();
            return View(myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult EditRedirect(EditVM model, string longtitude, string latitude, List<HttpPostedFileBase> file)
        {
            var obj = _BuildingsInfoService.GetById(model.Id);
            if (obj == null)
            {
                throw new Exception("Không tìm thấy thông tin");
            }
            ViewBag.dropdownListQuocGia = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.QuocGia, DanhMucConstant.VietNam).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListTinh = _tINHService.GetDropdown("TenTinh", "MaTinh", obj.TinhCode).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListHuyen = _hUYENService.GetDropdownOfTinh(obj.TinhCode, obj.HuyenCode);
            ViewBag.dropdownListXa = _xAService.GetDropdownOfHuyen(obj.HuyenCode, obj.XaCode);
            ViewBag.dropdownListStreet = _QuanLyDuongPhoService.GetDropdownOfStreetByHuyen(obj.HuyenCode, obj.StreetCode);
            ViewBag.dropdownListDuAn = _QuanLyDuAnService.GetDropdownOfDuAnByHuyen(obj.HuyenCode, obj.DuAnId.ToString());
            ViewBag.dropdownListHuongNha = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.HUONGNHA, obj.HuongNha).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListHuongBanCong = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.HUONGNHA, obj.HuongBanCong).OrderBy(x => x.Text).ToList();
            ViewBag.dropdownListMucDichSuDung = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.MUCDICHSUDUNG, obj.MucDichSuDung).OrderBy(x => x.Text).ToList();
            var result = new JsonResultBO(true);
            try
            {
                if (string.IsNullOrEmpty(longtitude) && string.IsNullOrEmpty(latitude))
                {
                    ModelState.AddModelError("ToaDoViTri", "Bạn chưa chọn địa chỉ trên bản đồ");
                    return View(model);
                }
                if (file != null && file.Any())
                {
                    foreach (var item in file)
                    {
                        if (item != null && item.ContentLength > 0)
                        {
                            string fileDinhKem = Path.GetExtension(item.FileName);
                            List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg", ".gif" };
                            if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                            {
                                ModelState.AddModelError("file", "Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                                return View(model);
                            }
                        }
                    }
                }
                if (ModelState.IsValid)
                {
                    obj = _mapper.Map(model, obj);
                    obj.Longitude = longtitude;
                    obj.Latitude = latitude;
                    if (obj.Status == ItemStatusConstant.MoiTao)
                    {
                        obj.Status = ItemStatusConstant.MoiTao;
                    }
                    else if (obj.Status == ItemStatusConstant.YeuCauBoSung)
                    {
                        obj.Status = ItemStatusConstant.ChoDuyet;
                    }
                    _BuildingsInfoService.Update(obj);

                    if (file != null && file.Any())
                    {
                        foreach (var item in file)
                        {
                            if (item != null && item.ContentLength > 0)
                            {
                                string fileDinhKem = Path.GetExtension(item.FileName);
                                List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg", ".gif" };
                                if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                                {
                                    ModelState.AddModelError("file", "Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                                    return View(model);
                                }
                                var resultUpload = UploadProvider.SaveFile(item, null, UploadProvider.ListExtensionCommonImage, UploadProvider.MaxSizeCommon, "Uploads/TaiLieuToaNha/", HostingEnvironment.MapPath("/"));
                                if (resultUpload.status)
                                {
                                    var fileDinhKemNewModel = new FileDinhKem();
                                    fileDinhKemNewModel.ImageUrl = resultUpload.path;
                                    fileDinhKemNewModel.TypeItem = ItemTypeConstant.Building;
                                    fileDinhKemNewModel.IdItem = obj.Id;
                                    _fileDinhKemService.Create(fileDinhKemNewModel);
                                }
                                else
                                {
                                    ModelState.AddModelError("file", resultUpload.message);
                                    return View(model);
                                }
                            }
                        }
                    }

                    var logHistory = new History();
                    logHistory.IdItem = obj.Id;
                    logHistory.TypeItem = ItemTypeConstant.Building;
                    logHistory.LogId = CurrentUserId;
                    logHistory.HistoryContent = CurrentUserInfo.FullName + " đã chỉnh sửa thông tin tòa nhà/khu nhà";
                    _historyService.Create(logHistory);

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Nhà trọ", ex);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(BuildingsInfoSearchDto form)
        {
            var PermissionWatchAll = HasPermission(PermissionCodeConst.Super_Admin);
            var searchModel = SessionManager.GetValue(searchKey) as BuildingsInfoSearchDto;

            if (searchModel == null)
            {
                searchModel = new BuildingsInfoSearchDto();
                searchModel.pageSize = 20;
            }
            searchModel.BuildingsNameFilter = form.BuildingsNameFilter;
            searchModel.AddressFilter = form.AddressFilter;
            searchModel.DienTichFilterFrom = form.DienTichFilterFrom;
            searchModel.DienTichFilterTo = form.DienTichFilterTo;
            searchModel.NumberOfPeoperPerRoomAdultFilterFrom = form.NumberOfPeoperPerRoomAdultFilterFrom;
            searchModel.NumberOfPeoperPerRoomAdultFilterTo = form.NumberOfPeoperPerRoomAdultFilterTo;
            searchModel.NumberOfPeoperPerRoomKidFilterFrom = form.NumberOfPeoperPerRoomKidFilterFrom;
            searchModel.NumberOfPeoperPerRoomKidFilterTo = form.NumberOfPeoperPerRoomKidFilterTo;
            searchModel.PriceFilterFrom = form.PriceFilterFrom;
            searchModel.PriceFilterTo = form.PriceFilterTo;
            searchModel.StatusFilter = form.StatusFilter;
            searchModel.TrangThaiDangTinFilter = form.TrangThaiDangTinFilter;
            searchModel.CreatedDateFromFilter = form.CreatedDateFromFilter;
            searchModel.CreatedDateToFilter = form.CreatedDateToFilter;
            searchModel.UpdatedDateFromFilter = form.UpdatedDateFromFilter;
            searchModel.UpdatedDateToFilter = form.UpdatedDateToFilter;


            SessionManager.SetValue((searchKey), searchModel);

            var data = _BuildingsInfoService.GetDaTaByPageByCreatedId(PermissionWatchAll, CurrentUserId, searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Nhà trọ thành công");
            try
            {
                var user = _BuildingsInfoService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _BuildingsInfoService.SoftDelete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }


        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _BuildingsInfoService.GetDtoById(id);
            model.historys = _historyService.GetDaTaByIdHistory(id, ItemTypeConstant.Building);
            model.imageList = _fileDinhKemService.GetListImageFile(id, ItemTypeConstant.Building);
            model.ListRoom = _roomInfoService.GetListRoomByCurrentCreateAndBuildingId(CurrentUserInfo.UserName, id);
            model.ListAgreement = _agreementsInfoService.GetListAgreementByCreatedByAndBuldingId(CurrentUserInfo.UserName, id);
            return View(model);
        }

        public PartialViewResult DetailPopup(long id)
        {
            var model = new DetailVM();
            model.objInfo = _BuildingsInfoService.GetDtoById(id);
            model.historys = _historyService.GetDaTaByIdHistory(id, ItemTypeConstant.Building);
            model.imageList = _fileDinhKemService.GetListImageFile(id, ItemTypeConstant.Building);
            model.ListRoom = _roomInfoService.GetListRoomByCurrentCreateAndBuildingId(CurrentUserInfo.UserName, id);
            model.ListAgreement = _agreementsInfoService.GetListAgreementByCreatedByAndBuldingId(CurrentUserInfo.UserName, id);
            return PartialView("_detailPartial", model);
        }

        [HttpPost]
        public JsonResult GetHuyenDropdownOfTinh(string id)
        {
            var result = _hUYENService.GetDropdownOfTinh(id, null).OrderBy(x => x.Text).ToList();
            return Json(result);
        }
        [HttpPost]
        public JsonResult GetXaDropdownOfHuyen(string id)
        {
            var result = _xAService.GetDropdownOfHuyen(id, null).OrderBy(x => x.Text).ToList();
            return Json(result);
        }
        [HttpPost]
        public JsonResult GetXaDropdownOfStreet(string id)
        {
            var result = _QuanLyDuongPhoService.GetDropdownOfStreetByHuyen(id, null).OrderBy(x => x.Text).ToList();
            return Json(result);
        }
        [HttpPost]
        public JsonResult GetXaDropdownOfDuAn(string id)
        {
            var result = _QuanLyDuAnService.GetDropdownOfDuAnByHuyen(id, null).OrderBy(x => x.Text).ToList();
            return Json(result);
        }

        public PartialViewResult RequestStatus(long id, string status)
        {
            var myModel = new RequestStatusVM();
            var obj = _BuildingsInfoService.GetById(id);
            if (obj == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }
            myModel.IdItem = id;
            myModel.Status = status;
            var statusBegin = obj.Status;
            //Ghi trạng thái bắt đầu của hồ sơ
            myModel.StatusBegin = ConstantExtension.GetName<ItemStatusConstant>(statusBegin.ToString());
            myModel.QLBuildingsInfo = obj;
            //Ghi trạng thái đã nhấn vào
            ViewBag.StatusClick = ConstantExtension.GetName<ItemStatusConstant>(status.ToString());
            return PartialView("_RequestStatusPartial", myModel);
        }
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult RequestStatus(long id, string status, string comment)
        {
            var obj = _BuildingsInfoService.GetById(id);
            if (obj == null)
            {
                throw new Exception("Không tìm thấy thông tin");
            }

            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {
                    var logHistory = new History();
                    logHistory.IdItem = id;
                    logHistory.TypeItem = ItemTypeConstant.Building;
                    logHistory.LogId = CurrentUserId;
                    if (CurrentUserInfo.Id == 1)
                    {
                        logHistory.HistoryContent = "Hệ thống "+ ConstantExtension.GetName<NotificationLinkAndtextItemStatusConstant>(status);
                    }
                    else
                    {
                        if (status == ItemStatusConstant.ChoDuyet)
                        {
                            logHistory.HistoryContent = CurrentUserInfo.FullName + " gửi duyệt thông tin nhà cho thuê";
                        }
                        else {
                            logHistory.HistoryContent = CurrentUserInfo.FullName + ConstantExtension.GetName<NotificationLinkAndtextItemStatusConstant>(status);
                        }
                    }
                    logHistory.Comment = comment;
                    //logHistory.Note = model.Note;
                    logHistory.StatusBegin = obj.Status;
                    obj.Status = status;
                    //if (model.Status == QLCongViecKeHoachStatusConstant.ChoDuyet)
                    var StatusEnd = status;
                    logHistory.StatusEnd = StatusEnd;
                    logHistory.Name = StatusEnd;
                    _BuildingsInfoService.Update(obj);
                    _historyService.Create(logHistory);
                    var notification = new Notification();
                    notification.IsRead = false;
                    notification.Type = NotificationTypeConstant.System;
                    notification.FromUser = CurrentUserId;
                    notification.Link = "/BuildingsInfoArea/BuildingsInfo/Detail/" + obj.Id;
                    if (CurrentUserInfo.Id == 1)
                    {
                        notification.Message = "Hệ thống " + ConstantExtension.GetName<NotificationLinkAndtextItemStatusConstant>(status);
                        notification.ToUser = obj.CreatedID;
                    }
                    else
                    {
                        if (status == ItemStatusConstant.ChoDuyet)
                        {
                            notification.Message = CurrentUserInfo.FullName + " gửi duyệt thông tin nhà cho thuê";
                            notification.ToUser = 1;
                        }
                        else
                        {
                            notification.Message = CurrentUserInfo.FullName + ConstantExtension.GetName<NotificationLinkAndtextItemStatusConstant>(status);
                            notification.ToUser = 1;
                        }
                    }
                    _NotificationService.Create(notification);
                    Task.Run(() => NotificationProvider.SendMessage(notification));
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = ex.Message;
                _Ilog.Error("Lỗi ghi lại lịch sử làm việc", ex);
            }
            return Json(result);
        }


        public PartialViewResult ThanhToanPhiDangTin(long id, string status)
        {
            var myModel = new ThanhToanVM();
            var obj = _BuildingsInfoService.GetById(id);
            if (obj == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }
            ViewBag.ListBanhCode = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.MANGANHANG, null);
            var PhiDichVuTheoNgay = _commonConfigurationService.GetDataByCode("PHIDICHVU");
            myModel.PhiDichVuTheoNgay = decimal.Parse(PhiDichVuTheoNgay);
            myModel.StartDate = DateTime.Now;
            myModel.OrderType = OrderTypeConstant.PhiDangTin;
            myModel.ItemType = ItemTypeConstant.Building;
            myModel.IdItem = obj.Id;
            if (!string.IsNullOrEmpty(obj.BuildingsName))
            {
                myModel.ItemName = obj.BuildingsName;
            }
            return PartialView("_ThanhToanPhiDangTinPartial", myModel);
        }
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult ThanhToanPhiDangTin(ThanhToanVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {
                    string vnp_Returnurl = ConfigurationManager.AppSettings["vnp_Returnurl"]; //URL nhan ket qua tra ve 
                    string vnp_Url = ConfigurationManager.AppSettings["vnp_Url"]; //URL thanh toan cua VNPAY 
                    string vnp_TmnCode = ConfigurationManager.AppSettings["vnp_TmnCode"]; //Ma website
                    string vnp_HashSecret = ConfigurationManager.AppSettings["vnp_HashSecret"]; //Chuoi bi ma

                    OrderInfo order = new OrderInfo();
                    //Save order to db
                    //order.OrderId = DateTime.Now.ToLongDateString;
                    order.Amount = Convert.ToDecimal(model.Amount);
                    order.CreatedDate = DateTime.Now;
                    order.OrderType = model.OrderType;
                    order.ItemType = model.ItemType;
                    order.IdItem = model.IdItem;
                    order.BankCode = model.BankCode;
                    if (model.ItemType == ItemTypeConstant.Building)
                    {
                        order.OrderDescription = "Thanh toán phí đăng tin cho khu nhà/tòa nhà " + model.ItemName;
                        var BuildingInfo = _BuildingsInfoService.GetById(model.IdItem);
                        if (BuildingInfo != null) {
                            BuildingInfo.StartDate = model.StartDate;
                            BuildingInfo.EndDate = model.EndDate;
                            _BuildingsInfoService.Update(BuildingInfo);
                        }
                    }
                    else if (model.ItemType == ItemTypeConstant.Room)
                    {
                        order.OrderDescription = "Thanh toán phí đăng tin cho phòng trọ " + model.ItemName;
                        var RoomInfo = _roomInfoService.GetById(model.IdItem);
                        if (RoomInfo != null)
                        {
                            RoomInfo.StartDate = model.StartDate;
                            RoomInfo.EndDate = model.EndDate;
                            _roomInfoService.Update(RoomInfo);
                        }
                    }
                    _orderInfoService.Create(order);
                    order.OrderId = order.Id;
                    _orderInfoService.Update(order);

                    VnPayLibrary vnpay = new VnPayLibrary();
                    vnpay.AddRequestData("vnp_Version", "2.0.0");
                    vnpay.AddRequestData("vnp_Command", "pay");
                    vnpay.AddRequestData("vnp_TmnCode", vnp_TmnCode);

                    string locale = model.Locale;
                    if (!string.IsNullOrEmpty(locale))
                    {
                        vnpay.AddRequestData("vnp_Locale", locale);
                    }
                    else
                    {
                        vnpay.AddRequestData("vnp_Locale", "vn");
                    }

                    vnpay.AddRequestData("vnp_CurrCode", "VND");
                    vnpay.AddRequestData("vnp_TxnRef", order.OrderId.ToString());
                    vnpay.AddRequestData("vnp_OrderInfo", order.OrderDescription);
                    vnpay.AddRequestData("vnp_OrderType", "250006"); //default value: other
                    vnpay.AddRequestData("vnp_Amount", (order.Amount * 100).ToString());
                    vnpay.AddRequestData("vnp_ReturnUrl", vnp_Returnurl);
                    vnpay.AddRequestData("vnp_IpAddr", Utils.GetIpAddress());
                    vnpay.AddRequestData("vnp_CreateDate", order.CreatedDate.ToString("yyyyMMddHHmmss"));
                    if (!string.IsNullOrEmpty(model.BankCode))
                    {
                        vnpay.AddRequestData("vnp_BankCode", model.BankCode);
                    }
                    string paymentUrl = vnpay.CreateRequestUrl(vnp_Url, vnp_HashSecret);
                    //log.InfoFormat("VNPAY URL: {0}", paymentUrl);
                    return Json(paymentUrl);
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = ex.Message;
                _Ilog.Error("Lỗi ghi lại lịch sử làm việc", ex);
            }
            return Json(model);
        }
    }
}