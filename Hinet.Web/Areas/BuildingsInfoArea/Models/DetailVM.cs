using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.BuildingsInfoService.Dto;
using Nami.Service.HistoryService.Dto;

namespace Nami.Web.Areas.BuildingsInfoArea.Models
{
    public class DetailVM
    {
        public List<HistoryDto> historys { get; set; }

        public List<FileDinhKem> imageList { get; set; }

        public BuildingsInfoDto objInfo { get; set; }
        public List<RoomInfo> ListRoom { get; set; }
        public List<AgreementsInfo> ListAgreement { get; set; }
    }
}