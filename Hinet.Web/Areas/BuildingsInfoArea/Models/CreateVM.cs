﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.BuildingsInfoArea.Models
{
    public class CreateVM
    {
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string BuildingsName { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string Description { get; set; }
		public string ServicesCode { get; set; }
		public int? InputWaterElectricFrom { get; set; }
		public int? InputWaterElectricTo { get; set; }
		public int? BillsFrom { get; set; }
		public int? BillsTo { get; set; }
		public int? CollectionMoneyFrom { get; set; }
		public int? CollectionMoneyTo { get; set; }
		public int? EndOfConstractFrom { get; set; }
		public int? EndOfConstractTo { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string TinhCode { get; set; }
		public string XaCode { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string HuyenCode { get; set; }
		public string QuocGiaCode { get; set; }
        [DisplayName("Số tầng")]
        public int? Floor { get; set; }

        [DisplayName("Số phòng trọ")]
        public int? NumberOfRoom { get; set; }

        [DisplayName("Số lượng người lớn có thể chứa trong 1 phòng")]
        public int? NumberOfPeoperPerRoomAdult { get; set; }

        [DisplayName("Số lượng trẻ con có thể chứa trong 1 phòng")]
        public int? NumberOfPeoperPerRoomKid { get; set; }

        public string StreetCode { get; set; }
        public long? DuAnId { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string MucDichSuDung { get; set; }
        public string DonVi { get; set; }
        public string HuongNha { get; set; }
        public string HuongBanCong { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public int SoPhongNgu { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public int SoToiLet { get; set; }
        public string NoiThat { get; set; }
        public string ThongTinPhapLy { get; set; }
        public string TenLienHe { get; set; }
        public string DiaChiLienHe { get; set; }
        public string DienThoai { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin này")]
        public string DiDong { get; set; }
        public string Email { get; set; }
        public decimal? DienTich { get; set; }
        public decimal? DienTichMoiPhong { get; set; }

        public string file { get; set; }

        public bool IsCreateRoom { get; set; }
        public decimal? Price { get; set; }

        public string ToaDoViTri { get; set; }
    }
}