﻿using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nami.Web.Areas.BuildingsInfoArea.Models
{
    public class RequestStatusVM
    {
        [StringLength(255)]
        public string Name { get; set; }

        public long LogId { get; set; }

        public long IdItem { get; set; }

        [StringLength(255)]
        public string TypeItem { get; set; }

        [Column(TypeName = "nvarchar")]
        public string Note { get; set; }

        [Column(TypeName = "ntext")]
        [AllowHtml]
        public string Comment { get; set; }

        public string Status { get; set; }

        [StringLength(255)]
        public string StatusBegin { get; set; }

        [StringLength(255)]
        public string StatusEnd { get; set; }

        [StringLength(255)]
        public string StatusClick { get; set; }
        public BuildingsInfo QLBuildingsInfo { get; set; }
    }
}