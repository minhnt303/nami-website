﻿using Nami.Model.Entities;
using Nami.Service.BuildingsInfoService.Dto;
using Nami.Service.Common;
using Nami.Service.RentersInfoService.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.BuildingsInfoArea.Models
{
    public class IndexVM
    {
        public PageListResultBO<RentersInfoDto> objInfoXemNha;

        public PageListResultBO<BuildingsInfoDto> objInfo { get; set; }
        public BuildingsInfoDto firstBuldings { get; set; }
    }
}