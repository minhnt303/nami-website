﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.Entities;

namespace Nami.Web.Areas.BuildingsInfoArea.Models
{
    public class ThanhToanVM
    {
        public string ItemName { get; set; }

        public decimal Amount { get; set; }
        public string OrderDescription { get; set; }

        public string BankCode { get; set; }
        public string Locale { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal PhiDichVuTheoNgay { get; set; }
        public string OrderType { get; set; }
        public long IdItem { get; set; }
        public string ItemType { get; set; }
    }
}