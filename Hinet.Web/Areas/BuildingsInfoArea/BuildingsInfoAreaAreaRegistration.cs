using System.Web.Mvc;

namespace Nami.Web.Areas.BuildingsInfoArea
{
    public class BuildingsInfoAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "BuildingsInfoArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "BuildingsInfoArea_default",
                "BuildingsInfoArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}