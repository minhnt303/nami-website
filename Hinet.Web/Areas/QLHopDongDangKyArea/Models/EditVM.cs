using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.QLHopDongDangKyArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		public long? ItemId { get; set; }
		public string ItemType { get; set; }
		public string UserIdDangKy { get; set; }

        
    }
}