using System.Web.Mvc;

namespace Nami.Web.Areas.QLHopDongDangKyArea
{
    public class QLHopDongDangKyAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "QLHopDongDangKyArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "QLHopDongDangKyArea_default",
                "QLHopDongDangKyArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}