using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.QLHopDongDangKyArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.QLHopDongDangKyService;
using Nami.Service.QLHopDongDangKyService.Dto;
using CommonHelper.Excel;
using CommonHelper.ObjectExtention;
using Nami.Web.Common;
using System.IO;
using System.Web.Configuration;
using CommonHelper;
using Nami.Service.DM_DulieuDanhmucService;



namespace Nami.Web.Areas.QLHopDongDangKyArea.Controllers
{
    public class QLHopDongDangKyController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "QLHopDongDangKy_index";
        public const string permissionCreate = "QLHopDongDangKy_create";
        public const string permissionEdit = "QLHopDongDangKy_edit";
        public const string permissionDelete = "QLHopDongDangKy_delete";
        public const string permissionImport = "QLHopDongDangKy_Inport";
        public const string permissionExport = "QLHopDongDangKy_export";
        public const string searchKey = "QLHopDongDangKyPageSearchModel";
        private readonly IQLHopDongDangKyService _QLHopDongDangKyService;
	private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;


        public QLHopDongDangKyController(IQLHopDongDangKyService QLHopDongDangKyService, ILog Ilog,

		IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IMapper mapper
            )
        {
            _QLHopDongDangKyService = QLHopDongDangKyService;
            _Ilog = Ilog;
            _mapper = mapper;
		_dM_DulieuDanhmucService = dM_DulieuDanhmucService;

        }
        // GET: QLHopDongDangKyArea/QLHopDongDangKy
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {

            var listData = _QLHopDongDangKyService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLHopDongDangKySearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new QLHopDongDangKySearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _QLHopDongDangKyService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Quản lý đăng ký làm hợp đồng thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<QLHopDongDangKy>(model);
                    _QLHopDongDangKyService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý đăng ký làm hợp đồng", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _QLHopDongDangKyService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _QLHopDongDangKyService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _QLHopDongDangKyService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý đăng ký làm hợp đồng", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(QLHopDongDangKySearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLHopDongDangKySearchDto;

            if (searchModel == null)
            {
                searchModel = new QLHopDongDangKySearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.ItemIdFilter = form.ItemIdFilter;
			searchModel.ItemTypeFilter = form.ItemTypeFilter;
			searchModel.UserIdDangKyFilter = form.UserIdDangKyFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _QLHopDongDangKyService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý đăng ký làm hợp đồng thành công");
            try
            {
                var user = _QLHopDongDangKyService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _QLHopDongDangKyService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _QLHopDongDangKyService.GetById(id);
            return View(model);
        }
	//[PermissionAccess(Code = permissionImport)]
        public FileResult ExportExcel()
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLHopDongDangKySearchDto;
            var data = _QLHopDongDangKyService.GetDaTaByPage(searchModel).ListItem;
		var dataExport = _mapper.Map<List<QLHopDongDangKyExportDto>>(data);
            var fileExcel = ExportExcelV2Helper.Export<QLHopDongDangKyExportDto>(dataExport);
            return File(fileExcel, "application/octet-stream", "QLHopDongDangKy.xlsx");
        }

        
    }
}