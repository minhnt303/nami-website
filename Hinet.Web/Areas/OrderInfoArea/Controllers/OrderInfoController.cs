using AutoMapper;
using CommonHelper.Excel;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.OrderInfoService;
using Nami.Service.OrderInfoService.Dto;
using Nami.Web.Areas.OrderInfoArea.Models;
using Nami.Web.Filters;
using log4net;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Nami.Service.Constant;
using Nami.Service.GiaiTrinhPhanAnhService;
using Nami.Service.GiaiTrinhPhanAnhFileAttackService;
using CommonHelper.Upload;
using System.IO;
using System.Threading.Tasks;
using System.Web.Hosting;
using Nami.Web.Core;
using Nami.Service.NotificationService;

namespace Nami.Web.Areas.OrderInfoArea.Controllers
{
    public class OrderInfoController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "OrderInfo_index";
        public const string permissionCreate = "OrderInfo_create";
        public const string permissionEdit = "OrderInfo_edit";
        public const string permissionDelete = "OrderInfo_delete";
        public const string permissionImport = "OrderInfo_Inport";
        public const string permissionExport = "OrderInfo_export";
        public const string searchKey = "OrderInfoPageSearchModel";
        private readonly IOrderInfoService _OrderInfoService;
        private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        private readonly IGiaiTrinhPhanAnhService _giaiTrinhPhanAnhService;
        private readonly IGiaiTrinhPhanAnhFileAttackService _GiaiTrinhPhanAnhFileAttackService;
        private readonly INotificationService _NotificationService;


        public OrderInfoController(IOrderInfoService OrderInfoService, ILog Ilog,

        IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IGiaiTrinhPhanAnhService giaiTrinhPhanAnhService,
            IGiaiTrinhPhanAnhFileAttackService GiaiTrinhPhanAnhFileAttackService,
            INotificationService NotificationService,
            IMapper mapper
            )
        {
            _OrderInfoService = OrderInfoService;
            _giaiTrinhPhanAnhService = giaiTrinhPhanAnhService;
            _Ilog = Ilog;
            _mapper = mapper;
            _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _GiaiTrinhPhanAnhFileAttackService = GiaiTrinhPhanAnhFileAttackService;
            _NotificationService = NotificationService;

            var droplistMaNganHang = _dM_DulieuDanhmucService.GetDropDownListByCodeGroup(DanhMucConstant.MANGANHANG);
            SessionManager.SetValue("droplistMaNganHang", droplistMaNganHang);
            var droplistOrderType = ConstantExtension.GetDropdownData<OrderTypeConstant>(null);
            SessionManager.SetValue("droplistOrderType", droplistOrderType);

        }
        // GET: OrderInfoArea/OrderInfo
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var PermissionWatchAll = HasPermission(PermissionCodeConst.Super_Admin);
            var listData = _OrderInfoService.GetDaTaByPageByCurrentUserIdOrAdmin(PermissionWatchAll, CurrentUserId, null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as OrderInfoSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new OrderInfoSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var PermissionWatchAll = HasPermission(PermissionCodeConst.Super_Admin);
            var data = _OrderInfoService.GetDaTaByPageByCurrentUserIdOrAdmin(PermissionWatchAll, CurrentUserId, searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Quản lý thông tin giao dịch thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<OrderInfo>(model);
                    _OrderInfoService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý thông tin giao dịch", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj = _OrderInfoService.GetById(id);
            if (obj == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _OrderInfoService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj = _mapper.Map(model, obj);
                    _OrderInfoService.Update(obj);

                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý thông tin giao dịch", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(OrderInfoSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as OrderInfoSearchDto;

            if (searchModel == null)
            {
                searchModel = new OrderInfoSearchDto();
                searchModel.pageSize = 20;
            }
            searchModel.IdFilterFrom = form.IdFilterFrom;
            searchModel.IdFilterTo = form.IdFilterTo;
            searchModel.AmountFilterFrom = form.AmountFilterFrom;
            searchModel.AmountFilterTo = form.AmountFilterTo;
            searchModel.OrderDescriptionFilter = form.OrderDescriptionFilter;
            searchModel.BankCodeFilter = form.BankCodeFilter;
            searchModel.OrderTypeFilter = form.OrderTypeFilter;
            searchModel.ItemTypeFilter = form.ItemTypeFilter;
            searchModel.TrangThaiThanhToanFilter = form.TrangThaiThanhToanFilter;

            SessionManager.SetValue((searchKey), searchModel);

            var PermissionWatchAll = HasPermission(PermissionCodeConst.Super_Admin);
            var data = _OrderInfoService.GetDaTaByPageByCurrentUserIdOrAdmin(PermissionWatchAll, CurrentUserId, searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý thông tin giao dịch thành công");
            try
            {
                var user = _OrderInfoService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _OrderInfoService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }


        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _OrderInfoService.GetDtoById(id);
            model.objGiaiTrinh = _giaiTrinhPhanAnhService.GetListGiaTrinhByPhanAnhIdAndPhanAnhType(id, ItemTypeConstant.Order);
            return View(model);
        }
        //[PermissionAccess(Code = permissionImport)]
        public FileResult ExportExcel()
        {
            var searchModel = SessionManager.GetValue(searchKey) as OrderInfoSearchDto;
            var data = _OrderInfoService.GetDaTaByPage(searchModel).ListItem;
            var dataExport = _mapper.Map<List<OrderInfoExportDto>>(data);
            var fileExcel = ExportExcelV2Helper.Export<OrderInfoExportDto>(dataExport);
            return File(fileExcel, "application/octet-stream", "OrderInfo.xlsx");
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GiaiTrinhVuViec(GiaitrinhCommentViewModel data, List<HttpPostedFileBase> file)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (file != null)
                {
                    foreach (var item in file)
                    {
                        string LogoFileExtension = Path.GetExtension(item.FileName);
                        List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg", ".doc", ".docx", ".pdf" };
                        if (!ExtensionSupport.Contains(LogoFileExtension.ToLower()))
                        {
                            result.MessageFail("Vui lòng nhập đúng định dạng ảnh bao gồm doc, docx, pdf, png, jpg hoặc jpeg");
                            return Json(result);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(data.Comments))
                {
                    GiaiTrinhPhanAnh model = new GiaiTrinhPhanAnh();


                    model.GiaiTrinhComment = data.Comments;
                    model.PhanAnhType = data.PhanAnhType;
                    model.PhanAnhId = data.id;
                    model.IdHoSoGiaiTrinh = CurrentUserId;
                    model.TypeHoSoGiaiTrinh = CurrentUserInfo.TypeOrganization;
                    model.NgayGiaiTrinh = DateTime.Now;
                    model.IsDelete = false;
                    //model.LoaiGiaiTrinh = GiaiTrinhTypeConstant.CapNhatGiaiTrinh;
                    _giaiTrinhPhanAnhService.Create(model);

                    if (file != null)
                    {
                        foreach (var item in file)
                        {
                            var resultUpload = UploadProvider.SaveFile(item, null, ".png,.jpg,.jpeg,.doc,.docx,.pdf", 5242880, "Uploads/TaiLieuBinhLuan/", HostingEnvironment.MapPath("/"));

                            if (resultUpload.status == true)
                            {
                                GiaiTrinhPhanAnhFileAttack doc = new GiaiTrinhPhanAnhFileAttack()
                                {
                                    IdGiaiTrinhPhanAnh = (int)model.Id,
                                    FileUrlGiaiTrinhPhanAnh = resultUpload.path,
                                    IsDelete = false,
                                };
                                _GiaiTrinhPhanAnhFileAttackService.Create(doc);
                            }
                        }
                    }


                    var messageContent = "";
                    messageContent = CurrentUserInfo.FullName + " đã yêu cầu hỗ trợ thông tin giao dịch id là " + model.PhanAnhId;

                    #region log history
                    var logHistory = new History();
                    logHistory.IdItem = model.PhanAnhId.GetValueOrDefault();
                    logHistory.TypeItem = ItemTypeConstant.Order;
                    logHistory.HistoryContent = messageContent;
                    //_historyService.Create(logHistory);
                    #endregion
                    var tb = new Notification();
                    tb.ToUser = 1;
                    tb.FromUser = CurrentUserId;
                    tb.IsRead = false;
                    tb.Message = messageContent;
                    tb.Link = "/OrderInfoArea/OrderInfo/Detail/" + model.PhanAnhId;
                    tb.Type = NotificationTypeConstant.System;
                    _NotificationService.Create(tb);
                    Task.Run(() => NotificationProvider.SendMessage(tb));

                }
            }
            catch (Exception ex)
            {
                _Ilog.Error(ex.Message, ex);
                throw ex;
            }
            return Json(result);
        }

    }
}