﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.OrderInfoArea.Models
{
    public class GiaitrinhCommentViewModel
    {
        public string Comments { get; set; }
        public string file { get; set; }
        public string PhanAnhType { get; set; }
        public long? id { get; set; }
    }
}