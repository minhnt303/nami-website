using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.OrderInfoArea.Models
{
    public class CreateVM
    {
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public decimal OrderId { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public decimal Amount { get; set; }
		public string OrderDescription { get; set; }
		public string BankCode { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public int Status { get; set; }
		public string OrderType { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public long IdItem { get; set; }
		public string ItemType { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public bool TrangThaiThanhToan { get; set; }

        
    }
}