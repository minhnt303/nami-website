using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.OrderInfoService.Dto;
using Nami.Service.GiaiTrinhPhanAnhService.Dto;

namespace Nami.Web.Areas.OrderInfoArea.Models
{
    public class DetailVM
    {
        public List<GiaiTrinhPhanAnhDto> objGiaiTrinh { get; set; }

        public OrderInfoDto objInfo { get; set; }
    }
}