using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.QuanLySTREETArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.QuanLySTREETService;
using Nami.Service.QuanLySTREETService.Dto;
using CommonHelper.Excel;
using CommonHelper.ObjectExtention;
using Nami.Web.Common;
using System.IO;
using System.Web.Configuration;
using CommonHelper;
using Nami.Service.DM_DulieuDanhmucService;



namespace Nami.Web.Areas.QuanLySTREETArea.Controllers
{
    public class QuanLySTREETController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "QuanLySTREET_index";
        public const string permissionCreate = "QuanLySTREET_create";
        public const string permissionEdit = "QuanLySTREET_edit";
        public const string permissionDelete = "QuanLySTREET_delete";
        public const string permissionImport = "QuanLySTREET_Inport";
        public const string permissionExport = "QuanLySTREET_export";
        public const string searchKey = "QuanLySTREETPageSearchModel";
        private readonly IQuanLySTREETService _QuanLySTREETService;
	private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;


        public QuanLySTREETController(IQuanLySTREETService QuanLySTREETService, ILog Ilog,

		IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IMapper mapper
            )
        {
            _QuanLySTREETService = QuanLySTREETService;
            _Ilog = Ilog;
            _mapper = mapper;
		_dM_DulieuDanhmucService = dM_DulieuDanhmucService;

        }
        // GET: QuanLySTREETArea/QuanLySTREET
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {

            var listData = _QuanLySTREETService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QuanLySTREETSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new QuanLySTREETSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _QuanLySTREETService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Quản lý đường phố thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<QuanLySTREET>(model);
                    _QuanLySTREETService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý đường phố", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _QuanLySTREETService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _QuanLySTREETService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _QuanLySTREETService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý đường phố", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(QuanLySTREETSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QuanLySTREETSearchDto;

            if (searchModel == null)
            {
                searchModel = new QuanLySTREETSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.MaDuongFilter = form.MaDuongFilter;
			searchModel.TenDuongFilter = form.TenDuongFilter;
			searchModel.DuongIdFilter = form.DuongIdFilter;
			searchModel.TinhIdFilter = form.TinhIdFilter;
			searchModel.HuyenIdFilter = form.HuyenIdFilter;
			searchModel.LoaiFilter = form.LoaiFilter;
			searchModel.LocationFilter = form.LocationFilter;
			searchModel.LongitudeFilter = form.LongitudeFilter;
			searchModel.LatitudeFilter = form.LatitudeFilter;
			searchModel.DeleteIdFilter = form.DeleteIdFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _QuanLySTREETService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý đường phố thành công");
            try
            {
                var user = _QuanLySTREETService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _QuanLySTREETService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _QuanLySTREETService.GetById(id);
            return View(model);
        }


        
    }
}