using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.QuanLySTREETArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		public string MaDuong { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public string TenDuong { get; set; }
		public string DuongId { get; set; }
		public string TinhId { get; set; }
		public string HuyenId { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public string Loai { get; set; }
		public string Location { get; set; }
		public string Longitude { get; set; }
		public string Latitude { get; set; }
		public long? DeleteId { get; set; }

        
    }
}