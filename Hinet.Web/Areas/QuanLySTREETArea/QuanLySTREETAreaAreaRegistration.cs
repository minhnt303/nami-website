using System.Web.Mvc;

namespace Nami.Web.Areas.QuanLySTREETArea
{
    public class QuanLySTREETAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "QuanLySTREETArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "QuanLySTREETArea_default",
                "QuanLySTREETArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}