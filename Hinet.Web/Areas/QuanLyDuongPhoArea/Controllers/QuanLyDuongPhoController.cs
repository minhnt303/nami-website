using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.QuanLyDuongPhoArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.QuanLyDuongPhoService;
using Nami.Service.QuanLyDuongPhoService.Dto;
using CommonHelper.Excel;
using CommonHelper.ObjectExtention;
using Nami.Web.Common;
using System.IO;
using System.Web.Configuration;
using CommonHelper;
using Nami.Service.DM_DulieuDanhmucService;



namespace Nami.Web.Areas.QuanLyDuongPhoArea.Controllers
{
    public class QuanLyDuongPhoController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "QuanLyDuongPho_index";
        public const string permissionCreate = "QuanLyDuongPho_create";
        public const string permissionEdit = "QuanLyDuongPho_edit";
        public const string permissionDelete = "QuanLyDuongPho_delete";
        public const string permissionImport = "QuanLyDuongPho_Inport";
        public const string permissionExport = "QuanLyDuongPho_export";
        public const string searchKey = "QuanLyDuongPhoPageSearchModel";
        private readonly IQuanLyDuongPhoService _QuanLyDuongPhoService;
	private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;


        public QuanLyDuongPhoController(IQuanLyDuongPhoService QuanLyDuongPhoService, ILog Ilog,

		IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IMapper mapper
            )
        {
            _QuanLyDuongPhoService = QuanLyDuongPhoService;
            _Ilog = Ilog;
            _mapper = mapper;
		_dM_DulieuDanhmucService = dM_DulieuDanhmucService;

        }
        // GET: QuanLyDuongPhoArea/QuanLyDuongPho
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {

            var listData = _QuanLyDuongPhoService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QuanLyDuongPhoSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new QuanLyDuongPhoSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _QuanLyDuongPhoService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Quản lý đường phố thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<QuanLyDuongPho>(model);
                    _QuanLyDuongPhoService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý đường phố", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _QuanLyDuongPhoService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _QuanLyDuongPhoService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _QuanLyDuongPhoService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý đường phố", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(QuanLyDuongPhoSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QuanLyDuongPhoSearchDto;

            if (searchModel == null)
            {
                searchModel = new QuanLyDuongPhoSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.MaDuongFilter = form.MaDuongFilter;
			searchModel.TenDuongFilter = form.TenDuongFilter;
			searchModel.DuongIdFilter = form.DuongIdFilter;
			searchModel.TinhIdFilter = form.TinhIdFilter;
			searchModel.HuyenIdFilter = form.HuyenIdFilter;
			searchModel.LoaiFilter = form.LoaiFilter;
			searchModel.LocationFilter = form.LocationFilter;
			searchModel.LongitudeFilter = form.LongitudeFilter;
			searchModel.LatitudeFilter = form.LatitudeFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _QuanLyDuongPhoService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý đường phố thành công");
            try
            {
                var user = _QuanLyDuongPhoService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _QuanLyDuongPhoService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _QuanLyDuongPhoService.GetById(id);
            return View(model);
        }


        
    }
}