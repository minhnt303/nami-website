using System.Web.Mvc;

namespace Nami.Web.Areas.QuanLyDuongPhoArea
{
    public class QuanLyDuongPhoAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "QuanLyDuongPhoArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "QuanLyDuongPhoArea_default",
                "QuanLyDuongPhoArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}