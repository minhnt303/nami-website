using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.ServicesInfoArea.Models
{
    public class EditVM
    {
		public long Id { get; set; }
		public string ServicesName { get; set; }
		public int? Price { get; set; }
		public string UnitsCode { get; set; }
		public string Description { get; set; }

        
    }
}