using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;

namespace Nami.Web.Areas.ServicesInfoArea.Models
{
    public class DetailVM
    {
       public ServicesInfo objInfo { get; set; }
    }
}