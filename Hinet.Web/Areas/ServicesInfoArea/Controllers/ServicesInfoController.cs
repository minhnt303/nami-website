using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.ServicesInfoArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.ServicesInfoService;
using Nami.Service.ServicesInfoService.Dto;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.DM_DulieuDanhmucService.DTO;
using Nami.Service.DM_NhomDanhmucService;
using Nami.Service.DM_NhomDanhmucService.DTO;


namespace Nami.Web.Areas.ServicesInfoArea.Controllers
{
    public class ServicesInfoController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "ServicesInfo_index";
        public const string permissionCreate = "ServicesInfo_create";
        public const string permissionEdit = "ServicesInfo_edit";
        public const string permissionDelete = "ServicesInfo_delete";
        public const string permissionImport = "ServicesInfo_Inport";
        public const string permissionExport = "ServicesInfo_export";
        public const string searchKey = "ServicesInfoPageSearchModel";
        private readonly IServicesInfoService _ServicesInfoService;
        private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        private readonly IDM_NhomDanhmucService _dM_NhomDanhmucService;

        public ServicesInfoController(IServicesInfoService ServicesInfoService, ILog Ilog,
            IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IDM_NhomDanhmucService dM_NhomDanhmucService,
            IMapper mapper
            )
        {
            _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _dM_NhomDanhmucService = dM_NhomDanhmucService;
            _ServicesInfoService = ServicesInfoService;
            _Ilog = Ilog;
            _mapper = mapper;

        }
        // GET: ServicesInfoArea/ServicesInfo
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var listData = _ServicesInfoService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as ServicesInfoSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new ServicesInfoSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _ServicesInfoService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();
            var dropdownListDonVi = _dM_DulieuDanhmucService.GetDropdownlist("DON_VI", "Code");
            ViewBag.dropdownListDonVi = dropdownListDonVi;
            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Dịch vụ thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<ServicesInfo>(model);
                    _ServicesInfoService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Dịch vụ", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _ServicesInfoService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _ServicesInfoService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _ServicesInfoService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Dịch vụ", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(ServicesInfoSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as ServicesInfoSearchDto;

            if (searchModel == null)
            {
                searchModel = new ServicesInfoSearchDto();
                searchModel.pageSize = 20;
            }

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _ServicesInfoService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Dịch vụ thành công");
            try
            {
                var user = _ServicesInfoService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _ServicesInfoService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _ServicesInfoService.GetById(id);
            return View(model);
        }

        
    }
}