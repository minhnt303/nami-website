using System.Web.Mvc;

namespace Nami.Web.Areas.ServicesInfoArea
{
    public class ServicesInfoAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ServicesInfoArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ServicesInfoArea_default",
                "ServicesInfoArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}