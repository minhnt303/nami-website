using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.QLTuVanArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.QLTuVanService;
using Nami.Service.QLTuVanService.Dto;
using CommonHelper.Excel;
using CommonHelper.ObjectExtention;
using Nami.Web.Common;
using System.IO;
using System.Web.Configuration;
using CommonHelper;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.QLCauHoiService;


namespace Nami.Web.Areas.QLTuVanArea.Controllers
{
    public class QLTuVanController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "QLTuVan_index";
        public const string permissionCreate = "QLTuVan_create";
        public const string permissionEdit = "QLTuVan_edit";
        public const string permissionDelete = "QLTuVan_delete";
        public const string permissionImport = "QLTuVan_Inport";
        public const string permissionExport = "QLTuVan_export";
        public const string searchKey = "QLTuVanPageSearchModel";
        private readonly IQLTuVanService _QLTuVanService;
	private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        private readonly IQLCauHoiService _qLCauHoiService;

        public QLTuVanController(IQLTuVanService QLTuVanService, ILog Ilog,
            IQLCauHoiService qLCauHoiService,
        IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IMapper mapper
            )
        {
            _QLTuVanService = QLTuVanService;
            _Ilog = Ilog;
            _mapper = mapper;
		_dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _qLCauHoiService = qLCauHoiService;
        }
        // GET: QLTuVanArea/QLTuVan
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {

            var listData = _QLTuVanService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLTuVanSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new QLTuVanSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _QLTuVanService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();
            ViewBag.dropdownlistChuDe = _dM_DulieuDanhmucService.GetDropdownlistId(DanhMucConstant.CHUDETINTUC, null);
            ViewBag.dropdownlistCauHoi = _qLCauHoiService.GetDropdown("CauHoi", "Id", null);
            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult Create(CreateVM model, HttpPostedFileBase file)
        {
            ViewBag.dropdownlistChuDe = _dM_DulieuDanhmucService.GetDropdownlistId(DanhMucConstant.CHUDETINTUC, null);
            ViewBag.dropdownlistCauHoi = _qLCauHoiService.GetDropdown("CauHoi", "Id", null);
            var result = new JsonResultBO(true, "Tạo Quản lý tin tức thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<QLTuVan>(model);
                    if (file != null && file.ContentLength > 0)
                    {
                        string fileDinhKem = Path.GetExtension(file.FileName);
                        List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg", ".gif" };
                        if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                        {
                            result.MessageFail("Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                            return Json(result);
                        }
                        var resultUpload = UploadProvider.SaveFile(file, null, UploadProvider.ListExtensionCommonImage, UploadProvider.MaxSizeCommon, "Uploads/LinkAnhTuVan/", HostingEnvironment.MapPath("/"));
                        if (resultUpload.status)
                        {
                            EntityModel.ImageLink = resultUpload.path;
                        }
                        else
                        {
                            result.MessageFail(resultUpload.message);
                            return Json(result);
                        }
                    }
                    EntityModel.SlugTitle = EntityModel.Name.SlugTitleName();
                    _QLTuVanService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Quản lý tin tức", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _QLTuVanService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }
            ViewBag.dropdownlistChuDe = _dM_DulieuDanhmucService.GetDropdownlistId(DanhMucConstant.CHUDETINTUC, obj.ChuDeId.ToString());
            ViewBag.dropdownlistCauHoi = _qLCauHoiService.GetDropdown("CauHoi", "Id", obj.IdCauHoi);
            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult Edit(EditVM model, HttpPostedFileBase file)
        {
            var obj = _QLTuVanService.GetById(model.Id);
            if (obj == null)
            {
                throw new Exception("Không tìm thấy thông tin");
            }

            ViewBag.dropdownlistChuDe = _dM_DulieuDanhmucService.GetDropdownlistId(DanhMucConstant.CHUDETINTUC, obj.ChuDeId.ToString());
            ViewBag.dropdownlistCauHoi = _qLCauHoiService.GetDropdown("CauHoi", "Id", obj.IdCauHoi);
            var result = new JsonResultBO(true);
            try
            {
                #region Kiểm tra valid tài liệu đính kèm
                if (file != null && file.ContentLength > 0)
                {
                    string fileDinhKem = Path.GetExtension(file.FileName);
                    List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg", ".gif" };
                    if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                    {
                        result.MessageFail("Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                        return Json(result);
                    }
                }
                #endregion
                if (ModelState.IsValid)
                {

                    obj= _mapper.Map(model, obj);
                    if (file != null && file.ContentLength > 0)
                    {
                        string fileDinhKem = Path.GetExtension(file.FileName);
                        List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg" };
                        if (!ExtensionSupport.Contains(fileDinhKem.ToLower()))
                        {
                            result.MessageFail("Vui lòng nhập đúng định dạng tài liệu đính kèm bao gồm " + UploadProvider.ListExtensionCommonImage);
                            return Json(result);
                        }
                        var resultUpload = UploadProvider.SaveFile(file, null, UploadProvider.ListExtensionCommon, UploadProvider.MaxSizeCommon, "Uploads/LinkAnhTinTuc/", HostingEnvironment.MapPath("/"));
                        if (resultUpload.status)
                        {
                            obj.ImageLink = resultUpload.path;
                        }
                        else
                        {
                            result.MessageFail(resultUpload.message);
                            return Json(result);
                        }
                    }
                    if (string.IsNullOrEmpty(obj.SlugTitle))
                    {
                        obj.SlugTitle = model.Name.SlugTitleName();
                    }
                    _QLTuVanService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Quản lý tin tức", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(QLTuVanSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLTuVanSearchDto;

            if (searchModel == null)
            {
                searchModel = new QLTuVanSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.ImageLinkFilter = form.ImageLinkFilter;
			searchModel.NameFilter = form.NameFilter;
			searchModel.NamePhuFilter = form.NamePhuFilter;
			searchModel.DescriptionFilter = form.DescriptionFilter;
			searchModel.StatusFilter = form.StatusFilter;
			searchModel.AuthorFilter = form.AuthorFilter;
			searchModel.ChuDeIdFilter = form.ChuDeIdFilter;
			searchModel.IdCauHoiFilter = form.IdCauHoiFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _QLTuVanService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Quản lý tin tức thành công");
            try
            {
                var user = _QLTuVanService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _QLTuVanService.SoftDelete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _QLTuVanService.GetDtoById(id);
            return View(model);
        }
	//[PermissionAccess(Code = permissionImport)]
        public FileResult ExportExcel()
        {
            var searchModel = SessionManager.GetValue(searchKey) as QLTuVanSearchDto;
            var data = _QLTuVanService.GetDaTaByPage(searchModel).ListItem;
		var dataExport = _mapper.Map<List<QLTuVanExportDto>>(data);
            var fileExcel = ExportExcelV2Helper.Export<QLTuVanExportDto>(dataExport);
            return File(fileExcel, "application/octet-stream", "QLTuVan.xlsx");
        }

        
    }
}