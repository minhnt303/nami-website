using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.QLTuVanService.Dto;

namespace Nami.Web.Areas.QLTuVanArea.Models
{
    public class DetailVM
    {
       public QLTuVanDto objInfo { get; set; }
    }
}