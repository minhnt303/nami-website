using System.Web.Mvc;

namespace Nami.Web.Areas.QLTuVanArea
{
    public class QLTuVanAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "QLTuVanArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "QLTuVanArea_default",
                "QLTuVanArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}