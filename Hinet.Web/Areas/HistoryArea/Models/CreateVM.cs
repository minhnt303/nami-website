using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.HistoryArea.Models
{
    public class CreateVM
    {
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public int IdItem { get; set; }
		public string Name { get; set; }
		public string TypeItem { get; set; }
		public string Note { get; set; }
		public string Comment { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public int LogId { get; set; }

        
    }
}