using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.HistoryArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.HistoryService;
using Nami.Service.HistoryService.Dto;



namespace Nami.Web.Areas.HistoryArea.Controllers
{
    public class HistoryController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "History_index";
        public const string permissionCreate = "History_create";
        public const string permissionEdit = "History_edit";
        public const string permissionDelete = "History_delete";
        public const string permissionImport = "History_Inport";
        public const string permissionExport = "History_export";
        public const string searchKey = "HistoryPageSearchModel";
        private readonly IHistoryService _HistoryService;


        public HistoryController(IHistoryService HistoryService, ILog Ilog,

            IMapper mapper
            )
        {
            _HistoryService = HistoryService;
            _Ilog = Ilog;
            _mapper = mapper;

        }
        // GET: HistoryArea/History
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var listData = _HistoryService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as HistorySearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new HistorySearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _HistoryService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[ValidateInput(false)]
        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Dữ liệu nhật ký thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<History>(model);
                    _HistoryService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Dữ liệu nhật ký", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _HistoryService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
[ValidateInput(false)]
        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _HistoryService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _HistoryService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Dữ liệu nhật ký", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(HistorySearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as HistorySearchDto;

            if (searchModel == null)
            {
                searchModel = new HistorySearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.NameFilter = form.NameFilter;
			searchModel.TypeItemFilter = form.TypeItemFilter;
			searchModel.NoteFilter = form.NoteFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _HistoryService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Dữ liệu nhật ký thành công");
            try
            {
                var user = _HistoryService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _HistoryService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _HistoryService.GetById(id);
            return View(model);
        }

        
    }
}