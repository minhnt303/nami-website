using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.XAArea.Models
{
    public class CreateVM
    {
		public string MaXa { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public string TenXa { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public string Loai { get; set; }
		public string Location { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public int HuyenId { get; set; }

        
    }
}