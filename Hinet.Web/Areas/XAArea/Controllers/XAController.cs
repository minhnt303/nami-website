using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.XAArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.XAService;
using Nami.Service.XAService.Dto;
using Nami.Service.HUYENService;

namespace Nami.Web.Areas.XAArea.Controllers
{
    public class XAController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "XA_index";
        public const string permissionCreate = "XA_create";
        public const string permissionEdit = "XA_edit";
        public const string permissionDelete = "XA_delete";
        public const string permissionImport = "XA_Inport";
        public const string permissionExport = "XA_export";
        public const string searchKey = "XAPageSearchModel";
        private readonly IXAService _XAService;
        private readonly IHUYENService _HUYENService;


        public XAController(IXAService XAService, ILog Ilog,
            IHUYENService HUYENService,
        IMapper mapper
            )
        {
            _XAService = XAService;
            _Ilog = Ilog;
            _mapper = mapper;
            _HUYENService = HUYENService;

        }
        // GET: XAArea/XA
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index(string id)
        {
            ViewBag.Huyen = _HUYENService.GetById(id.ToIntOrZero());
            var listData = _XAService.GetDaTaByPage(ViewBag.Huyen.MaHuyen, null);
            SessionManager.SetValue(searchKey, null);
            
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(string huyenid, int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as XASearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new XASearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            ViewBag.Huyen = _HUYENService.GetById(huyenid.ToIntOrZero());
            var data = _XAService.GetDaTaByPage(ViewBag.Huyen.MaHuyen, searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Xã/Phường thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<XA>(model);
                    _XAService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Xã/Phường", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(int id)
        {
            var myModel = new EditVM();

            var obj = _XAService.GetById(id);
            if (obj == null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _XAService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj = _mapper.Map(model, obj);
                    _XAService.Update(obj);

                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Xã/Phường", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(string huyenId, XASearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as XASearchDto;

            if (searchModel == null)
            {
                searchModel = new XASearchDto();
                searchModel.pageSize = 20;
            }
            searchModel.MaXaFilter = form.MaXaFilter;
            searchModel.TenXaFilter = form.TenXaFilter;
            searchModel.LoaiFilter = form.LoaiFilter;

            SessionManager.SetValue((searchKey), searchModel);
            var Huyen = _HUYENService.GetById(huyenId.ToIntOrZero());
            var data = _XAService.GetDaTaByPage(Huyen.MaHuyen, searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            var result = new JsonResultBO(true, "Xóa Xã/Phường thành công");
            try
            {
                var user = _XAService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _XAService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }


        public ActionResult Detail(int id)
        {
            var model = new DetailVM();
            model.objInfo = _XAService.GetById(id);
            return View(model);
        }


    }
}