using AutoMapper;
using CommonHelper.String;
using CommonHelper.Upload;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Web.Areas.DepositsInfoArea.Models;
using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Nami.Web.Filters;
using Nami.Service.DepositsInfoService;
using Nami.Service.DepositsInfoService.Dto;



namespace Nami.Web.Areas.DepositsInfoArea.Controllers
{
    public class DepositsInfoController : BaseController
    {
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "DepositsInfo_index";
        public const string permissionCreate = "DepositsInfo_create";
        public const string permissionEdit = "DepositsInfo_edit";
        public const string permissionDelete = "DepositsInfo_delete";
        public const string permissionImport = "DepositsInfo_Inport";
        public const string permissionExport = "DepositsInfo_export";
        public const string searchKey = "DepositsInfoPageSearchModel";
        private readonly IDepositsInfoService _DepositsInfoService;


        public DepositsInfoController(IDepositsInfoService DepositsInfoService, ILog Ilog,

            IMapper mapper
            )
        {
            _DepositsInfoService = DepositsInfoService;
            _Ilog = Ilog;
            _mapper = mapper;

        }
        // GET: DepositsInfoArea/DepositsInfo
        //[PermissionAccess(Code = permissionIndex)]
        public ActionResult Index()
        {
            var listData = _DepositsInfoService.GetDaTaByPage(null);
            SessionManager.SetValue(searchKey, null);
            return View(listData);
        }

        [HttpPost]
        public JsonResult getData(int indexPage, string sortQuery, int pageSize)
        {
            var searchModel = SessionManager.GetValue(searchKey) as DepositsInfoSearchDto;
            if (!string.IsNullOrEmpty(sortQuery))
            {
                if (searchModel == null)
                {
                    searchModel = new DepositsInfoSearchDto();
                }
                searchModel.sortQuery = sortQuery;
                if (pageSize > 0)
                {
                    searchModel.pageSize = pageSize;
                }
                SessionManager.SetValue(searchKey, searchModel);
            }
            var data = _DepositsInfoService.GetDaTaByPage(searchModel, indexPage, pageSize);
            return Json(data);
        }
        public PartialViewResult Create()
        {
            var myModel = new CreateVM();

            return PartialView("_CreatePartial", myModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(CreateVM model)
        {
            var result = new JsonResultBO(true, "Tạo Đặt cọc thành công");
            try
            {
                if (ModelState.IsValid)
                {
                    var EntityModel = _mapper.Map<DepositsInfo>(model);
                    _DepositsInfoService.Create(EntityModel);

                }

            }
            catch (Exception ex)
            {
                result.MessageFail(ex.Message);
                _Ilog.Error("Lỗi tạo mới Đặt cọc", ex);
            }
            return Json(result);
        }

        public PartialViewResult Edit(long id)
        {
            var myModel = new EditVM();

            var obj= _DepositsInfoService.GetById(id);
            if (obj== null)
            {
                throw new HttpException(404, "Không tìm thấy thông tin");
            }

            myModel = _mapper.Map(obj, myModel);
            return PartialView("_EditPartial", myModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EditVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {

                    var obj = _DepositsInfoService.GetById(model.Id);
                    if (obj == null)
                    {
                        throw new Exception("Không tìm thấy thông tin");
                    }

                    obj= _mapper.Map(model, obj);
                    _DepositsInfoService.Update(obj);
                    
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Không cập nhật được";
                _Ilog.Error("Lỗi cập nhật thông tin Đặt cọc", ex);
            }
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult searchData(DepositsInfoSearchDto form)
        {
            var searchModel = SessionManager.GetValue(searchKey) as DepositsInfoSearchDto;

            if (searchModel == null)
            {
                searchModel = new DepositsInfoSearchDto();
                searchModel.pageSize = 20;
            }
			searchModel.DepositDateFilter = form.DepositDateFilter;
			searchModel.ExpirationDateFilter = form.ExpirationDateFilter;
			searchModel.DepositsMoneyFilter = form.DepositsMoneyFilter;

            SessionManager.SetValue((searchKey) , searchModel);

            var data = _DepositsInfoService.GetDaTaByPage(searchModel, 1, searchModel.pageSize);
            return Json(data);
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            var result = new JsonResultBO(true, "Xóa Đặt cọc thành công");
            try
            {
                var user = _DepositsInfoService.GetById(id);
                if (user == null)
                {
                    throw new Exception("Không tìm thấy thông tin để xóa");
                }
                _DepositsInfoService.Delete(user);
            }
            catch (Exception ex)
            {
                result.MessageFail("Không thực hiện được");
                _Ilog.Error("Lỗi khi xóa tài khoản id=" + id, ex);
            }
            return Json(result);
        }

        
        public ActionResult Detail(long id)
        {
            var model = new DetailVM();
            model.objInfo = _DepositsInfoService.GetById(id);
            return View(model);
        }

        
    }
}