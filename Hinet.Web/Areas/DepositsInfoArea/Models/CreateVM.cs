using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nami.Web.Areas.DepositsInfoArea.Models
{
    public class CreateVM
    {
		public long? BuildingsId { get; set; }
		public long? RoomsId { get; set; }
		public long? RentersId { get; set; }
		public DateTime? DepositDate { get; set; }
		[Required(ErrorMessage = "Vui lòng nhập thông tin này")]
		public DateTime ExpirationDate { get; set; }
		public int? DepositsMoney { get; set; }
		public string Note { get; set; }

        
    }
}