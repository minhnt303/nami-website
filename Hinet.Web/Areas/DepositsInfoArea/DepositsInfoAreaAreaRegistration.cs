using System.Web.Mvc;

namespace Nami.Web.Areas.DepositsInfoArea
{
    public class DepositsInfoAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DepositsInfoArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DepositsInfoArea_default",
                "DepositsInfoArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}