﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nami.Service.AppUserService.Dto;
using Newtonsoft.Json;

namespace Nami.Web.Core
{
    public static class LinkRoleExtention
    {
        public static MvcHtmlString LinkRole(this HtmlHelper helper, string id, string Url, string Name, string Role = "", string strClass = "", string title = "")
        {
            var strUrl = Url;

            var builder = new TagBuilder("a");
            builder.GenerateId(id);
            builder.InnerHtml = Name;
            builder.AddCssClass(strClass);
            builder.MergeAttribute("href", strUrl);
            builder.MergeAttribute("class", strClass);
            builder.MergeAttribute("title", title);

            return new MvcHtmlString(builder.ToString());

        }
        public static MvcHtmlString LinkRole(this HtmlHelper helper, string Url, string Name, string Role = "", string strClass = "", string title = "")
        {

            var CurrentUser = SessionManager.GetUserInfo() as UserDto;
            //Nếu Role - Mã thao tác tồn tại thì xét quyền truy cập
            if (!string.IsNullOrEmpty(Role))
            {
                //Nếu Không tồn tại user hoặc User không có bất cứ thao tác nào
                if (CurrentUser == null || CurrentUser.ListOperations == null)
                {
                    return null;
                }
                else
                {
                    //Trường hợp tồn tại thao tác xét Role có nằm trong danh sách thao tác người dùng có không ?
                    if (!CurrentUser.ListOperations.Any(x => x.Code.Equals(Role)))
                    {
                        return null;
                    }
                }
            }

            var strUrl = Url;

            var builder = new TagBuilder("a");
            builder.InnerHtml = Name;
            builder.MergeAttribute("href", strUrl);
            builder.MergeAttribute("class", strClass);
            builder.MergeAttribute("title", title);

            return new MvcHtmlString(builder.ToString());

        }

        public static MvcHtmlString LinkRole(this HtmlHelper helper, string Url, string Name, string Role = "", string strClass = "", string[] Param = null, string title = "")
        {
            var CurrentUser = SessionManager.GetUserInfo() as UserDto;
            //Nếu Role - Mã thao tác tồn tại thì xét quyền truy cập
            if (!string.IsNullOrEmpty(Role))
            {
                //Nếu Không tồn tại user hoặc User không có bất cứ thao tác nào
                if (CurrentUser == null || CurrentUser.ListOperations == null)
                {
                    return null;
                }
                else
                {
                    //Trường hợp tồn tại thao tác xét Role có nằm trong danh sách thao tác người dùng có không ?
                    if (!CurrentUser.ListOperations.Any(x => x.Code.Equals(Role)))
                    {
                        return null;
                    }
                }
            }

            var strUrl = Url;
            strUrl = strUrl + "(";
            //Nếu liên kết là Method JavaScript 

            if (Param != null && Param.Any())
            {
                strUrl += string.Join(",", Param);
            }

            strUrl += ")";
            var builder = new TagBuilder("a");
            builder.InnerHtml = Name;
            builder.AddCssClass(strClass);
            builder.MergeAttribute("href", "javascript:void(0)");
            builder.MergeAttribute("title", title);
            builder.MergeAttribute("onClick", strUrl);

            return new MvcHtmlString(builder.ToString());
        }

        //public static MvcHtmlString LinkRole(this HtmlHelper helper, string id, string Url, string Name, string Role = "", string strClass = "", string[] Param = null, bool IsJavaScriptFunction = false)
        //{
        //    var CurrentUser = SessionManager.GetUserInfo() as UserInfoBO;
        //    //Nếu Role - Mã thao tác tồn tại thì xét quyền truy cập
        //    if (!string.IsNullOrEmpty(Role))
        //    {
        //        //Nếu Không tồn tại user hoặc User không có bất cứ thao tác nào
        //        if (CurrentUser == null || CurrentUser.ListThaoTac == null)
        //        {
        //            return null;
        //        }
        //        else
        //        {
        //            //Trường hợp tồn tại thao tác xét Role có nằm trong danh sách thao tác người dùng có không ?
        //            if (!CurrentUser.ListThaoTac.Any(x => x.MA_THAOTAC.Equals(Role)))
        //            {
        //                return null;
        //            }
        //        }
        //    }

        //    var strUrl = Url;
        //    //Nếu liên kết là Method JavaScript 
        //    if (IsJavaScriptFunction)
        //    {
        //        strUrl = strUrl + "(";
        //        if (Param != null && Param.Any())
        //        {
        //            strUrl += string.Join(",", Param);
        //        }
        //        strUrl += ")";
        //    }
        //    var builder = new TagBuilder("a");
        //    builder.GenerateId(id);
        //    builder.InnerHtml = Name;
        //    builder.AddCssClass(strClass);
        //    if (IsJavaScriptFunction)
        //    {
        //        builder.MergeAttribute("href", "javascript:void(0)");
        //        builder.MergeAttribute("onClick", strUrl);

        //    }
        //    else
        //    {
        //        builder.MergeAttribute("href", strUrl);
        //    }

        //    return new MvcHtmlString(builder.ToString());
        //}

        public static MvcHtmlString GetRole(this HtmlHelper helper)
        {
            var CurrentUser = SessionManager.GetUserInfo() as UserDto;
            if (CurrentUser == null || CurrentUser.ListOperations == null)
            {
                return null;
            }
            else
            {
                //Trường hợp tồn tại thao tác xét Role có nằm trong danh sách thao tác người dùng có không ?
                var strRole = JsonConvert.SerializeObject(CurrentUser.ListOperations.Select(x => x.Code).ToList());
                return new MvcHtmlString(strRole);
            }
            return null;
        }
        public static bool HasRole(this HtmlHelper helper, string role)
        {
            if (string.IsNullOrEmpty(role))
            {
                return false;
            }

            var CurrentUser = SessionManager.GetUserInfo() as UserDto;
            if (CurrentUser == null || CurrentUser.ListOperations == null)
            {
                return false;
            }
            else
            {
                if (CurrentUser.ListOperations.Any(x => x.Code.Equals(role)))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
