﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nami.Web.Core
{
    public class RepositoryConnectUser
    {
        private static List<UserConncect> UserEntities { get; set; }

        static RepositoryConnectUser()
        {
            UserEntities = new List<UserConncect>();
        }
        public static UserConncect Find(long? id)
        {
            return UserEntities.Where(x => x.UserId == id).FirstOrDefault();
        }
        public static List<string> All()
        {
            var listResult = new List<string>();

            var listUser= UserEntities.Where(x=>x.LstConnection!=null).Select(x=>x.LstConnection).ToList();
            foreach (var item in listUser)
            {
                if (item.Any())
                {
                    listResult.AddRange(item);
                }
            }
            return listResult;
        }
        public static void Save(long idUser, string type, string ConnectionId)
        {
            var user = Find(idUser);
            if (user == null)
            {
                user = new UserConncect();
                user.UserId = idUser;
                user.TypeAccount = type;
                user.LstConnection.Add(ConnectionId);
                UserEntities.Add(user);
            }
            else
            {
                var isExist = user.LstConnection.Where(x => x.Equals(ConnectionId)).Any();
                if (!isExist)
                {
                    user.LstConnection.Add(ConnectionId);
                }
            }
        }
        public static void Remove(string connectId)
        {
            foreach (var item in UserEntities)
            {
                if (item.LstConnection != null && item.LstConnection.Any() && item.LstConnection.Contains(connectId))
                {
                    item.LstConnection.Remove(connectId);
                }
            }
        }
        public static void Remove(long idUser)
        {
            var user = Find(idUser);
            if (user != null)
            {
                UserEntities.Remove(user);
            }
        }
        public static void Remove(long idUser, string ConnectionId)
        {
            var user = Find(idUser);
            if (user != null)
            {
                var item = user.LstConnection.Where(x => x.Equals(ConnectionId)).FirstOrDefault();
                if (item != null)
                {
                    user.LstConnection.Remove(item);
                }
            }
        }
    }
    public class UserConncect
    {
        public long? UserId { get; set; }
        public string TypeAccount { get; set; }
        public List<string> LstConnection { get; set; }
        public UserConncect()
        {
            LstConnection = new List<string>();
        }
    }
}