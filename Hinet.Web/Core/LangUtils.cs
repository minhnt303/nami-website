﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Web.LocationResouces;

namespace Nami
{
    public class LangUtils
    {
        public static string Get(string key)
        {
            var manager=ResourceWeb.ResourceManager;
            return manager.GetString(key);
        }
       

    }
}