﻿using AutoMapper;
using Nami.Model.IdentityEntities;
using Nami.Service.AppUserService;
using Nami.Service.Constant;
using Nami.Service.OperationService;
using Nami.Web.Filters;
using Nami.Web.Models;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Nami.Web.Controllers
{

    [Authorize]
    public class AccountController : EndUserController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private readonly IMapper _mapper;
        private readonly ILog _Ilog;
        private readonly IAppUserService _appUserService;
        private readonly IOperationService _operationService;

        public AccountController(
            IAppUserService appUserService,
            IOperationService operationService,
             ILog Ilog,
             IMapper mapper)
        {
            _operationService = operationService;
            _appUserService = appUserService;
            _Ilog = Ilog;
            _mapper = mapper;
        }


        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult LoginAccount()
        {
            var myModel = new LoginViewModel();
            return PartialView("_LoginAccountPartial2", myModel);
        }

        public PartialViewResult RegisterAccount2()
        {
            var myModel = new RegisterViewModel();
            return PartialView("_RegisterAccount2Partial", myModel);
        }

        public PartialViewResult RegisterAccount()
        {
            var myModel = new RegisterViewModel();
            return PartialView("_RegisterAccountPartial", myModel);
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterAccount(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_appUserService.CheckExistEmail(model.Email))
                {
                    ModelState.AddModelError("Email", "Email đã được sử dụng vui lòng chọn Email khác.");
                }
                if (_appUserService.CheckExistUserName(model.UserName))
                {
                    ModelState.AddModelError("UserName", "Tên đăng nhập đã được sử dụng vui lòng chọn Tên đăng nhập khác.");
                }
            }
            if (ModelState.IsValid)
            {
                var user = new AppUser()
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    TypeAccount = AccountTypeConstant.BussinessUser,
                    //TypeOrganization = OrganizationTypeConstant.Personal,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    CreatedBy = model.UserName
                };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var result2 = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, true, shouldLockout: false);
                    switch (result2)
                    {
                        case SignInStatus.Success:
                            //return RedirectToLocal(returnUrl);
                            var userId = SignInManager.AuthenticationManager.AuthenticationResponseGrant.Identity.GetUserId<long>();

                            var userDto = _appUserService.GetDtoById(userId);
                            //if (userDto.TypeAccount != AccountTypeConstant.NguoiDung)
                            //{
                            //    //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                            //    ModelState.AddModelError("", "Thông tin đăng nhập không đúng");
                            //    TempData["falselogin"] = true;
                            //    //return View(model);
                            //    return RedirectToAction("Index", "Home");
                            //}
                            if (userDto.TypeAccount == AccountTypeConstant.NguoiDung)
                            {
                                SessionManager.SetValue(SessionManager.USER_INFO, userDto);
                                var listOperation = _operationService.GetListOperationOfUser(userId);
                                //_cacheDatabase.StringSet("Operation:" + userId, JsonConvert.SerializeObject(listOperation));
                                SessionManager.SetValue(SessionManager.LIST_PERMISSTION, listOperation);
                                return RedirectToAction("Index", "EndUserInfo");
                            }
                            else if (userDto.TypeAccount == AccountTypeConstant.BussinessUser || userDto.TypeAccount == AccountTypeConstant.ChuNha)
                            {
                                SessionManager.SetValue(SessionManager.USER_INFO, userDto);
                                var listOperation = _operationService.GetListOperationOfUser(userId);
                                //_cacheDatabase.StringSet("Operation:" + userId, JsonConvert.SerializeObject(listOperation));
                                SessionManager.SetValue(SessionManager.LIST_PERMISSTION, listOperation);
                                return RedirectToAction("Index", "Dashboard", new { area = "DashboardArea" });
                            }
                            else
                            {
                                //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                ModelState.AddModelError("", "Thông tin đăng nhập không đúng");
                                TempData["falselogin"] = true;
                                //return View(model);
                                return RedirectToAction("Index", "Home");
                            }
                        case SignInStatus.Failure:
                        default:
                            ModelState.AddModelError("", "Không đăng ký được tài khoản");
                            //return View(model);
                            return RedirectToAction("Index", "Home");
                    }
                }
                ModelState.AddModelError("", "Không đăng ký được tài khoản");
                TempData["falselogin"] = true;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Không đăng ký được tài khoản");
                TempData["falselogin"] = true;
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LoginAccount(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    //return RedirectToLocal(returnUrl);
                    var userId = SignInManager.AuthenticationManager.AuthenticationResponseGrant.Identity.GetUserId<long>();

                    var userDto = _appUserService.GetDtoById(userId);
                    //if (userDto.TypeAccount != AccountTypeConstant.NguoiDung)
                    //{
                    //    //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    //    ModelState.AddModelError("", "Thông tin đăng nhập không đúng");
                    //    TempData["falselogin"] = true;
                    //    //return View(model);
                    //    return RedirectToAction("Index", "Home");
                    //}
                    if (userDto.TypeAccount == AccountTypeConstant.NguoiDung)
                    {
                        SessionManager.SetValue(SessionManager.USER_INFO, userDto);
                        var listOperation = _operationService.GetListOperationOfUser(userId);
                        //_cacheDatabase.StringSet("Operation:" + userId, JsonConvert.SerializeObject(listOperation));
                        SessionManager.SetValue(SessionManager.LIST_PERMISSTION, listOperation);
                        return RedirectToAction("Index", "EndUserInfo");
                    }
                    else if (userDto.TypeAccount == AccountTypeConstant.BussinessUser || userDto.TypeAccount == AccountTypeConstant.ChuNha)
                    {
                        SessionManager.SetValue(SessionManager.USER_INFO, userDto);
                        var listOperation = _operationService.GetListOperationOfUser(userId);
                        //_cacheDatabase.StringSet("Operation:" + userId, JsonConvert.SerializeObject(listOperation));
                        SessionManager.SetValue(SessionManager.LIST_PERMISSTION, listOperation);
                        return RedirectToAction("Index", "Dashboard", new { area = "DashboardArea" });
                    }
                    else
                    {
                        //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                        ModelState.AddModelError("", "Thông tin đăng nhập không đúng");
                        TempData["falselogin"] = true;
                        //return View(model);
                        return RedirectToAction("Index", "Home");
                    }
                case SignInStatus.LockedOut:
                    //return View("Lockout");
                    return RedirectToAction("Index", "Home");
                case SignInStatus.RequiresVerification:
                    //return RedirectToAction("SendCode", new { ReturnUrl = "/index", RememberMe = model.RememberMe });
                    return RedirectToAction("Index", "Home");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Thông tin đăng nhập không đúng");
                    //return View(model);
                    return RedirectToAction("Index", "Home");
            }
        }

        [AllowAnonymous]
        [HttpGet]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            SessionManager.SetValue(SessionManager.USER_INFO, null);
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

    }
}