﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Nami.Web.Models;
using Nami.Model.IdentityEntities;
using Microsoft.AspNet.Identity.EntityFramework;
using Nami.Model;
using StackExchange.Redis;
using Nami.Service.OperationService;
using Newtonsoft.Json;
using Nami.Service.Constant;
using Nami.Service.Common;
using Nami.Service.AppUserService;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.TINHService;
using Nami.Service.HUYENService;
using Nami.Service.XAService;
using System.Collections.Generic;
using Nami.Model.Entities;
using log4net;
using Nami.Web.Filters;

namespace Nami.Web.Controllers
{
    [Authorize]
    public class AccountAdminController : BaseController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        //private IDatabase _cacheDatabase;
        private IOperationService _operationService;
        private IAppUserService _appUserService;
        private IDM_DulieuDanhmucService _iDM_DuLieuDanhMucService;
        private ITINHService _iTINHService;
        private IHUYENService _iHUYENService;
        private IXAService _iXAService;
      
        private readonly ILog _Ilog;
        public AccountAdminController(
            IOperationService operationService,
            IDM_DulieuDanhmucService iDM_DuLieuDanhMucService,
            ITINHService iTINHService,
             IHUYENService iHUYENService,
             IXAService iXAService,
             
             ILog Ilog,
        IAppUserService appUserService)
        //:this(new UserManager<AppUser,long>(new UserStore<AppUser,AppRole,long,AppLogin,AppUserRole,AppClaim>(new QLNSContext())))
        {
            _operationService = operationService;
            //_cacheDatabase = cacheDatabase;
            _appUserService = appUserService;
            _iDM_DuLieuDanhMucService = iDM_DuLieuDanhMucService;
            _iTINHService = iTINHService;
            _iHUYENService = iHUYENService;
            _iXAService = iXAService;
         
            _Ilog = Ilog;
        }

        public AccountAdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    //return RedirectToLocal(returnUrl);
                    var userId = SignInManager.AuthenticationManager.AuthenticationResponseGrant.Identity.GetUserId<long>();

                    var userDto = _appUserService.GetDtoById(userId);
                    if (userDto.TypeAccount != AccountTypeConstant.BussinessUser)
                    {
                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                        ModelState.AddModelError("", "Tài khoản không phù hợp");
                        return View(model);
                    }
                    SessionManager.SetValue(SessionManager.USER_INFO, userDto);
                    var listOperation = _operationService.GetListOperationOfUser(userId);
                    //_cacheDatabase.StringSet("Operation:" + userId, JsonConvert.SerializeObject(listOperation));
                    SessionManager.SetValue(SessionManager.LIST_PERMISSTION, listOperation);
                    return RedirectToAction("Index", "Dashboard", new { area = "DashboardArea" });
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Thông tin đăng nhập không đúng");
                    return View(model);
            }
        }
      

        //
        // POST: /Account/LogOff
        [HttpGet]
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            SessionManager.SetValue(SessionManager.USER_INFO, null);
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            //return RedirectToAction("login");
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }
        /// <summary>
        /// Thay đổi ngôn ngữ
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        /// 
        [AllowAnonymous]
        public ActionResult ChangeLang(string lang, string returnUrl)
        {

            if (ConstantExtension.GetListData<LangConstant>().Contains(lang))
            {
                Response.Cookies["Language"].Value = lang;
                System.Threading.Thread.CurrentThread.CurrentCulture =
                   new System.Globalization.CultureInfo(lang);
                System.Threading.Thread.CurrentThread.CurrentUICulture =
                    new System.Globalization.CultureInfo(lang);
            }
            else
            {
                System.Threading.Thread.CurrentThread.CurrentCulture =
                    new System.Globalization.CultureInfo("");
                System.Threading.Thread.CurrentThread.CurrentUICulture =
                    new System.Globalization.CultureInfo("en");
            }
            if (string.IsNullOrEmpty(returnUrl))
            {
                return RedirectToAction("index", "home");
            }
            return Redirect(returnUrl);
        }
        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }



        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId<long>(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<long>());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return View("ActionDone", new ResultRequestPageSuccessVM {Title="Đổi mật khẩu", Message = "Thay đổi mật khẩu thành công" });
            }
            AddErrors(result);
            return View(model);
        }

    }
}