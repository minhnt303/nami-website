﻿using Nami.Service.Constant;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Web.Filters;
using Nami.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nami.Service.HUYENService;
using Nami.Model.Entities;
using AutoMapper;
using Nami.Service.XAService;
using Nami.Service.Common;
using System.Web.Helpers;
using Nami.Web.Core;
using CommonHelper.Upload;
using log4net;
using System.Web.Hosting;
using BotDetect.Web.Mvc;
using Web.Common;
using Nami.Service.TINHService;
using Nami.Service.DM_NhomDanhmucService;
using Nami.Service.BannerQuangCaoService;
using Nami.Service.QLTinTucService;
using Nami.Service.QLCauHoiService;
using Nami.Service.QLTuVanService;
using Nami.Service.BuildingsInfoService;
using Nami.Service.RoomInfoService;
using Nami.Service.QuanLyDuAnService;
namespace Nami.Web.Controllers
{
    public class HomeController : EndUserController
    {

        private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;

        private readonly IHUYENService _hUYENService;
        private readonly IXAService _xAService;
        private readonly ITINHService _tINHService;
        private readonly IDM_NhomDanhmucService _dM_NhomDanhmucService;
        private readonly IMapper _mapper;
        private readonly ILog _Ilog;
        private readonly IBannerQuangCaoService _bannerQuangCaoService;
        private readonly IQLTinTucService _qLTinTucService;
        private readonly IQLCauHoiService _qLCauHoiService;
        private readonly IQLTuVanService _qLTuVanService;
        private readonly IBuildingsInfoService _BuildingsInfoService;
        private readonly IRoomInfoService _RoomInfoService;
        private readonly IQuanLyDuAnService _QuanLyDuAnService;
        public const string searchKey = "WebsiteInfoPageSearchModel";
        public HomeController(
                IDM_DulieuDanhmucService dM_DulieuDanhmucService,
             IDM_NhomDanhmucService dM_NhomDanhmucService,
                IHUYENService hUYENService,
                IXAService xaService,
                ITINHService tINHService,
                IMapper mapper, ILog iLog,
                IBannerQuangCaoService bannerQuangCaoService,
                IQLTinTucService qLTinTucService,
                IQLCauHoiService qLCauHoiService,
                IQLTuVanService qLTuVanService,
                IBuildingsInfoService BuildingsInfoService,
                IQuanLyDuAnService QuanLyDuAnService,
                IRoomInfoService RoomInfoService
            )
        {
            
            _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _dM_NhomDanhmucService = dM_NhomDanhmucService;
            _tINHService = tINHService;
             _hUYENService = hUYENService;
            _mapper = mapper;
            _Ilog = iLog;
            _xAService = xaService;
            _bannerQuangCaoService = bannerQuangCaoService;
            _qLTinTucService = qLTinTucService;
            _qLCauHoiService = qLCauHoiService;
            _qLTuVanService = qLTuVanService;
            _BuildingsInfoService = BuildingsInfoService;
            _RoomInfoService = RoomInfoService;
            _QuanLyDuAnService = QuanLyDuAnService;
        }
        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.dropdownListTinhId = _tINHService.GetDropdown("TenTinh", "MaTinh");
            ViewBag.dropdownListHuyenId = new List<SelectListItem>();
            ViewBag.dropdownListXaId = new List<SelectListItem>();
            ViewBag.dropdownListBuilding = ConstantExtension.GetDropdownData<BuildingTypeConstant>(null);
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            ViewBag.listTinNoiBat = _qLTinTucService.GetListTinTuc(true);
            ViewBag.listTinTuc = _qLTinTucService.GetListTinTuc(false);
            ViewBag.listTuVan = _qLTuVanService.GetListTuVan();
            ViewBag.listTinRaoBuilding = _BuildingsInfoService.GetListBuildingForTrangChu(8);
            ViewBag.listTinRaoRoom = _RoomInfoService.GetListRoomByTrangChu(8);
            ViewBag.listDuAn = _QuanLyDuAnService.GetListDuAnHasImage(8);
            ViewBag.numberOfTinRaoDiaDiem1 = _BuildingsInfoService.GetDaTaByPageByTinhCode("t1", string.Empty, 1, -1).Count + _RoomInfoService.GetDaTaByPageByTinhCode("t1", string.Empty, 1, -1).Count;
            ViewBag.numberOfTinRaoDiaDiem2 = _BuildingsInfoService.GetDaTaByPageByTinhCode("t2", string.Empty, 1, -1).Count + _RoomInfoService.GetDaTaByPageByTinhCode("t2", string.Empty, 1, -1).Count;
            ViewBag.numberOfTinRaoDiaDiem3 = _BuildingsInfoService.GetDaTaByPageByTinhCode("t3", string.Empty, 1, -1).Count + _RoomInfoService.GetDaTaByPageByTinhCode("t3", string.Empty, 1, -1).Count;
            ViewBag.numberOfTinRaoDiaDiem4 = _BuildingsInfoService.GetDaTaByPageByTinhCode("t4", string.Empty, 1, -1).Count + _RoomInfoService.GetDaTaByPageByTinhCode("t4", string.Empty, 1, -1).Count;
            ViewBag.numberOfTinRaoDiaDiem5 = _BuildingsInfoService.GetDaTaByPageByTinhCode("t5", string.Empty, 1, -1).Count + _RoomInfoService.GetDaTaByPageByTinhCode("t5", string.Empty, 1, -1).Count;
            return View();
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.Message = "Cập nhật chỉ mục thành công.";
         


            return View();
        }
        [AllowAnonymous]
        public PartialViewResult SearchCommon(string key)
        {
            var listdata = LuceneSearchArticle.Search(key).Take(10).ToList();
            return PartialView(listdata);
        }
        [AllowAnonymous]
        public ActionResult UnAuthor()
        {
            return View();
        }

        [AllowAnonymous]
        public PartialViewResult UnAuthorPartial()
        {
            return PartialView("UnAuthorPartial");
        }

        [AllowAnonymous]
        public PartialViewResult TimeOutSession()
        {
            return PartialView("TimeOutSession");
        }
        [AllowAnonymous]
        public ActionResult NotFound()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult License()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult LicensePartial()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Error()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult BackHome()
        {
            if (User.Identity.IsAuthenticated)
            {
                return Redirect("/Dashboard/Index");
            }
            else
            {
                return Redirect("/Account/login");
            }
        }


        [AllowAnonymous]
        public ActionResult DetailTinRao(long ItemId, string ItemType)
        {
            var model = new DetailTinVM();
            model.ItemType = ItemType;
            model.ItemId = ItemId;
            if (ItemType == ItemTypeConstant.Building)
            {
                model.objBuilding = _BuildingsInfoService.GetDtoById(ItemId);
                model.listBuilding = _BuildingsInfoService.GetListBuildingForTrangChu(10);
            }
            else if (ItemType == ItemTypeConstant.Room)
            {
                model.objRoom = _RoomInfoService.GetDtoById(ItemId);
                model.listRoom = _RoomInfoService.GetListRoomByTrangChu(10);
            }
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            model.CurrentUserInfo = CurrentUserInfo;
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult DetailTinRaoRoom(long ItemId, string ItemType)
        {
            var model = new DetailTinVM();
            model.ItemType = ItemType;
            model.ItemId = ItemId;
            if (ItemType == ItemTypeConstant.Building)
            {
                model.objBuilding = _BuildingsInfoService.GetDtoById(ItemId);
                model.listBuilding = _BuildingsInfoService.GetListBuildingForTrangChu(10);
            }
            else if (ItemType == ItemTypeConstant.Room)
            {
                model.objRoom = _RoomInfoService.GetDtoById(ItemId);
                model.listRoom = _RoomInfoService.GetListRoomByTrangChu(10);
            }
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            model.CurrentUserInfo = CurrentUserInfo;
            return View(model);
        }


        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Search(FormCollection form)
        {
            SessionManager.SetValue("form", form);
            return View();
        }

      
        [AllowAnonymous]
        public string sendmail(string mess)
        {
            return EmailProvider.sendEmailSingle(mess, "Hello", "xulazy.play@gmail.com").ToString();
        }


        [AllowAnonymous]
        [HttpGet]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            SessionManager.SetValue(SessionManager.USER_INFO, null);
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("/Index");
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetHuyenDropdownOfTinh(string id)
        {
            var result = _hUYENService.GetDropdownOfTinh(id, null).OrderBy(x => x.Text).ToList();
            return Json(result);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetXaDropdownOfHuyen(string id)
        {
            var result = _xAService.GetDropdownOfHuyen(id, null).OrderBy(x => x.Text).ToList();
            return Json(result);
        }


        [AllowAnonymous]
        [HttpPost]
        public ActionResult SearchGuest(HomeSearchModels model)
        {
            var result = new JsonResultBO(true, "Tìm kiếm nhà thành công");
            return View("SearchGuest", model);
        }
    }
}