﻿using AutoMapper;
using Hangfire.Logging;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Web.Filters;
using Nami.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nami.Service.QLTinTucService;
using Nami.Service.BannerQuangCaoService;
using Nami.Service.QLTuVanService;
using Nami.Service.BuildingsInfoService;
using Nami.Service.RoomInfoService;

namespace Nami.Web.Controllers
{
    public class NhaChoThueTheoDiaDiemVaDuAnController : SecondaryController
    {
        private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        private readonly IBannerQuangCaoService _bannerQuangCaoService;
        private readonly IBuildingsInfoService _BuildingsInfoService;
        private readonly IRoomInfoService _RoomInfoService;

        public NhaChoThueTheoDiaDiemVaDuAnController(
           IBannerQuangCaoService bannerQuangCaoService,
           IDM_DulieuDanhmucService dM_DulieuDanhmucService,
           IBuildingsInfoService BuildingsInfoService,
           IRoomInfoService RoomInfoService
           )
        {
            _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _bannerQuangCaoService = bannerQuangCaoService;
            _BuildingsInfoService = BuildingsInfoService;
            _RoomInfoService = RoomInfoService;
        }

        // GET: TinTuc
        [AllowAnonymous]
        public ActionResult PageIndexTinRaoTheoDiaDiem(string tinhCode, int page = 1)
        {
            var model = new TinRaoViewModel();
            var TinRaoBuilding = _BuildingsInfoService.GetDaTaByPageByTinhCode(tinhCode, string.Empty, page, 10);
            model.TinRaoBuilding = TinRaoBuilding;
            var TinRaoRoom = _RoomInfoService.GetDaTaByPageByTinhCode(tinhCode, string.Empty, page, 10);
            model.TinRaoRoom = TinRaoRoom;
            model.TinhCode = tinhCode;
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return View(model);
        }


        [HttpPost]
        public PartialViewResult PanigationTinRaoTheoDiaDiemBuildings(string KeySearch, int page, string tinhCode)
        {
            var model = _BuildingsInfoService.GetDaTaByPageByTinhCode(tinhCode, KeySearch, page, 10);
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return PartialView("PanigationTinRaoTheoDiaDiemBuildings", model);
        }


        [HttpPost]
        public PartialViewResult PanigationTinRaoTheoDiaDiemRoom(string KeySearch, int page, string tinhCode)
        {
            var model = _RoomInfoService.GetDaTaByPageByTinhCode(tinhCode, KeySearch, page, 10);
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return PartialView("PanigationTinRaoTheoDiaDiemRoom", model);
        }


        // GET: TinTuc
        [AllowAnonymous]
        public ActionResult PageIndexTinRaoTheoDuAn(long? duanId, int page = 1)
        {
            var model = new TinRaoViewModel();
            var TinRaoBuilding = _BuildingsInfoService.GetDaTaByPageByDuAnId(duanId, string.Empty, page, 10);
            model.TinRaoBuilding = TinRaoBuilding;
            var TinRaoRoom = _RoomInfoService.GetDaTaByPageByDuAnId(duanId, string.Empty, page, 10);
            model.TinRaoRoom = TinRaoRoom;
            model.TinhCode = duanId.ToString();
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return View(model);
        }


        [HttpPost]
        public PartialViewResult PanigationTinRaoTheoDuAnBuildings(string KeySearch, int page, long? duanId)
        {
            var model = _BuildingsInfoService.GetDaTaByPageByDuAnId(duanId, KeySearch, page, 10);
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return PartialView("PanigationTinRaoTheoDuAnBuildings", model);
        }


        [HttpPost]
        public PartialViewResult PanigationTinRaoTheoDuAnRoom(string KeySearch, int page, long? duanId)
        {
            var model = _RoomInfoService.GetDaTaByPageByDuAnId(duanId, KeySearch, page, 10);
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return PartialView("PanigationTinRaoTheoDuAnRoom", model);
        }
    }
}