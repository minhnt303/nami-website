﻿using AutoMapper;
using Hangfire.Logging;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Web.Filters;
using Nami.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nami.Service.QLTinTucService;
using Nami.Service.BannerQuangCaoService;
using Nami.Service.QLTuVanService;

namespace Nami.Web.Controllers
{
    public class TinTucController : SecondaryController
    {
        private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        private readonly IQLTinTucService _qLTinTucService;
        private readonly IBannerQuangCaoService _bannerQuangCaoService;
        private readonly IQLTuVanService _qLTuVanService;

        public TinTucController(
            IBannerQuangCaoService bannerQuangCaoService,
            IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IQLTinTucService qLTinTucService,
            IQLTuVanService qLTuVanService
            )
        {
            _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _qLTinTucService = qLTinTucService;
            _bannerQuangCaoService = bannerQuangCaoService;
            _qLTuVanService = qLTuVanService;
        }

        // GET: TinTuc
        [AllowAnonymous]
        public ActionResult Index(string slug)
        {
            var model = new TinTucViewModel();
            var objInfo = _qLTinTucService.GetBySlugName(slug);

            if (objInfo == null)
            {
                return new HttpNotFoundResult();
            }
            model.objInfo = objInfo;

            var listTinTucByChuDe = _qLTinTucService.GetListTinTucByIdChuDe(objInfo.IsHot, objInfo.ChuDeId, 8, objInfo.Id);
            model.listTinTucByChuDe = listTinTucByChuDe;
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return View(model);
        }


        // GET: TinTuc
        [AllowAnonymous]
        public ActionResult IndexTuVan(string slug)
        {
            var model = new TinTucViewModel();
            var objInfo = _qLTuVanService.GetBySlugName(slug);

            if (objInfo == null)
            {
                return new HttpNotFoundResult();
            }
            model.TuVanObj = objInfo;

            var listTuVanTheoChuDe = _qLTuVanService.GetListTuVanByIdChuDe(objInfo.ChuDeId, 8, objInfo.Id);
            model.listTuVanTheoChuDe = listTuVanTheoChuDe;
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return View(model);
        }

        // GET: TinTuc
        [AllowAnonymous]
        public ActionResult PageIndex(int page = 1)
        {
            var model = new TinTucViewModel();
            var TinTucs = _qLTinTucService.GetDaTaShowByPage(string.Empty, false, page, 20);
            model.TinTucs = TinTucs;
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return View(model);
        }

        // GET: TinTuc
        [AllowAnonymous]
        public ActionResult PageIndexTuVan(int page = 1)
        {
            var model = new TinTucViewModel();
            var TuVans = _qLTuVanService.GetDaTaShowByPage(string.Empty, page, 20);
            model.TuVans = TuVans;
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return View(model);
        }

        // GET: TinTuc
        [AllowAnonymous]
        public ActionResult PageIndexNoiBat(int page = 1)
        {
            var model = new TinTucViewModel();
            var TinTucs = _qLTinTucService.GetDaTaShowByPage(string.Empty, true, page, 20);
            model.TinTucs = TinTucs;
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return View(model);
        }

        [HttpPost]
        public PartialViewResult PanigationNews(string KeySearch, int page)
        {
            var model = _qLTinTucService.GetDaTaShowByPage(KeySearch, false, page, 20);
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return PartialView("PanigationNews", model);
        }

        [HttpPost]
        public PartialViewResult PanigationNewsNoiBat(string KeySearch, int page)
        {
            var model = _qLTinTucService.GetDaTaShowByPage(KeySearch, true, page, 20);
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return PartialView("PanigationNewsNoiBat", model);
        }

        [HttpPost]
        public PartialViewResult PanigationNewsTuVan(string KeySearch, int page)
        {
            var model = _qLTuVanService.GetDaTaShowByPage(KeySearch, page, 20);
            ViewBag.listBanner = _bannerQuangCaoService.GetListBannerQuangCaoIsActive();
            return PartialView("PanigationNewsTuVan", model);
        }
    }
}