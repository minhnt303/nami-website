﻿using Nami.Service.BuildingsInfoService;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.FileDinhKemService;
using Nami.Service.HUYENService;
using Nami.Service.RoomInfoService;
using Nami.Service.TINHService;
using Nami.Service.XAService;
using Nami.Web.Filters;
using Nami.Web.Models;
using System.Linq;
using System.Web.Mvc;

namespace Nami.Web.Controllers
{
    public class SearchGuestController : SecondaryController
    {
        private readonly IBuildingsInfoService _buildingsInfoService;
        private readonly IRoomInfoService _roomInfoService;
        private readonly IFileDinhKemService _fileDinhKemService;
        private readonly ITINHService _tINHService;
        private readonly IHUYENService _hUYENService;
        private readonly IXAService _xAService;

        public SearchGuestController(
            IBuildingsInfoService buildingsInfoService,
            IRoomInfoService roomInfoService,
            IFileDinhKemService fileDinhKemService,
            ITINHService tINHService,
            IHUYENService hUYENService,
            IXAService xAService
            )
        {
            _buildingsInfoService = buildingsInfoService;
            _roomInfoService = roomInfoService;
            _fileDinhKemService = fileDinhKemService;
            _tINHService = tINHService;
            _hUYENService = hUYENService;
            _xAService = xAService;
        }


        // GET: SearchGuest
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Index(HomeSearchModels models)
        {
            var dropdownListTinhId = _tINHService.GetDropdown("TenTinh", "MaTinh");
            foreach (var item in dropdownListTinhId)
            {
                if (!string.IsNullOrEmpty(models.TinhId))
                {
                    if (models.TinhId == item.Value)
                    {
                        item.Selected = true;
                    }
                }
            }
            ViewBag.dropdownListTinhId = dropdownListTinhId;
            var dropdownListHuyenId = _hUYENService.GetDropdown("TenHuyen", "MaHuyen");
            foreach (var item in dropdownListHuyenId)
            {
                if (!string.IsNullOrEmpty(models.HuyenId))
                {
                    if (models.HuyenId == item.Value)
                    {
                        item.Selected = true;
                    }
                }
            }
            ViewBag.dropdownListHuyenId = dropdownListHuyenId;
            var dropdownListXaId = _xAService.GetDropdown("TenXa", "MaXa");
            foreach (var item in dropdownListXaId)
            {
                if (!string.IsNullOrEmpty(models.XaId))
                {
                    if (models.XaId == item.Value)
                    {
                        item.Selected = true;
                    }
                }
            }
            ViewBag.dropdownListXaId = dropdownListXaId;
            ViewBag.dropdownListBuilding = ConstantExtension.GetDropdownData<BuildingTypeConstant>(models.HomeTypeId);
            var model = new HomeSearchModels();
            model.HomeTypeId = models.HomeTypeId;
            model.HuyenId = models.HuyenId;
            model.MoneyFrom = models.MoneyFrom;
            model.MoneyTo = models.MoneyTo;
            model.TinhId = models.TinhId;
            model.XaId = models.XaId;
            model.TrongSoTinh = !string.IsNullOrEmpty(models.TrongSoTinh) ? models.TrongSoTinh : "0.5";
            model.TrongSoHuyen = !string.IsNullOrEmpty(models.TrongSoHuyen) ? models.TrongSoHuyen : "0.1";
            model.TrongSoXa = !string.IsNullOrEmpty(models.TrongSoXa) ? models.TrongSoXa : "0.1";
            model.TrongSoGia = !string.IsNullOrEmpty(models.TrongSoGia) ? models.TrongSoGia : "0.3";
            model.HuyenArea = (models.HuyenArea == false) ? true : false;
            model.XaArea = (models.XaArea == false) ? true : false;
            if (models.HomeTypeId == BuildingTypeConstant.Building)
            {
                model.ListBuilding = _buildingsInfoService.GetListSearch(models.TinhId, models.HuyenId, models.XaId, models.MoneyFrom, models.MoneyTo, true, models.HuyenArea, models.XaArea, models.TrongSoTinh, models.TrongSoHuyen, models.TrongSoXa, models.TrongSoGia);
            }
            else if (models.HomeTypeId == BuildingTypeConstant.Room)
            {
                model.ListRoom = _roomInfoService.GetListSearch(models.TinhId, models.HuyenId, models.XaId, models.MoneyFrom, models.MoneyTo, true, models.HuyenArea, models.XaArea, models.TrongSoTinh, models.TrongSoHuyen, models.TrongSoXa, models.TrongSoGia);
            }
            return View(model);
        }

        [AllowAnonymous]
        public PartialViewResult DetailSearchBuilding(long id)
        {
            var model = new BuildingInfoViewModel();
            model.objInfo = _buildingsInfoService.GetById(id);
            model.imageList = _fileDinhKemService.GetListImageFile(id, ItemTypeConstant.Building);
            return PartialView("DetailSearchBuilding", model);
        }

        [AllowAnonymous]
        public PartialViewResult DetailSearchRoom(long id)
        {
            var model = new RoomInfoViewModel();
            model.objInfo = _roomInfoService.GetById(id);
            model.imageList = _fileDinhKemService.GetListImageFile(id, ItemTypeConstant.Room);
            return PartialView("DetailSearchRoom", model);
        }


        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetHuyenDropdownOfTinh(string id)
        {
            var result = _hUYENService.GetDropdownOfTinh(id, null).OrderBy(x => x.Text).ToList();
            return Json(result);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetXaDropdownOfHuyen(string id)
        {
            var result = _xAService.GetDropdownOfHuyen(id, null).OrderBy(x => x.Text).ToList();
            return Json(result);
        }
    }
}