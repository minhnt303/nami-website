﻿using AutoMapper;
using CommonHelper.Upload;
using Nami.Model.Entities;
using Nami.Service.AgreementsInfoService;
using Nami.Service.AppUserService;
using Nami.Service.BuildingsInfoService;
using Nami.Service.CommentByAccountService;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.DM_NhomDanhmucService;
using Nami.Service.FileDinhKemService;
using Nami.Service.GiaiTrinhPhanAnhFileAttackService;
using Nami.Service.GiaiTrinhPhanAnhService;
using Nami.Service.HistoryService;
using Nami.Service.HUYENService;
using Nami.Service.NotificationService;
using Nami.Service.OrderInfoService;
using Nami.Service.OrderInfoService.Dto;
using Nami.Service.QLHopDongDangKyService;
using Nami.Service.QLHopDongItemService;
using Nami.Service.QLHopDongService;
using Nami.Service.RoomInfoService;
using Nami.Service.TINHService;
using Nami.Service.XAService;
using Nami.Web.Core;
using Nami.Web.Filters;
using Nami.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Nami.Web.Controllers
{
    public class EndUserPersonalInfoController : EndUserController
    {
        private readonly IAppUserService _appUserService;
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        private readonly INotificationService _notificationService;
        private readonly IDM_DulieuDanhmucService _iDM_DuLieuDanhMucService;
        private readonly ITINHService _iTINHService;
        private readonly IHUYENService _hUYENService;
        private readonly IXAService _xAService;
        private readonly IAgreementsInfoService _agreementsInfoService;
        private readonly IRoomInfoService _roomInfoService;
        private readonly IBuildingsInfoService _buildingsInfoService;
        private readonly IFileDinhKemService _fileDinhKemService;
        private readonly ICommentByAccountService _commentByAccountService;
        private readonly IQLHopDongDangKyService _QLHopDongDangKyService;
        private readonly IQLHopDongService _QLHopDongService;
        private readonly IQLHopDongItemService _QLHopDongItemService;
        private readonly IHistoryService _HistoryService;
        private readonly IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        private readonly IGiaiTrinhPhanAnhService _giaiTrinhPhanAnhService;
        private readonly IGiaiTrinhPhanAnhFileAttackService _giaiTrinhPhanAnhFileAttackService;
        private readonly IDM_NhomDanhmucService _dM_NhomDanhmucService;
        private readonly IOrderInfoService _OrderInfoService;

        public EndUserPersonalInfoController(IAppUserService appUserService, INotificationService notificationService,
            IDM_DulieuDanhmucService iDM_DulieuDanhmucService, ITINHService iTINHService,
            ILog Ilog,
            IHUYENService hUYENService,
            IXAService xAService,
            IAgreementsInfoService agreementsInfoService,
            IRoomInfoService roomInfoService,
            IBuildingsInfoService buildingsInfoService,
            IFileDinhKemService fileDinhKemService,
            ICommentByAccountService commentByAccountService,
            IQLHopDongDangKyService QLHopDongDangKyService,
            IQLHopDongService QLHopDongService,
            IQLHopDongItemService QLHopDongItemService,
            IGiaiTrinhPhanAnhService giaiTrinhPhanAnhService,
            IGiaiTrinhPhanAnhFileAttackService giaiTrinhPhanAnhFileAttackService,
            IHistoryService HistoryService,
            IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IDM_NhomDanhmucService dM_NhomDanhmucService,
            IOrderInfoService OrderInfoService,
            IMapper mapper)
        {
            _Ilog = Ilog;
            _mapper = mapper;
            _appUserService = appUserService;
            _notificationService = notificationService;
            _iDM_DuLieuDanhMucService = iDM_DulieuDanhmucService;
            _iTINHService = iTINHService;
            _hUYENService = hUYENService;
            _xAService = xAService;
            _agreementsInfoService = agreementsInfoService;
            _roomInfoService = roomInfoService;
            _buildingsInfoService = buildingsInfoService;
            _fileDinhKemService = fileDinhKemService;
            _commentByAccountService = commentByAccountService;
            _QLHopDongDangKyService = QLHopDongDangKyService;
            _QLHopDongService = QLHopDongService;
            _QLHopDongItemService = QLHopDongItemService;
            _HistoryService = HistoryService;
            _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _dM_NhomDanhmucService = dM_NhomDanhmucService;
            _giaiTrinhPhanAnhService = giaiTrinhPhanAnhService;
            _giaiTrinhPhanAnhFileAttackService = giaiTrinhPhanAnhFileAttackService;
            _OrderInfoService = OrderInfoService;
        }
        // GET: EndUserPersonalInfo
        public ActionResult Index()
        {
            var model = new EndUserPersonalDashboardViewModel();
            var user = _appUserService.GetById(CurrentUserId);
            model.ListAgreement = _agreementsInfoService.GetListAggrementEndUserByRentersGroupId(CurrentUserId);
            return View(model);
        }

        public ActionResult IndexOrder()
        {
            var model = new IndexOrderVM();
            var PermissionWatchAll = HasPermission(PermissionCodeConst.Super_Admin);
            model.ListOrder = _OrderInfoService.GetDaTaByPageByCurrentUserIdOrAdmin(PermissionWatchAll, CurrentUserId, null);
            return View(model);
        }

        public ActionResult IndexAgreement()
        {
            var model = new EndUserPersonalAgreementViewModel();
            model.objInfo = _agreementsInfoService.GetListAggrementEndUserByRentersGroupId(CurrentUserId);
            return View(model);
        }

        public ActionResult CreateAgreement()
        {
            var model = new EndUserPersonalCreateAgreementViewModel();
            var dropdownListTinhId = _iTINHService.GetDropdown("TenTinh", "MaTinh");
            ViewBag.dropdownListTinhId = dropdownListTinhId;
            var dropdownListHuyenId = _hUYENService.GetDropdown("TenHuyen", "MaHuyen");
            ViewBag.dropdownListHuyenId = dropdownListHuyenId;
            var dropdownListXaId = _xAService.GetDropdown("TenXa", "MaXa");
            ViewBag.dropdownListXaId = dropdownListXaId;
            ViewBag.dropdownListBuilding = ConstantExtension.GetDropdownData<BuildingTypeConstant>(null);
            return View(model);
        }

        public ActionResult CreateDangKyHopDong()
        {
            var model = new EndUserPersonalCreateAgreementViewModel();
            var dropdownListTinhId = _iTINHService.GetDropdown("TenTinh", "MaTinh");
            ViewBag.dropdownListTinhId = dropdownListTinhId;
            var dropdownListHuyenId = _hUYENService.GetDropdown("TenHuyen", "MaHuyen");
            ViewBag.dropdownListHuyenId = dropdownListHuyenId;
            var dropdownListXaId = _xAService.GetDropdown("TenXa", "MaXa");
            ViewBag.dropdownListXaId = dropdownListXaId;
            ViewBag.dropdownListBuilding = ConstantExtension.GetDropdownData<BuildingTypeConstant>(null);
            return View(model);
        }

        public ActionResult DetailBuilding(long id)
        {
            var model = new DetailBuildingVM();
            model.objInfo = _buildingsInfoService.GetDtoById(id);
            model.imageList = _fileDinhKemService.GetListImageFile(id, ItemTypeConstant.Building);
            model.ListRoom = _roomInfoService.GetListRoomByCurrentCreateAndBuildingId(CurrentUserInfo.UserName, id);
            return View(model);
        }

        public PartialViewResult GetBuildingData(string TinhId, string HuyenId, string XaId, decimal? MoneyFrom, decimal? MoneyTo)
        {
            var model = new EndUserPersonalCreateAgreementViewModel();
            model.ListBuilding = _buildingsInfoService.GetListBuildingSearch(TinhId, HuyenId, XaId, MoneyFrom, MoneyTo);
            model.CurrentUserId = CurrentUserId;
            return PartialView(model);
        }


        public PartialViewResult GetRoomData(string TinhId, string HuyenId, string XaId, decimal? MoneyFrom, decimal? MoneyTo)
        {
            var model = new EndUserPersonalCreateAgreementViewModel();
            model.ListRoom = _roomInfoService.GetListRoomSearch(TinhId, HuyenId, XaId, MoneyFrom, MoneyTo);
            model.CurrentUserId = CurrentUserId;
            return PartialView(model);
        }

        [HttpPost]
        public JsonResult DangKyLamHopDong(long? ItemId, string ItemType)
        {
            var result = new JsonResultBO(true, "Xóa Nhà trọ thành công");
            if (ItemId != null && !string.IsNullOrEmpty(ItemType))
            {
                var newQlHopDongDk = new QLHopDongDangKy();
                newQlHopDongDk.ItemId = ItemId;
                newQlHopDongDk.ItemType = ItemType;
                newQlHopDongDk.UserIdDangKy = CurrentUserId;
                _QLHopDongDangKyService.Create(newQlHopDongDk);
            }
            if (ItemType == ItemTypeConstant.Building)
            {
                var buildingInfo = _buildingsInfoService.GetById(ItemId);
                if (buildingInfo != null)
                {
                    var noti = new Notification();
                    noti.Message = CurrentUserInfo.FullName + " vừa gửi đăng ký làm hợp đồng tới bạn";
                    noti.FromUser = CurrentUserId;
                    noti.ToUser = buildingInfo.CreatedID;
                    noti.Link = "/BuildingsInfoArea/BuildingsInfo/Index";
                    noti.IsRead = false;
                    noti.Type = NotificationTypeConstant.System;
                    _notificationService.Create(noti);
                }
            }
            else if (ItemType == ItemTypeConstant.Room)
            {
                var rommInfo = _roomInfoService.GetById(ItemId);
                if (rommInfo != null)
                {
                    var noti = new Notification();
                    noti.Message = CurrentUserInfo.FullName + " vừa gửi đăng ký làm hợp đồng tới bạn";
                    noti.FromUser = CurrentUserId;
                    noti.ToUser = rommInfo.CreatedID;
                    noti.Link = "/RoomInfoArea/RoomInfo/Index";
                    noti.IsRead = false;
                    noti.Type = NotificationTypeConstant.System;
                    _notificationService.Create(noti);
                }
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult GetDataBuldingRoom(EndUserPersonalCreateAgreementViewModel models)
        {
            var model = new EndUserPersonalCreateAgreementViewModel();
            model.HomeTypeId = models.HomeTypeId;
            model.HuyenId = models.HuyenId;
            model.MoneyFrom = models.MoneyFrom;
            model.MoneyTo = models.MoneyTo;
            model.TinhId = models.TinhId;
            model.XaId = models.XaId;
            if (models.HomeTypeId == BuildingTypeConstant.Building)
            {
                model.ListBuilding = _buildingsInfoService.GetListSearch(models.TinhId, models.HuyenId, models.XaId, models.MoneyFrom, models.MoneyTo, true, true, false, models.TrongSoTinh, models.TrongSoHuyen, models.TrongSoXa, models.TrongSoGia);
            }
            else if (models.HomeTypeId == BuildingTypeConstant.Room)
            {
                model.ListRoom = _roomInfoService.GetListSearch(models.TinhId, models.HuyenId, models.XaId, models.MoneyFrom, models.MoneyTo, true, true, false, models.TrongSoTinh, models.TrongSoHuyen, models.TrongSoXa, models.TrongSoGia);
            }
            return Json(model);
        }

        [HttpPost]
        public JsonResult GetDataChatBuilding(BuildingInfoViewModel model)
        {
            var objBuilding = _buildingsInfoService.GetById(model.objInfo.Id);
            var CommentModel = new CommentByAccount();
            CommentModel.IdItem = objBuilding.Id;
            CommentModel.TypeItem = ItemTypeConstant.Building;
            CommentModel.PortUserId = CurrentUserId;
            CommentModel.Comments = model.objComment.Comments;
            _commentByAccountService.Create(CommentModel);
            model.objInfo = objBuilding;
            model.imageList = _fileDinhKemService.GetListImageFile(objBuilding.Id, ItemTypeConstant.Building);
            model.listComment = _commentByAccountService.GetListCommentBuidingId(objBuilding.Id, ItemTypeConstant.Building, CurrentUserId);
            model.CurrentUserId = CurrentUserId;
            var noti = new Notification();
            noti.Message = "Có khách hàng gửi tin nhắn đến cho bạn";
            noti.FromUser = CurrentUserId;
            var IdCreate = _appUserService.GetByCreatedBy(model.objInfo.CreatedBy);
            if (IdCreate != null)
            {
                noti.ToUser = IdCreate.Id;
            }
            noti.Link = "";
            noti.IsRead = false;
            noti.Type = NotificationTypeConstant.System;
            _notificationService.Create(noti);
            Task.Run(() => NotificationProvider.SendMessage(noti));
            return Json(model);
        }

        public ActionResult IndexRoomBuilding()
        {
            var model = new EndUserPersonalRoomAndBuildingViewModel();
            //model.objInfo = _agreementsInfoService.GetListAggrementEndUserByRentersGroupId(CurrentUserId);
            model.listHopDongDangKy = _QLHopDongDangKyService.GetListDangKyByIdUser(CurrentUserId.Value);
            return View(model);
        }

        public ActionResult IndexHopDong()
        {
            var model = new IndexHopDongDaKyVM();
            model.listHopDongDaKy = _QLHopDongService.GetListHopDongByUserIdEndUser(CurrentUserId);
            return View(model);
        }

        public ActionResult ThanhToanTienHopDong(long idItemHopDong)
        {
            var model = new ThanhToanVM();
            model.HopDongItemInfo = _QLHopDongItemService.GetById(idItemHopDong);
            ViewBag.ListBanhCode = _dM_DulieuDanhmucService.GetDropdownlist(DanhMucConstant.MANGANHANG, null);
            model.OrderType = OrderTypeConstant.TienNha;
            model.ItemType = ItemTypeConstant.Building;
            model.IdItem = model.HopDongItemInfo.Id;
            if (model.HopDongItemInfo.TienThangNay != null)
            {
                model.Amount = model.HopDongItemInfo.TienThangNay.Value;
            }
            return View(model);
        }
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult ThanhToanPhiDangTin(ThanhToanVM model)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (ModelState.IsValid)
                {
                    string vnp_Returnurl = ConfigurationManager.AppSettings["vnp_ReturnurlEndUser"]; //URL nhan ket qua tra ve 
                    string vnp_Url = ConfigurationManager.AppSettings["vnp_Url"]; //URL thanh toan cua VNPAY 
                    string vnp_TmnCode = ConfigurationManager.AppSettings["vnp_TmnCode"]; //Ma website
                    string vnp_HashSecret = ConfigurationManager.AppSettings["vnp_HashSecret"]; //Chuoi bi ma

                    var HopDongItemInfo = _QLHopDongItemService.GetById(model.IdItem);

                    OrderInfo order = new OrderInfo();
                    //Save order to db
                    //order.OrderId = DateTime.Now.ToLongDateString;
                    order.Amount = Convert.ToDecimal(model.Amount);
                    order.CreatedDate = DateTime.Now;
                    order.OrderType = model.OrderType;
                    order.ItemType = model.ItemType;
                    order.IdItem = model.IdItem;
                    order.BankCode = model.BankCode;
                    order.OrderDescription = "Thanh toán tiền nhà cho hợp đồng mã " + model.IdItem;
                    _OrderInfoService.Create(order);
                    order.OrderId = order.Id;
                    _OrderInfoService.Update(order);

                    VnPayLibrary vnpay = new VnPayLibrary();
                    vnpay.AddRequestData("vnp_Version", "2.0.0");
                    vnpay.AddRequestData("vnp_Command", "pay");
                    vnpay.AddRequestData("vnp_TmnCode", vnp_TmnCode);

                    string locale = model.Locale;
                    if (!string.IsNullOrEmpty(locale))
                    {
                        vnpay.AddRequestData("vnp_Locale", locale);
                    }
                    else
                    {
                        vnpay.AddRequestData("vnp_Locale", "vn");
                    }

                    vnpay.AddRequestData("vnp_CurrCode", "VND");
                    vnpay.AddRequestData("vnp_TxnRef", order.OrderId.ToString());
                    vnpay.AddRequestData("vnp_OrderInfo", order.OrderDescription);
                    vnpay.AddRequestData("vnp_OrderType", "250006"); //default value: other
                    vnpay.AddRequestData("vnp_Amount", (order.Amount * 100).ToString());
                    vnpay.AddRequestData("vnp_ReturnUrl", vnp_Returnurl);
                    vnpay.AddRequestData("vnp_IpAddr", Utils.GetIpAddress());
                    vnpay.AddRequestData("vnp_CreateDate", order.CreatedDate.ToString("yyyyMMddHHmmss"));
                    if (!string.IsNullOrEmpty(model.BankCode))
                    {
                        vnpay.AddRequestData("vnp_BankCode", model.BankCode);
                    }
                    string paymentUrl = vnpay.CreateRequestUrl(vnp_Url, vnp_HashSecret);
                    //log.InfoFormat("VNPAY URL: {0}", paymentUrl);
                    return Json(paymentUrl);
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = ex.Message;
                _Ilog.Error("Lỗi ghi lại lịch sử làm việc", ex);
            }
            return Json(model);
        }
        public ActionResult ReturnEndUser(decimal vnp_Amount, string vnp_BankCode, string vnp_BankTranNo, string vnp_CardType, string vnp_OrderInfo, string vnp_PayDate, string vnp_ResponseCode, string vnp_TmnCode, string vnp_TransactionNo, string vnp_TxnRef, string vnp_SecureHashType, string vnp_SecureHash)
        {
            var model = new ReturnVM();
            //log.InfoFormat("Begin VNPAY Return, URL={0}", Request.RawUrl);
            if (Request.QueryString.Count > 0)
            {
                string vnp_HashSecret = ConfigurationManager.AppSettings["vnp_HashSecret"]; //Chuoi bi mat
                var vnpayData = Request.QueryString;
                VnPayLibrary vnpay = new VnPayLibrary();
                //if (vnpayData.Count > 0)
                //{
                foreach (string s in vnpayData)
                {
                    //get all querystring data
                    if (!string.IsNullOrEmpty(s) && s.StartsWith("vnp_"))
                    {
                        vnpay.AddResponseData(s, vnpayData[s]);
                    }
                }
                // }
                var OrderInfoId = long.Parse(vnp_TxnRef);
                var OrderInfoModel = new OrderInfo();
                var OrderInfoDtoModel = new OrderInfoDto();
                if (OrderInfoId != 0)
                {
                    OrderInfoModel = _OrderInfoService.GetById(OrderInfoId);
                    OrderInfoDtoModel = _OrderInfoService.GetDtoById(OrderInfoId);
                    if (OrderInfoDtoModel != null)
                    {
                        model.OrderInfoDtoModel = OrderInfoDtoModel;
                    }
                }

                //vnp_TxnRef: Ma don hang merchant gui VNPAY tai command=pay    
                long orderId = Convert.ToInt64(vnpay.GetResponseData("vnp_TxnRef"));
                //vnp_TransactionNo: Ma GD tai he thong VNPAY
                long vnpayTranId = Convert.ToInt64(vnpay.GetResponseData("vnp_TransactionNo"));
                ////vnp_ResponseCode:Response code from VNPAY: 00: Thanh cong, Khac 00: Xem tai lieu
                //string vnp_ResponseCode = vnpay.GetResponseData("vnp_ResponseCode");
                ////vnp_SecureHash: MD5 cua du lieu tra ve
                //String vnp_SecureHash = Request.QueryString["vnp_SecureHash"];
                bool checkSignature = vnpay.ValidateSignature(vnp_SecureHash, vnp_HashSecret);
                model.checkSignature = checkSignature;
                model.vnp_ResponseCode = vnp_ResponseCode;
                if (checkSignature)
                {
                    if (vnp_ResponseCode == "00")
                    {
                        //Thanh toan thanh cong
                        model.DisplayMsg = "Thanh toán thành công";
                        //displayMsg.InnerText = "Thanh toán thành công";
                        //log.InfoFormat("Thanh toan thanh cong, OrderId={0}, VNPAY TranId={1}", orderId, vnpayTranId);
                        if (OrderInfoModel != null)
                        {
                            OrderInfoModel.TrangThaiThanhToan = true;
                            OrderInfoModel.BankCode = vnp_BankCode;
                            _OrderInfoService.Update(OrderInfoModel);
                            var HopDongItemInfo = _QLHopDongItemService.GetById(OrderInfoModel.IdItem);
                            HopDongItemInfo.TienThangNay = HopDongItemInfo.TienThangNay - Convert.ToInt32(OrderInfoModel.Amount);
                            _QLHopDongItemService.Update(HopDongItemInfo);
                        }
                    }
                    else
                    {
                        //Thanh toan khong thanh cong. Ma loi: vnp_ResponseCode
                        model.DisplayMsg = "Có lỗi xảy ra trong quá trình xử lý.Mã lỗi: " + vnp_ResponseCode;
                        //displayMsg.InnerText = "Có lỗi xảy ra trong quá trình xử lý.Mã lỗi: " + vnp_ResponseCode;
                        //log.InfoFormat("Thanh toan loi, OrderId={0}, VNPAY TranId={1},ResponseCode={2}", orderId, vnpayTranId, vnp_ResponseCode);
                    }
                }
                else
                {
                    //log.InfoFormat("Invalid signature, InputData={0}", Request.RawUrl);
                    //displayMsg.InnerText = "Có lỗi xảy ra trong quá trình xử lý";
                    model.DisplayMsg = "Có lỗi xảy ra trong quá trình xử lý";
                }
            }
            return View(model);
        }


        public ActionResult DetailHopDong(long id)
        {
            var model = new DetailHopDongVM();
            model.objInfo = _QLHopDongService.GetDtoById(id);
            model.listItem = _QLHopDongItemService.GetListQLHopDongItemByIdHopDong(id);
            model.historys = _HistoryService.GetDaTaByIdHistory(id, ItemTypeConstant.Agreement);
            return View(model);
        }

        public ActionResult DetailOrder(long id)
        {
            var model = new DetailOrderVM();
            model.objInfo = _OrderInfoService.GetDtoById(id);
            model.GiaiTrinhList = _giaiTrinhPhanAnhService.GetListGiaTrinhByPhanAnhIdAndPhanAnhType(id, ItemTypeConstant.Order);
            return View(model);
        }

        public ActionResult CreateRoomBuilding()
        {
            var model = new EndUserPersonalCreateRoomBuildingViewModel();
            //model.ListComment = _commentByAccountService.GetListCommentBuidingId
            return View(model);
        }

        public ActionResult IndexMessage()
        {
            var model = new EndUserPersonalMessageController();
            return View(model);
        }

        public JsonResult CreateNewMessage()
        {
            var data = "";
            return Json(data);
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetHuyenDropdownOfTinh(string id)
        {
            var result = _hUYENService.GetDropdownOfTinh(id, null).OrderBy(x => x.Text).ToList();
            return Json(result);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetXaDropdownOfHuyen(string id)
        {
            var result = _xAService.GetDropdownOfHuyen(id, null).OrderBy(x => x.Text).ToList();
            return Json(result);
        }
        [AllowAnonymous]
        public PartialViewResult DetailSearchBuilding(long id)
        {
            var model = new BuildingInfoViewModel();
            model.objInfo = _buildingsInfoService.GetById(id);
            model.imageList = _fileDinhKemService.GetListImageFile(id, ItemTypeConstant.Building);
            model.listComment = _commentByAccountService.GetListCommentBuidingId(id, ItemTypeConstant.Building, CurrentUserId);
            model.CurrentUserId = CurrentUserId;
            return PartialView("DetailSearchBuilding", model);
        }

        [AllowAnonymous]
        public PartialViewResult DetailSearchRoom(long id)
        {
            var model = new RoomInfoViewModel();
            model.objInfo = _roomInfoService.GetById(id);
            model.imageList = _fileDinhKemService.GetListImageFile(id, ItemTypeConstant.Room);
            model.listComment = _commentByAccountService.GetListCommentBuidingId(id, ItemTypeConstant.Building, CurrentUserId);
            return PartialView("DetailSearchRoom", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GiaiTrinhVuViec(GiaitrinhCommentViewModel data, List<HttpPostedFileBase> file)
        {
            var result = new JsonResultBO(true);
            try
            {
                if (file != null)
                {
                    foreach (var item in file)
                    {
                        string LogoFileExtension = Path.GetExtension(item.FileName);
                        List<string> ExtensionSupport = new List<string>() { ".png", ".jpg", ".jpeg", ".doc", ".docx", ".pdf" };
                        if (!ExtensionSupport.Contains(LogoFileExtension.ToLower()))
                        {
                            result.MessageFail("Vui lòng nhập đúng định dạng ảnh bao gồm doc, docx, pdf, png, jpg hoặc jpeg");
                            return Json(result);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(data.Comments))
                {
                    GiaiTrinhPhanAnh model = new GiaiTrinhPhanAnh();


                    model.GiaiTrinhComment = data.Comments;
                    model.PhanAnhType = data.PhanAnhType;
                    model.PhanAnhId = data.id;
                    model.IdHoSoGiaiTrinh = CurrentUserId;
                    model.TypeHoSoGiaiTrinh = CurrentUserInfo.TypeOrganization;
                    model.NgayGiaiTrinh = DateTime.Now;
                    model.IsDelete = false;
                    //model.LoaiGiaiTrinh = GiaiTrinhTypeConstant.CapNhatGiaiTrinh;
                    _giaiTrinhPhanAnhService.Create(model);

                    if (file != null)
                    {
                        foreach (var item in file)
                        {
                            var resultUpload = UploadProvider.SaveFile(item, null, ".png,.jpg,.jpeg,.doc,.docx,.pdf", 5242880, "Uploads/TaiLieuBinhLuan/", HostingEnvironment.MapPath("/"));

                            if (resultUpload.status == true)
                            {
                                GiaiTrinhPhanAnhFileAttack doc = new GiaiTrinhPhanAnhFileAttack()
                                {
                                    IdGiaiTrinhPhanAnh = (int)model.Id,
                                    FileUrlGiaiTrinhPhanAnh = resultUpload.path,
                                    IsDelete = false,
                                };
                                _giaiTrinhPhanAnhFileAttackService.Create(doc);
                            }
                        }
                    }


                    var messageContent = "";
                    messageContent = CurrentUserInfo.FullName + " đã yêu cầu hỗ trợ thông tin giao dịch id là " + model.PhanAnhId;

                    #region log history
                    var logHistory = new History();
                    logHistory.IdItem = model.PhanAnhId.GetValueOrDefault();
                    logHistory.TypeItem = ItemTypeConstant.Order;
                    logHistory.HistoryContent = messageContent;
                    //_historyService.Create(logHistory);
                    #endregion
                    var tb = new Notification();
                    tb.ToUser = 1;
                    tb.FromUser = CurrentUserId;
                    tb.IsRead = false;
                    tb.Message = messageContent;
                    tb.Link = "/OrderInfoArea/OrderInfo/Detail/" + model.PhanAnhId;
                    tb.Type = NotificationTypeConstant.System;
                    _notificationService.Create(tb);
                    Task.Run(() => NotificationProvider.SendMessage(tb));

                }
            }
            catch (Exception ex)
            {
                _Ilog.Error(ex.Message, ex);
                throw ex;
            }
            return Json(result);
        }
    }
}