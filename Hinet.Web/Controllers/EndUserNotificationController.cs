﻿using AutoMapper;
using Nami.Service.NotificationService;
using Nami.Web.Filters;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nami.Web.Controllers
{
    public class EndUserNotificationController : EndUserController
    {
        private readonly INotificationService _NotificationService;
        private readonly ILog _Ilog;
        private readonly IMapper _mapper;
        public const string permissionIndex = "Notification_index";
        public const string permissionCreate = "Notification_create";
        public const string permissionEdit = "Notification_edit";
        public const string permissionDelete = "Notification_delete";
        public const string permissionImport = "Notification_Inport";
        public const string permissionExport = "Notification_export";
        public const string searchKey = "NotificationPageSearchModel";
        public EndUserNotificationController(INotificationService NotificationService, ILog Ilog, IMapper mapper)
        {
            _NotificationService = NotificationService;
            _Ilog = Ilog;
            _mapper = mapper;
        }
        // GET: EndUserNotification
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult ShowNotification()
        {
            var listNotification = _NotificationService.GetByUserId(CurrentUserId, 10);
            return PartialView(listNotification);
        }

        public ActionResult ReadNotification(long id)
        {
            var Noti = _NotificationService.GetById(id);
            Noti.IsRead = true;
            _NotificationService.Update(Noti);
            return Redirect(Noti.Link);
        }
    }
}