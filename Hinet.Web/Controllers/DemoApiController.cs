﻿using Nami.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nami.Service.Constant;
using Nami.Model.Entities;
using System.Web.Mvc;
using log4net;
using AutoMapper;
using System.Web.Http;
using System.Web.Http.Cors;
using Nami.Service.AppUserService;
using HttpGetAttribute = System.Web.Mvc.HttpGetAttribute;
using RouteAttribute = System.Web.Mvc.RouteAttribute;
using RoutePrefixAttribute = System.Web.Mvc.RoutePrefixAttribute;
using Nami.Model.IdentityEntities;
using Nami.Service.Common;
using Nami.Service.AppUserService.Dto;

namespace Nami.Web.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("Api/DemoApi")]
    public class DemoApiController: BaseApiController
    {
        private ILog _ilog;
        private IMapper _mapper;
        private IAppUserService _appUserService;

        public DemoApiController()
        {
            _ilog = System.Web.Mvc.DependencyResolver.Current.GetService(typeof(log4net.ILog)) as ILog;
            _mapper = System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IMapper)) as IMapper;
            _appUserService = System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IAppUserService)) as IAppUserService;
        }

        [Route("GetAllAppUserByPage")]
        [HttpGet]
        public PageListResultBO<UserDto> GetAllAppUser(int pageIndex, int pageSize)
        {
            var ListAppUser = _appUserService.GetDaTaByPage(null, pageIndex, pageSize);
            if(ListAppUser != null && ListAppUser.ListItem.Any())
            {
                return ListAppUser;
            }
            return new PageListResultBO<UserDto>();
        }



    }
}