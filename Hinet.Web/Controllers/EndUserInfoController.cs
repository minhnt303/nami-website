﻿using Nami.Service.AppUserService;
using Nami.Web.Filters;
using Nami.Web.Models;
using System.Web.Mvc;
using Nami.Service.NotificationService;
using AutoMapper;
using System.Web;
using CommonHelper.Upload;
using System;
using log4net;
using System.Web.Hosting;
using Nami.Service.Constant;

namespace Nami.Web.Controllers
{
    public class EndUserInfoController : EndUserController
    {
        private readonly IAppUserService _appUserService;
        private readonly INotificationService _notificationService;
        private readonly IMapper _mapper;
        private readonly ILog _Ilog;

        public EndUserInfoController(
            IAppUserService appUserService,
            INotificationService notificationService,
            ILog Ilog,
            IMapper mapper
            )
        {
            _appUserService = appUserService;
            _notificationService = notificationService;
            _Ilog = Ilog;
            _mapper = mapper;
        }
        // GET: EndUserInfo
        public ActionResult Index()
        {
            var model = new EndUserInfoViewModel();
            model.AppUser = _appUserService.GetById(CurrentUserId);
            if (model.AppUser.TypeAccount == AccountTypeConstant.NguoiDung)
            {
                return RedirectToAction("Index", "EndUserPersonalInfo");
            }
            else
            {
                return RedirectToAction("Index", "Dashboard", new { area = "DashboardArea" });
            }
        }

        public PartialViewResult EndUserMenuLeft()
        {
            var model = new EndUserInfoViewModel();
            model.AppUser = _appUserService.GetById(CurrentUserId);
            //if (model.User.TypeAccount == "EndUser" && model.User.TypeOrganization == OrganizationTypeConstant.Personal)
            //{
            //    model.Personal = _personalInfoService.GetInfoEndUser(model.User.OrganizationId);
            //    return PartialView(model);
            //}
            //else if (model.User.TypeAccount == "EndUser" && model.User.TypeOrganization == OrganizationTypeConstant.Company)
            //{
            //    model.Company = _companyInfoService.GetInfoEndUser(model.User.OrganizationId);
            //    return PartialView("EndUserMenuLeftCompany", model);
            //}
            //else if (model.User.TypeAccount == "EndUser" && model.User.TypeOrganization == OrganizationTypeConstant.Organization)
            //{
            //    model.Company = _companyInfoService.GetInfoEndUser(model.User.OrganizationId);
            //    return PartialView("EndUserMenuLeftOrganization", model);
            //}
            return PartialView(model);

        }


        [HttpGet]
        public ActionResult Notification()
        {
            var model = new EndUserNotificationViewModel();
            model.NotiList = _notificationService.GetDaTaByPage(CurrentUserId, null, 1, 15);
            return View(model);
        }
        [HttpPost]
        public PartialViewResult getNotification(int pageindex = 1, int amount = 30)
        {
            var model = new EndUserNotificationViewModel();
            model.NotiList = _notificationService.GetDaTaByPage(CurrentUserId, null, pageindex, amount);
            return PartialView(model.NotiList);
        }
        [Authorize]
        public ActionResult Account()
        {
            EndUserAccountViewModel model = new EndUserAccountViewModel();
            var user = _appUserService.GetById(CurrentUserId);
            model = _mapper.Map(user, model);
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Account(EndUserAccountViewModel model, HttpPostedFileBase File)
        {
            try
            {
                var user = _appUserService.GetById(CurrentUserId);
                if (ModelState.IsValid)
                {
                    if (File != null && File.ContentLength > 0)
                    {
                        var resultUpload = UploadProvider.SaveFile(File, null, ".jpg,.png", 5242880, "Uploads/Avatars/", HostingEnvironment.MapPath("/"));

                        if (resultUpload.status == true)
                        {
                            model.Avatar = resultUpload.path;
                        }
                    }
                    else
                    {
                        model.Avatar = user.Avatar;
                    }

                    user = _mapper.Map(model, user);

                    //#region log history
                    //var logHistory = new History();
                    //logHistory.IdItem = user.Id;
                    //logHistory.LogId = user.Id;
                    //logHistory.Name = "Cập nhật hồ sơ ứng dụng";
                    //logHistory.TypeItem = ItemTypeConstant.AppUser;
                    //logHistory.HistoryContent = CurrentUserInfo.FullName + " Gửi thông tin sau khi chỉnh sửa tài khoản cá nhân";
                    //_historyService.Create(logHistory);
                    //#endregion

                    TempData["Msg"] = 1;
                    return View();
                }
                return View();
            }
            catch (Exception ex)
            {
                _Ilog.Error("Chỉnh sửa thông tin người dùng lỗi " + ex.Message, ex);
                return View();
            }
        }
    }
}