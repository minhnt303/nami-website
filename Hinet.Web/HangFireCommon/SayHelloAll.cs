﻿using Nami.Model.Entities;
using Nami.Service.Constant;
using Nami.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nami.Web.HangFireCommon
{
    public class SayHelloAll
    {
        public void hello(string mes)
        {
            var notifycation = new Notification()
            {
                FromUser = 1,
                Message = mes,
                Type = NotificationTypeConstant.Global,
            };

            NotificationProvider.SendMessage(notifycation);
        }
    }
}