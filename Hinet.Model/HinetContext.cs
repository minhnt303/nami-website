﻿using Microsoft.AspNet.Identity.EntityFramework;
using Nami.Model.Entities;
using Nami.Model.IdentityEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nami.Model
{
    public class NamiContext : IdentityDbContext<AppUser, AppRole, long, AppLogin, AppUserRole, AppClaim>
    {

        public NamiContext()
            : base("Name=NamiContext")
        {
            //sử dụng cho việc unit test
            Database.SetInitializer<NamiContext>(null);
        }
        public DbSet<Audit> Audit { get; set; }
        public DbSet<Module> Module { get; set; }
        public DbSet<Operation> Operation { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<RoleOperation> RoleOperation { get; set; }
        public DbSet<DM_DulieuDanhmuc> DM_DulieuDanhmuc { get; set; }
        public DbSet<DM_NhomDanhmuc> DM_NhomDanhmuc { get; set; }
        public DbSet<CommonConfiguration> CommonConfiguration { get; set; }

        public DbSet<Department> Department { get; set; }
        public DbSet<TaiLieuDinhKem> TaiLieuDinhKem { get; set; }

        public DbSet<UserRole> UserRole { get; set; }

        public DbSet<TINH> TINH { get; set; }
        public DbSet<HUYEN> HUYEN { get; set; }
        public DbSet<XA> XA { get; set; }

        public DbSet<AgreementsInfo> AgreementsInfo { get; set; }
        public DbSet<BuildingsInfo> BuildingsInfo { get; set; }
        public DbSet<DepositsInfo> DepositsInfo { get; set; }
        public DbSet<DevicesInfo> DevicesInfo { get; set; }
        public DbSet<RentersInfo> RentersInfo { get; set; }
        public DbSet<RentersInfoImage> RentersInfoImage { get; set; }
        public DbSet<RoomInfo> RoomInfo { get; set; }
        public DbSet<ServicesInfo> ServicesInfo { get; set; }
        public DbSet<MapLocationInfo> MapLocationInfo { get; set; }
        public DbSet<CongThuc> CongThuc { get; set; }
        public DbSet<CauHinhCongThuc> CauHinhCongThuc { get; set; }
        public DbSet<FileDinhKem> FileDinhKem { get; set; }

        public DbSet<Notification> Notification { get; set; }
        public DbSet<CommentByAccount> CommentByAccount { get; set; }
        public DbSet<CommentByAccountFileAttack> CommentByAccountFileAttack { get; set; }
        public DbSet<BannerQuangCao> BannerQuangCao { get; set; }
        public DbSet<QLTinTuc> QLTinTuc { get; set; }
        public DbSet<QLTuVan> QLTuVan { get; set; }
        public DbSet<QLCauHoi> QLCauHoi { get; set; }
        public DbSet<UserOperation> UserOperation { get; set; }
        public DbSet<QuanLyPROJECT> QuanLyPROJECT { get; set; }
        public DbSet<QuanLySTREET> QuanLySTREET { get; set; }
        public DbSet<QuanLyDuongPho> QuanLyDuongPho { get; set; }
        public DbSet<QuanLyDuAn> QuanLyDuAn { get; set; }
        public DbSet<TINH2> TINH2 { get; set; }
        public DbSet<HUYEN2> HUYEN2 { get; set; }
        public DbSet<XA2> XA2 { get; set; }
        public DbSet<OrderInfo> OrderInfo { get; set; }
        public DbSet<History> History { get; set; }

        #region biểu đồ
        public DbSet<BDBieuDo> BDBieuDo { get; set; }
        public DbSet<BDDataProvince> BDDataProvince { get; set; }
        public DbSet<BDGraphs> BDGraphs { get; set; }
        public DbSet<BDSetUp> BDSetUp { get; set; }
        public DbSet<BDTitle> BDTitle { get; set; }
        public DbSet<BDValueAxes> BDValueAxes { get; set; }
        #endregion

        #region Hợp đồng
        public DbSet<QLHopDong> QLHopDong { get; set; }
        public DbSet<QLHopDongItem> QLHopDongItem { get; set; }
        public DbSet<QLHopDongDangKy> QLHopDongDangKy { get; set; }
        #endregion


        public DbSet<GiaiTrinhPhanAnh> GiaiTrinhPhanAnh { get; set; }
        public DbSet<GiaiTrinhPhanAnhFileAttack> GiaiTrinhPhanAnhFileAttack { get; set; }

        public static NamiContext Create()
        {
            return new NamiContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<AppUser>().ToTable("AppUser");
            modelBuilder.Entity<AppUserRole>().ToTable("AppUserRole");
            modelBuilder.Entity<AppRole>().ToTable("AppRole");
            modelBuilder.Entity<AppClaim>().ToTable("AppClaim");
            modelBuilder.Entity<AppLogin>().ToTable("AppLogin");

            modelBuilder.Entity<AppUser>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<AppRole>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<AppClaim>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
        public override int SaveChanges()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity
                    && (x.State == System.Data.Entity.EntityState.Added || x.State == System.Data.Entity.EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                IAuditableEntity entity = entry.Entity as IAuditableEntity;
                if (entity != null)
                {
                    string identityName = Thread.CurrentPrincipal.Identity.Name;
                    var userId = this.Users.Where(x => x.UserName == identityName).Select(x => x.Id).FirstOrDefault();

                    DateTime now = DateTime.Now;

                    if (entry.State == System.Data.Entity.EntityState.Added)
                    {
                        entity.CreatedBy = identityName;
                        entity.CreatedDate = now;
                        entity.CreatedID = userId;
                    }
                    else
                    {
                        base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                    }

                    entity.UpdatedBy = identityName;
                    entity.UpdatedDate = now;
                    entity.UpdatedID = userId;
                }
            }

            return base.SaveChanges();
        }
    }
}
