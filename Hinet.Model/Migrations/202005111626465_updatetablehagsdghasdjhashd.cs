namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetablehagsdghasdjhashd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BuildingsInfo", "StatusTin", c => c.Boolean());
            AddColumn("dbo.BuildingsInfo", "IsCreateRoom", c => c.Boolean(nullable: false));
            AddColumn("dbo.RoomInfo", "StatusTin", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RoomInfo", "StatusTin");
            DropColumn("dbo.BuildingsInfo", "IsCreateRoom");
            DropColumn("dbo.BuildingsInfo", "StatusTin");
        }
    }
}
