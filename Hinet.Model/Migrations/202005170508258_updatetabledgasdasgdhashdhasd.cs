namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetabledgasdasgdhashdhasd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QLHopDong",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ItemId = c.Long(),
                        ItemType = c.String(),
                        AgreementCode = c.String(),
                        RepresenterName = c.String(),
                        DepositsMoney = c.Int(),
                        Month = c.Int(),
                        Ngay = c.Int(),
                        DateStart = c.DateTime(),
                        DateEnd = c.DateTime(),
                        PayPerMonth = c.Int(),
                        Note = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QLHopDongItem",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IdQLHopDong = c.Long(),
                        Thang = c.Int(),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        DepositsMoney = c.Int(),
                        TienPhaiTraHangThang = c.Int(),
                        TienThangNay = c.Int(),
                        PayDate = c.DateTime(),
                        Note = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.QLHopDongItem");
            DropTable("dbo.QLHopDong");
        }
    }
}
