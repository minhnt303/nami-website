namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addnewtable26136213 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BDBieuDo",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BDDataProvince",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IdBieuDo = c.Long(),
                        IsCategory = c.Boolean(nullable: false),
                        Key = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BDGraphs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IdBieuDo = c.Long(),
                        BalloonText = c.String(),
                        IsHidden = c.Boolean(nullable: false),
                        IdGraphs = c.String(),
                        Title = c.String(),
                        Type = c.String(),
                        ValueField = c.String(),
                        BalloonColor = c.String(),
                        ShowBalloon = c.Boolean(nullable: false),
                        ColumnWidth = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ConnerRadius = c.Int(nullable: false),
                        DashLength = c.Int(nullable: false),
                        FillColor = c.String(),
                        FixedColoumWidth = c.Int(nullable: false),
                        FontSize = c.Int(nullable: false),
                        Bullet = c.String(),
                        BulletAlpha = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BulletColor = c.String(),
                        BulletSize = c.Int(nullable: false),
                        CustomBullet = c.String(),
                        LabelText = c.String(),
                        LabelRotation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LabelPosition = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BDSetUp",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IdBieuDo = c.Long(),
                        CategoryFeild = c.String(),
                        StartDuration = c.Int(),
                        Rotate = c.Boolean(nullable: false),
                        TextColor = c.String(),
                        Theme = c.String(),
                        FontSize = c.Int(nullable: false),
                        FontFamily = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BDTitle",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IdBieuDo = c.Long(),
                        IdTitle = c.String(),
                        Size = c.Int(nullable: false),
                        Text = c.String(),
                        IsBold = c.Boolean(nullable: false),
                        Color = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BDValueAxes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IdBieuDo = c.Long(),
                        IdValue = c.String(),
                        StackType = c.String(),
                        Title = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BDValueAxes");
            DropTable("dbo.BDTitle");
            DropTable("dbo.BDSetUp");
            DropTable("dbo.BDGraphs");
            DropTable("dbo.BDDataProvince");
            DropTable("dbo.BDBieuDo");
        }
    }
}
