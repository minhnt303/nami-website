namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateahsdhsadhasd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QuanLyDuAn",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TenDuAn = c.String(nullable: false, maxLength: 50),
                        DuAnId = c.String(),
                        TinhId = c.String(),
                        HuyenId = c.String(),
                        Location = c.String(maxLength: 250),
                        Longitude = c.String(),
                        Latitude = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.QuanLyDuAn");
        }
    }
}
