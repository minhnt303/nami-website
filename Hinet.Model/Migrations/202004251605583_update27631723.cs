namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update27631723 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DM_DulieuDanhmuc", "SlugTitle", c => c.String());
            DropColumn("dbo.QLCauHoi", "SlugTitle");
        }
        
        public override void Down()
        {
            AddColumn("dbo.QLCauHoi", "SlugTitle", c => c.String());
            DropColumn("dbo.DM_DulieuDanhmuc", "SlugTitle");
        }
    }
}
