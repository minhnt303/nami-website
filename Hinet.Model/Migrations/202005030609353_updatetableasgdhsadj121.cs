namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetableasgdhsadj121 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AppUser", "CreatedDate", c => c.DateTime());
            AddColumn("dbo.AppUser", "CreatedBy", c => c.String(maxLength: 256));
            AddColumn("dbo.AppUser", "CreatedID", c => c.Long());
            AddColumn("dbo.AppUser", "UpdatedDate", c => c.DateTime());
            AddColumn("dbo.AppUser", "UpdatedBy", c => c.String());
            AddColumn("dbo.AppUser", "UpdatedID", c => c.Long());
            AddColumn("dbo.AppUser", "IsDelete", c => c.Boolean());
            AddColumn("dbo.AppUser", "DeleteTime", c => c.DateTime());
            AddColumn("dbo.AppUser", "DeleteId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AppUser", "DeleteId");
            DropColumn("dbo.AppUser", "DeleteTime");
            DropColumn("dbo.AppUser", "IsDelete");
            DropColumn("dbo.AppUser", "UpdatedID");
            DropColumn("dbo.AppUser", "UpdatedBy");
            DropColumn("dbo.AppUser", "UpdatedDate");
            DropColumn("dbo.AppUser", "CreatedID");
            DropColumn("dbo.AppUser", "CreatedBy");
            DropColumn("dbo.AppUser", "CreatedDate");
        }
    }
}
