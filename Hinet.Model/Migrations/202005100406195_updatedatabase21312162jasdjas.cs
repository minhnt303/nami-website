namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatabase21312162jasdjas : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BuildingsInfo", "DuAnId", c => c.Long());
            AddColumn("dbo.BuildingsInfo", "MucDichSuDung", c => c.String());
            AddColumn("dbo.BuildingsInfo", "DonVi", c => c.String());
            AddColumn("dbo.BuildingsInfo", "HuongNha", c => c.String());
            AddColumn("dbo.BuildingsInfo", "HuongBanCong", c => c.String());
            AddColumn("dbo.BuildingsInfo", "SoPhongNgu", c => c.Int(nullable: false));
            AddColumn("dbo.BuildingsInfo", "SoToiLet", c => c.Int(nullable: false));
            AddColumn("dbo.BuildingsInfo", "NoiThat", c => c.String());
            AddColumn("dbo.BuildingsInfo", "ThongTinPhapLy", c => c.String());
            AddColumn("dbo.BuildingsInfo", "TenLienHe", c => c.String());
            AddColumn("dbo.BuildingsInfo", "DiaChiLienHe", c => c.String());
            AddColumn("dbo.BuildingsInfo", "DienThoai", c => c.String());
            AddColumn("dbo.BuildingsInfo", "DiDong", c => c.String());
            AddColumn("dbo.BuildingsInfo", "Email", c => c.String());
            AddColumn("dbo.RoomInfo", "DuAnId", c => c.Long());
            AddColumn("dbo.RoomInfo", "MucDichSuDung", c => c.String());
            AddColumn("dbo.RoomInfo", "DonVi", c => c.String());
            AddColumn("dbo.RoomInfo", "HuongNha", c => c.String());
            AddColumn("dbo.RoomInfo", "HuongBanCong", c => c.String());
            AddColumn("dbo.RoomInfo", "SoPhongNgu", c => c.Int(nullable: false));
            AddColumn("dbo.RoomInfo", "SoToiLet", c => c.Int(nullable: false));
            AddColumn("dbo.RoomInfo", "NoiThat", c => c.String());
            AddColumn("dbo.RoomInfo", "ThongTinPhapLy", c => c.String());
            AddColumn("dbo.RoomInfo", "TenLienHe", c => c.String());
            AddColumn("dbo.RoomInfo", "DiaChiLienHe", c => c.String());
            AddColumn("dbo.RoomInfo", "DienThoai", c => c.String());
            AddColumn("dbo.RoomInfo", "DiDong", c => c.String());
            AddColumn("dbo.RoomInfo", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RoomInfo", "Email");
            DropColumn("dbo.RoomInfo", "DiDong");
            DropColumn("dbo.RoomInfo", "DienThoai");
            DropColumn("dbo.RoomInfo", "DiaChiLienHe");
            DropColumn("dbo.RoomInfo", "TenLienHe");
            DropColumn("dbo.RoomInfo", "ThongTinPhapLy");
            DropColumn("dbo.RoomInfo", "NoiThat");
            DropColumn("dbo.RoomInfo", "SoToiLet");
            DropColumn("dbo.RoomInfo", "SoPhongNgu");
            DropColumn("dbo.RoomInfo", "HuongBanCong");
            DropColumn("dbo.RoomInfo", "HuongNha");
            DropColumn("dbo.RoomInfo", "DonVi");
            DropColumn("dbo.RoomInfo", "MucDichSuDung");
            DropColumn("dbo.RoomInfo", "DuAnId");
            DropColumn("dbo.BuildingsInfo", "Email");
            DropColumn("dbo.BuildingsInfo", "DiDong");
            DropColumn("dbo.BuildingsInfo", "DienThoai");
            DropColumn("dbo.BuildingsInfo", "DiaChiLienHe");
            DropColumn("dbo.BuildingsInfo", "TenLienHe");
            DropColumn("dbo.BuildingsInfo", "ThongTinPhapLy");
            DropColumn("dbo.BuildingsInfo", "NoiThat");
            DropColumn("dbo.BuildingsInfo", "SoToiLet");
            DropColumn("dbo.BuildingsInfo", "SoPhongNgu");
            DropColumn("dbo.BuildingsInfo", "HuongBanCong");
            DropColumn("dbo.BuildingsInfo", "HuongNha");
            DropColumn("dbo.BuildingsInfo", "DonVi");
            DropColumn("dbo.BuildingsInfo", "MucDichSuDung");
            DropColumn("dbo.BuildingsInfo", "DuAnId");
        }
    }
}
