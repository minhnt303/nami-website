namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateahysdhsadasbdh2432 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PROJECT", "DuAnId", c => c.String());
            AddColumn("dbo.STREET", "DuongId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.STREET", "DuongId");
            DropColumn("dbo.PROJECT", "DuAnId");
        }
    }
}
