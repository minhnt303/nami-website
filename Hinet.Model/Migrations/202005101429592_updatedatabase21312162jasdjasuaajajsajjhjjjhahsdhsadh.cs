namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatabase21312162jasdjasuaajajsajjhjjjhahsdhsadh : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BuildingsInfo", "DienTich", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.RoomInfo", "DienTich", c => c.Decimal(precision: 18, scale: 2));
            DropColumn("dbo.BuildingsInfo", "ChieuDaiRoom");
            DropColumn("dbo.BuildingsInfo", "ChieuRongRoom");
            DropColumn("dbo.RoomInfo", "ChieuDaiRoom");
            DropColumn("dbo.RoomInfo", "ChieuRongRoom");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RoomInfo", "ChieuRongRoom", c => c.Int());
            AddColumn("dbo.RoomInfo", "ChieuDaiRoom", c => c.Int());
            AddColumn("dbo.BuildingsInfo", "ChieuRongRoom", c => c.Int());
            AddColumn("dbo.BuildingsInfo", "ChieuDaiRoom", c => c.Int());
            DropColumn("dbo.RoomInfo", "DienTich");
            DropColumn("dbo.BuildingsInfo", "DienTich");
        }
    }
}
