namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetable123345345 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QLTinTuc", "IsHot", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.QLTinTuc", "IsHot");
        }
    }
}
