namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatabase21312162jasdjasuaajajsaj1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AppUser", "TaiKhoanNganHang", c => c.String());
            AddColumn("dbo.AppUser", "TenTaiKhoan", c => c.String());
            AddColumn("dbo.AppUser", "MaNganHang", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AppUser", "MaNganHang");
            DropColumn("dbo.AppUser", "TenTaiKhoan");
            DropColumn("dbo.AppUser", "TaiKhoanNganHang");
        }
    }
}
