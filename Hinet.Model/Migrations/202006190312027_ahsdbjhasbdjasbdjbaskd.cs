namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ahsdbjhasbdjasbdjbaskd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GiaiTrinhPhanAnh",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LoaiGiaiTrinh = c.String(),
                        GiaiTrinhComment = c.String(),
                        PhanAnhType = c.String(),
                        PhanAnhId = c.Long(),
                        IdHoSoGiaiTrinh = c.Long(),
                        TypeHoSoGiaiTrinh = c.String(),
                        NgayGiaiTrinh = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GiaiTrinhPhanAnhFileAttack",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IdGiaiTrinhPhanAnh = c.Long(),
                        FileUrlGiaiTrinhPhanAnh = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.GiaiTrinhPhanAnhFileAttack");
            DropTable("dbo.GiaiTrinhPhanAnh");
        }
    }
}
