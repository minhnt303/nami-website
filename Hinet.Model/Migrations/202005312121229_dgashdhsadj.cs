namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dgashdhsadj : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.QLHopDongDangKy", "UserIdDangKy", c => c.Long());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.QLHopDongDangKy", "UserIdDangKy", c => c.String());
        }
    }
}
