namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatabase21312162 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BuildingsInfo", "StreetCode", c => c.String());
            AddColumn("dbo.RoomInfo", "StreetCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RoomInfo", "StreetCode");
            DropColumn("dbo.BuildingsInfo", "StreetCode");
        }
    }
}
