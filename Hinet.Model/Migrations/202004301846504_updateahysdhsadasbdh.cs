namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateahysdhsadasbdh : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HUYEN2",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaHuyen = c.String(maxLength: 10),
                        TinhId = c.String(),
                        TenHuyen = c.String(nullable: false, maxLength: 50),
                        Location = c.String(maxLength: 250),
                        Loai = c.String(nullable: false, maxLength: 50),
                        Longitude = c.String(),
                        Latitude = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PROJECT",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenDuAn = c.String(nullable: false, maxLength: 50),
                        TinhId = c.String(),
                        HuyenId = c.String(),
                        Location = c.String(maxLength: 250),
                        Longitude = c.String(),
                        Latitude = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.STREET",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaDuong = c.String(maxLength: 10),
                        TenDuong = c.String(nullable: false, maxLength: 50),
                        TinhId = c.String(),
                        HuyenId = c.String(),
                        Loai = c.String(nullable: false, maxLength: 50),
                        Location = c.String(maxLength: 250),
                        Longitude = c.String(),
                        Latitude = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TINH2",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaTinh = c.String(maxLength: 10),
                        TenTinh = c.String(nullable: false, maxLength: 50),
                        Loai = c.String(nullable: false, maxLength: 50),
                        Longitude = c.String(),
                        Latitude = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.XA2",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaXa = c.String(maxLength: 10),
                        TenXa = c.String(nullable: false, maxLength: 50),
                        HuyenId = c.String(),
                        Loai = c.String(nullable: false, maxLength: 50),
                        Location = c.String(maxLength: 250),
                        Longitude = c.String(),
                        Latitude = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.XA2");
            DropTable("dbo.TINH2");
            DropTable("dbo.STREET");
            DropTable("dbo.PROJECT");
            DropTable("dbo.HUYEN2");
        }
    }
}
