namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addnewtable123123 : DbMigration
    {
        public override void Up()
        {
           
            CreateTable(
                "dbo.BannerQuangCao",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LinkUrl = c.String(),
                        ImageLink = c.String(),
                        Status = c.Boolean(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        ViTriHienThi = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
           
            
            CreateTable(
                "dbo.QLCauHoi",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        CauHoi = c.String(),
                        IdNguoiXuLy = c.Long(nullable: false),
                        Status = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QLTinTuc",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ImageLink = c.String(),
                        Name = c.String(),
                        NamePhu = c.String(),
                        Description = c.String(),
                        Status = c.Boolean(nullable: false),
                        Author = c.String(),
                        ChuDeId = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QLTuVan",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ImageLink = c.String(),
                        Name = c.String(),
                        NamePhu = c.String(),
                        Description = c.String(),
                        Status = c.Boolean(nullable: false),
                        Author = c.String(),
                        ChuDeId = c.Long(nullable: false),
                        IdCauHoi = c.Long(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
         
            
            
            
        }
        
        public override void Down()
        {
            DropTable("dbo.QLTuVan");
            DropTable("dbo.QLTinTuc");
            DropTable("dbo.QLCauHoi");
            DropTable("dbo.BannerQuangCao");
        }
    }
}
