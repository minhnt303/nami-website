namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetableqlhopdongitemasjdasd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QLHopDongItem", "Status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.QLHopDongItem", "Status");
        }
    }
}
