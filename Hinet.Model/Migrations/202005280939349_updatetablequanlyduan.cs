namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetablequanlyduan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuanLyDuAn", "ImageUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuanLyDuAn", "ImageUrl");
        }
    }
}
