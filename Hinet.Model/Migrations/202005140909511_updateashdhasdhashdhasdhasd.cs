namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateashdhasdhashdhasdhasd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderInfo", "OrderType", c => c.String());
            AddColumn("dbo.OrderInfo", "IdItem", c => c.Long(nullable: false));
            AddColumn("dbo.OrderInfo", "ItemType", c => c.String());
            AddColumn("dbo.OrderInfo", "TrangThaiThanhToan", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderInfo", "TrangThaiThanhToan");
            DropColumn("dbo.OrderInfo", "ItemType");
            DropColumn("dbo.OrderInfo", "IdItem");
            DropColumn("dbo.OrderInfo", "OrderType");
        }
    }
}
