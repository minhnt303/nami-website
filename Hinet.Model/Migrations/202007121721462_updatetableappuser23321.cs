namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetableappuser23321 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AppUser", "IsUpdateNewPass", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AppUser", "IsUpdateNewPass");
        }
    }
}
