namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatabase21312162jasdjasád : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BuildingsInfo", "Status", c => c.Boolean());
            AddColumn("dbo.RoomInfo", "Status", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RoomInfo", "Status");
            DropColumn("dbo.BuildingsInfo", "Status");
        }
    }
}
