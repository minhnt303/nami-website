namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetable172367123812 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QLCauHoi", "SlugTitle", c => c.String());
            AddColumn("dbo.QLTinTuc", "SlugTitle", c => c.String());
            AddColumn("dbo.QLTuVan", "SlugTitle", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.QLTuVan", "SlugTitle");
            DropColumn("dbo.QLTinTuc", "SlugTitle");
            DropColumn("dbo.QLCauHoi", "SlugTitle");
        }
    }
}
