namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatejhajajasjdasd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BuildingsInfo", "StartDate", c => c.DateTime());
            AddColumn("dbo.BuildingsInfo", "EndDate", c => c.DateTime());
            AddColumn("dbo.RoomInfo", "StartDate", c => c.DateTime());
            AddColumn("dbo.RoomInfo", "EndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RoomInfo", "EndDate");
            DropColumn("dbo.RoomInfo", "StartDate");
            DropColumn("dbo.BuildingsInfo", "EndDate");
            DropColumn("dbo.BuildingsInfo", "StartDate");
        }
    }
}
