namespace Nami.Model.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Nami.Model.IdentityEntities;
    using Nami.Model.Seed;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Nami.Model.NamiContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Nami.Model.NamiContext context)
        {
            InitAccountSeed.init(context);
            InitDanhMucSeed.Init(context);
        }
    }
}
