namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateajhsdhhsabdjksabdahsdbs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HUYEN", "LastMaHuyen", c => c.String());
            AddColumn("dbo.HUYEN2", "LastMaHuyen", c => c.String());
            AddColumn("dbo.XA", "LastMaXa", c => c.String());
            AddColumn("dbo.XA2", "LastMaXa", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.XA2", "LastMaXa");
            DropColumn("dbo.XA", "LastMaXa");
            DropColumn("dbo.HUYEN2", "LastMaHuyen");
            DropColumn("dbo.HUYEN", "LastMaHuyen");
        }
    }
}
