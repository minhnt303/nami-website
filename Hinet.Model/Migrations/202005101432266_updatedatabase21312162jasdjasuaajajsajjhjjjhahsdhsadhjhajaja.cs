namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedatabase21312162jasdjasuaajajsajjhjjjhahsdhsadhjhajaja : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BuildingsInfo", "Status", c => c.String());
            AlterColumn("dbo.RoomInfo", "Status", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RoomInfo", "Status", c => c.Boolean());
            AlterColumn("dbo.BuildingsInfo", "Status", c => c.Boolean());
        }
    }
}
