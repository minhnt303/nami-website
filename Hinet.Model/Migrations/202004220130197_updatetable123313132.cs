namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetable123313132 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserOperation",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        OperationId = c.Long(nullable: false),
                        IsAccess = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 256),
                        CreatedID = c.Long(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 256),
                        UpdatedID = c.Long(),
                        IsDelete = c.Boolean(),
                        DeleteTime = c.DateTime(),
                        DeleteId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Module", "IsMobile", c => c.Boolean());
            AddColumn("dbo.Operation", "Icon", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Operation", "Icon");
            DropColumn("dbo.Module", "IsMobile");
            DropTable("dbo.UserOperation");
        }
    }
}
