namespace Nami.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateahysdhsadasbdh24327776663248322734732 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TINH", "Code", c => c.String());
            AddColumn("dbo.XA", "TinhId", c => c.String());
            AlterColumn("dbo.TINH", "Loai", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TINH", "Loai", c => c.String(nullable: false, maxLength: 50));
            DropColumn("dbo.XA", "TinhId");
            DropColumn("dbo.TINH", "Code");
        }
    }
}
