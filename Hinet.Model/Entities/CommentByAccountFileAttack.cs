﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Nami.Model.Entities
{
    [Table("CommentByAccountFileAttack")]
    public class CommentByAccountFileAttack: AuditableEntity<long>
    {
        //Id của bình luận
        public long? CommentId { get; set; }

        //Đường dẫn file
        public string FileUrl { get; set; }

        //Loại bình luận
        public string CommentType { get; set; }


        [StringLength(1)]
        public string flag { get; set; }
    }
}
