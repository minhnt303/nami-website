﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("UserOperation")]
    public class UserOperation:AuditableEntity<long>
    {
        [Required]
        public long UserId { get; set; }

        [Required]
        public long OperationId { get; set; }

        [Required]
        public int IsAccess { get; set; }
    }
}
