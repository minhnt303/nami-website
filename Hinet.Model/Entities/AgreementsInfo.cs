﻿using Nami.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("AgreementsInfo")]
    public class AgreementsInfo: AuditableEntity<long>
    {
        [DisplayName("Id Khu/Tòa nhà")]
        public long? BuildingsId { get; set; }

        [DisplayName("Id Phòng")]
        public long? RoomsId { get; set; }
        
        [DisplayName("Loại hợp đồng")]
        public string AgreementCode {get; set; }
        
        [DisplayName("Khách thuê")]
        public long? RentersGroupId { get; set; }
        
        [DisplayName("Dịch vụ")]
        public string ServicesGroupId { get; set; }
        
        [DisplayName("Người đại diện")]
        public string RepresenterName { get; set; }

        [DisplayName("Tiền đặt cọc")]
        public int? DepositsMoney { get; set; }
        
        [DisplayName("Thời hạn hợp đồng")]
        public int? Month { get; set; }
        
        [DisplayName("Ngày bắt đầu")]
        public DateTime? DateStart { get; set; }

        [DisplayName("Ngày kết thúc")]
        public DateTime? DateEnd { get; set; }

        [DisplayName("Kỳ thanh toán")]
        public int? PayPerMonth { get; set; }

        [DisplayName("Ghi chú")]
        public string Note { get; set; }
    }
}
