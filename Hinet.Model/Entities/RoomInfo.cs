﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("RoomInfo")]
    public class RoomInfo : AuditableEntity<long>
    {
        [DisplayName("Id Khu/Tòa nhà")]
        public long? BuildingsId { get; set; }

        [DisplayName("Tên phòng")]
        public string RoomName { get; set; }
        
        [DisplayName("Tầng")]
        public int? Floor { get; set; }

        //[DisplayName("Chiều dài phòng")]
        //public int? ChieuDaiRoom { get; set; }

        //[DisplayName("Chiều rộng phòng")]
        //public int? ChieuRongRoom { get; set; }

        [DisplayName("Diện tích")]
        public decimal? DienTich { get; set; }

        [DisplayName("Số lượng người lớn có thể chứa trong 1 phòng")]
        public int? NumberOfPeoperPerRoomAdult { get; set; }

        [DisplayName("Số lượng trẻ con có thể chứa trong 1 phòng")]
        public int? NumberOfPeoperPerRoomKid { get; set; }

        [DisplayName("Số người tối đa")]
        public int? NumberOfPersonPerRoom { get; set; }

        [DisplayName("Đơn giá")]
        public decimal? Price { get; set; }

        [DisplayName("Mô tả")]
        public string Description { get; set; }
        
        [DisplayName("Mã Tỉnh")]
        public string TinhCode { get; set; }

        [DisplayName("Mã Xã")]
        public string XaCode { get; set; }

        [DisplayName("Mã Huyện")]
        public string HuyenCode { get; set; }

        [DisplayName("Mã Quốc Gia")]
        public string QuocGiaCode { get; set; }
        
        [DisplayName("Địa chỉ")]
        public string Address { get; set; }

        [DisplayName("Danh sách dịch vụ theo khu")]
        public string ServicesCode { get; set; }

        [DisplayName("Nhập điện nước từ")]
        public int? InputWaterElectricFrom { get; set; }

        [DisplayName("Nhập điện nước đến")]
        public int? InputWaterElectricTo { get; set; }

        [DisplayName("Xuất hóa đơn từ")]
        public int? BillsFrom { get; set; }

        [DisplayName("Xuất hóa đơn đến")]
        public int? BillsTo { get; set; }

        [DisplayName("Ngày thu tiền từ")]
        public int? CollectionMoneyFrom { get; set; }

        [DisplayName("Ngày thu tiền đến")]
        public int? CollectionMoneyTo { get; set; }

        [DisplayName("Hết hạn hợp đồng từ")]
        public int? EndOfConstractFrom { get; set; }

        [DisplayName("Hết hạn hợp đồng đến")]
        public int? EndOfConstractTo { get; set; }


        [DisplayName("Đã ký hợp đồng hay chưa")]
        public bool? IsSignedOfContact { get; set; }

        [DisplayName("Kinh độ")]
        public string Longitude { get; set; }

        [DisplayName("Vĩ độ")]
        public string Latitude { get; set; }
        
        [DisplayName("Mã Đường")]
        public string StreetCode { get; set; }

        [DisplayName("Id dự án")]
        public long? DuAnId { get; set; }

        [DisplayName("Mục đích sử dụng")]
        public string MucDichSuDung { get; set; }

        [DisplayName("Đơn vị")]
        public string DonVi { get; set; }

        [DisplayName("Hướng nhà")]
        public string HuongNha { get; set; }

        [DisplayName("Hướng ban công")]
        public string HuongBanCong { get; set; }

        [DisplayName("Số phòng ngủ")]
        public int SoPhongNgu { get; set; }

        [DisplayName("Số toilet")]
        public int SoToiLet { get; set; }

        [DisplayName("Nội thất")]
        public string NoiThat { get; set; }

        [DisplayName("Thông tin pháp lý")]
        public string ThongTinPhapLy { get; set; }

        [DisplayName("Tên liên hệ")]
        public string TenLienHe { get; set; }

        [DisplayName("Địa chỉ liên hệ")]
        public string DiaChiLienHe { get; set; }

        [DisplayName("Điện thoại")]
        public string DienThoai { get; set; }

        [DisplayName("Di động")]
        public string DiDong { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Trạng thái")]
        public string Status { get; set; }

        [DisplayName("Trạng thái tin")]
        public bool? StatusTin { get; set; }

        [DisplayName("Ngày bắt đầu đăng tin")]
        public DateTime? StartDate { get; set; }

        [DisplayName("Ngày kết thúc đăng tin")]
        public DateTime? EndDate { get; set; }
    }
}
