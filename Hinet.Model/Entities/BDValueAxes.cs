﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("BDValueAxes")]
    public class BDValueAxes : AuditableEntity<long>
    {
        public long? IdBieuDo { get; set; }
        public string IdValue { get; set; }
        public string StackType { get; set; }
        public string Title { get; set; }
    }
}
