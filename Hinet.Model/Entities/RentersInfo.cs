﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("RentersInfo")]
    public class RentersInfo : AuditableEntity<long>
    {
        [DisplayName("Id Khu/Tòa nhà")]
        public long? BuildingsId { get; set; }

        [DisplayName("Id phòng")]
        public long? RoomsId { get; set; }
        
        [DisplayName("Tên khách thuê")]
        public string RentersName { get; set; }
        
        [DisplayName("Số điện thoại")]
        public long? Phone { get; set; }
        
        [DisplayName("Email")]
        public string Email { get; set; }
        
        [DisplayName("Ngày sinh")]
        public DateTime? DateOfBirth { get; set; }
        
        [DisplayName("Giới tính")]
        public string SexCode { get; set; }
        
        [DisplayName("Số SMND/CCCD")]
        public int? NumberId { get; set; }
        
        [DisplayName("Ngày cấp")]
        public DateTime? NumberDate { get; set; }
        
        [DisplayName("Nơi cấp")]
        public string NumberAddress { get; set; }
        
        [DisplayName("Hộ khẩu")]
        public string HoKhau { get; set; }
        
        [DisplayName("Nghề nghiệp")]
        public string JobCode { get; set; }
        
        [DisplayName("Nơi công tác")]
        public string JobAddress { get; set; }
        
        [DisplayName("Đăng ký tạm trú")]
        public bool? DangKyTamTru { get; set; }

        [DisplayName("Ghi chú")]
        public string Note { get; set; }
        
        [DisplayName("Họ tên bố/mẹ")]
        public string ParentName { get; set; }
        
        [DisplayName("Số điện thoại liên hệ")]
        public int? ParentPhone { get; set; }
    }
}
