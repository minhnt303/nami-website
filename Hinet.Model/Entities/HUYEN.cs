﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("HUYEN")]
    public class HUYEN : Entity<int>
    {
        [StringLength(10)]
        public string MaHuyen { get; set; }
        public string TinhId { get; set; }
        [Required]
        [StringLength(50)]
        public string TenHuyen { get; set; }
        [StringLength(250)]
        public string Location { get; set; }
        [Required]
        [StringLength(50)]
        public string Loai { get; set; }

        [DisplayName("Kinh độ")]
        public string Longitude { get; set; }

        [DisplayName("Vĩ độ")]
        public string Latitude { get; set; }
        public string LastMaHuyen { get; set; }
    }
}
