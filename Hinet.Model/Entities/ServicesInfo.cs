﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("ServicesInfo")]
    public class ServicesInfo : AuditableEntity<long>
    {
        [DisplayName("Tên dịch vụ")]
        public string ServicesName { get; set; }
        
        [DisplayName("Đơn giá")]
        public int? Price { get; set; }

        [DisplayName("Đơn vị")]
        public string UnitsCode { get; set; }

        [DisplayName("Mô tả")]
        public string Description { get; set; }
    }
}
