﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("QuanLyDuAn")]
    public class QuanLyDuAn : AuditableEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string TenDuAn { get; set; }
        public string DuAnId { get; set; }
        public string TinhId { get; set; }
        public string HuyenId { get; set; }
        [StringLength(250)]
        public string Location { get; set; }

        [DisplayName("Kinh độ")]
        public string Longitude { get; set; }

        [DisplayName("Vĩ độ")]
        public string Latitude { get; set; }

        public string ImageUrl { get; set; }
    }
}
