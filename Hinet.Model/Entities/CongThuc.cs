﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("CongThuc")]
    public class CongThuc : AuditableEntity<long>
    {
        [DisplayName("Tên công thức")]
        public string CongThucName { get; set; }
        
        [DisplayName("Dịch vụ")]
        public string DichVuCode { get; set; }
        
        [DisplayName("Mô tả")]
        public string Description { get; set; }
    }
}
