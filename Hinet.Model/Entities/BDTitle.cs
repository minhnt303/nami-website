﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("BDTitle")]
    public class BDTitle : AuditableEntity<long>
    {
        public long? IdBieuDo { get; set; }
        public string IdTitle { get; set; }
        public int Size { get; set; }
        public string Text { get; set; }
        public bool IsBold { get; set; }
        public string Color { get; set; }
    }
}
