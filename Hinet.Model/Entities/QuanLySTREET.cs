﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("QuanLySTREET")]
    public class QuanLySTREET : AuditableEntity<long>
    {
        [StringLength(10)]
        public string MaDuong { get; set; }
        [Required]
        [StringLength(50)]
        public string TenDuong { get; set; }
        public string DuongId { get; set; }
        public string TinhId { get; set; }
        public string HuyenId { get; set; }
        [Required]
        [StringLength(50)]
        public string Loai { get; set; }
        [StringLength(250)]
        public string Location { get; set; }

        [DisplayName("Kinh độ")]
        public string Longitude { get; set; }

        [DisplayName("Vĩ độ")]
        public string Latitude { get; set; }
    }
}
