﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("DevicesInfo")]
    public class DevicesInfo:AuditableEntity<long>
    {
        [DisplayName("Id Khu/Tòa nhà")]
        public long? BuildingsId { get; set; }

        [DisplayName("Id Phòng")]
        public long? RoomsId { get; set; }

        [DisplayName("Tên thiết bị")]
        public string DeviceName { get; set; }

        [DisplayName("Giá")]
        public int? Price { get; set; }

        [DisplayName("Giá bồi thường")]
        public int? CompensationPrice { get; set; }

        [DisplayName("Trạng thái")]
        public string Status { get; set; }
        
        [DisplayName("Đơn vị")]
        public string UnitsCode { get; set; }
        
        [DisplayName("Mô tả")]
        public string Description { get; set; }
        
        [DisplayName("Số lượng")]
        public int Quantity { get; set; }
    }
}
