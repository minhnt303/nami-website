﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("BDSetUp")]
    public class BDSetUp : AuditableEntity<long>
    {
        public long? IdBieuDo { get; set; }

        public string CategoryFeild { get; set; }

        public int? StartDuration { get; set; }

        public bool Rotate { get; set; }

        public string TextColor { get; set; }

        public string Theme { get; set; }

        public int FontSize { get; set; }

        public string FontFamily { get; set; }
    }
}
