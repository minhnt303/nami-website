﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("TINH2")]
    public class TINH2 : Entity<int>
    {
        [StringLength(10)]
        public string MaTinh { get; set; }
        [Required]
        [StringLength(50)]

        public string TenTinh { get; set; }
        public string Code { get; set; }
        [StringLength(50)]
        public string Loai { get; set; }

        [DisplayName("Kinh độ")]
        public string Longitude { get; set; }

        [DisplayName("Vĩ độ")]
        public string Latitude { get; set; }
        public string LastMaTinh { get; set; }
    }
}
