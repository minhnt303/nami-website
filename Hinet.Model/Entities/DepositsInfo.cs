﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("DepositsInfo")]
    public class DepositsInfo : AuditableEntity<long>
    {
        [DisplayName("Id Khu/Tòa nhà")]
        public long? BuildingsId { get; set; }

        [DisplayName("Id Phòng")]
        public long? RoomsId { get; set; }

        [DisplayName("Id khách thuê")]
        public long? RentersId { get; set; }
        
        [DisplayName("Ngày đặt cọc")]
        public DateTime? DepositDate { get; set; }

        [DisplayName("Ngày hết hạn")]
        public DateTime ExpirationDate { get; set; }

        [DisplayName("Tiền đặt cọc")]
        public int? DepositsMoney { get; set; }
        
        [DisplayName("Ghi chú")]
        public string Note { get; set; }
    }
}
