﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("QLHopDongDangKy")]
    public class QLHopDongDangKy : AuditableEntity<long>
    {
        [DisplayName("Id đối tượng")]
        public long? ItemId { get; set; }

        [DisplayName("Loại đối tượng")]
        public string ItemType { get; set; }

        [DisplayName("Id người đăng ký")]
        public long? UserIdDangKy { get; set; }
    }
}
