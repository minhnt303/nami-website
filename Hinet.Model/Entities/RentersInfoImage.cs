﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("RentersInfoImage")]
    public class RentersInfoImage : AuditableEntity<long>
    {
        [DisplayName("Id người thuê")]
        public long? RentersId { get; set; }
        
        [DisplayName("Loại ảnh")]
        public string RentersType { get; set; }

        [DisplayName("Đường dẫn ảnh")]
        public string ImageUrl { get; set; }
    }
}
