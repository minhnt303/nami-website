﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("MapLocationInfo")]
    public class MapLocationInfo: AuditableEntity<long>
    {
        [DisplayName("Id Khu/Tòa nhà")]
        public long? BuildingsId { get; set; }

        [DisplayName("Id Phòng")]
        public long? RoomsId { get; set; }

        [DisplayName("Loại phòng hay khu/ tòa nhà")]
        public string ItemTypeCode { get; set; }
        
        [DisplayName("Tọa độ X")]
        public string Xlocation { get; set; }

        [DisplayName("Loại phòng hay khu/ tòa nhà")]
        public string Ylocation { get; set; }
    }
}
