﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("TinRaoNha")]
    public class TinRaoNha : AuditableEntity<long>
    {
        public long IdItem { get; set; }

        public long TypeItem { get; set; }

        public string TieuDe { get; set; }

        public string Loai { get; set; }

        [DisplayName("Mã Tỉnh")]
        public string TinhCode { get; set; }

        [DisplayName("Mã Xã")]
        public string XaCode { get; set; }

        [DisplayName("Mã Huyện")]
        public string HuyenCode { get; set; }

        [DisplayName("Mã Quốc Gia")]
        public string QuocGiaCode { get; set; }

        [DisplayName("Mã Đường")]
        public string StreetCode { get; set; }

        public long? DuAnId { get; set; }

        public decimal? DienTich { get; set; }

        [DisplayName("Kinh độ")]
        public string Longitude { get; set; }

        [DisplayName("Vĩ độ")]
        public string Latitude { get; set; }

        public decimal? Giá { get; set; }

        public string DonViCode { get; set; }

        public string DiaChi { get; set; }

        public string MoTa { get; set; }

        public decimal? MatTien { get; set; }

        public decimal? DuongVao { get; set; }

        public string HuongNha { get; set; }

        public string HuongBanCong { get; set; }

        public int SoTang { get; set; }

        public int SoPhongNgu { get; set; }

        public int SoToiLet { get; set; }

        public string NoiThat { get; set; }

        public string ThongTinPhapLy { get; set; }

        public string TenLienHe { get; set; }

        public string DiaChiLienHe { get; set; }

        public int DienThoai { get; set; }

        public string Email { get; set; }

        public string LoiTinRao { get; set; }

        public DateTime? NgayBatDau { get; set; }

        public DateTime? NgayKetThuc { get; set; }

        public int SoTien { get; set; }

        public bool Status { get; set; }
    }
}
