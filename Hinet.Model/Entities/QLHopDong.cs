﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("QLHopDong")]
    public class QLHopDong : AuditableEntity<long>
    {
        [DisplayName("Id đối tượng")]
        public long? ItemId { get; set; }

        [DisplayName("Loại đối tượng")]
        public string ItemType { get; set; }

        [DisplayName("Loại hợp đồng")]
        public string AgreementCode { get; set; }

        [DisplayName("Người đại diện")]
        public string RepresenterName { get; set; }

        [DisplayName("Tiền đặt cọc")]
        public int? DepositsMoney { get; set; }

        [DisplayName("Thời hạn hợp đồng tháng")]
        public int? Month { get; set; }

        [DisplayName("Thời hạn hợp đồng ngày")]
        public int? Ngay { get; set; }

        [DisplayName("Ngày bắt đầu")]
        public DateTime? DateStart { get; set; }

        [DisplayName("Ngày kết thúc")]
        public DateTime? DateEnd { get; set; }

        [DisplayName("Kỳ thanh toán")]
        public int? PayPerMonth { get; set; }

        [DisplayName("Ghi chú")]
        public string Note { get; set; }

        [DisplayName("Id người đăng ký")]
        public long? UserId { get; set; }
    }
}
