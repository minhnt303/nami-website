﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("CauHinhCongThuc")]
    public class CauHinhCongThuc : AuditableEntity<long>
    {
        [DisplayName("Id của công thức")]
        public long? CongThucId { get; set; }
        
        [DisplayName("Tên định mức")]
        public string DinhMucName { get; set; }

        [DisplayName("Số đầu")]
        public int? SoDau { get; set; }

        [DisplayName("Số cuối")]
        public int? SoCuoi { get; set; }
        
        [DisplayName("Giá")]
        public int? Price { get; set; }

        [DisplayName("Mô tả")]
        public string Description { get; set; }
    }
}
