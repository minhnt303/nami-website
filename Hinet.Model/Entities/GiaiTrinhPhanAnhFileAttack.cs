﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    /// <summary>
    /// bảng về file đính kèm của giải trình phản ánh
    /// </summary>
    [Table("GiaiTrinhPhanAnhFileAttack")]
    public class GiaiTrinhPhanAnhFileAttack : AuditableEntity<long>
    {
        /// <summary>
        /// Id của giải trình phản ánh
        /// </summary>
        [DisplayName("Id của giải trình phản ánh")]
        public long? IdGiaiTrinhPhanAnh { get; set; }

        /// <summary>
        /// Đường dẫn file giải trình phản ánh
        /// </summary>
        [DisplayName("Đường dẫn file giải trình phản ánh")]
        public string FileUrlGiaiTrinhPhanAnh { get; set; }
    }
}
