﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("QLCauHoi")]
    public class QLCauHoi : AuditableEntity<long>
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string CauHoi { get; set; }

        public long IdNguoiXuLy { get; set; }

        public string Status { get; set; }
    }
}
