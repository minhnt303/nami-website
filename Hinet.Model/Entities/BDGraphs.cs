﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("BDGraphs")]
    public class BDGraphs : AuditableEntity<long>
    {
        public long? IdBieuDo { get; set; }

        public string BalloonText { get; set; } 

        public bool IsHidden { get; set; }

        public string IdGraphs { get; set; }

        public string Title { get; set; }

        public string Type { get; set; }

        public string ValueField { get; set; }

        public string BalloonColor { get; set; }

        public bool ShowBalloon { get; set; }

        public decimal ColumnWidth { get; set; }

        public int ConnerRadius { get; set; }

        public int DashLength { get; set; }

        public string FillColor { get; set; }

        public int FixedColoumWidth { get; set; }

        public int FontSize { get; set; }

        public string Bullet { get; set; }

        public decimal BulletAlpha { get; set; }

        public string BulletColor { get; set; }

        public int BulletSize { get; set; }

        public string CustomBullet { get; set; }

        public string LabelText { get; set; }

        public decimal LabelRotation { get; set; }

        public string LabelPosition { get; set; }
    }
}
