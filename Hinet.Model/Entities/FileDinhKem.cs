﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("FileDinhKem")]
    public class FileDinhKem : AuditableEntity<long>
    {
        [DisplayName("Id đối tượng")]
        public long? IdItem { get; set; }

        [DisplayName("Loại đối tượng")]
        public string TypeItem { get; set; }

        [DisplayName("Đường dẫn ảnh")]
        public string ImageUrl { get; set; }
    }
}
