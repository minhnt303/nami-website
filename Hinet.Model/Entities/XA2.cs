﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("XA2")]
    public class XA2 : Entity<int>
    {
        [StringLength(10)]
        public string MaXa { get; set; }
        [Required]
        [StringLength(50)]
        public string TenXa { get; set; }
        public string HuyenId { get; set; }
        public string TinhId { get; set; }
        [Required]
        [StringLength(50)]
        public string Loai { get; set; }
        [StringLength(250)]
        public string Location { get; set; }

        [DisplayName("Kinh độ")]
        public string Longitude { get; set; }

        [DisplayName("Vĩ độ")]
        public string Latitude { get; set; }
        public string LastMaXa { get; set; }
    }
}
