﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("BDBieuDo")]
    public class BDBieuDo : AuditableEntity<long>
    {
        public string Name { get; set; }

        public string Type { get; set; }
    }
}
