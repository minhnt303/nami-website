﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("BDDataProvince")]
    public class BDDataProvince : AuditableEntity<long>
    {
        public long? IdBieuDo { get; set; }
        public bool IsCategory { get; set; }
        public string Key { get; set; }
        public decimal Value { get; set; }
    }
}
