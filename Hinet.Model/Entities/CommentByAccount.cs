﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nami.Model.Entities
{
    [Table("CommentByAccount")]
    public class CommentByAccount: AuditableEntity<long>
    {
        //Id của người đăng bình luận
        public long? PortUserId { get; set; }

        //Id của người bình luận trước (nếu có)
        public int? ParentPostUserId { get; set; }

        //Id của loại hồ sơ được bình luận
        public long? IdItem { get; set; }

        //Loại hồ sơ bình luận
        public string TypeItem { get; set; }

        //Bình luận
        public string Comments { get; set; }

        //Ghim: dùng để lưu lại bài viết hoặc bình luận
        public string Ghim { get; set; }

        //flag: Gắn cờ dùng để hiện thái xóa tạm
        [StringLength(1)]
        public string flag { get; set; }
    }
}
