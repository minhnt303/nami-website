﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    // giải trình từ phía bên hồ sơ bị phản ánh
    [Table("GiaiTrinhPhanAnh")]
    public class GiaiTrinhPhanAnh : AuditableEntity<long>
    {
        /// <summary>
        /// Loại giải trình là : yêu cầu giải trình hoặc là trình bày giải trình lý do bị phản ánh và quy trình hòa giải
        /// </summary>
        [DisplayName("Loại giải trình")]
        public string LoaiGiaiTrinh { get; set; }

        /// <summary>
        /// Đoạn giải trình phản ánh
        /// </summary>
        [DisplayName("Đoạn giải trình phản ánh")]
        public string GiaiTrinhComment { get; set; }

        /// <summary>
        /// Loại phản ánh: website hay ứng dụng
        /// </summary>
        [DisplayName("Loại phản ánh")]
        public string PhanAnhType { get; set; }

        /// <summary>
        /// Id của phản ánh
        /// </summary>
        [DisplayName("Id phản ánh")]
        public long? PhanAnhId { get; set; }

        /// <summary>
        /// Id của hồ sơ  giải trình lấy theo OrganizationId
        /// </summary>
        [DisplayName("Id Hồ sơ giải trình")]
        public long? IdHoSoGiaiTrinh { get; set; }

        /// <summary>
        /// Loại hồ sơ  giải trình lấy theo TypeOrganization
        /// </summary>
        [DisplayName("Loại hồ sơ giải trình")]
        public string TypeHoSoGiaiTrinh { get; set; }

        /// <summary>
        /// Ngày  giải trình
        /// </summary>
        [DisplayName("Ngày giải trình")]
        public DateTime? NgayGiaiTrinh { get; set; }
    }
}
