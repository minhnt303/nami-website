﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("QLHopDongItem")]
    public class QLHopDongItem : AuditableEntity<long>
    {
        [DisplayName("Id hợp đồng")]
        public long? IdQLHopDong { get; set; }

        [DisplayName("Tháng")]
        public int? Thang { get; set; }

        [DisplayName("Ngày bắt đầu")]
        public DateTime? StartDate { get; set; }

        [DisplayName("Ngày kết thúc")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Tiền đặt cọc")]
        public int? DepositsMoney { get; set; }

        [DisplayName("Tiền phải trả hàng tháng")]
        public int? TienPhaiTraHangThang { get; set; }

        [DisplayName("Tiền tháng này")]
        public int? TienThangNay { get; set; }

        [DisplayName("Ngày trả tiền hàng tháng")]
        public DateTime? PayDate { get; set; }

        [DisplayName("Ghi chú")]
        public string Note { get; set; }

        public bool Status { get; set; }
    }
}
