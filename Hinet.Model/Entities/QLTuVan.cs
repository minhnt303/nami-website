﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Model.Entities
{
    [Table("QLTuVan")]
    public class QLTuVan : AuditableEntity<long>
    {
        public string ImageLink { get; set; }
        public string Name { get; set; }
        public string NamePhu { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public string Author { get; set; }
        public long ChuDeId { get; set; }
        public long IdCauHoi { get; set; }
        public string SlugTitle { get; set; }
    }
}
