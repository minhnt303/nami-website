﻿namespace Nami.Model.Common
{
    public class DanhMucConstantBase
    {
        public static string DanToc = "DANTOC";
        public static string TonGiao = "TONGIAO";
        public static string QuocGia = "QUOCGIA";
        public static string XuatThan = "XUATTHAN";
        public static string ChucVu = "CHUCVU";
        public static string NEWS_STATUS = "NEWS_STATUS";
        public static string DOCUMENT_LEGAL_STATUS = "DOCUMENT_LEGAL_STATUS";
        public static string DOCUMENT_DATA_STATUS = "DOCUMENT_DATA_STATUS";
        public static string REFLECTION_INFO_KIND = "REFLECTION_INFO_KIND";
        public static string LEVEL_OF_REFLECTION = "LEVEL_OF_REFLECTION";
        public static string SOURCE_WEBSITE_VI_PHAM = "SOURCE_WEBSITE_VI_PHAM";
        public static string SOURCE_APP_VI_PHAM = "SOURCE_APP_VI_PHAM";
        public static string PRODUCT_TYPE = "PRODUCT_TYPE";
        public static string WEBSITE_APP_TYPE = "WEBSITE_APP_TYPE";
        public static string APP_PRODUCT = "APP_PRODUCT";
        public static string APP_UTILITY = "APP_UTILITY";
        public static string APP_SERVICE_PROVIDER = "APP_SERVICE_PROVIDER";
        public static string APP_HOSTING_PROVIDER = "APP_HOSTING_PROVIDER";
        public static string DON_VI = "DON_VI";
        public static string CHUDETINTUC = "CHUDETINTUC";
        public static string HUONGNHA { get; set; } = "HUONGNHA";
        public static string MANGANHANG { get; set; } = "MANGANHANG";
        public static string MUCDICHSUDUNG { get; set; } = "MUCDICHSUDUNG";
    }
}
