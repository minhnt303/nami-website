using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.QLTuVanRepository;
using Nami.Service.Common;
using Nami.Service.QLTuVanService.Dto;
using log4net;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Nami.Repository.DanhmucRepository;
using Nami.Service.Constant;
using System;
using Nami.Repository.QLCauHoiRepository;

namespace Nami.Service.QLTuVanService
{
    public class QLTuVanService : EntityService<QLTuVan>, IQLTuVanService
    {
        IUnitOfWork _unitOfWork;
        IQLTuVanRepository _QLTuVanRepository;
        ILog _loger;
        IMapper _mapper;
        IDM_DulieuDanhmucRepository _dM_DulieuDanhmucRepository;
        IDM_NhomDanhmucRepository _dM_NhomDanhmucRepository;
        IQLCauHoiRepository _qLCauHoiRepository;

        public QLTuVanService(IUnitOfWork unitOfWork,
        IQLTuVanRepository QLTuVanRepository,
        ILog loger,
        IDM_DulieuDanhmucRepository dM_DulieuDanhmucRepository,
        IDM_NhomDanhmucRepository dM_NhomDanhmucRepository,
        IQLCauHoiRepository qLCauHoiRepository,
                IMapper mapper
            )
            : base(unitOfWork, QLTuVanRepository)
        {
            _unitOfWork = unitOfWork;
            _QLTuVanRepository = QLTuVanRepository;
            _loger = loger;
            _mapper = mapper;
            _dM_DulieuDanhmucRepository = dM_DulieuDanhmucRepository;
            _dM_NhomDanhmucRepository = dM_NhomDanhmucRepository;
            _qLCauHoiRepository = qLCauHoiRepository;
        }

        public PageListResultBO<QLTuVanDto> GetDaTaByPage(QLTuVanSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var GroupDanhMucId = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.CHUDETINTUC).Select(x => x.Id).FirstOrDefault();
            var query = from QLTuVantbl in _QLTuVanRepository.GetAllAsQueryable()

                        select new QLTuVanDto
                        {
                            ImageLink = QLTuVantbl.ImageLink,
                            Name = QLTuVantbl.Name,
                            NamePhu = QLTuVantbl.NamePhu,
                            Description = QLTuVantbl.Description,
                            Status = QLTuVantbl.Status,
                            Author = QLTuVantbl.Author,
                            ChuDeId = QLTuVantbl.ChuDeId,
                            IdCauHoi = QLTuVantbl.IdCauHoi,
                            IsDelete = QLTuVantbl.IsDelete,
                            DeleteTime = QLTuVantbl.DeleteTime,
                            DeleteId = QLTuVantbl.DeleteId,
                            Id = QLTuVantbl.Id,
                            CreatedDate = QLTuVantbl.CreatedDate,
                            CreatedBy = QLTuVantbl.CreatedBy,
                            CreatedID = QLTuVantbl.CreatedID,
                            UpdatedDate = QLTuVantbl.UpdatedDate,
                            UpdatedBy = QLTuVantbl.UpdatedBy,
                            UpdatedID = QLTuVantbl.UpdatedID,
                            SlugTitle = QLTuVantbl.SlugTitle
                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.ImageLinkFilter))
                {
                    query = query.Where(x => x.ImageLink.Contains(searchModel.ImageLinkFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.NameFilter))
                {
                    query = query.Where(x => x.Name.Contains(searchModel.NameFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.NamePhuFilter))
                {
                    query = query.Where(x => x.NamePhu.Contains(searchModel.NamePhuFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.DescriptionFilter))
                {
                    query = query.Where(x => x.Description.Contains(searchModel.DescriptionFilter));
                }
                if (searchModel.StatusFilter != null)
                {
                    query = query.Where(x => x.Status == searchModel.StatusFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.AuthorFilter))
                {
                    query = query.Where(x => x.Author.Contains(searchModel.AuthorFilter));
                }
                if (searchModel.ChuDeIdFilter != null)
                {
                    query = query.Where(x => x.ChuDeId == searchModel.ChuDeIdFilter);
                }
                if (searchModel.IdCauHoiFilter != null)
                {
                    query = query.Where(x => x.IdCauHoi == searchModel.IdCauHoiFilter);
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.UpdatedDate);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.UpdatedDate);
            }
            var resultmodel = new PageListResultBO<QLTuVanDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            foreach (var item in resultmodel.ListItem)
            {
                if (item.ChuDeId != 0 && GroupDanhMucId != 0)
                {
                    var ChuDeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == GroupDanhMucId && x.Id == item.ChuDeId).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(ChuDeName))
                    {
                        item.ChuDeName = ChuDeName;
                    }
                }
                if (item.IdCauHoi != 0) {
                    var CauHoiInfo = _qLCauHoiRepository.GetAllAsQueryable().Where(x => x.Id == item.IdCauHoi).FirstOrDefault();
                    if (CauHoiInfo != null) {
                        item.CauHoiInfo = CauHoiInfo;
                    }
                }
            }
            return resultmodel;
        }

        public QLTuVan GetById(long id)
        {
            return _QLTuVanRepository.GetById(id);
        }

        public List<QLTuVanDto> GetListTuVan() {
            var GroupDanhMucId = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.CHUDETINTUC).Select(x => x.Id).FirstOrDefault();
            var query = (from QLTuVantbl in _QLTuVanRepository.GetAllAsQueryable().Where(x=>x.Status == true)

                        select new QLTuVanDto
                        {
                            ImageLink = QLTuVantbl.ImageLink,
                            Name = QLTuVantbl.Name,
                            NamePhu = QLTuVantbl.NamePhu,
                            Description = QLTuVantbl.Description,
                            Status = QLTuVantbl.Status,
                            Author = QLTuVantbl.Author,
                            ChuDeId = QLTuVantbl.ChuDeId,
                            IdCauHoi = QLTuVantbl.IdCauHoi,
                            IsDelete = QLTuVantbl.IsDelete,
                            DeleteTime = QLTuVantbl.DeleteTime,
                            DeleteId = QLTuVantbl.DeleteId,
                            Id = QLTuVantbl.Id,
                            CreatedDate = QLTuVantbl.CreatedDate,
                            CreatedBy = QLTuVantbl.CreatedBy,
                            CreatedID = QLTuVantbl.CreatedID,
                            UpdatedDate = QLTuVantbl.UpdatedDate,
                            UpdatedBy = QLTuVantbl.UpdatedBy,
                            UpdatedID = QLTuVantbl.UpdatedID,
                            SlugTitle = QLTuVantbl.SlugTitle
                        }).OrderByDescending(x=>x.UpdatedDate).ToList();

            foreach (var item in query)
            {
                if (item.ChuDeId != 0 && GroupDanhMucId != 0)
                {
                    var ChuDeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == GroupDanhMucId && x.Id == item.ChuDeId).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(ChuDeName))
                    {
                        item.ChuDeName = ChuDeName;
                    }
                }
                if (item.IdCauHoi != 0)
                {
                    var CauHoiInfo = _qLCauHoiRepository.GetAllAsQueryable().Where(x => x.Id == item.IdCauHoi).FirstOrDefault();
                    if (CauHoiInfo != null)
                    {
                        item.CauHoiInfo = CauHoiInfo;
                    }
                }
            }
            return query;
        }

        public QLTuVanDto GetBySlugName(string slugName)
        {
            var GroupDanhMucId = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.CHUDETINTUC).Select(x => x.Id).FirstOrDefault();
            var item = (from QLTuVantbl in _QLTuVanRepository.GetAllAsQueryable().Where(x => x.Status == true && x.SlugTitle == slugName)

                         select new QLTuVanDto
                         {
                             ImageLink = QLTuVantbl.ImageLink,
                             Name = QLTuVantbl.Name,
                             NamePhu = QLTuVantbl.NamePhu,
                             Description = QLTuVantbl.Description,
                             Status = QLTuVantbl.Status,
                             Author = QLTuVantbl.Author,
                             ChuDeId = QLTuVantbl.ChuDeId,
                             IdCauHoi = QLTuVantbl.IdCauHoi,
                             IsDelete = QLTuVantbl.IsDelete,
                             DeleteTime = QLTuVantbl.DeleteTime,
                             DeleteId = QLTuVantbl.DeleteId,
                             Id = QLTuVantbl.Id,
                             CreatedDate = QLTuVantbl.CreatedDate,
                             CreatedBy = QLTuVantbl.CreatedBy,
                             CreatedID = QLTuVantbl.CreatedID,
                             UpdatedDate = QLTuVantbl.UpdatedDate,
                             UpdatedBy = QLTuVantbl.UpdatedBy,
                             UpdatedID = QLTuVantbl.UpdatedID,
                             SlugTitle = QLTuVantbl.SlugTitle
                         }).FirstOrDefault();
            if (item.ChuDeId != 0 && GroupDanhMucId != 0)
            {
                var ChuDeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == GroupDanhMucId && x.Id == item.ChuDeId).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ChuDeName))
                {
                    item.ChuDeName = ChuDeName;
                }
            }
            if (item.IdCauHoi != 0)
            {
                var CauHoiInfo = _qLCauHoiRepository.GetAllAsQueryable().Where(x => x.Id == item.IdCauHoi).FirstOrDefault();
                if (CauHoiInfo != null)
                {
                    item.CauHoiInfo = CauHoiInfo;
                }
            }
            return item;
        }

        public QLTuVanDto GetDtoById(long id)
        {
            var GroupDanhMucId = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.CHUDETINTUC).Select(x => x.Id).FirstOrDefault();
            var item = (from QLTuVantbl in _QLTuVanRepository.GetAllAsQueryable().Where(x => x.Id == id)

                        select new QLTuVanDto
                        {
                            ImageLink = QLTuVantbl.ImageLink,
                            Name = QLTuVantbl.Name,
                            NamePhu = QLTuVantbl.NamePhu,
                            Description = QLTuVantbl.Description,
                            Status = QLTuVantbl.Status,
                            Author = QLTuVantbl.Author,
                            ChuDeId = QLTuVantbl.ChuDeId,
                            IdCauHoi = QLTuVantbl.IdCauHoi,
                            IsDelete = QLTuVantbl.IsDelete,
                            DeleteTime = QLTuVantbl.DeleteTime,
                            DeleteId = QLTuVantbl.DeleteId,
                            Id = QLTuVantbl.Id,
                            CreatedDate = QLTuVantbl.CreatedDate,
                            CreatedBy = QLTuVantbl.CreatedBy,
                            CreatedID = QLTuVantbl.CreatedID,
                            UpdatedDate = QLTuVantbl.UpdatedDate,
                            UpdatedBy = QLTuVantbl.UpdatedBy,
                            UpdatedID = QLTuVantbl.UpdatedID,
                            SlugTitle = QLTuVantbl.SlugTitle
                        }).FirstOrDefault();
            if (item.ChuDeId != 0 && GroupDanhMucId != 0)
            {
                var ChuDeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == GroupDanhMucId && x.Id == item.ChuDeId).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ChuDeName))
                {
                    item.ChuDeName = ChuDeName;
                }
            }
            if (item.IdCauHoi != 0)
            {
                var CauHoiInfo = _qLCauHoiRepository.GetAllAsQueryable().Where(x => x.Id == item.IdCauHoi).FirstOrDefault();
                if (CauHoiInfo != null)
                {
                    item.CauHoiInfo = CauHoiInfo;
                }
            }
            return item;
        }


        public List<QLTuVanDto> GetListTuVanByIdChuDe(long IdChuDe, int amount, long CurrentTuVanId)
        {
            var GroupDanhMucId = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.CHUDETINTUC).Select(x => x.Id).FirstOrDefault();
            var query = (from QLTuVantbl in _QLTuVanRepository.GetAllAsQueryable().Where(x => x.ChuDeId == IdChuDe && x.Id != CurrentTuVanId && x.Status == true)

                         select new QLTuVanDto
                         {
                             ImageLink = QLTuVantbl.ImageLink,
                             Name = QLTuVantbl.Name,
                             NamePhu = QLTuVantbl.NamePhu,
                             Description = QLTuVantbl.Description,
                             Status = QLTuVantbl.Status,
                             Author = QLTuVantbl.Author,
                             ChuDeId = QLTuVantbl.ChuDeId,
                             IdCauHoi = QLTuVantbl.IdCauHoi,
                             IsDelete = QLTuVantbl.IsDelete,
                             DeleteTime = QLTuVantbl.DeleteTime,
                             DeleteId = QLTuVantbl.DeleteId,
                             Id = QLTuVantbl.Id,
                             CreatedDate = QLTuVantbl.CreatedDate,
                             CreatedBy = QLTuVantbl.CreatedBy,
                             CreatedID = QLTuVantbl.CreatedID,
                             UpdatedDate = QLTuVantbl.UpdatedDate,
                             UpdatedBy = QLTuVantbl.UpdatedBy,
                             UpdatedID = QLTuVantbl.UpdatedID,
                             SlugTitle = QLTuVantbl.SlugTitle
                         }).OrderByDescending(x => x.UpdatedDate).Take(amount).ToList();

            foreach (var item in query)
            {
                if (item.ChuDeId != 0 && GroupDanhMucId != 0)
                {
                    var ChuDeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == GroupDanhMucId && x.Id == item.ChuDeId).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(ChuDeName))
                    {
                        item.ChuDeName = ChuDeName;
                    }
                }
                if (item.IdCauHoi != 0)
                {
                    var CauHoiInfo = _qLCauHoiRepository.GetAllAsQueryable().Where(x => x.Id == item.IdCauHoi).FirstOrDefault();
                    if (CauHoiInfo != null)
                    {
                        item.CauHoiInfo = CauHoiInfo;
                    }
                }
            }
            return query;
        }

        public PageListResultBO<QLTuVanDto> GetDaTaShowByPage(string KeySearch, int pageIndex = 1, int pageSize = 20)
        {
            var currentDate = DateTime.Now.Date;
            var GroupDanhMucId = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.CHUDETINTUC).Select(x => x.Id).FirstOrDefault();
            var query = from QLTuVantbl in _QLTuVanRepository.GetAllAsQueryable().Where(x => x.UpdatedDate <= currentDate && x.Status == true)

                        select new QLTuVanDto
                        {
                            ImageLink = QLTuVantbl.ImageLink,
                            Name = QLTuVantbl.Name,
                            NamePhu = QLTuVantbl.NamePhu,
                            Description = QLTuVantbl.Description,
                            Status = QLTuVantbl.Status,
                            Author = QLTuVantbl.Author,
                            ChuDeId = QLTuVantbl.ChuDeId,
                            IdCauHoi = QLTuVantbl.IdCauHoi,
                            IsDelete = QLTuVantbl.IsDelete,
                            DeleteTime = QLTuVantbl.DeleteTime,
                            DeleteId = QLTuVantbl.DeleteId,
                            Id = QLTuVantbl.Id,
                            CreatedDate = QLTuVantbl.CreatedDate,
                            CreatedBy = QLTuVantbl.CreatedBy,
                            CreatedID = QLTuVantbl.CreatedID,
                            UpdatedDate = QLTuVantbl.UpdatedDate,
                            UpdatedBy = QLTuVantbl.UpdatedBy,
                            UpdatedID = QLTuVantbl.UpdatedID,
                            SlugTitle = QLTuVantbl.SlugTitle
                        };

            if (!string.IsNullOrEmpty(KeySearch))
            {
                query = query.Where(x => x.Name.Contains(KeySearch) || x.CreatedBy.Contains(KeySearch));
            }
            query = query.OrderByDescending(x => x.UpdatedDate);
            var resultmodel = new PageListResultBO<QLTuVanDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
                resultmodel.CurrentPage = 1;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
                resultmodel.CurrentPage = pageIndex;
            }
            foreach (var item in resultmodel.ListItem)
            {
                if (item.ChuDeId != 0 && GroupDanhMucId != 0)
                {
                    var ChuDeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == GroupDanhMucId && x.Id == item.ChuDeId).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(ChuDeName))
                    {
                        item.ChuDeName = ChuDeName;
                    }
                }
                if (item.IdCauHoi != 0)
                {
                    var CauHoiInfo = _qLCauHoiRepository.GetAllAsQueryable().Where(x => x.Id == item.IdCauHoi).FirstOrDefault();
                    if (CauHoiInfo != null)
                    {
                        item.CauHoiInfo = CauHoiInfo;
                    }
                }
            }
            return resultmodel;
        }
    }
}
