using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.QLTuVanService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLTuVanService
{
    public interface IQLTuVanService:IEntityService<QLTuVan>
    {
        PageListResultBO<QLTuVanDto> GetDaTaByPage(QLTuVanSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        QLTuVan GetById(long id);
        List<QLTuVanDto> GetListTuVan();
        PageListResultBO<QLTuVanDto> GetDaTaShowByPage(string KeySearch, int pageIndex = 1, int pageSize = 20);
        List<QLTuVanDto> GetListTuVanByIdChuDe(long IdChuDe, int amount, long CurrentTuVanId);
        QLTuVanDto GetDtoById(long id);
        QLTuVanDto GetBySlugName(string slugName);
    }
}
