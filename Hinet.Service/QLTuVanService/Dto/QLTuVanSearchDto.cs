using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLTuVanService.Dto
{
    public class QLTuVanSearchDto : SearchBase
    {
		public string ImageLinkFilter { get; set; }
		public string NameFilter { get; set; }
		public string NamePhuFilter { get; set; }
		public string DescriptionFilter { get; set; }
		public bool StatusFilter { get; set; }
		public string AuthorFilter { get; set; }
		public long ChuDeIdFilter { get; set; }
		public long IdCauHoiFilter { get; set; }


    }
}