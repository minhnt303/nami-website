using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLTuVanService.Dto
{
    public class QLTuVanDto : QLTuVan
    {
        public string ChuDeName { get; set; }
        public QLCauHoi CauHoiInfo { get; set; }
    }
}