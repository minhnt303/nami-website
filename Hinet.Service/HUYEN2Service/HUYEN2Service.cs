using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.HUYEN2Repository;
using Nami.Service.HUYEN2Service.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;




namespace Nami.Service.HUYEN2Service
{
    public class HUYEN2Service : EntityService<HUYEN2>, IHUYEN2Service
    {
        IUnitOfWork _unitOfWork;
        IHUYEN2Repository _HUYEN2Repository;
	ILog _loger;
        IMapper _mapper;


        
        public HUYEN2Service(IUnitOfWork unitOfWork, 
		IHUYEN2Repository HUYEN2Repository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, HUYEN2Repository)
        {
            _unitOfWork = unitOfWork;
            _HUYEN2Repository = HUYEN2Repository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<HUYEN2Dto> GetDaTaByPage(HUYEN2SearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from HUYEN2tbl in _HUYEN2Repository.GetAllAsQueryable()

                        select new HUYEN2Dto
                        {

                            
                        };

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<HUYEN2Dto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public HUYEN2 GetById(long id)
        {
            return _HUYEN2Repository.GetById(id);
        }
    

    }
}
