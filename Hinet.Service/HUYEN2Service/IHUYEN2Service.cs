using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.HUYEN2Service.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.HUYEN2Service
{
    public interface IHUYEN2Service:IEntityService<HUYEN2>
    {
        PageListResultBO<HUYEN2Dto> GetDaTaByPage(HUYEN2SearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        HUYEN2 GetById(long id);
    }
}
