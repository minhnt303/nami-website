using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.QLHopDongService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLHopDongService
{
    public interface IQLHopDongService:IEntityService<QLHopDong>
    {
        PageListResultBO<QLHopDongDto> GetDaTaByPage(QLHopDongSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        PageListResultBO<QLHopDongDto> GetDaTaByPageByCurrentUserId(bool IsViewAll, long? CurrentUserId, QLHopDongSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        QLHopDongDto GetDtoById(long id);
        QLHopDong GetById(long id);
        List<QLHopDongDto> GetListHopDongByUserIdEndUser(long? UserId);
    }
}
