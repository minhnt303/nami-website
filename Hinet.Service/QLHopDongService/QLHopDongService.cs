using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.BuildingsInfoRepository;
using Nami.Repository.QLHopDongRepository;
using Nami.Repository.RoomInfoRepository;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.QLHopDongService.Dto;
using log4net;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;



namespace Nami.Service.QLHopDongService
{
    public class QLHopDongService : EntityService<QLHopDong>, IQLHopDongService
    {
        IUnitOfWork _unitOfWork;
        IQLHopDongRepository _QLHopDongRepository;
        ILog _loger;
        IMapper _mapper;
        IBuildingsInfoRepository _BuildingsInfoRepository;
        IRoomInfoRepository _RoomInfoRepository;

        public QLHopDongService(IUnitOfWork unitOfWork,
        IQLHopDongRepository QLHopDongRepository,
        ILog loger,
        IBuildingsInfoRepository BuildingsInfoRepository,
        IRoomInfoRepository RoomInfoRepository,
                IMapper mapper
            )
            : base(unitOfWork, QLHopDongRepository)
        {
            _unitOfWork = unitOfWork;
            _QLHopDongRepository = QLHopDongRepository;
            _loger = loger;
            _mapper = mapper;
            _BuildingsInfoRepository = BuildingsInfoRepository;
            _RoomInfoRepository = RoomInfoRepository;
        }

        public PageListResultBO<QLHopDongDto> GetDaTaByPage(QLHopDongSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from QLHopDongtbl in _QLHopDongRepository.GetAllAsQueryable()

                        select new QLHopDongDto
                        {
                            ItemId = QLHopDongtbl.ItemId,
                            ItemType = QLHopDongtbl.ItemType,
                            AgreementCode = QLHopDongtbl.AgreementCode,
                            RepresenterName = QLHopDongtbl.RepresenterName,
                            DepositsMoney = QLHopDongtbl.DepositsMoney,
                            Month = QLHopDongtbl.Month,
                            Ngay = QLHopDongtbl.Ngay,
                            DateStart = QLHopDongtbl.DateStart,
                            DateEnd = QLHopDongtbl.DateEnd,
                            PayPerMonth = QLHopDongtbl.PayPerMonth,
                            Note = QLHopDongtbl.Note,
                            IsDelete = QLHopDongtbl.IsDelete,
                            DeleteTime = QLHopDongtbl.DeleteTime,
                            DeleteId = QLHopDongtbl.DeleteId,
                            Id = QLHopDongtbl.Id,
                            CreatedDate = QLHopDongtbl.CreatedDate,
                            CreatedBy = QLHopDongtbl.CreatedBy,
                            CreatedID = QLHopDongtbl.CreatedID,
                            UpdatedDate = QLHopDongtbl.UpdatedDate,
                            UpdatedBy = QLHopDongtbl.UpdatedBy,
                            UpdatedID = QLHopDongtbl.UpdatedID

                        };

            if (searchModel != null)
            {
                if (searchModel.ItemIdFilter != null)
                {
                    query = query.Where(x => x.ItemId == searchModel.ItemIdFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.ItemTypeFilter))
                {
                    query = query.Where(x => x.ItemType.Contains(searchModel.ItemTypeFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.AgreementCodeFilter))
                {
                    query = query.Where(x => x.AgreementCode.Contains(searchModel.AgreementCodeFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.RepresenterNameFilter))
                {
                    query = query.Where(x => x.RepresenterName.Contains(searchModel.RepresenterNameFilter));
                }
                if (searchModel.DepositsMoneyFilter != null)
                {
                    query = query.Where(x => x.DepositsMoney == searchModel.DepositsMoneyFilter);
                }
                if (searchModel.MonthFilter != null)
                {
                    query = query.Where(x => x.Month == searchModel.MonthFilter);
                }
                if (searchModel.NgayFilter != null)
                {
                    query = query.Where(x => x.Ngay == searchModel.NgayFilter);
                }
                if (searchModel.DateStartFilter != null)
                {
                    query = query.Where(x => x.DateStart == searchModel.DateStartFilter);
                }
                if (searchModel.DateEndFilter != null)
                {
                    query = query.Where(x => x.DateEnd == searchModel.DateEndFilter);
                }
                if (searchModel.PayPerMonthFilter != null)
                {
                    query = query.Where(x => x.PayPerMonth == searchModel.PayPerMonthFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.NoteFilter))
                {
                    query = query.Where(x => x.Note.Contains(searchModel.NoteFilter));
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<QLHopDongDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public PageListResultBO<QLHopDongDto> GetDaTaByPageByCurrentUserId(bool IsViewAll, long? CurrentUserId, QLHopDongSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from QLHopDongtbl in _QLHopDongRepository.GetAllAsQueryable().Where(x => (IsViewAll == false ? x.CreatedID == CurrentUserId : true))

                        select new QLHopDongDto
                        {
                            ItemId = QLHopDongtbl.ItemId,
                            ItemType = QLHopDongtbl.ItemType,
                            AgreementCode = QLHopDongtbl.AgreementCode,
                            RepresenterName = QLHopDongtbl.RepresenterName,
                            DepositsMoney = QLHopDongtbl.DepositsMoney,
                            Month = QLHopDongtbl.Month,
                            Ngay = QLHopDongtbl.Ngay,
                            DateStart = QLHopDongtbl.DateStart,
                            DateEnd = QLHopDongtbl.DateEnd,
                            PayPerMonth = QLHopDongtbl.PayPerMonth,
                            Note = QLHopDongtbl.Note,
                            IsDelete = QLHopDongtbl.IsDelete,
                            DeleteTime = QLHopDongtbl.DeleteTime,
                            DeleteId = QLHopDongtbl.DeleteId,
                            Id = QLHopDongtbl.Id,
                            CreatedDate = QLHopDongtbl.CreatedDate,
                            CreatedBy = QLHopDongtbl.CreatedBy,
                            CreatedID = QLHopDongtbl.CreatedID,
                            UpdatedDate = QLHopDongtbl.UpdatedDate,
                            UpdatedBy = QLHopDongtbl.UpdatedBy,
                            UpdatedID = QLHopDongtbl.UpdatedID,
                            UserId = QLHopDongtbl.UserId,

                        };

            if (searchModel != null)
            {
                if (searchModel.ItemIdFilter != null)
                {
                    query = query.Where(x => x.ItemId == searchModel.ItemIdFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.ItemTypeFilter))
                {
                    query = query.Where(x => x.ItemType.Contains(searchModel.ItemTypeFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.AgreementCodeFilter))
                {
                    query = query.Where(x => x.AgreementCode.Contains(searchModel.AgreementCodeFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.RepresenterNameFilter))
                {
                    query = query.Where(x => x.RepresenterName.Contains(searchModel.RepresenterNameFilter));
                }
                if (searchModel.DepositsMoneyFilter != null)
                {
                    query = query.Where(x => x.DepositsMoney == searchModel.DepositsMoneyFilter);
                }
                if (searchModel.MonthFilter != null)
                {
                    query = query.Where(x => x.Month == searchModel.MonthFilter);
                }
                if (searchModel.NgayFilter != null)
                {
                    query = query.Where(x => x.Ngay == searchModel.NgayFilter);
                }
                if (searchModel.DateStartFilter != null)
                {
                    query = query.Where(x => x.DateStart == searchModel.DateStartFilter);
                }
                if (searchModel.DateEndFilter != null)
                {
                    query = query.Where(x => x.DateEnd == searchModel.DateEndFilter);
                }
                if (searchModel.PayPerMonthFilter != null)
                {
                    query = query.Where(x => x.PayPerMonth == searchModel.PayPerMonthFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.NoteFilter))
                {
                    query = query.Where(x => x.Note.Contains(searchModel.NoteFilter));
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<QLHopDongDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            foreach (var item in resultmodel.ListItem)
            {
                if (!string.IsNullOrEmpty(item.ItemType))
                {
                    var ItemTypeName = ConstantExtension.GetName<ItemTypeConstant>(item.ItemType);
                    if (!string.IsNullOrEmpty(ItemTypeName))
                    {
                        item.ItemTypeName = ItemTypeName;
                    }
                }
                if (item.ItemId != null)
                {
                    if (item.ItemType == ItemTypeConstant.Building)
                    {
                        item.ItemUrl = "/BuildingsInfoArea/BuildingsInfo/Detail?id=" + item.ItemId.ToString();
                        var BuildindInfo = _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.ItemId).FirstOrDefault();
                        if (BuildindInfo != null && !string.IsNullOrEmpty(BuildindInfo.BuildingsName)) {
                            item.ItemName = BuildindInfo.BuildingsName;
                        }
                    }
                    else if (item.ItemType == ItemTypeConstant.Room)
                    {
                        item.ItemUrl = "/RoomInfoArea/RoomInfo/Detail?id=" + item.ItemId.ToString();
                        var RoomInfo = _RoomInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.ItemId).FirstOrDefault();
                        if (RoomInfo != null && !string.IsNullOrEmpty(RoomInfo.RoomName))
                        {
                            item.ItemName = RoomInfo.RoomName;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(item.AgreementCode))
                {
                    var AgreementCodeName = ConstantExtension.GetName<AgreementTypeConstant>(item.AgreementCode);
                    if (!string.IsNullOrEmpty(item.AgreementCode))
                    {
                        item.AgreementCodeName = AgreementCodeName;
                    }
                }
            }
            return resultmodel;
        }
        public QLHopDong GetById(long id)
        {
            return _QLHopDongRepository.GetById(id);
        }

        public QLHopDongDto GetDtoById(long id)
        {
            var item = (from QLHopDongtbl in _QLHopDongRepository.GetAllAsQueryable().Where(x => x.Id == id)

                        select new QLHopDongDto
                        {
                            ItemId = QLHopDongtbl.ItemId,
                            ItemType = QLHopDongtbl.ItemType,
                            AgreementCode = QLHopDongtbl.AgreementCode,
                            RepresenterName = QLHopDongtbl.RepresenterName,
                            DepositsMoney = QLHopDongtbl.DepositsMoney,
                            Month = QLHopDongtbl.Month,
                            Ngay = QLHopDongtbl.Ngay,
                            DateStart = QLHopDongtbl.DateStart,
                            DateEnd = QLHopDongtbl.DateEnd,
                            PayPerMonth = QLHopDongtbl.PayPerMonth,
                            Note = QLHopDongtbl.Note,
                            IsDelete = QLHopDongtbl.IsDelete,
                            DeleteTime = QLHopDongtbl.DeleteTime,
                            DeleteId = QLHopDongtbl.DeleteId,
                            Id = QLHopDongtbl.Id,
                            CreatedDate = QLHopDongtbl.CreatedDate,
                            CreatedBy = QLHopDongtbl.CreatedBy,
                            CreatedID = QLHopDongtbl.CreatedID,
                            UpdatedDate = QLHopDongtbl.UpdatedDate,
                            UpdatedBy = QLHopDongtbl.UpdatedBy,
                            UpdatedID = QLHopDongtbl.UpdatedID,
                            UserId = QLHopDongtbl.UserId,

                        }).FirstOrDefault();

            if (!string.IsNullOrEmpty(item.ItemType))
            {
                var ItemTypeName = ConstantExtension.GetName<ItemTypeConstant>(item.ItemType);
                if (!string.IsNullOrEmpty(ItemTypeName))
                {
                    item.ItemTypeName = ItemTypeName;
                }
            }
            if (item.ItemId != null)
            {
                if (item.ItemType == ItemTypeConstant.Building)
                {
                    item.ItemUrl = "/BuildingsInfoArea/BuildingsInfo/Detail?id=" + item.ItemId.ToString();
                    var BuildindInfo = _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.ItemId).FirstOrDefault();
                    if (BuildindInfo != null && !string.IsNullOrEmpty(BuildindInfo.BuildingsName))
                    {
                        item.ItemName = BuildindInfo.BuildingsName;
                    }
                }
                else if (item.ItemType == ItemTypeConstant.Room)
                {
                    item.ItemUrl = "/RoomInfoArea/RoomInfo/Detail?id=" + item.ItemId.ToString();
                    var RoomInfo = _RoomInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.ItemId).FirstOrDefault();
                    if (RoomInfo != null && !string.IsNullOrEmpty(RoomInfo.RoomName))
                    {
                        item.ItemName = RoomInfo.RoomName;
                    }
                }
            }
            if (!string.IsNullOrEmpty(item.AgreementCode))
            {
                var AgreementCodeName = ConstantExtension.GetName<AgreementTypeConstant>(item.AgreementCode);
                if (!string.IsNullOrEmpty(item.AgreementCode))
                {
                    item.AgreementCodeName = AgreementCodeName;
                }
            }

            return item;
        }

        public List<QLHopDongDto> GetListHopDongByUserIdEndUser(long? UserId)
        {
            var query = (from QLHopDongtbl in _QLHopDongRepository.GetAllAsQueryable().Where(x => x.UserId == UserId)

                        select new QLHopDongDto
                        {
                            ItemId = QLHopDongtbl.ItemId,
                            ItemType = QLHopDongtbl.ItemType,
                            AgreementCode = QLHopDongtbl.AgreementCode,
                            RepresenterName = QLHopDongtbl.RepresenterName,
                            DepositsMoney = QLHopDongtbl.DepositsMoney,
                            Month = QLHopDongtbl.Month,
                            Ngay = QLHopDongtbl.Ngay,
                            DateStart = QLHopDongtbl.DateStart,
                            DateEnd = QLHopDongtbl.DateEnd,
                            PayPerMonth = QLHopDongtbl.PayPerMonth,
                            Note = QLHopDongtbl.Note,
                            IsDelete = QLHopDongtbl.IsDelete,
                            DeleteTime = QLHopDongtbl.DeleteTime,
                            DeleteId = QLHopDongtbl.DeleteId,
                            Id = QLHopDongtbl.Id,
                            CreatedDate = QLHopDongtbl.CreatedDate,
                            CreatedBy = QLHopDongtbl.CreatedBy,
                            CreatedID = QLHopDongtbl.CreatedID,
                            UpdatedDate = QLHopDongtbl.UpdatedDate,
                            UpdatedBy = QLHopDongtbl.UpdatedBy,
                            UpdatedID = QLHopDongtbl.UpdatedID,
                            UserId = QLHopDongtbl.UserId,
                        }).OrderByDescending(x=>x.UpdatedDate).ToList();
            foreach (var item in query)
            {
                if (!string.IsNullOrEmpty(item.ItemType))
                {
                    var ItemTypeName = ConstantExtension.GetName<ItemTypeConstant>(item.ItemType);
                    if (!string.IsNullOrEmpty(ItemTypeName))
                    {
                        item.ItemTypeName = ItemTypeName;
                    }
                }
                if (item.ItemId != null)
                {
                    if (item.ItemType == ItemTypeConstant.Building)
                    {
                        item.ItemUrl = "/EndUserPersonalInfo/DetailBuilding?id=" + item.ItemId.ToString();
                        var BuildindInfo = _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.ItemId).FirstOrDefault();
                        if (BuildindInfo != null && !string.IsNullOrEmpty(BuildindInfo.BuildingsName))
                        {
                            item.ItemName = BuildindInfo.BuildingsName;
                        }
                    }
                    else if (item.ItemType == ItemTypeConstant.Room)
                    {
                        item.ItemUrl = "/EndUserPersonalInfo/DetailRoom?id=" + item.ItemId.ToString();
                        var RoomInfo = _RoomInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.ItemId).FirstOrDefault();
                        if (RoomInfo != null && !string.IsNullOrEmpty(RoomInfo.RoomName))
                        {
                            item.ItemName = RoomInfo.RoomName;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(item.AgreementCode))
                {
                    var AgreementCodeName = ConstantExtension.GetName<AgreementTypeConstant>(item.AgreementCode);
                    if (!string.IsNullOrEmpty(item.AgreementCode))
                    {
                        item.AgreementCodeName = AgreementCodeName;
                    }
                }
            }

            return query;
        }
    }
}
