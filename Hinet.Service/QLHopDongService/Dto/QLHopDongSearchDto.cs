using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLHopDongService.Dto
{
    public class QLHopDongSearchDto : SearchBase
    {
		public long? ItemIdFilter { get; set; }
		public string ItemTypeFilter { get; set; }
		public string AgreementCodeFilter { get; set; }
		public string RepresenterNameFilter { get; set; }
		public int? DepositsMoneyFilter { get; set; }
		public int? MonthFilter { get; set; }
		public int? NgayFilter { get; set; }
		public DateTime? DateStartFilter { get; set; }
		public DateTime? DateEndFilter { get; set; }
		public int? PayPerMonthFilter { get; set; }
		public string NoteFilter { get; set; }


    }
}