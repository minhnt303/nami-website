using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLHopDongService.Dto
{
    public class QLHopDongDto : QLHopDong
    {
        public string ItemTypeName { get; set; }
        public string ItemUrl { get; set; }
        public string AgreementCodeName { get; set; }
        public string ItemName { get; set; }
    }
}