using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.HistoryService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.HistoryService
{
    public interface IHistoryService:IEntityService<History>
    {
        PageListResultBO<HistoryDto> GetDaTaByPage(HistorySearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        List<HistoryDto> GetDaTaByIdHistory(long IdItem, string TypeItem);
        HistoryDto GetDaTaByIdHistoryLated(long IdItem, string TypeItem);
        List<HistoryDto> GetByIdListHistoryLated(long IdItem, string TypeItem);
        History GetById(long id);
        string getTheLastMessage(long Id, string typeItemHistory);
    }
}
