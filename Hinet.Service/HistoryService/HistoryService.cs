using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.HistoryRepository;
using Nami.Service.HistoryService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Repository.AppUserRepository;
using Nami.Repository.DepartmentRepository;

namespace Nami.Service.HistoryService
{
    public class HistoryService : EntityService<History>, IHistoryService
    {
        IUnitOfWork _unitOfWork;
        IHistoryRepository _HistoryRepository;
        ILog _loger;
        IMapper _mapper;
        IAppUserRepository _appUserRepository;
        IDepartmentRepository _departmentRepository;

        public HistoryService(IUnitOfWork unitOfWork,
        IHistoryRepository HistoryRepository,
        ILog loger,
        IAppUserRepository appUserRepository,
        IDepartmentRepository departmentRepository,
                IMapper mapper
            )
            : base(unitOfWork, HistoryRepository)
        {
            _unitOfWork = unitOfWork;
            _HistoryRepository = HistoryRepository;
            _loger = loger;
            _mapper = mapper;
            _appUserRepository = appUserRepository;
            _departmentRepository = departmentRepository;
        }

        public PageListResultBO<HistoryDto> GetDaTaByPage(HistorySearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from Historytbl in _HistoryRepository.GetAllAsQueryable()

                        select new HistoryDto
                        {
                            HistoryContent = Historytbl.HistoryContent,
                            IdItem = Historytbl.IdItem,
                            Name = Historytbl.Name,
                            TypeItem = Historytbl.TypeItem,
                            Note = Historytbl.Note,
                            Comment = Historytbl.Comment,
                            LogId = Historytbl.LogId,
                            CreatedDate = Historytbl.CreatedDate,
                            UpdatedDate = Historytbl.UpdatedDate,
                            CreatedBy = Historytbl.CreatedBy,
                            UpdatedBy = Historytbl.UpdatedBy,
                            Id = Historytbl.Id,
                            StatusBegin = Historytbl.StatusBegin,
                            StatusEnd = Historytbl.StatusEnd,
                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.NameFilter))
                {
                    query = query.Where(x => x.Name.Contains(searchModel.NameFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.TypeItemFilter))
                {
                    query = query.Where(x => x.TypeItem.Contains(searchModel.TypeItemFilter));
                }
                if (!String.IsNullOrEmpty(searchModel.IdItemFilter))
                {
                    int IdItem = int.Parse(searchModel.IdItemFilter);
                    query = query.Where(x => x.IdItem == IdItem);
                }
                if (!string.IsNullOrEmpty(searchModel.CreatorFilter))
                {
                    query = query.Where(x => x.CreatedBy.Contains(searchModel.CreatorFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.NoteFilter))
                {
                    query = query.Where(x => x.Note.Contains(searchModel.NoteFilter));
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<HistoryDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public List<HistoryDto> GetDaTaByIdHistory(long IdItem, string TypeItem)
        {
            var query = from Historytbl in _HistoryRepository.GetAllAsQueryable().Where(x => x.IdItem == IdItem && x.TypeItem == TypeItem)

                        join appUsertbl in _appUserRepository.GetAllAsQueryable()
                        on Historytbl.CreatedBy equals appUsertbl.UserName into appUsertable
                        from appUsertbl2 in appUsertable.DefaultIfEmpty()

                        join departmenttbl in _departmentRepository.GetAllAsQueryable()
                        on appUsertbl2.IdDepartment equals departmenttbl.Id into departmenttable
                        from departmenttbl2 in departmenttable.DefaultIfEmpty()

                        select new HistoryDto
                        {
                            DepartmentName = departmenttbl2.Name,
                            CreateName = appUsertbl2.FullName,
                            HistoryContent = Historytbl.HistoryContent,
                            IdItem = Historytbl.IdItem,
                            Name = Historytbl.Name,
                            TypeItem = Historytbl.TypeItem,
                            Note = Historytbl.Note,
                            Comment = Historytbl.Comment,
                            LogId = Historytbl.LogId,
                            CreatedDate = Historytbl.CreatedDate,
                            UpdatedDate = Historytbl.UpdatedDate,
                            CreatedBy = Historytbl.CreatedBy,
                            UpdatedBy = Historytbl.UpdatedBy,
                            Id = Historytbl.Id,
                            StatusBegin = Historytbl.StatusBegin,
                            StatusEnd = Historytbl.StatusEnd,
                            userInfo = appUsertbl2
                        };


            var resultmodel = query.OrderByDescending(x => x.Id).ToList();
            return resultmodel;
        }

        public History GetById(long id)
        {
            return _HistoryRepository.GetById(id);
        }

        public HistoryDto GetDaTaByIdHistoryLated(long IdItem, string TypeItem)
        {
            var query = from Historytbl in _HistoryRepository.GetAllAsQueryable().Where(x => x.IdItem == IdItem && x.TypeItem == TypeItem)

                        select new HistoryDto
                        {
                            HistoryContent = Historytbl.HistoryContent,
                            IdItem = Historytbl.IdItem,
                            Name = Historytbl.Name,
                            TypeItem = Historytbl.TypeItem,
                            Note = Historytbl.Note,
                            Comment = Historytbl.Comment,
                            LogId = Historytbl.LogId,
                            CreatedDate = Historytbl.CreatedDate,
                            UpdatedDate = Historytbl.UpdatedDate,
                            CreatedBy = Historytbl.CreatedBy,
                            UpdatedBy = Historytbl.UpdatedBy,
                            Id = Historytbl.Id,
                            StatusBegin = Historytbl.StatusBegin,
                            StatusEnd = Historytbl.StatusEnd,
                        };

            query = query.OrderByDescending(x => x.CreatedDate);
            var resultmodel = query.OrderByDescending(x => x.Id).FirstOrDefault();
            return resultmodel;
        }

        public List<HistoryDto> GetByIdListHistoryLated(long IdItem, string TypeItem)
        {
            var query = from Historytbl in _HistoryRepository.GetAllAsQueryable().Where(x => x.IdItem == IdItem && x.TypeItem == TypeItem)

                        select new HistoryDto
                        {
                            HistoryContent = Historytbl.HistoryContent,
                            IdItem = Historytbl.IdItem,
                            Name = Historytbl.Name,
                            TypeItem = Historytbl.TypeItem,
                            Note = Historytbl.Note,
                            Comment = Historytbl.Comment,
                            LogId = Historytbl.LogId,
                            CreatedDate = Historytbl.CreatedDate,
                            UpdatedDate = Historytbl.UpdatedDate,
                            CreatedBy = Historytbl.CreatedBy,
                            UpdatedBy = Historytbl.UpdatedBy,
                            Id = Historytbl.Id,
                            StatusBegin = Historytbl.StatusBegin,
                            StatusEnd = Historytbl.StatusEnd,
                        };
            query = query.OrderByDescending(x => x.CreatedDate);
            var resultmodel = query.OrderByDescending(x => x.Id).ToList();
            return resultmodel;
        }

        public string getTheLastMessage(long Id, string typeItemHistory)
        {
            string result = "";
            var message = _HistoryRepository.GetAllAsQueryable().Where(x => x.TypeItem == typeItemHistory && x.IdItem == Id).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
            if (message != null)
            {
                result = message.Comment;
            }
            return result;
        }
    }
}
