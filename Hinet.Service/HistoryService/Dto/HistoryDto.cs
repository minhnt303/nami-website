using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.HistoryService.Dto
{
    public class HistoryDto : History
    {
        public string CreateName { get; set; }
        public string DepartmentName { get; set; }
        public AppUser userInfo { get; set; }
    }
}