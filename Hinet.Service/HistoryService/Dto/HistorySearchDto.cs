using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.HistoryService.Dto
{
    public class HistorySearchDto : SearchBase
    {
		public string NameFilter { get; set; }
		public string TypeItemFilter { get; set; }
		public string NoteFilter { get; set; }
        public string IdItemFilter { get; set; }
        public string CreatorFilter { get; set; }
    }
}