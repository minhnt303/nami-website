using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.RentersInfoImageRepository;
using Nami.Service.RentersInfoImageService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;



namespace Nami.Service.RentersInfoImageService
{
    public class RentersInfoImageService : EntityService<RentersInfoImage>, IRentersInfoImageService
    {
        IUnitOfWork _unitOfWork;
        IRentersInfoImageRepository _RentersInfoImageRepository;
	ILog _loger;
        IMapper _mapper;

        
        public RentersInfoImageService(IUnitOfWork unitOfWork, 
		IRentersInfoImageRepository RentersInfoImageRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, RentersInfoImageRepository)
        {
            _unitOfWork = unitOfWork;
            _RentersInfoImageRepository = RentersInfoImageRepository;
            _loger = loger;
            _mapper = mapper;

        }

        public PageListResultBO<RentersInfoImageDto> GetDaTaByPage(RentersInfoImageSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from RentersInfoImagetbl in _RentersInfoImageRepository.GetAllAsQueryable()

                        select new RentersInfoImageDto
                        {
							Id = RentersInfoImagetbl.Id,
							RentersId = RentersInfoImagetbl.RentersId,
							RentersType = RentersInfoImagetbl.RentersType,
							ImageUrl = RentersInfoImagetbl.ImageUrl,
							CreatedDate = RentersInfoImagetbl.CreatedDate,
							CreatedBy = RentersInfoImagetbl.CreatedBy,
							UpdatedDate = RentersInfoImagetbl.UpdatedDate,
							UpdatedBy = RentersInfoImagetbl.UpdatedBy
                            
                        };

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<RentersInfoImageDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public RentersInfoImage GetById(long id)
        {
            return _RentersInfoImageRepository.GetById(id);
        }
    

    }
}
