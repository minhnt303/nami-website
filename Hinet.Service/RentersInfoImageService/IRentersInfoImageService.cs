using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.RentersInfoImageService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.RentersInfoImageService
{
    public interface IRentersInfoImageService:IEntityService<RentersInfoImage>
    {
        PageListResultBO<RentersInfoImageDto> GetDaTaByPage(RentersInfoImageSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        RentersInfoImage GetById(long id);
    }
}
