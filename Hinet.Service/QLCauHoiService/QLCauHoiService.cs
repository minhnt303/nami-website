using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.QLCauHoiRepository;
using Nami.Service.Common;
using Nami.Service.QLCauHoiService.Dto;
using log4net;
using PagedList;
using System.Linq;
using System.Linq.Dynamic;
using Nami.Service.Constant;
using Nami.Repository.AppUserRepository;



namespace Nami.Service.QLCauHoiService
{
    public class QLCauHoiService : EntityService<QLCauHoi>, IQLCauHoiService
    {
        IUnitOfWork _unitOfWork;
        IQLCauHoiRepository _QLCauHoiRepository;
        ILog _loger;
        IMapper _mapper;
        IAppUserRepository _appUserRepository;


        public QLCauHoiService(IUnitOfWork unitOfWork,
        IQLCauHoiRepository QLCauHoiRepository,
        ILog loger,
        IAppUserRepository appUserRepository,
                IMapper mapper
            )
            : base(unitOfWork, QLCauHoiRepository)
        {
            _unitOfWork = unitOfWork;
            _QLCauHoiRepository = QLCauHoiRepository;
            _loger = loger;
            _mapper = mapper;
            _appUserRepository = appUserRepository;


        }

        public PageListResultBO<QLCauHoiDto> GetDaTaByPage(QLCauHoiSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from QLCauHoitbl in _QLCauHoiRepository.GetAllAsQueryable()

                        select new QLCauHoiDto
                        {
                            Name = QLCauHoitbl.Name,
                            Email = QLCauHoitbl.Email,
                            PhoneNumber = QLCauHoitbl.PhoneNumber,
                            CauHoi = QLCauHoitbl.CauHoi,
                            IdNguoiXuLy = QLCauHoitbl.IdNguoiXuLy,
                            Status = QLCauHoitbl.Status,
                            IsDelete = QLCauHoitbl.IsDelete,
                            DeleteTime = QLCauHoitbl.DeleteTime,
                            DeleteId = QLCauHoitbl.DeleteId,
                            Id = QLCauHoitbl.Id,
                            CreatedDate = QLCauHoitbl.CreatedDate,
                            CreatedBy = QLCauHoitbl.CreatedBy,
                            CreatedID = QLCauHoitbl.CreatedID,
                            UpdatedDate = QLCauHoitbl.UpdatedDate,
                            UpdatedBy = QLCauHoitbl.UpdatedBy,
                            UpdatedID = QLCauHoitbl.UpdatedID

                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.NameFilter))
                {
                    query = query.Where(x => x.Name.Contains(searchModel.NameFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.EmailFilter))
                {
                    query = query.Where(x => x.Email.Contains(searchModel.EmailFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.PhoneNumberFilter))
                {
                    query = query.Where(x => x.PhoneNumber.Contains(searchModel.PhoneNumberFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.CauHoiFilter))
                {
                    query = query.Where(x => x.CauHoi.Contains(searchModel.CauHoiFilter));
                }
                if (searchModel.IdNguoiXuLyFilter != null)
                {
                    query = query.Where(x => x.IdNguoiXuLy == searchModel.IdNguoiXuLyFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.StatusFilter))
                {
                    query = query.Where(x => x.Status.Contains(searchModel.StatusFilter));
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<QLCauHoiDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            foreach (var item in resultmodel.ListItem)
            {
                if (!string.IsNullOrEmpty(item.Status))
                {
                    var StatusName = ConstantExtension.GetName<QLCauHoiStatusConstant>(item.Status);
                    if (!string.IsNullOrEmpty(StatusName))
                    {
                        item.StatusName = StatusName;
                    }
                    var StatusBGColor = ConstantExtension.GetBackgroundColor<QLCauHoiStatusConstant>(item.Status);
                    if (!string.IsNullOrEmpty(StatusBGColor))
                    {
                        item.StatusBGColor = StatusBGColor;
                    }
                }
                if (item.IdNguoiXuLy != 0) {
                    var NguoiXuLyInfo = _appUserRepository.GetAllAsQueryable().Where(x => x.Id == item.IdNguoiXuLy).FirstOrDefault();
                    if (NguoiXuLyInfo != null && !string.IsNullOrEmpty(NguoiXuLyInfo.FullName)) {
                        item.NguoiXuLyName = NguoiXuLyInfo.FullName;
                    }
                }
            }
            return resultmodel;
        }

        public QLCauHoi GetById(long id)
        {
            return _QLCauHoiRepository.GetById(id);
        }


    }
}
