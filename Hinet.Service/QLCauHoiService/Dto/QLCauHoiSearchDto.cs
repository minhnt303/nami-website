using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLCauHoiService.Dto
{
    public class QLCauHoiSearchDto : SearchBase
    {
		public string NameFilter { get; set; }
		public string EmailFilter { get; set; }
		public string PhoneNumberFilter { get; set; }
		public string CauHoiFilter { get; set; }
		public long IdNguoiXuLyFilter { get; set; }
		public string StatusFilter { get; set; }


    }
}