using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLCauHoiService.Dto
{
    public class QLCauHoiDto : QLCauHoi
    {
        public string StatusName { get; set; }
        public string StatusBGColor { get; set; }
        public string NguoiXuLyName { get; set; }
    }
}