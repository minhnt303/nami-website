using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Nami.Service.QLCauHoiService.Dto
{
    public class QLCauHoiExportDto
    {
		[DisplayName("Tên khách hàng")]
public string Name { get; set; }
		[DisplayName("Email")]
public string Email { get; set; }
		[DisplayName("Số điện thoại")]
public string PhoneNumber { get; set; }
		[DisplayName("Câu hỏi (Tin nhắn)")]
public string CauHoi { get; set; }
		[DisplayName("Id người xử lý")]
public long IdNguoiXuLy { get; set; }
		[DisplayName("Trạng thái")]
public string Status { get; set; }

    }
}