using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.OrderInfoService.Dto
{
    public class OrderInfoDto : OrderInfo
    {
        public string BankCodeName { get; set; }
        public string OrderTypeName { get; set; }
        public string ItemTypeName { get; set; }
        public string OrderTypeBGColor { get; set; }
        public string UrlItem { get; set; }
        public string ItemName { get; set; }
    }
}