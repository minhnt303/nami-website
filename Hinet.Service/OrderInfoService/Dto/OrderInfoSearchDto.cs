using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.OrderInfoService.Dto
{
    public class OrderInfoSearchDto : SearchBase
    {
		public decimal OrderIdFilter { get; set; }
		public decimal? IdFilterFrom { get; set; }
		public decimal? IdFilterTo { get; set; }
		public decimal AmountFilter { get; set; }
		public decimal? AmountFilterFrom { get; set; }
		public decimal? AmountFilterTo { get; set; }
		public string OrderDescriptionFilter { get; set; }
		public string BankCodeFilter { get; set; }
		public int StatusFilter { get; set; }
		public string OrderTypeFilter { get; set; }
		public long IdItemFilter { get; set; }
		public string ItemTypeFilter { get; set; }
		public bool? TrangThaiThanhToanFilter { get; set; }


    }
}