using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.BuildingsInfoRepository;
using Nami.Repository.DanhmucRepository;
using Nami.Repository.OrderInfoRepository;
using Nami.Repository.RoomInfoRepository;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.OrderInfoService.Dto;
using log4net;
using PagedList;
using System.Linq;
using System.Linq.Dynamic;



namespace Nami.Service.OrderInfoService
{
    public class OrderInfoService : EntityService<OrderInfo>, IOrderInfoService
    {
        IUnitOfWork _unitOfWork;
        IOrderInfoRepository _OrderInfoRepository;
        ILog _loger;
        IMapper _mapper;
        IDM_DulieuDanhmucRepository _dM_DulieuDanhmucRepository;
        IDM_NhomDanhmucRepository _dM_NhomDanhmucRepository;
        IBuildingsInfoRepository _buildingsInfoRepository;
        IRoomInfoRepository _roomInfoRepository;

        public OrderInfoService(IUnitOfWork unitOfWork,
        IOrderInfoRepository OrderInfoRepository,
        ILog loger,
        IDM_DulieuDanhmucRepository dM_DulieuDanhmucRepository,
        IDM_NhomDanhmucRepository dM_NhomDanhmucRepository,
        IBuildingsInfoRepository buildingsInfoRepository,
        IRoomInfoRepository roomInfoRepository,
                IMapper mapper
            )
            : base(unitOfWork, OrderInfoRepository)
        {
            _unitOfWork = unitOfWork;
            _OrderInfoRepository = OrderInfoRepository;
            _loger = loger;
            _mapper = mapper;
            _dM_DulieuDanhmucRepository = dM_DulieuDanhmucRepository;
            _dM_NhomDanhmucRepository = dM_NhomDanhmucRepository;
            _buildingsInfoRepository = buildingsInfoRepository;
            _roomInfoRepository = roomInfoRepository;
        }

        public PageListResultBO<OrderInfoDto> GetDaTaByPage(OrderInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from OrderInfotbl in _OrderInfoRepository.GetAllAsQueryable()

                        select new OrderInfoDto
                        {
                            OrderId = OrderInfotbl.OrderId,
                            Amount = OrderInfotbl.Amount,
                            OrderDescription = OrderInfotbl.OrderDescription,
                            BankCode = OrderInfotbl.BankCode,
                            Status = OrderInfotbl.Status,
                            vnp_TransactionNo = OrderInfotbl.vnp_TransactionNo,
                            vpn_Message = OrderInfotbl.vpn_Message,
                            vpn_TxnResponseCode = OrderInfotbl.vpn_TxnResponseCode,
                            IsDelete = OrderInfotbl.IsDelete,
                            DeleteTime = OrderInfotbl.DeleteTime,
                            DeleteId = OrderInfotbl.DeleteId,
                            OrderType = OrderInfotbl.OrderType,
                            IdItem = OrderInfotbl.IdItem,
                            ItemType = OrderInfotbl.ItemType,
                            TrangThaiThanhToan = OrderInfotbl.TrangThaiThanhToan,
                            Id = OrderInfotbl.Id,
                            CreatedDate = OrderInfotbl.CreatedDate,
                            CreatedBy = OrderInfotbl.CreatedBy,
                            CreatedID = OrderInfotbl.CreatedID,
                            UpdatedDate = OrderInfotbl.UpdatedDate,
                            UpdatedBy = OrderInfotbl.UpdatedBy,
                            UpdatedID = OrderInfotbl.UpdatedID

                        };

            if (searchModel != null)
            {
                if (searchModel.OrderIdFilter != null)
                {
                    query = query.Where(x => x.OrderId == searchModel.OrderIdFilter);
                }
                if (searchModel.AmountFilter != null)
                {
                    query = query.Where(x => x.Amount == searchModel.AmountFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.OrderDescriptionFilter))
                {
                    query = query.Where(x => x.OrderDescription.Contains(searchModel.OrderDescriptionFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.BankCodeFilter))
                {
                    query = query.Where(x => x.BankCode.Contains(searchModel.BankCodeFilter));
                }
                if (searchModel.StatusFilter != null)
                {
                    query = query.Where(x => x.Status == searchModel.StatusFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.OrderTypeFilter))
                {
                    query = query.Where(x => x.OrderType.Contains(searchModel.OrderTypeFilter));
                }
                if (searchModel.IdItemFilter != null)
                {
                    query = query.Where(x => x.IdItem == searchModel.IdItemFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.ItemTypeFilter))
                {
                    query = query.Where(x => x.ItemType.Contains(searchModel.ItemTypeFilter));
                }
                if (searchModel.TrangThaiThanhToanFilter != null)
                {
                    query = query.Where(x => x.TrangThaiThanhToan == searchModel.TrangThaiThanhToanFilter);
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<OrderInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }


        public PageListResultBO<OrderInfoDto> GetDaTaByPageByCurrentUserIdOrAdmin(bool IsViewAll, long? CurrentUserId, OrderInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var IdGroupBankCode = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MANGANHANG).Select(x => x.Id).FirstOrDefault();
            var query = from OrderInfotbl in _OrderInfoRepository.GetAllAsQueryable().Where(x => (IsViewAll == false ? x.CreatedID == CurrentUserId : true))

                        select new OrderInfoDto
                        {
                            OrderId = OrderInfotbl.OrderId,
                            Amount = OrderInfotbl.Amount,
                            OrderDescription = OrderInfotbl.OrderDescription,
                            BankCode = OrderInfotbl.BankCode,
                            Status = OrderInfotbl.Status,
                            vnp_TransactionNo = OrderInfotbl.vnp_TransactionNo,
                            vpn_Message = OrderInfotbl.vpn_Message,
                            vpn_TxnResponseCode = OrderInfotbl.vpn_TxnResponseCode,
                            IsDelete = OrderInfotbl.IsDelete,
                            DeleteTime = OrderInfotbl.DeleteTime,
                            DeleteId = OrderInfotbl.DeleteId,
                            OrderType = OrderInfotbl.OrderType,
                            IdItem = OrderInfotbl.IdItem,
                            ItemType = OrderInfotbl.ItemType,
                            TrangThaiThanhToan = OrderInfotbl.TrangThaiThanhToan,
                            Id = OrderInfotbl.Id,
                            CreatedDate = OrderInfotbl.CreatedDate,
                            CreatedBy = OrderInfotbl.CreatedBy,
                            CreatedID = OrderInfotbl.CreatedID,
                            UpdatedDate = OrderInfotbl.UpdatedDate,
                            UpdatedBy = OrderInfotbl.UpdatedBy,
                            UpdatedID = OrderInfotbl.UpdatedID

                        };

            if (searchModel != null)
            {
                if (searchModel.IdFilterFrom != null)
                {
                    query = query.Where(x => x.Id >= searchModel.IdFilterFrom);
                }
                if (searchModel.IdFilterTo != null)
                {
                    query = query.Where(x => x.Id <= searchModel.IdFilterTo);
                }
                if (searchModel.AmountFilterFrom != null)
                {
                    query = query.Where(x => x.Amount >= searchModel.AmountFilterFrom);
                }
                if (searchModel.AmountFilterTo != null)
                {
                    query = query.Where(x => x.Amount <= searchModel.AmountFilterTo);
                }
                if (!string.IsNullOrEmpty(searchModel.OrderDescriptionFilter))
                {
                    query = query.Where(x => x.OrderDescription.Contains(searchModel.OrderDescriptionFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.BankCodeFilter))
                {
                    query = query.Where(x => x.BankCode == searchModel.BankCodeFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.OrderTypeFilter))
                {
                    query = query.Where(x => x.OrderType == searchModel.OrderTypeFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.ItemTypeFilter))
                {
                    query = query.Where(x => x.ItemType == searchModel.ItemTypeFilter);
                }
                if (searchModel.TrangThaiThanhToanFilter != null)
                {
                    query = query.Where(x => x.TrangThaiThanhToan == searchModel.TrangThaiThanhToanFilter);
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.UpdatedDate);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.UpdatedDate);
            }
            var resultmodel = new PageListResultBO<OrderInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            foreach (var item in resultmodel.ListItem)
            {
                if (!string.IsNullOrEmpty(item.BankCode))
                {
                    var BankCodeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupBankCode && x.Code == item.BankCode).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BankCodeName))
                    {
                        item.BankCodeName = BankCodeName;
                    }
                }
                if (!string.IsNullOrEmpty(item.ItemType))
                {
                    var ItemTypeName = ConstantExtension.GetName<ItemTypeConstant>(item.ItemType);
                    if (!string.IsNullOrEmpty(ItemTypeName))
                    {
                        item.ItemTypeName = ItemTypeName;
                    }
                    if (item.ItemType == ItemTypeConstant.Building)
                    {
                        item.UrlItem = "/BuildingsInfoArea/BuildingsInfo/Detail?id=" + item.IdItem.ToString();
                        var BuildingName = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.IdItem).Select(x => x.BuildingsName).FirstOrDefault();
                        if (!string.IsNullOrEmpty(BuildingName))
                        {
                            item.ItemName = BuildingName;
                        }
                    }
                    else if (item.ItemType == ItemTypeConstant.Room)
                    {
                        item.UrlItem = "/RoomInfoArea/RoomInfo/Detail?id=" + item.IdItem.ToString();
                        var RoomName = _roomInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.IdItem).Select(x => x.RoomName).FirstOrDefault();
                        if (!string.IsNullOrEmpty(RoomName))
                        {
                            item.ItemName = RoomName;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(item.OrderType))
                {
                    var OrderTypeName = ConstantExtension.GetName<OrderTypeConstant>(item.OrderType);
                    if (!string.IsNullOrEmpty(OrderTypeName))
                    {
                        item.OrderTypeName = OrderTypeName;
                    }
                    var OrderTypeBGColor = ConstantExtension.GetBackgroundColor<OrderTypeConstant>(item.OrderType);
                    if (!string.IsNullOrEmpty(OrderTypeBGColor))
                    {
                        item.OrderTypeBGColor = OrderTypeBGColor;
                    }
                }
            }
            return resultmodel;
        }

        public OrderInfo GetById(long id)
        {
            return _OrderInfoRepository.GetById(id);
        }


        public OrderInfoDto GetDtoById(long id)
        {
            var IdGroupBankCode = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MANGANHANG).Select(x => x.Id).FirstOrDefault();
            var item = (from OrderInfotbl in _OrderInfoRepository.GetAllAsQueryable().Where(x => x.Id == id)

                       select new OrderInfoDto
                       {
                           OrderId = OrderInfotbl.OrderId,
                           Amount = OrderInfotbl.Amount,
                           OrderDescription = OrderInfotbl.OrderDescription,
                           BankCode = OrderInfotbl.BankCode,
                           Status = OrderInfotbl.Status,
                           vnp_TransactionNo = OrderInfotbl.vnp_TransactionNo,
                           vpn_Message = OrderInfotbl.vpn_Message,
                           vpn_TxnResponseCode = OrderInfotbl.vpn_TxnResponseCode,
                           IsDelete = OrderInfotbl.IsDelete,
                           DeleteTime = OrderInfotbl.DeleteTime,
                           DeleteId = OrderInfotbl.DeleteId,
                           OrderType = OrderInfotbl.OrderType,
                           IdItem = OrderInfotbl.IdItem,
                           ItemType = OrderInfotbl.ItemType,
                           TrangThaiThanhToan = OrderInfotbl.TrangThaiThanhToan,
                           Id = OrderInfotbl.Id,
                           CreatedDate = OrderInfotbl.CreatedDate,
                           CreatedBy = OrderInfotbl.CreatedBy,
                           CreatedID = OrderInfotbl.CreatedID,
                           UpdatedDate = OrderInfotbl.UpdatedDate,
                           UpdatedBy = OrderInfotbl.UpdatedBy,
                           UpdatedID = OrderInfotbl.UpdatedID
                       }).FirstOrDefault();

            if (!string.IsNullOrEmpty(item.BankCode))
            {
                var BankCodeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupBankCode && x.Code == item.BankCode).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(BankCodeName))
                {
                    item.BankCodeName = BankCodeName;
                }
            }
            if (!string.IsNullOrEmpty(item.ItemType))
            {
                var ItemTypeName = ConstantExtension.GetName<ItemTypeConstant>(item.ItemType);
                if (!string.IsNullOrEmpty(ItemTypeName))
                {
                    item.ItemTypeName = ItemTypeName;
                }
                if (item.ItemType == ItemTypeConstant.Building)
                {
                    item.UrlItem = "/BuildingsInfoArea/BuildingsInfo/Detail?id=" + item.IdItem.ToString();
                    var BuildingName = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.IdItem).Select(x => x.BuildingsName).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuildingName))
                    {
                        item.ItemName = BuildingName;
                    }
                }
                else if (item.ItemType == ItemTypeConstant.Room)
                {
                    item.UrlItem = "/RoomInfoArea/RoomInfo/Detail?id=" + item.IdItem.ToString();
                    var RoomName = _roomInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.IdItem).Select(x => x.RoomName).FirstOrDefault();
                    if (!string.IsNullOrEmpty(RoomName))
                    {
                        item.ItemName = RoomName;
                    }
                }
            }
            if (!string.IsNullOrEmpty(item.OrderType))
            {
                var OrderTypeName = ConstantExtension.GetName<OrderTypeConstant>(item.OrderType);
                if (!string.IsNullOrEmpty(OrderTypeName))
                {
                    item.OrderTypeName = OrderTypeName;
                }
                var OrderTypeBGColor = ConstantExtension.GetBackgroundColor<OrderTypeConstant>(item.OrderType);
                if (!string.IsNullOrEmpty(OrderTypeBGColor))
                {
                    item.OrderTypeBGColor = OrderTypeBGColor;
                }
            }
            return item;
        }
    }
}
