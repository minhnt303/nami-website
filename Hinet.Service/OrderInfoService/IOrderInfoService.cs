using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.OrderInfoService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.OrderInfoService
{
    public interface IOrderInfoService:IEntityService<OrderInfo>
    {
        PageListResultBO<OrderInfoDto> GetDaTaByPage(OrderInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        OrderInfo GetById(long id);
        OrderInfoDto GetDtoById(long id);
        PageListResultBO<OrderInfoDto> GetDaTaByPageByCurrentUserIdOrAdmin(bool IsViewAll, long? CurrentUserId, OrderInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
    }
}
