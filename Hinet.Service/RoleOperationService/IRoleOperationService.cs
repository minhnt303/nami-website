﻿using Nami.Model.Entities;
using Nami.Service.RoleOperationService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.RoleOperationService
{
    public interface IRoleOperationService : IEntityService<RoleOperation>
    {
        RoleOperationDTO GetConfigureOperation(int roleId);
        RoleOperationDTO GetConfigureProvince(int roleId);
    }
}
