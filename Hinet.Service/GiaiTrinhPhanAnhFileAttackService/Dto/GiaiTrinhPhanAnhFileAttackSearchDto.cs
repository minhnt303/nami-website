using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.GiaiTrinhPhanAnhFileAttackService.Dto
{
    public class GiaiTrinhPhanAnhFileAttackSearchDto : SearchBase
    {
		public long? IdGiaiTrinhPhanAnhFilter { get; set; }
		public string FileUrlGiaiTrinhPhanAnhFilter { get; set; }
		public bool? IsDeleteFilter { get; set; }
		public DateTime? DeleteTimeFilter { get; set; }
		public long? DeleteIdFilter { get; set; }


    }
}