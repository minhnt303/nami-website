using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Nami.Service.GiaiTrinhPhanAnhFileAttackService.Dto
{
    public class GiaiTrinhPhanAnhFileAttackExportDto
    {
		[DisplayName("")]
public long? IdGiaiTrinhPhanAnh { get; set; }
		[DisplayName("")]
public string FileUrlGiaiTrinhPhanAnh { get; set; }
		[DisplayName("")]
public bool? IsDelete { get; set; }
		[DisplayName("")]
public DateTime? DeleteTime { get; set; }
		[DisplayName("")]
public long? DeleteId { get; set; }

    }
}