using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.GiaiTrinhPhanAnhFileAttackRepository;
using Nami.Service.GiaiTrinhPhanAnhFileAttackService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;




namespace Nami.Service.GiaiTrinhPhanAnhFileAttackService
{
    public class GiaiTrinhPhanAnhFileAttackService : EntityService<GiaiTrinhPhanAnhFileAttack>, IGiaiTrinhPhanAnhFileAttackService
    {
        IUnitOfWork _unitOfWork;
        IGiaiTrinhPhanAnhFileAttackRepository _GiaiTrinhPhanAnhFileAttackRepository;
	ILog _loger;
        IMapper _mapper;


        
        public GiaiTrinhPhanAnhFileAttackService(IUnitOfWork unitOfWork, 
		IGiaiTrinhPhanAnhFileAttackRepository GiaiTrinhPhanAnhFileAttackRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, GiaiTrinhPhanAnhFileAttackRepository)
        {
            _unitOfWork = unitOfWork;
            _GiaiTrinhPhanAnhFileAttackRepository = GiaiTrinhPhanAnhFileAttackRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<GiaiTrinhPhanAnhFileAttackDto> GetDaTaByPage(GiaiTrinhPhanAnhFileAttackSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from GiaiTrinhPhanAnhFileAttacktbl in _GiaiTrinhPhanAnhFileAttackRepository.GetAllAsQueryable()

                        select new GiaiTrinhPhanAnhFileAttackDto
                        {
							IdGiaiTrinhPhanAnh = GiaiTrinhPhanAnhFileAttacktbl.IdGiaiTrinhPhanAnh,
							FileUrlGiaiTrinhPhanAnh = GiaiTrinhPhanAnhFileAttacktbl.FileUrlGiaiTrinhPhanAnh,
							IsDelete = GiaiTrinhPhanAnhFileAttacktbl.IsDelete,
							DeleteTime = GiaiTrinhPhanAnhFileAttacktbl.DeleteTime,
							DeleteId = GiaiTrinhPhanAnhFileAttacktbl.DeleteId,
							Id = GiaiTrinhPhanAnhFileAttacktbl.Id,
							CreatedDate = GiaiTrinhPhanAnhFileAttacktbl.CreatedDate,
							CreatedBy = GiaiTrinhPhanAnhFileAttacktbl.CreatedBy,
							CreatedID = GiaiTrinhPhanAnhFileAttacktbl.CreatedID,
							UpdatedDate = GiaiTrinhPhanAnhFileAttacktbl.UpdatedDate,
							UpdatedBy = GiaiTrinhPhanAnhFileAttacktbl.UpdatedBy,
							UpdatedID = GiaiTrinhPhanAnhFileAttacktbl.UpdatedID
                            
                        };

            if (searchModel != null)
            {
		if (searchModel.IdGiaiTrinhPhanAnhFilter!=null)
		{
			query = query.Where(x => x.IdGiaiTrinhPhanAnh==searchModel.IdGiaiTrinhPhanAnhFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.FileUrlGiaiTrinhPhanAnhFilter))
		{
			query = query.Where(x => x.FileUrlGiaiTrinhPhanAnh.Contains(searchModel.FileUrlGiaiTrinhPhanAnhFilter));
		}
		if (searchModel.IsDeleteFilter!=null)
		{
			query = query.Where(x => x.IsDelete==searchModel.IsDeleteFilter);
		}
		if (searchModel.DeleteTimeFilter!=null)
		{
			query = query.Where(x => x.DeleteTime==searchModel.DeleteTimeFilter);
		}
		if (searchModel.DeleteIdFilter!=null)
		{
			query = query.Where(x => x.DeleteId==searchModel.DeleteIdFilter);
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<GiaiTrinhPhanAnhFileAttackDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public GiaiTrinhPhanAnhFileAttack GetById(long id)
        {
            return _GiaiTrinhPhanAnhFileAttackRepository.GetById(id);
        }
    

    }
}
