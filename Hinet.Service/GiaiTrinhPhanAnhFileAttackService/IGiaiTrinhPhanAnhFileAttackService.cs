using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.GiaiTrinhPhanAnhFileAttackService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.GiaiTrinhPhanAnhFileAttackService
{
    public interface IGiaiTrinhPhanAnhFileAttackService:IEntityService<GiaiTrinhPhanAnhFileAttack>
    {
        PageListResultBO<GiaiTrinhPhanAnhFileAttackDto> GetDaTaByPage(GiaiTrinhPhanAnhFileAttackSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        GiaiTrinhPhanAnhFileAttack GetById(long id);
    }
}
