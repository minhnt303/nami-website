using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.TINH2Service.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.TINH2Service
{
    public interface ITINH2Service:IEntityService<TINH2>
    {
        PageListResultBO<TINH2Dto> GetDaTaByPage(TINH2SearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        TINH2 GetById(long id);
    }
}
