using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.TINH2Repository;
using Nami.Service.TINH2Service.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;




namespace Nami.Service.TINH2Service
{
    public class TINH2Service : EntityService<TINH2>, ITINH2Service
    {
        IUnitOfWork _unitOfWork;
        ITINH2Repository _TINH2Repository;
	ILog _loger;
        IMapper _mapper;


        
        public TINH2Service(IUnitOfWork unitOfWork, 
		ITINH2Repository TINH2Repository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, TINH2Repository)
        {
            _unitOfWork = unitOfWork;
            _TINH2Repository = TINH2Repository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<TINH2Dto> GetDaTaByPage(TINH2SearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from TINH2tbl in _TINH2Repository.GetAllAsQueryable()

                        select new TINH2Dto
                        {

                            
                        };

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<TINH2Dto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public TINH2 GetById(long id)
        {
            return _TINH2Repository.GetById(id);
        }
    

    }
}
