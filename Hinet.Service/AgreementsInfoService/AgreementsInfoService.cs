﻿using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.AgreementsInfoRepository;
using Nami.Repository.BuildingsInfoRepository;
using Nami.Repository.FileDinhKemRepository;
using Nami.Repository.RoomInfoRepository;
using Nami.Service.AgreementsInfoService.Dto;
using Nami.Service.Common;
using Nami.Service.Constant;
using log4net;
using PagedList;
using System.Linq;
using System.Linq.Dynamic;
using Nami.Repository.RentersInfoImageRepository;
using Nami.Repository.RentersInfoRepository;
using System.Collections.Generic;
using Nami.Repository.AppUserRepository;

namespace Nami.Service.AgreementsInfoService
{
    public class AgreementsInfoService : EntityService<AgreementsInfo>, IAgreementsInfoService
    {
        IUnitOfWork _unitOfWork;
        IAgreementsInfoRepository _AgreementsInfoRepository;
        ILog _loger;
        IMapper _mapper;
        IFileDinhKemRepository _fileDinhKemRepository;
        IRoomInfoRepository _roomInfoRepository;
        IBuildingsInfoRepository _buildingsInfoRepository;
        IRentersInfoImageRepository _rentersInfoImageRepository;
        IRentersInfoRepository _rentersInfoRepository;
        IAppUserRepository _appUserRepository;

        public AgreementsInfoService(IUnitOfWork unitOfWork,
        IAgreementsInfoRepository AgreementsInfoRepository,
        IFileDinhKemRepository fileDinhKemRepository,
        IRoomInfoRepository roomInfoRepository,
        IBuildingsInfoRepository buildingsInfoRepository,
        IRentersInfoImageRepository rentersInfoImageRepository,
        IRentersInfoRepository rentersInfoRepository,
        IAppUserRepository appUserRepository,
        ILog loger,

                IMapper mapper
            )
            : base(unitOfWork, AgreementsInfoRepository)
        {
            _unitOfWork = unitOfWork;
            _AgreementsInfoRepository = AgreementsInfoRepository;
            _loger = loger;
            _mapper = mapper;
            _fileDinhKemRepository = fileDinhKemRepository;
            _roomInfoRepository = roomInfoRepository;
            _buildingsInfoRepository = buildingsInfoRepository;
            _rentersInfoImageRepository = rentersInfoImageRepository;
            _rentersInfoRepository = rentersInfoRepository;
            _appUserRepository = appUserRepository;
        }

        public PageListResultBO<AgreementsInfoDto> GetDaTaByPage(AgreementsInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from AgreementsInfotbl in _AgreementsInfoRepository.GetAllAsQueryable()

                        select new AgreementsInfoDto
                        {
                            Id = AgreementsInfotbl.Id,
                            BuildingsId = AgreementsInfotbl.BuildingsId,
                            RoomsId = AgreementsInfotbl.RoomsId,
                            AgreementCode = AgreementsInfotbl.AgreementCode,
                            RentersGroupId = AgreementsInfotbl.RentersGroupId,
                            ServicesGroupId = AgreementsInfotbl.ServicesGroupId,
                            RepresenterName = AgreementsInfotbl.RepresenterName,
                            DepositsMoney = AgreementsInfotbl.DepositsMoney,
                            Month = AgreementsInfotbl.Month,
                            DateStart = AgreementsInfotbl.DateStart,
                            DateEnd = AgreementsInfotbl.DateEnd,
                            PayPerMonth = AgreementsInfotbl.PayPerMonth,
                            Note = AgreementsInfotbl.Note,
                            CreatedDate = AgreementsInfotbl.CreatedDate,
                            CreatedBy = AgreementsInfotbl.CreatedBy,
                            UpdatedDate = AgreementsInfotbl.UpdatedDate,
                            UpdatedBy = AgreementsInfotbl.UpdatedBy

                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.AgreementCodeFilter))
                {
                    query = query.Where(x => x.AgreementCode.Contains(searchModel.AgreementCodeFilter));
                }
                if (searchModel.DepositsMoneyFilter != null)
                {
                    query = query.Where(x => x.DepositsMoney == searchModel.DepositsMoneyFilter);
                }
                if (searchModel.MonthFilter != null)
                {
                    query = query.Where(x => x.Month == searchModel.MonthFilter);
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<AgreementsInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public PageListResultBO<AgreementsInfoDto> GetDaTaByPageCreatedBy(string CreatedBy, AgreementsInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from AgreementsInfotbl in _AgreementsInfoRepository.GetAllAsQueryable().Where(x => x.CreatedBy == CreatedBy)

                        select new AgreementsInfoDto
                        {
                            Id = AgreementsInfotbl.Id,
                            BuildingsId = AgreementsInfotbl.BuildingsId,
                            RoomsId = AgreementsInfotbl.RoomsId,
                            AgreementCode = AgreementsInfotbl.AgreementCode,
                            RentersGroupId = AgreementsInfotbl.RentersGroupId,
                            ServicesGroupId = AgreementsInfotbl.ServicesGroupId,
                            RepresenterName = AgreementsInfotbl.RepresenterName,
                            DepositsMoney = AgreementsInfotbl.DepositsMoney,
                            Month = AgreementsInfotbl.Month,
                            DateStart = AgreementsInfotbl.DateStart,
                            DateEnd = AgreementsInfotbl.DateEnd,
                            PayPerMonth = AgreementsInfotbl.PayPerMonth,
                            Note = AgreementsInfotbl.Note,
                            CreatedDate = AgreementsInfotbl.CreatedDate,
                            CreatedBy = AgreementsInfotbl.CreatedBy,
                            UpdatedDate = AgreementsInfotbl.UpdatedDate,
                            UpdatedBy = AgreementsInfotbl.UpdatedBy

                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.AgreementCodeFilter))
                {
                    query = query.Where(x => x.AgreementCode.Contains(searchModel.AgreementCodeFilter));
                }
                if (searchModel.DepositsMoneyFilter != null)
                {
                    query = query.Where(x => x.DepositsMoney == searchModel.DepositsMoneyFilter);
                }
                if (searchModel.MonthFilter != null)
                {
                    query = query.Where(x => x.Month == searchModel.MonthFilter);
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<AgreementsInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            foreach (var item in resultmodel.ListItem)
            {
                item.FileDinhKem = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.CreatedBy == CreatedBy && x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Agreement).ToList();
                var RoomsIdName = _roomInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.RoomsId && x.CreatedBy == CreatedBy).Select(x => x.RoomName).FirstOrDefault();
                item.RoomsIdName = !string.IsNullOrEmpty(RoomsIdName) ? RoomsIdName : string.Empty;
                var BuildingsIdName = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.BuildingsId && x.CreatedBy == CreatedBy).Select(x => x.BuildingsName).FirstOrDefault();
                item.BuildingsIdName = !string.IsNullOrEmpty(BuildingsIdName) ? BuildingsIdName : string.Empty;
                var RenterInfo = _rentersInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.RentersGroupId).FirstOrDefault();
                if(RenterInfo != null)
                {
                    item.RenterInfo = RenterInfo;
                }
                item.AgreementCodeName = ConstantExtension.GetName<AgreementTypeConstant>(item.AgreementCode);
            }
            return resultmodel;
        }

        public AgreementsInfo GetById(long id)
        {
            return _AgreementsInfoRepository.GetById(id);
        }

        public int CountNumberOfAgreementCreatedBy(string CreatedBy)
        {
            var numberofAgreement = _AgreementsInfoRepository.GetAllAsQueryable().Where(x => x.CreatedBy == CreatedBy).OrderByDescending(x => x.Id).ToList();
            return (numberofAgreement != null && numberofAgreement.Any()) ? numberofAgreement.Count() : 0;
        }
        public AgreementsInfoDto GetDtoById(long id)
        {
            var query = (from AgreementsInfotbl in _AgreementsInfoRepository.GetAllAsQueryable().Where(x => x.Id == id)

                        select new AgreementsInfoDto
                        {
                            Id = AgreementsInfotbl.Id,
                            BuildingsId = AgreementsInfotbl.BuildingsId,
                            RoomsId = AgreementsInfotbl.RoomsId,
                            AgreementCode = AgreementsInfotbl.AgreementCode,
                            RentersGroupId = AgreementsInfotbl.RentersGroupId,
                            ServicesGroupId = AgreementsInfotbl.ServicesGroupId,
                            RepresenterName = AgreementsInfotbl.RepresenterName,
                            DepositsMoney = AgreementsInfotbl.DepositsMoney,
                            Month = AgreementsInfotbl.Month,
                            DateStart = AgreementsInfotbl.DateStart,
                            DateEnd = AgreementsInfotbl.DateEnd,
                            PayPerMonth = AgreementsInfotbl.PayPerMonth,
                            Note = AgreementsInfotbl.Note,
                            CreatedDate = AgreementsInfotbl.CreatedDate,
                            CreatedBy = AgreementsInfotbl.CreatedBy,
                            UpdatedDate = AgreementsInfotbl.UpdatedDate,
                            UpdatedBy = AgreementsInfotbl.UpdatedBy
                        }).OrderByDescending(x => x.Id).FirstOrDefault();

            if (query.BuildingsId != null) {
                query.BuildingsIdInfo = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == query.BuildingsId).FirstOrDefault();
            }
            else if(query.RoomsId != null)
            {
                query.RoomsIdInfo = _roomInfoRepository.GetAllAsQueryable().Where(x => x.Id == query.RoomsId).FirstOrDefault();
            }
            return query;
        }

        public List<AgreementsInfo> GetListAgreementByCreatedByAndBuldingId(string CreatedBy, long buildingId)
        {
            return _AgreementsInfoRepository.GetAllAsQueryable().Where(x => x.CreatedBy == CreatedBy && x.BuildingsId == buildingId).ToList();
        }

        public List<AgreementsInfoDto> GetListAggrementEndUserByRentersGroupId(long? RentersGroupId)
        {
            var query = (from AgreementsInfotbl in _AgreementsInfoRepository.GetAllAsQueryable().Where(x => x.RentersGroupId == RentersGroupId)

                        select new AgreementsInfoDto
                        {
                            Id = AgreementsInfotbl.Id,
                            BuildingsId = AgreementsInfotbl.BuildingsId,
                            RoomsId = AgreementsInfotbl.RoomsId,
                            AgreementCode = AgreementsInfotbl.AgreementCode,
                            RentersGroupId = AgreementsInfotbl.RentersGroupId,
                            ServicesGroupId = AgreementsInfotbl.ServicesGroupId,
                            RepresenterName = AgreementsInfotbl.RepresenterName,
                            DepositsMoney = AgreementsInfotbl.DepositsMoney,
                            Month = AgreementsInfotbl.Month,
                            DateStart = AgreementsInfotbl.DateStart,
                            DateEnd = AgreementsInfotbl.DateEnd,
                            PayPerMonth = AgreementsInfotbl.PayPerMonth,
                            Note = AgreementsInfotbl.Note,
                            CreatedDate = AgreementsInfotbl.CreatedDate,
                            CreatedBy = AgreementsInfotbl.CreatedBy,
                            UpdatedDate = AgreementsInfotbl.UpdatedDate,
                            UpdatedBy = AgreementsInfotbl.UpdatedBy

                        }).OrderByDescending(x=>x.UpdatedDate).ToList();
            foreach (var item in query)
            {
                if(item.RoomsId != null && item.BuildingsId == null)
                {
                    var RoomInfo = _roomInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.RoomsId).FirstOrDefault();
                    if(RoomInfo != null)
                    {
                        item.Name = RoomInfo.RoomName;
                        var userName = _appUserRepository.GetAllAsQueryable().Where(x => x.UserName == RoomInfo.CreatedBy).Select(x => x.FullName).FirstOrDefault();
                        if (!string.IsNullOrEmpty(userName))
                        {
                            item.TenChuSoHuu = userName;
                        }
                        item.Price = RoomInfo.Price;
                    }
                }
                else if (item.RoomsId == null && item.BuildingsId != null)
                {
                    var BuildingInfo = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.BuildingsId).FirstOrDefault();
                    if (BuildingInfo != null)
                    {
                        item.Name = BuildingInfo.BuildingsName;
                        var userName = _appUserRepository.GetAllAsQueryable().Where(x => x.UserName == BuildingInfo.CreatedBy).Select(x => x.FullName).FirstOrDefault();
                        if (!string.IsNullOrEmpty(userName))
                        {
                            item.TenChuSoHuu = userName;
                        }
                        item.Price = BuildingInfo.Price;
                    }
                }
            }
            return query;
        }
    }
}
