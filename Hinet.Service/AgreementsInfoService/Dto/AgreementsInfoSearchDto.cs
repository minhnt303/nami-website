using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.AgreementsInfoService.Dto
{
    public class AgreementsInfoSearchDto : SearchBase
    {
		public string AgreementCodeFilter { get; set; }
		public int? DepositsMoneyFilter { get; set; }
		public int? MonthFilter { get; set; }
		public int? DateStartFilter { get; set; }
		public int? DateEndFilter { get; set; }
		public int? PayPerMonthFilter { get; set; }


    }
}