using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.AgreementsInfoService.Dto
{
    public class AgreementsInfoDto : AgreementsInfo
    {
        public List<FileDinhKem> FileDinhKem { get; set; }
        public string RoomsIdName { get; set; }
        public string BuildingsIdName { get; set; }
        public RentersInfo RenterInfo { get; set; }
        public string AgreementCodeName { get; set; }
        public BuildingsInfo BuildingsIdInfo { get; set; }
        public RoomInfo RoomsIdInfo { get; set; }
        public string Name { get; set; }
        public string TenChuSoHuu { get; set; }
        public decimal? Price { get; set; }
    }
}