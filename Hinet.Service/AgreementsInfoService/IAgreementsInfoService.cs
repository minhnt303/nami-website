using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.AgreementsInfoService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.AgreementsInfoService
{
    public interface IAgreementsInfoService:IEntityService<AgreementsInfo>
    {
        PageListResultBO<AgreementsInfoDto> GetDaTaByPage(AgreementsInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        AgreementsInfo GetById(long id);
        PageListResultBO<AgreementsInfoDto> GetDaTaByPageCreatedBy(string CreatedBy, AgreementsInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        int CountNumberOfAgreementCreatedBy(string CreatedBy);
        AgreementsInfoDto GetDtoById(long id);
        List<AgreementsInfo> GetListAgreementByCreatedByAndBuldingId(string CreatedBy, long buildingId);
        List<AgreementsInfoDto> GetListAggrementEndUserByRentersGroupId(long? RentersGroupId);
    }
}
