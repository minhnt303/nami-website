using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.DevicesInfoService.Dto
{
    public class DevicesInfoSearchDto : SearchBase
    {
		public string DeviceNameFilter { get; set; }
		public int? PriceFilter { get; set; }
		public int? CompensationPriceFilter { get; set; }
		public string StatusFilter { get; set; }
		public string UnitsCodeFilter { get; set; }
		public int QuantityFilter { get; set; }


    }
}