using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.DevicesInfoRepository;
using Nami.Service.DevicesInfoService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;



namespace Nami.Service.DevicesInfoService
{
    public class DevicesInfoService : EntityService<DevicesInfo>, IDevicesInfoService
    {
        IUnitOfWork _unitOfWork;
        IDevicesInfoRepository _DevicesInfoRepository;
	ILog _loger;
        IMapper _mapper;

        
        public DevicesInfoService(IUnitOfWork unitOfWork, 
		IDevicesInfoRepository DevicesInfoRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, DevicesInfoRepository)
        {
            _unitOfWork = unitOfWork;
            _DevicesInfoRepository = DevicesInfoRepository;
            _loger = loger;
            _mapper = mapper;

        }

        public PageListResultBO<DevicesInfoDto> GetDaTaByPage(DevicesInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from DevicesInfotbl in _DevicesInfoRepository.GetAllAsQueryable()

                        select new DevicesInfoDto
                        {
							Id = DevicesInfotbl.Id,
							BuildingsId = DevicesInfotbl.BuildingsId,
							RoomsId = DevicesInfotbl.RoomsId,
							DeviceName = DevicesInfotbl.DeviceName,
							Price = DevicesInfotbl.Price,
							CompensationPrice = DevicesInfotbl.CompensationPrice,
							Status = DevicesInfotbl.Status,
							UnitsCode = DevicesInfotbl.UnitsCode,
							Description = DevicesInfotbl.Description,
							Quantity = DevicesInfotbl.Quantity,
							CreatedDate = DevicesInfotbl.CreatedDate,
							CreatedBy = DevicesInfotbl.CreatedBy,
							UpdatedDate = DevicesInfotbl.UpdatedDate,
							UpdatedBy = DevicesInfotbl.UpdatedBy
                            
                        };

            if (searchModel != null)
            {
		if (!string.IsNullOrEmpty(searchModel.DeviceNameFilter))
		{
			query = query.Where(x => x.DeviceName.Contains(searchModel.DeviceNameFilter));
		}
		if (searchModel.PriceFilter!=null)
		{
			query = query.Where(x => x.Price==searchModel.PriceFilter);
		}
		if (searchModel.CompensationPriceFilter!=null)
		{
			query = query.Where(x => x.CompensationPrice==searchModel.CompensationPriceFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.StatusFilter))
		{
			query = query.Where(x => x.Status.Contains(searchModel.StatusFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.UnitsCodeFilter))
		{
			query = query.Where(x => x.UnitsCode.Contains(searchModel.UnitsCodeFilter));
		}
		if (searchModel.QuantityFilter!=null)
		{
			query = query.Where(x => x.Quantity==searchModel.QuantityFilter);
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<DevicesInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public DevicesInfo GetById(long id)
        {
            return _DevicesInfoRepository.GetById(id);
        }
    

    }
}
