using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.DevicesInfoService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.DevicesInfoService
{
    public interface IDevicesInfoService:IEntityService<DevicesInfo>
    {
        PageListResultBO<DevicesInfoDto> GetDaTaByPage(DevicesInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        DevicesInfo GetById(long id);
    }
}
