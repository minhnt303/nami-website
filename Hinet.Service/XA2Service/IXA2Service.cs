using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.XA2Service.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.XA2Service
{
    public interface IXA2Service:IEntityService<XA2>
    {
        PageListResultBO<XA2Dto> GetDaTaByPage(XA2SearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        XA2 GetById(long id);
    }
}
