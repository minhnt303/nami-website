using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.XA2Repository;
using Nami.Service.XA2Service.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;




namespace Nami.Service.XA2Service
{
    public class XA2Service : EntityService<XA2>, IXA2Service
    {
        IUnitOfWork _unitOfWork;
        IXA2Repository _XA2Repository;
	ILog _loger;
        IMapper _mapper;


        
        public XA2Service(IUnitOfWork unitOfWork, 
		IXA2Repository XA2Repository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, XA2Repository)
        {
            _unitOfWork = unitOfWork;
            _XA2Repository = XA2Repository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<XA2Dto> GetDaTaByPage(XA2SearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from XA2tbl in _XA2Repository.GetAllAsQueryable()

                        select new XA2Dto
                        {

                            
                        };

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<XA2Dto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public XA2 GetById(long id)
        {
            return _XA2Repository.GetById(id);
        }
    

    }
}
