﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nami.Service.Common
{
    public class Constant
    {
        public enum EDIT_OBJECT_ENUM
        {
            FAIL,
            SUCCESS
        }

        public enum WORKFLOW_REQUEST_EDIT
        {
            NEW,
            PENDING,
            RESOLVED
        }

        public enum FILE_TYPE_ENUM
        {
            IMAGE,
            PDF,
            OTHER
        }

        public class NHOM_DANHMUC_CONSTANT
        {
            public const string GIOITINH = "GIOITINH";
            public const string VITRI = "VITRI";
        }
        public class MODULE_INFO
        {
            public string Name { get; set; }
            public string Code { get; set; }
            public string Link { get; set; }
        }
        public class MODULE_CONSTANT
        {
            public static string KhieuNai = "KhieuNai";
            public static string TinTuc = "TinTuc";
            public static int ID_KhieuNai = 1;

            public static List<SelectListItem> GetList()
            {
                var listItem = new List<SelectListItem>();
                listItem.Add(new SelectListItem()
                {
                    Text = "Khiếu nại tố cáo",
                    Value = KhieuNai
                });
                listItem.Add(new SelectListItem()
                {
                    Text = "Tin tức",
                    Value = TinTuc
                });
                return listItem;
            }
            public static MODULE_INFO GetNameModule(string module, long ItemId)
            {
                var mo = new MODULE_INFO();
                if (module.Equals(KhieuNai))
                {
                    mo.Code = module;
                    mo.Name = "Phản ánh, tố cáo website";
                    mo.Link = "/PhanAnhInfoarea/PhanAnhInfo/Detail/" + ItemId.ToString();
                    return mo;
                }
                else if (module.Equals(TinTuc))
                {
                    mo.Code = module;
                    mo.Name = " tin bài";
                    mo.Link = "/NewsArea/News/detail/" + ItemId.ToString();
                    return mo;
                }

                return mo;
            }
            public static string GetCode(int idLoaiThe)
            {
                switch (idLoaiThe)
                {
                    case 1:
                        return KhieuNai;
                        break;
                }
                return "";
            }

            public static string TINBAI = "TINBAI";
            public static string LOAI_DANHMUC_ANPHAM = "LOAI_DANHMUC_ANPHAM";
            public static string ANPHAM = "ANPHAM";
        }

        public class VAITRO_CONSTANT
        {
            public static string BIENTAPVIEN = "BIENTAPVIEN";
            public static string TRUONGPHONG = "TRUONGPHONG";
            public static string UYVIEN_BIENTAP = "UYVIENBIENTAP";
            public static string THUKY_BIENTAP = "THUKYBIENTAP";
            public static string TONG_BIENTAP = "TONGBIENTAP";
            public static string PHOTONG_BIENTAP = "PHOTONGBIENTAP";
            public static string MORAT = "MORAT";
            public static string KITHUAT_BAOIN = "KITHUATBAOIN";
        }

        public class TRANGTHAI_TINBAI_CONSTANT
        {
            public static string MOITAO = "MOITAO";
            public static string DAGUI_TRUONGPHONG = "DAGUI_TRUONGPHONG";
            public static string DAGUI_UVBT = "DAGUI_UVBT";
            public static string DAGUI_THUKY = "DAGUI_THUKY";
            public static string DAGUI_TONGBIENTAP = "DAGUI_TONGBIENTAP";
            public static string DAGUI_MORAT = "DAGUI_MORAT";
            public static string DA_GUI_IN = "DA_GUI_IN";
            public static string DA_GUI_TBT_DUYET = "DA_GUI_TBT_DUYET";
            public static string DA_DUYET_BAN_IN = "DA_DUYET_BAN_IN";
        }

        public class TRANGTHAI_ANPHAM_CONSTANT
        {
            public static string MOITAO = "AP_MOITAO";
            public static string DAGUI_THUKY = "AP_DAGUI_THUKY";
            public static string DAGUI_TONGBIENTAP = "AP_DAGUI_TONGBIENTAP";
            public static string DA_DUYET = "AP_DA_DUYET";
        }

    }
}
