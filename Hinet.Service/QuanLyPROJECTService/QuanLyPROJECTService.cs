using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.QuanLyPROJECTRepository;
using Nami.Service.QuanLyPROJECTService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;




namespace Nami.Service.QuanLyPROJECTService
{
    public class QuanLyPROJECTService : EntityService<QuanLyPROJECT>, IQuanLyPROJECTService
    {
        IUnitOfWork _unitOfWork;
        IQuanLyPROJECTRepository _QuanLyPROJECTRepository;
	ILog _loger;
        IMapper _mapper;


        
        public QuanLyPROJECTService(IUnitOfWork unitOfWork, 
		IQuanLyPROJECTRepository QuanLyPROJECTRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, QuanLyPROJECTRepository)
        {
            _unitOfWork = unitOfWork;
            _QuanLyPROJECTRepository = QuanLyPROJECTRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<QuanLyPROJECTDto> GetDaTaByPage(QuanLyPROJECTSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from QuanLyPROJECTtbl in _QuanLyPROJECTRepository.GetAllAsQueryable()

                        select new QuanLyPROJECTDto
                        {
							TenDuAn = QuanLyPROJECTtbl.TenDuAn,
							DuAnId = QuanLyPROJECTtbl.DuAnId,
							TinhId = QuanLyPROJECTtbl.TinhId,
							HuyenId = QuanLyPROJECTtbl.HuyenId,
							Location = QuanLyPROJECTtbl.Location,
							Longitude = QuanLyPROJECTtbl.Longitude,
							Latitude = QuanLyPROJECTtbl.Latitude,
							IsDelete = QuanLyPROJECTtbl.IsDelete,
							DeleteTime = QuanLyPROJECTtbl.DeleteTime,
							DeleteId = QuanLyPROJECTtbl.DeleteId,
							Id = QuanLyPROJECTtbl.Id,
							CreatedDate = QuanLyPROJECTtbl.CreatedDate,
							CreatedBy = QuanLyPROJECTtbl.CreatedBy,
							CreatedID = QuanLyPROJECTtbl.CreatedID,
							UpdatedDate = QuanLyPROJECTtbl.UpdatedDate,
							UpdatedBy = QuanLyPROJECTtbl.UpdatedBy,
							UpdatedID = QuanLyPROJECTtbl.UpdatedID
                            
                        };

            if (searchModel != null)
            {
		if (!string.IsNullOrEmpty(searchModel.TenDuAnFilter))
		{
			query = query.Where(x => x.TenDuAn.Contains(searchModel.TenDuAnFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.DuAnIdFilter))
		{
			query = query.Where(x => x.DuAnId.Contains(searchModel.DuAnIdFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.TinhIdFilter))
		{
			query = query.Where(x => x.TinhId.Contains(searchModel.TinhIdFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.HuyenIdFilter))
		{
			query = query.Where(x => x.HuyenId.Contains(searchModel.HuyenIdFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.LocationFilter))
		{
			query = query.Where(x => x.Location.Contains(searchModel.LocationFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.LongitudeFilter))
		{
			query = query.Where(x => x.Longitude.Contains(searchModel.LongitudeFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.LatitudeFilter))
		{
			query = query.Where(x => x.Latitude.Contains(searchModel.LatitudeFilter));
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<QuanLyPROJECTDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public QuanLyPROJECT GetById(long id)
        {
            return _QuanLyPROJECTRepository.GetById(id);
        }
    

    }
}
