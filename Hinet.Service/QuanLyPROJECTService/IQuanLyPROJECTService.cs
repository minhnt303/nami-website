using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.QuanLyPROJECTService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QuanLyPROJECTService
{
    public interface IQuanLyPROJECTService:IEntityService<QuanLyPROJECT>
    {
        PageListResultBO<QuanLyPROJECTDto> GetDaTaByPage(QuanLyPROJECTSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        QuanLyPROJECT GetById(long id);
    }
}
