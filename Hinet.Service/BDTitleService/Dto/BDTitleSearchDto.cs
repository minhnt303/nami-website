using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BDTitleService.Dto
{
    public class BDTitleSearchDto : SearchBase
    {
		public long? IdBieuDoFilter { get; set; }
		public string IdTitleFilter { get; set; }
		public int SizeFilter { get; set; }
		public string TextFilter { get; set; }
		public bool IsBoldFilter { get; set; }
		public string ColorFilter { get; set; }
		public bool? IsDeleteFilter { get; set; }
		public DateTime? DeleteTimeFilter { get; set; }
		public long? DeleteIdFilter { get; set; }


    }
}