using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Nami.Service.BDTitleService.Dto
{
    public class BDTitleExportDto
    {
		[DisplayName("")]
public long? IdBieuDo { get; set; }
		[DisplayName("")]
public string IdTitle { get; set; }
		[DisplayName("")]
public int Size { get; set; }
		[DisplayName("")]
public string Text { get; set; }
		[DisplayName("")]
public bool IsBold { get; set; }
		[DisplayName("")]
public string Color { get; set; }
		[DisplayName("")]
public bool? IsDelete { get; set; }
		[DisplayName("")]
public DateTime? DeleteTime { get; set; }
		[DisplayName("")]
public long? DeleteId { get; set; }

    }
}