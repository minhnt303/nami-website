using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.BDTitleService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BDTitleService
{
    public interface IBDTitleService:IEntityService<BDTitle>
    {
        PageListResultBO<BDTitleDto> GetDaTaByPage(BDTitleSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        BDTitle GetById(long id);
    }
}
