using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.BDTitleRepository;
using Nami.Service.BDTitleService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;




namespace Nami.Service.BDTitleService
{
    public class BDTitleService : EntityService<BDTitle>, IBDTitleService
    {
        IUnitOfWork _unitOfWork;
        IBDTitleRepository _BDTitleRepository;
	ILog _loger;
        IMapper _mapper;


        
        public BDTitleService(IUnitOfWork unitOfWork, 
		IBDTitleRepository BDTitleRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, BDTitleRepository)
        {
            _unitOfWork = unitOfWork;
            _BDTitleRepository = BDTitleRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<BDTitleDto> GetDaTaByPage(BDTitleSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from BDTitletbl in _BDTitleRepository.GetAllAsQueryable()

                        select new BDTitleDto
                        {
							IdBieuDo = BDTitletbl.IdBieuDo,
							IdTitle = BDTitletbl.IdTitle,
							Size = BDTitletbl.Size,
							Text = BDTitletbl.Text,
							IsBold = BDTitletbl.IsBold,
							Color = BDTitletbl.Color,
							IsDelete = BDTitletbl.IsDelete,
							DeleteTime = BDTitletbl.DeleteTime,
							DeleteId = BDTitletbl.DeleteId,
							Id = BDTitletbl.Id,
							CreatedDate = BDTitletbl.CreatedDate,
							CreatedBy = BDTitletbl.CreatedBy,
							CreatedID = BDTitletbl.CreatedID,
							UpdatedDate = BDTitletbl.UpdatedDate,
							UpdatedBy = BDTitletbl.UpdatedBy,
							UpdatedID = BDTitletbl.UpdatedID
                            
                        };

            if (searchModel != null)
            {
		if (searchModel.IdBieuDoFilter!=null)
		{
			query = query.Where(x => x.IdBieuDo==searchModel.IdBieuDoFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.IdTitleFilter))
		{
			query = query.Where(x => x.IdTitle.Contains(searchModel.IdTitleFilter));
		}
		if (searchModel.SizeFilter!=null)
		{
			query = query.Where(x => x.Size==searchModel.SizeFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.TextFilter))
		{
			query = query.Where(x => x.Text.Contains(searchModel.TextFilter));
		}
		if (searchModel.IsBoldFilter!=null)
		{
			query = query.Where(x => x.IsBold==searchModel.IsBoldFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.ColorFilter))
		{
			query = query.Where(x => x.Color.Contains(searchModel.ColorFilter));
		}
		if (searchModel.IsDeleteFilter!=null)
		{
			query = query.Where(x => x.IsDelete==searchModel.IsDeleteFilter);
		}
		if (searchModel.DeleteTimeFilter!=null)
		{
			query = query.Where(x => x.DeleteTime==searchModel.DeleteTimeFilter);
		}
		if (searchModel.DeleteIdFilter!=null)
		{
			query = query.Where(x => x.DeleteId==searchModel.DeleteIdFilter);
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<BDTitleDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public BDTitle GetById(long id)
        {
            return _BDTitleRepository.GetById(id);
        }
    

    }
}
