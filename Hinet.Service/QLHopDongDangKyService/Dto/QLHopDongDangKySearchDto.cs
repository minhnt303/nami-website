using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLHopDongDangKyService.Dto
{
    public class QLHopDongDangKySearchDto : SearchBase
    {
		public long? ItemIdFilter { get; set; }
		public string ItemTypeFilter { get; set; }
		public string UserIdDangKyFilter { get; set; }


    }
}