using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLHopDongDangKyService.Dto
{
    public class QLHopDongDangKyDto : QLHopDongDangKy
    {
        public string ItemUrl { get; set; }
        public string ItemName { get; set; }
        public string ItemTypeName { get; set; }
    }
}