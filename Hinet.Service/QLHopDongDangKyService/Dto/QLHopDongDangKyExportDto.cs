using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Nami.Service.QLHopDongDangKyService.Dto
{
    public class QLHopDongDangKyExportDto
    {
		[DisplayName("Id đối tượng")]
public long? ItemId { get; set; }
		[DisplayName("Loại đối tượng")]
public string ItemType { get; set; }
		[DisplayName("Id người đăng ký")]
public string UserIdDangKy { get; set; }

    }
}