using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.QLHopDongDangKyService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nami.Service.QLHopDongDangKyService
{
    public interface IQLHopDongDangKyService:IEntityService<QLHopDongDangKy>
    {
        PageListResultBO<QLHopDongDangKyDto> GetDaTaByPage(QLHopDongDangKySearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        QLHopDongDangKy GetById(long id);
        List<QLHopDongDangKyDto> GetListDangKyByIdUser(long userId);
        List<SelectListItem> GetDropDownDangKyHopDongById(long? ItemId, string ItemType, string valueSelected);
    }
}
