using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.QLHopDongDangKyRepository;
using Nami.Service.QLHopDongDangKyService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;
using Nami.Repository.BuildingsInfoRepository;
using Nami.Repository.RoomInfoRepository;
using System.Web.Mvc;
using Nami.Repository.AppUserRepository;

namespace Nami.Service.QLHopDongDangKyService
{
    public class QLHopDongDangKyService : EntityService<QLHopDongDangKy>, IQLHopDongDangKyService
    {
        IUnitOfWork _unitOfWork;
        IQLHopDongDangKyRepository _QLHopDongDangKyRepository;
	ILog _loger;
        IMapper _mapper;
        IBuildingsInfoRepository _BuildingsInfoRepository;
        IRoomInfoRepository _RoomInfoRepository;
        IAppUserRepository _AppUserRepository;

        public QLHopDongDangKyService(IUnitOfWork unitOfWork, 
		IQLHopDongDangKyRepository QLHopDongDangKyRepository, 
		ILog loger,
        IBuildingsInfoRepository BuildingsInfoRepository,
        IRoomInfoRepository RoomInfoRepository,
        IAppUserRepository AppUserRepository,
                IMapper mapper	
            )
            : base(unitOfWork, QLHopDongDangKyRepository)
        {
            _unitOfWork = unitOfWork;
            _QLHopDongDangKyRepository = QLHopDongDangKyRepository;
            _loger = loger;
            _mapper = mapper;
            _BuildingsInfoRepository = BuildingsInfoRepository;
            _RoomInfoRepository = RoomInfoRepository;
            _AppUserRepository = AppUserRepository;
        }

        public PageListResultBO<QLHopDongDangKyDto> GetDaTaByPage(QLHopDongDangKySearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from QLHopDongDangKytbl in _QLHopDongDangKyRepository.GetAllAsQueryable()

                        select new QLHopDongDangKyDto
                        {
							ItemId = QLHopDongDangKytbl.ItemId,
							ItemType = QLHopDongDangKytbl.ItemType,
							UserIdDangKy = QLHopDongDangKytbl.UserIdDangKy,
							IsDelete = QLHopDongDangKytbl.IsDelete,
							DeleteTime = QLHopDongDangKytbl.DeleteTime,
							DeleteId = QLHopDongDangKytbl.DeleteId,
							Id = QLHopDongDangKytbl.Id,
							CreatedDate = QLHopDongDangKytbl.CreatedDate,
							CreatedBy = QLHopDongDangKytbl.CreatedBy,
							CreatedID = QLHopDongDangKytbl.CreatedID,
							UpdatedDate = QLHopDongDangKytbl.UpdatedDate,
							UpdatedBy = QLHopDongDangKytbl.UpdatedBy,
							UpdatedID = QLHopDongDangKytbl.UpdatedID
                            
                        };

            if (searchModel != null)
            {
		if (searchModel.ItemIdFilter!=null)
		{
			query = query.Where(x => x.ItemId==searchModel.ItemIdFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.ItemTypeFilter))
		{
			query = query.Where(x => x.ItemType.Contains(searchModel.ItemTypeFilter));
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<QLHopDongDangKyDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public QLHopDongDangKy GetById(long id)
        {
            return _QLHopDongDangKyRepository.GetById(id);
        }


        public List<QLHopDongDangKyDto> GetListDangKyByIdUser(long userId)
        {
            var query = (from QLHopDongDangKytbl in _QLHopDongDangKyRepository.GetAllAsQueryable().Where(x=>x.UserIdDangKy == userId)

                        select new QLHopDongDangKyDto
                        {
                            ItemId = QLHopDongDangKytbl.ItemId,
                            ItemType = QLHopDongDangKytbl.ItemType,
                            UserIdDangKy = QLHopDongDangKytbl.UserIdDangKy,
                            IsDelete = QLHopDongDangKytbl.IsDelete,
                            DeleteTime = QLHopDongDangKytbl.DeleteTime,
                            DeleteId = QLHopDongDangKytbl.DeleteId,
                            Id = QLHopDongDangKytbl.Id,
                            CreatedDate = QLHopDongDangKytbl.CreatedDate,
                            CreatedBy = QLHopDongDangKytbl.CreatedBy,
                            CreatedID = QLHopDongDangKytbl.CreatedID,
                            UpdatedDate = QLHopDongDangKytbl.UpdatedDate,
                            UpdatedBy = QLHopDongDangKytbl.UpdatedBy,
                            UpdatedID = QLHopDongDangKytbl.UpdatedID

                        }).OrderByDescending(x=>x.CreatedDate).ToList();
            foreach (var item in query)
            {
                if (!string.IsNullOrEmpty(item.ItemType))
                {
                    var ItemTypeName = ConstantExtension.GetName<ItemTypeConstant>(item.ItemType);
                    if (!string.IsNullOrEmpty(ItemTypeName))
                    {
                        item.ItemTypeName = ItemTypeName;
                    }
                }
                if (item.ItemId != null)
                {
                    if (item.ItemType == ItemTypeConstant.Building)
                    {
                        item.ItemUrl = "/EndUserPersonalInfo/DetailBuilding?id=" + item.ItemId.ToString();
                        var BuildindInfo = _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.ItemId).FirstOrDefault();
                        if (BuildindInfo != null && !string.IsNullOrEmpty(BuildindInfo.BuildingsName))
                        {
                            item.ItemName = BuildindInfo.BuildingsName;
                        }
                    }
                    else if (item.ItemType == ItemTypeConstant.Room)
                    {
                        item.ItemUrl = "/EndUserPersonalInfo/DetailRoom?id=" + item.ItemId.ToString();
                        var RoomInfo = _RoomInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.ItemId).FirstOrDefault();
                        if (RoomInfo != null && !string.IsNullOrEmpty(RoomInfo.RoomName))
                        {
                            item.ItemName = RoomInfo.RoomName;
                        }
                    }
                }
            }
            return query;
        }

        public List<SelectListItem> GetDropDownDangKyHopDongById(long? ItemId, string ItemType, string valueSelected)
        {
            var model = _QLHopDongDangKyRepository.GetAllAsQueryable().Where(x => x.ItemId == ItemId && x.ItemType == ItemType)
                .Select(x => new SelectListItem()
                {
                    Text = x.CreatedBy,
                    Value = x.UserIdDangKy.ToString(),
                    Selected = x.UserIdDangKy.ToString() == valueSelected
                })
                .ToList();
            foreach (var item in model)
            {
                if (!string.IsNullOrEmpty(item.Value)) {
                    var UserInfo = _AppUserRepository.GetAllAsQueryable().Where(x => x.Id.ToString() == item.Value).FirstOrDefault();
                    if (UserInfo != null && !string.IsNullOrEmpty(UserInfo.FullName)) {
                        item.Text = UserInfo.FullName;
                    }
                }
            }
            return model;
        }
    }
}
