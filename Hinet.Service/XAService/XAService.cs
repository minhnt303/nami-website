using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.XARepository;
using Nami.Service.XAService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using System.Web.Mvc;

namespace Nami.Service.XAService
{
    public class XAService : EntityService<XA>, IXAService
    {
        IUnitOfWork _unitOfWork;
        IXARepository _XARepository;
        ILog _loger;
        IMapper _mapper;


        public XAService(IUnitOfWork unitOfWork,
        IXARepository XARepository,
        ILog loger,

                IMapper mapper
            )
            : base(unitOfWork, XARepository)
        {
            _unitOfWork = unitOfWork;
            _XARepository = XARepository;
            _loger = loger;
            _mapper = mapper;

        }

        public List<SelectListItem> GetDropdownOfHuyen(string id, string valueSelected)
        {
            var model = _XARepository.GetAllAsQueryable().Where(x => x.HuyenId == id)
                .Select(x => new SelectListItem()
                {
                    Text = x.TenXa,
                    Value = x.MaXa.ToString(),
                    Selected = x.MaXa == valueSelected
                })
                .ToList();
            return model;
        }

        public PageListResultBO<XADto> GetDaTaByPage(string huyenId, XASearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from XAtbl in _XARepository.GetAllAsQueryable().Where(x => x.HuyenId == huyenId)

                        select new XADto
                        {
                            MaXa = XAtbl.MaXa,
                            TenXa = XAtbl.TenXa,
                            Loai = XAtbl.Loai,
                            Location = XAtbl.Location,
                            Id = XAtbl.Id,
                            HuyenId = XAtbl.HuyenId
                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.MaXaFilter))
                {
                    query = query.Where(x => x.MaXa.Contains(searchModel.MaXaFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.TenXaFilter))
                {
                    query = query.Where(x => x.TenXa.Contains(searchModel.TenXaFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.LoaiFilter))
                {
                    query = query.Where(x => x.Loai.Contains(searchModel.LoaiFilter));
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<XADto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public XA GetById(long id)
        {
            return _XARepository.GetById(id);
        }

        public List<XA> GetDaTaAllXA()
        {
            var query = (from XAtbl in _XARepository.GetAllAsQueryable()

                         select XAtbl).ToList();
            return query;
        }
    }
}
