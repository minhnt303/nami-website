using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.XAService.Dto
{
    public class XASearchDto : SearchBase
    {
		public string MaXaFilter { get; set; }
		public string TenXaFilter { get; set; }
		public string LoaiFilter { get; set; }


    }
}