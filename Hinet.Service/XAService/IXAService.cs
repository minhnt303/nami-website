using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.XAService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nami.Service.XAService
{
    public interface IXAService:IEntityService<XA>
    {
        PageListResultBO<XADto> GetDaTaByPage(string huyenId, XASearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        List<SelectListItem> GetDropdownOfHuyen(string id, string valueSelected);
        List<XA> GetDaTaAllXA();
        XA GetById(long id);
    }
}
