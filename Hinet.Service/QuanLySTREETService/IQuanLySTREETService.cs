using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.QuanLySTREETService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QuanLySTREETService
{
    public interface IQuanLySTREETService:IEntityService<QuanLySTREET>
    {
        PageListResultBO<QuanLySTREETDto> GetDaTaByPage(QuanLySTREETSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        QuanLySTREET GetById(long id);
    }
}
