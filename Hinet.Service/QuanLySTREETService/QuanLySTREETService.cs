using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.QuanLySTREETRepository;
using Nami.Service.QuanLySTREETService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;




namespace Nami.Service.QuanLySTREETService
{
    public class QuanLySTREETService : EntityService<QuanLySTREET>, IQuanLySTREETService
    {
        IUnitOfWork _unitOfWork;
        IQuanLySTREETRepository _QuanLySTREETRepository;
	ILog _loger;
        IMapper _mapper;


        
        public QuanLySTREETService(IUnitOfWork unitOfWork, 
		IQuanLySTREETRepository QuanLySTREETRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, QuanLySTREETRepository)
        {
            _unitOfWork = unitOfWork;
            _QuanLySTREETRepository = QuanLySTREETRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<QuanLySTREETDto> GetDaTaByPage(QuanLySTREETSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from QuanLySTREETtbl in _QuanLySTREETRepository.GetAllAsQueryable()

                        select new QuanLySTREETDto
                        {
							MaDuong = QuanLySTREETtbl.MaDuong,
							TenDuong = QuanLySTREETtbl.TenDuong,
							DuongId = QuanLySTREETtbl.DuongId,
							TinhId = QuanLySTREETtbl.TinhId,
							HuyenId = QuanLySTREETtbl.HuyenId,
							Loai = QuanLySTREETtbl.Loai,
							Location = QuanLySTREETtbl.Location,
							Longitude = QuanLySTREETtbl.Longitude,
							Latitude = QuanLySTREETtbl.Latitude,
							IsDelete = QuanLySTREETtbl.IsDelete,
							DeleteTime = QuanLySTREETtbl.DeleteTime,
							DeleteId = QuanLySTREETtbl.DeleteId,
							Id = QuanLySTREETtbl.Id,
							CreatedDate = QuanLySTREETtbl.CreatedDate,
							CreatedBy = QuanLySTREETtbl.CreatedBy,
							CreatedID = QuanLySTREETtbl.CreatedID,
							UpdatedDate = QuanLySTREETtbl.UpdatedDate,
							UpdatedBy = QuanLySTREETtbl.UpdatedBy,
							UpdatedID = QuanLySTREETtbl.UpdatedID
                            
                        };

            if (searchModel != null)
            {
		if (!string.IsNullOrEmpty(searchModel.MaDuongFilter))
		{
			query = query.Where(x => x.MaDuong.Contains(searchModel.MaDuongFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.TenDuongFilter))
		{
			query = query.Where(x => x.TenDuong.Contains(searchModel.TenDuongFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.DuongIdFilter))
		{
			query = query.Where(x => x.DuongId.Contains(searchModel.DuongIdFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.TinhIdFilter))
		{
			query = query.Where(x => x.TinhId.Contains(searchModel.TinhIdFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.HuyenIdFilter))
		{
			query = query.Where(x => x.HuyenId.Contains(searchModel.HuyenIdFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.LoaiFilter))
		{
			query = query.Where(x => x.Loai.Contains(searchModel.LoaiFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.LocationFilter))
		{
			query = query.Where(x => x.Location.Contains(searchModel.LocationFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.LongitudeFilter))
		{
			query = query.Where(x => x.Longitude.Contains(searchModel.LongitudeFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.LatitudeFilter))
		{
			query = query.Where(x => x.Latitude.Contains(searchModel.LatitudeFilter));
		}
		if (searchModel.DeleteIdFilter!=null)
		{
			query = query.Where(x => x.DeleteId==searchModel.DeleteIdFilter);
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<QuanLySTREETDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public QuanLySTREET GetById(long id)
        {
            return _QuanLySTREETRepository.GetById(id);
        }
    

    }
}
