using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.CommentByAccountService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.CommentByAccountService
{
    public interface ICommentByAccountService:IEntityService<CommentByAccount>
    {
        PageListResultBO<CommentByAccountDto> GetDaTaByPage(CommentByAccountSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        CommentByAccount GetById(long id);
        List<CommentByAccountDto> GetListDataByIdItemAndTypeItemAndPortUserIdByCreateDate(long IdItem, string TypeItem, long? PortUserId);
        List<CommentByAccountDto> GetListCommentBuidingId(long IdItem, string TypeItem, long? PostId);
    }
}
