﻿using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.AppUserRepository;
using Nami.Repository.CommentByAccountFileAttackRepository;
using Nami.Repository.CommentByAccountRepository;
using Nami.Service.CommentByAccountFileAttackService.Dto;
using Nami.Service.CommentByAccountService.Dto;
using Nami.Service.Common;
using log4net;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Nami.Repository.DepartmentRepository;
using Nami.Repository.DanhmucRepository;
using Nami.Service.Constant;
namespace Nami.Service.CommentByAccountService
{
    public class CommentByAccountService : EntityService<CommentByAccount>, ICommentByAccountService
    {
        IUnitOfWork _unitOfWork;
        ICommentByAccountRepository _CommentByAccountRepository;
        ILog _loger;
        IMapper _mapper;
        ICommentByAccountFileAttackRepository _commentByAccountFileAttackRepository;
        IAppUserRepository _appUserRepository;
        IDepartmentRepository _departmentRepository;
        IDM_DulieuDanhmucRepository _dM_DulieuDanhmucRepository;
        IDM_NhomDanhmucRepository _nhomDanhmucRepository;
        public CommentByAccountService(IUnitOfWork unitOfWork,
        ICommentByAccountRepository CommentByAccountRepository,
        ILog loger,
        ICommentByAccountFileAttackRepository commentByAccountFileAttackRepository,
        IAppUserRepository appUserRepository,
        IDepartmentRepository departmentRepository,
        IDM_DulieuDanhmucRepository dM_DulieuDanhmucRepository,
        IDM_NhomDanhmucRepository nhomDanhmucRepository,
                IMapper mapper
            )
            : base(unitOfWork, CommentByAccountRepository)
        {
            _unitOfWork = unitOfWork;
            _CommentByAccountRepository = CommentByAccountRepository;
            _loger = loger;
            _mapper = mapper;
            _commentByAccountFileAttackRepository = commentByAccountFileAttackRepository;
            _appUserRepository = appUserRepository;
            _departmentRepository = departmentRepository;
            _dM_DulieuDanhmucRepository = dM_DulieuDanhmucRepository;
            _nhomDanhmucRepository = nhomDanhmucRepository;
        }

        public PageListResultBO<CommentByAccountDto> GetDaTaByPage(CommentByAccountSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from CommentByAccounttbl in _CommentByAccountRepository.GetAllAsQueryable()

                        select new CommentByAccountDto
                        {
                            PortUserId = CommentByAccounttbl.PortUserId,
                            ParentPostUserId = CommentByAccounttbl.ParentPostUserId,
                            IdItem = CommentByAccounttbl.IdItem,
                            CreatedDate = CommentByAccounttbl.CreatedDate,
                            UpdatedDate = CommentByAccounttbl.UpdatedDate,
                            Id = CommentByAccounttbl.Id,
                            Comments = CommentByAccounttbl.Comments,
                            Ghim = CommentByAccounttbl.Ghim,
                            flag = CommentByAccounttbl.flag,
                            CreatedBy = CommentByAccounttbl.CreatedBy,
                            UpdatedBy = CommentByAccounttbl.UpdatedBy

                        };

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<CommentByAccountDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public CommentByAccount GetById(long id)
        {
            return _CommentByAccountRepository.GetById(id);
        }

        //Lấy danh sách bình luận theo ngày tạo với điều kiện truyền vào là 
        //IdItem: Id của hồ sơ được bình luận
        //TypeItem: Loại hồ sơ được bình luận
        //PortUserId: Id của người đăng bài
        public List<CommentByAccountDto> GetListDataByIdItemAndTypeItemAndPortUserIdByCreateDate(long IdItem, string TypeItem, long? PortUserId)
        {
            var IdChucVus = _nhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.ChucVu).DefaultIfEmpty().FirstOrDefault().Id;
            var query = (from CommentByAccounttbl in _CommentByAccountRepository.GetAllAsQueryable().Where(x => x.IdItem == IdItem && x.TypeItem == TypeItem && x.ParentPostUserId == null)

                         join user in _appUserRepository.GetAllAsQueryable()
                         on CommentByAccounttbl.PortUserId equals user.Id into Usertable
                         from usertbl in Usertable.DefaultIfEmpty()

                         join department in _departmentRepository.GetAllAsQueryable()
                         on usertbl.IdDepartment equals department.Id into departmenttable
                         from departmenttbl in departmenttable.DefaultIfEmpty()

                         join dulieudanhmuc in _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x=>x.GroupId == IdChucVus)
                         on usertbl.IdChucVu equals dulieudanhmuc.Code into dulieudanhmuctable
                         from dulieudanhmuctbl in dulieudanhmuctable.DefaultIfEmpty()

                         select new CommentByAccountDto
                         {
                             ChucVuName = dulieudanhmuctbl.Name,
                             IdChucVu = usertbl.IdChucVu,
                             IdDepartment = usertbl.IdDepartment,
                             DepartmentName = departmenttbl.Name,
                             FullName = usertbl.FullName,
                             AvatarUrl = usertbl.Avatar,
                             PortUserId = CommentByAccounttbl.PortUserId,
                             ParentPostUserId = CommentByAccounttbl.ParentPostUserId,
                             IdItem = CommentByAccounttbl.IdItem,
                             CreatedDate = CommentByAccounttbl.CreatedDate,
                             UpdatedDate = CommentByAccounttbl.UpdatedDate,
                             Id = CommentByAccounttbl.Id,
                             Comments = CommentByAccounttbl.Comments,
                             Ghim = CommentByAccounttbl.Ghim,
                             TypeItem  = CommentByAccounttbl.TypeItem,
                             flag = CommentByAccounttbl.flag,
                             CreatedBy = CommentByAccounttbl.CreatedBy,
                             UpdatedBy = CommentByAccounttbl.UpdatedBy
                         }).OrderByDescending(x => x.CreatedDate).ToList();
            foreach (var item in query)
            {
                var queryFileUrl = (from CommentByAccountFileAttacktbl in _commentByAccountFileAttackRepository.GetAllAsQueryable().Where(x => x.CommentType == TypeItem && x.CommentId == item.Id)
                                    select new CommentByAccountFileAttackDto
                                    {
                                        CommentId = CommentByAccountFileAttacktbl.CommentId,
                                        CreatedDate = CommentByAccountFileAttacktbl.CreatedDate,
                                        UpdatedDate = CommentByAccountFileAttacktbl.UpdatedDate,
                                        Id = CommentByAccountFileAttacktbl.Id,
                                        FileUrl = CommentByAccountFileAttacktbl.FileUrl,
                                        CommentType = CommentByAccountFileAttacktbl.CommentType,
                                        flag = CommentByAccountFileAttacktbl.flag,
                                        CreatedBy = CommentByAccountFileAttacktbl.CreatedBy,
                                        UpdatedBy = CommentByAccountFileAttacktbl.UpdatedBy
                                    }).OrderByDescending(x => x.CommentId).FirstOrDefault();
                if (queryFileUrl != null)
                {
                    item.FileUrl = queryFileUrl.FileUrl;
                }
                    item.ListCommentChild = (from CommentByAccounttbl in _CommentByAccountRepository.GetAllAsQueryable().Where(x => x.IdItem == item.IdItem && x.TypeItem == item.TypeItem && x.ParentPostUserId == item.Id)

                                             join user in _appUserRepository.GetAllAsQueryable()
                                             on CommentByAccounttbl.PortUserId equals user.Id into Usertable
                                             from usertbl in Usertable.DefaultIfEmpty()

                                             join department in _departmentRepository.GetAllAsQueryable()
                                             on usertbl.IdDepartment equals department.Id into departmenttable
                                             from departmenttbl in departmenttable.DefaultIfEmpty()

                                             join dulieudanhmuc in _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdChucVus)
                                             on usertbl.IdChucVu equals dulieudanhmuc.Code into dulieudanhmuctable
                                             from dulieudanhmuctbl in dulieudanhmuctable.DefaultIfEmpty()

                                             select new CommentByAccountDto
                                             {
                                                 ChucVuName = dulieudanhmuctbl.Name,
                                                 IdChucVu = usertbl.IdChucVu,
                                                 IdDepartment = usertbl.IdDepartment,
                                                 DepartmentName = departmenttbl.Name,
                                                 FullName = usertbl.FullName,
                                                 AvatarUrl = usertbl.Avatar,
                                                 PortUserId = CommentByAccounttbl.PortUserId,
                                                 ParentPostUserId = CommentByAccounttbl.ParentPostUserId,
                                                 IdItem = CommentByAccounttbl.IdItem,
                                                 CreatedDate = CommentByAccounttbl.CreatedDate,
                                                 UpdatedDate = CommentByAccounttbl.UpdatedDate,
                                                 Id = CommentByAccounttbl.Id,
                                                 Comments = CommentByAccounttbl.Comments,
                                                 Ghim = CommentByAccounttbl.Ghim,
                                                 flag = CommentByAccounttbl.flag,
                                                 CreatedBy = CommentByAccounttbl.CreatedBy,
                                                 UpdatedBy = CommentByAccounttbl.UpdatedBy
                                             }).OrderByDescending(x => x.CreatedDate).ToList();
                foreach (var itemChild in item.ListCommentChild)
                {
                    var queryFileUrlChild = (from CommentByAccountFileAttacktbl in _commentByAccountFileAttackRepository.GetAllAsQueryable().Where(x => x.CommentType == item.TypeItem && x.CommentId == itemChild.Id)
                                        select new CommentByAccountFileAttackDto
                                        {
                                            CommentId = CommentByAccountFileAttacktbl.CommentId,
                                            CreatedDate = CommentByAccountFileAttacktbl.CreatedDate,
                                            UpdatedDate = CommentByAccountFileAttacktbl.UpdatedDate,
                                            Id = CommentByAccountFileAttacktbl.Id,
                                            FileUrl = CommentByAccountFileAttacktbl.FileUrl,
                                            CommentType = CommentByAccountFileAttacktbl.CommentType,
                                            flag = CommentByAccountFileAttacktbl.flag,
                                            CreatedBy = CommentByAccountFileAttacktbl.CreatedBy,
                                            UpdatedBy = CommentByAccountFileAttacktbl.UpdatedBy
                                        }).OrderByDescending(x => x.CommentId).FirstOrDefault();
                    if (queryFileUrlChild != null)
                    {
                        itemChild.FileUrl = queryFileUrlChild.FileUrl;
                    } 
                }
            }
            return query;
        }

        public List<CommentByAccountDto> GetListCommentBuidingId(long IdItem, string TypeItem, long? PostId)
        {
            var query = (from CommentByAccounttbl in _CommentByAccountRepository.GetAllAsQueryable().Where(x => x.IdItem == IdItem && x.TypeItem == TypeItem)

                         join user in _appUserRepository.GetAllAsQueryable()
                         on CommentByAccounttbl.PortUserId equals user.Id into Usertable
                         from usertbl in Usertable.DefaultIfEmpty()
                                                 
                         select new CommentByAccountDto
                         {
                             IdChucVu = usertbl.IdChucVu,
                             IdDepartment = usertbl.IdDepartment,
                             FullName = usertbl.FullName,
                             AvatarUrl = usertbl.Avatar,
                             PortUserId = CommentByAccounttbl.PortUserId,
                             ParentPostUserId = CommentByAccounttbl.ParentPostUserId,
                             IdItem = CommentByAccounttbl.IdItem,
                             CreatedDate = CommentByAccounttbl.CreatedDate,
                             UpdatedDate = CommentByAccounttbl.UpdatedDate,
                             Id = CommentByAccounttbl.Id,
                             Comments = CommentByAccounttbl.Comments,
                             Ghim = CommentByAccounttbl.Ghim,
                             TypeItem = CommentByAccounttbl.TypeItem,
                             flag = CommentByAccounttbl.flag,
                             CreatedBy = CommentByAccounttbl.CreatedBy,
                             UpdatedBy = CommentByAccounttbl.UpdatedBy
                         }).OrderByDescending(x => x.CreatedDate).ToList();
            return query;
        }

        //public List<CommentByAccountDto> GetListCommentDistinct(long IdItem, string TypeItem, long? PostId)
        //{
        //    var query = (from CommentByAccounttbl in _CommentByAccountRepository.GetAllAsQueryable().Where(x => x.IdItem == IdItem && x.TypeItem == TypeItem)

        //                 join user in _appUserRepository.GetAllAsQueryable()
        //                 on CommentByAccounttbl.PortUserId equals user.Id into Usertable
        //                 from usertbl in Usertable.DefaultIfEmpty()

        //                 select new CommentByAccountDto
        //                 {
        //                     IdChucVu = usertbl.IdChucVu,
        //                     IdDepartment = usertbl.IdDepartment,
        //                     FullName = usertbl.FullName,
        //                     AvatarUrl = usertbl.Avatar,
        //                     PortUserId = CommentByAccounttbl.PortUserId,
        //                     ParentPostUserId = CommentByAccounttbl.ParentPostUserId,
        //                     IdItem = CommentByAccounttbl.IdItem,
        //                     CreatedDate = CommentByAccounttbl.CreatedDate,
        //                     UpdatedDate = CommentByAccounttbl.UpdatedDate,
        //                     Id = CommentByAccounttbl.Id,
        //                     Comments = CommentByAccounttbl.Comments,
        //                     Ghim = CommentByAccounttbl.Ghim,
        //                     TypeItem = CommentByAccounttbl.TypeItem,
        //                     flag = CommentByAccounttbl.flag,
        //                     CreatedBy = CommentByAccounttbl.CreatedBy,
        //                     UpdatedBy = CommentByAccounttbl.UpdatedBy
        //                 }).OrderByDescending(x => x.CreatedDate).Select(x=>x.IdItem).Distinct().ToList();
        //    return query;
        //}
    }
}
