using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.CommentByAccountService.Dto
{
    public class CommentByAccountDto : CommentByAccount
    {
        public string FileUrl { get; set; }
        public string AvatarUrl { get; set; }
        public string FullName { get; set; }
        public long? IdDepartment { get; set; }
        public string DepartmentName { get; set; }
        public object ChucVuName { get; set; }
        public string IdChucVu { get; set; }
        public List<CommentByAccountDto> ListCommentChild{get;set; }
    }
}