using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BDDataProvinceService.Dto
{
    public class BDDataProvinceSearchDto : SearchBase
    {
		public long? IdBieuDoFilter { get; set; }
		public bool IsCategoryFilter { get; set; }
		public string KeyFilter { get; set; }
		public decimal ValueFilter { get; set; }
		public bool? IsDeleteFilter { get; set; }
		public DateTime? DeleteTimeFilter { get; set; }
		public long? DeleteIdFilter { get; set; }


    }
}