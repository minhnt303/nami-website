using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Nami.Service.BDDataProvinceService.Dto
{
    public class BDDataProvinceExportDto
    {
		[DisplayName("")]
public long? IdBieuDo { get; set; }
		[DisplayName("")]
public bool IsCategory { get; set; }
		[DisplayName("")]
public string Key { get; set; }
		[DisplayName("")]
public decimal Value { get; set; }
		[DisplayName("")]
public bool? IsDelete { get; set; }
		[DisplayName("")]
public DateTime? DeleteTime { get; set; }
		[DisplayName("")]
public long? DeleteId { get; set; }

    }
}