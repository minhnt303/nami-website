using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Nami.Service.BDDataProvinceService.Dto
{
    public class BDDataProvinceImportDto
    {
		[DisplayName("")]
public long? IdBieuDo { get; set; }
		[Required]
[DisplayName("")]
public bool IsCategory { get; set; }
		[DisplayName("")]
public string Key { get; set; }
		[Required]
[DisplayName("")]
public decimal Value { get; set; }
		[DisplayName("")]
public bool? IsDelete { get; set; }
		[DisplayName("")]
public DateTime? DeleteTime { get; set; }
		[DisplayName("")]
public long? DeleteId { get; set; }

    }
}