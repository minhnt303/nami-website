using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.BDDataProvinceService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BDDataProvinceService
{
    public interface IBDDataProvinceService:IEntityService<BDDataProvince>
    {
        PageListResultBO<BDDataProvinceDto> GetDaTaByPage(BDDataProvinceSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        BDDataProvince GetById(long id);
    }
}
