using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.BDDataProvinceRepository;
using Nami.Service.BDDataProvinceService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;




namespace Nami.Service.BDDataProvinceService
{
    public class BDDataProvinceService : EntityService<BDDataProvince>, IBDDataProvinceService
    {
        IUnitOfWork _unitOfWork;
        IBDDataProvinceRepository _BDDataProvinceRepository;
	ILog _loger;
        IMapper _mapper;


        
        public BDDataProvinceService(IUnitOfWork unitOfWork, 
		IBDDataProvinceRepository BDDataProvinceRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, BDDataProvinceRepository)
        {
            _unitOfWork = unitOfWork;
            _BDDataProvinceRepository = BDDataProvinceRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<BDDataProvinceDto> GetDaTaByPage(BDDataProvinceSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from BDDataProvincetbl in _BDDataProvinceRepository.GetAllAsQueryable()

                        select new BDDataProvinceDto
                        {
							IdBieuDo = BDDataProvincetbl.IdBieuDo,
							IsCategory = BDDataProvincetbl.IsCategory,
							Key = BDDataProvincetbl.Key,
							Value = BDDataProvincetbl.Value,
							IsDelete = BDDataProvincetbl.IsDelete,
							DeleteTime = BDDataProvincetbl.DeleteTime,
							DeleteId = BDDataProvincetbl.DeleteId,
							Id = BDDataProvincetbl.Id,
							CreatedDate = BDDataProvincetbl.CreatedDate,
							CreatedBy = BDDataProvincetbl.CreatedBy,
							CreatedID = BDDataProvincetbl.CreatedID,
							UpdatedDate = BDDataProvincetbl.UpdatedDate,
							UpdatedBy = BDDataProvincetbl.UpdatedBy,
							UpdatedID = BDDataProvincetbl.UpdatedID
                            
                        };

            if (searchModel != null)
            {
		if (searchModel.IdBieuDoFilter!=null)
		{
			query = query.Where(x => x.IdBieuDo==searchModel.IdBieuDoFilter);
		}
		if (searchModel.IsCategoryFilter!=null)
		{
			query = query.Where(x => x.IsCategory==searchModel.IsCategoryFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.KeyFilter))
		{
			query = query.Where(x => x.Key.Contains(searchModel.KeyFilter));
		}
		if (searchModel.ValueFilter!=null)
		{
			query = query.Where(x => x.Value==searchModel.ValueFilter);
		}
		if (searchModel.IsDeleteFilter!=null)
		{
			query = query.Where(x => x.IsDelete==searchModel.IsDeleteFilter);
		}
		if (searchModel.DeleteTimeFilter!=null)
		{
			query = query.Where(x => x.DeleteTime==searchModel.DeleteTimeFilter);
		}
		if (searchModel.DeleteIdFilter!=null)
		{
			query = query.Where(x => x.DeleteId==searchModel.DeleteIdFilter);
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<BDDataProvinceDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public BDDataProvince GetById(long id)
        {
            return _BDDataProvinceRepository.GetById(id);
        }
    

    }
}
