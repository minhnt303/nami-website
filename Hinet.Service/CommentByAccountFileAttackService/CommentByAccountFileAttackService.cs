using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.CommentByAccountFileAttackRepository;
using Nami.Service.CommentByAccountFileAttackService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;



namespace Nami.Service.CommentByAccountFileAttackService
{
    public class CommentByAccountFileAttackService : EntityService<CommentByAccountFileAttack>, ICommentByAccountFileAttackService
    {
        IUnitOfWork _unitOfWork;
        ICommentByAccountFileAttackRepository _CommentByAccountFileAttackRepository;
	ILog _loger;
        IMapper _mapper;

        
        public CommentByAccountFileAttackService(IUnitOfWork unitOfWork, 
		ICommentByAccountFileAttackRepository CommentByAccountFileAttackRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, CommentByAccountFileAttackRepository)
        {
            _unitOfWork = unitOfWork;
            _CommentByAccountFileAttackRepository = CommentByAccountFileAttackRepository;
            _loger = loger;
            _mapper = mapper;

        }

        public PageListResultBO<CommentByAccountFileAttackDto> GetDaTaByPage(CommentByAccountFileAttackSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from CommentByAccountFileAttacktbl in _CommentByAccountFileAttackRepository.GetAllAsQueryable()

                        select new CommentByAccountFileAttackDto
                        {
							CommentId = CommentByAccountFileAttacktbl.CommentId,
							CreatedDate = CommentByAccountFileAttacktbl.CreatedDate,
							UpdatedDate = CommentByAccountFileAttacktbl.UpdatedDate,
							Id = CommentByAccountFileAttacktbl.Id,
							FileUrl = CommentByAccountFileAttacktbl.FileUrl,
							CommentType = CommentByAccountFileAttacktbl.CommentType,
							flag = CommentByAccountFileAttacktbl.flag,
							CreatedBy = CommentByAccountFileAttacktbl.CreatedBy,
							UpdatedBy = CommentByAccountFileAttacktbl.UpdatedBy
                            
                        };

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<CommentByAccountFileAttackDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public CommentByAccountFileAttack GetById(long id)
        {
            return _CommentByAccountFileAttackRepository.GetById(id);
        }
    

    }
}
