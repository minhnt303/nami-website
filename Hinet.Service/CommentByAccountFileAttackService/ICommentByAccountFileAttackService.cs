using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.CommentByAccountFileAttackService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.CommentByAccountFileAttackService
{
    public interface ICommentByAccountFileAttackService:IEntityService<CommentByAccountFileAttack>
    {
        PageListResultBO<CommentByAccountFileAttackDto> GetDaTaByPage(CommentByAccountFileAttackSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        CommentByAccountFileAttack GetById(long id);
    }
}
