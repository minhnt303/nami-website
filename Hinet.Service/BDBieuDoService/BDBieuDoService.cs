using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.BDBieuDoRepository;
using Nami.Service.BDBieuDoService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;




namespace Nami.Service.BDBieuDoService
{
    public class BDBieuDoService : EntityService<BDBieuDo>, IBDBieuDoService
    {
        IUnitOfWork _unitOfWork;
        IBDBieuDoRepository _BDBieuDoRepository;
	ILog _loger;
        IMapper _mapper;


        
        public BDBieuDoService(IUnitOfWork unitOfWork, 
		IBDBieuDoRepository BDBieuDoRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, BDBieuDoRepository)
        {
            _unitOfWork = unitOfWork;
            _BDBieuDoRepository = BDBieuDoRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<BDBieuDoDto> GetDaTaByPage(BDBieuDoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from BDBieuDotbl in _BDBieuDoRepository.GetAllAsQueryable()

                        select new BDBieuDoDto
                        {
							Name = BDBieuDotbl.Name,
							Type = BDBieuDotbl.Type,
							IsDelete = BDBieuDotbl.IsDelete,
							DeleteTime = BDBieuDotbl.DeleteTime,
							DeleteId = BDBieuDotbl.DeleteId,
							Id = BDBieuDotbl.Id,
							CreatedDate = BDBieuDotbl.CreatedDate,
							CreatedBy = BDBieuDotbl.CreatedBy,
							CreatedID = BDBieuDotbl.CreatedID,
							UpdatedDate = BDBieuDotbl.UpdatedDate,
							UpdatedBy = BDBieuDotbl.UpdatedBy,
							UpdatedID = BDBieuDotbl.UpdatedID
                            
                        };

            if (searchModel != null)
            {
		if (!string.IsNullOrEmpty(searchModel.NameFilter))
		{
			query = query.Where(x => x.Name.Contains(searchModel.NameFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.TypeFilter))
		{
			query = query.Where(x => x.Type.Contains(searchModel.TypeFilter));
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<BDBieuDoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public BDBieuDo GetById(long id)
        {
            return _BDBieuDoRepository.GetById(id);
        }
    

    }
}
