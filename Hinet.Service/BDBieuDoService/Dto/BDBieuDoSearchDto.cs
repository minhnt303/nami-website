using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BDBieuDoService.Dto
{
    public class BDBieuDoSearchDto : SearchBase
    {
		public string NameFilter { get; set; }
		public string TypeFilter { get; set; }


    }
}