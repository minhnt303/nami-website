using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.BDBieuDoService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BDBieuDoService
{
    public interface IBDBieuDoService:IEntityService<BDBieuDo>
    {
        PageListResultBO<BDBieuDoDto> GetDaTaByPage(BDBieuDoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        BDBieuDo GetById(long id);
    }
}
