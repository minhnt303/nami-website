﻿using Nami.Model.Entities;
using Nami.Model.IdentityEntities;
using Nami.Service.AppUserService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.AppUserService
{
    public interface IAppUserService : IEntityService<AppUser>
    {
        PageListResultBO<UserDto> GetDaTaByPage(AppUserSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        AppUser GetById(long id);
        bool CheckExistUserName(string userName, long? id = null);
        bool CheckExistEmail(string email, long? id = null);
        UserDto GetDtoById(long id);
        UserDto GetUserDataByIdOfOrganizationIdAndTypeOrganization(long id, string typeOrganization);

        List<UserDto> GetAllByMaTinhAndDepartment(string MaTinh, long? IdDepartment, int CurrentUserId);
        List<long> GetUserIdByOrganizationId(long? id, string OrganizationType);
        AppUser GetByCreatedBy(string CreatedBy);

    }
}
