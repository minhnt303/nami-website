﻿using AutoMapper;
using Nami.Model.Entities;
using Nami.Model.IdentityEntities;
using Nami.Repository;
using Nami.Repository.AppUserRepository;
using Nami.Repository.DanhmucRepository;
using Nami.Repository.DepartmentRepository;
using Nami.Repository.RoleRepository;
using Nami.Repository.TINHRepository;
using Nami.Repository.UserRoleRepository;
using Nami.Service.AppUserService.Dto;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.DM_DulieuDanhmucService;
using Nami.Service.OperationService;
using log4net;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;

namespace Nami.Service.AppUserService
{
    public class AppUserService : EntityService<AppUser>, IAppUserService
    {
        IUnitOfWork _unitOfWork;
        IAppUserRepository _appUserRepository;
        IDepartmentRepository _departmentRepository;
        IRoleRepository _roleRepository;
        IOperationService _operationService;
        IUserRoleRepository _userRoleRepository;
        IDM_DulieuDanhmucService _dM_DulieuDanhmucService;
        IDM_DulieuDanhmucRepository _dM_DulieuDanhmucRepository;
        
        ITINHRepository _TINHRepository;
        ILog _loger;
        IMapper _mapper;
        public AppUserService(IUnitOfWork unitOfWork, IAppUserRepository appUserRepository, ILog loger,
            IDepartmentRepository departmentRepository,
            IUserRoleRepository userRoleRepository,
            IDM_DulieuDanhmucRepository dM_DulieuDanhmucRepository,
            IMapper mapper,
            IDM_DulieuDanhmucService dM_DulieuDanhmucService,
            IRoleRepository roleRepository,
           
            ITINHRepository TINHRepository,
             IOperationService operationService
            )
            : base(unitOfWork, appUserRepository)
        {
            _dM_DulieuDanhmucService = dM_DulieuDanhmucService;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _unitOfWork = unitOfWork;
            _appUserRepository = appUserRepository;
            _loger = loger;
            _mapper = mapper;
            _departmentRepository = departmentRepository;
            _operationService = operationService;
            _dM_DulieuDanhmucRepository = dM_DulieuDanhmucRepository;
          
            _TINHRepository = TINHRepository;
        }

        public PageListResultBO<UserDto> GetDaTaByPage(AppUserSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var userRoleDbSet = _userRoleRepository.DBSet();
            var RoleDbSet = _roleRepository.DBSet();
            var query = from user in _appUserRepository.GetAllAsQueryable().Where(x => x.TypeAccount == AccountTypeConstant.BussinessUser || x.TypeAccount == AccountTypeConstant.NguoiDung)
                        join departmentTbl in _departmentRepository.GetAllAsQueryable() on user.IdDepartment equals departmentTbl.Id into jDepartment
                        from departmentInfo in jDepartment.DefaultIfEmpty()

                        join DuLieuDanhMuctbl in _dM_DulieuDanhmucRepository.GetAllAsQueryable()
                        on user.IdChucVu equals DuLieuDanhMuctbl.Code into DulieuDanhMucTable
                        from DuLieuDanhMuctbl2 in DulieuDanhMucTable.DefaultIfEmpty()
                        select new UserDto
                        {
                            ProvinceManagement = user.ProvinceManagement,
                            IdChucVuName = DuLieuDanhMuctbl2.Name,
                            IdChucVu = user.IdChucVu,
                            UserName = user.UserName,
                            FullName = user.FullName,
                            Id = user.Id,
                            IdDepartment = user.IdDepartment,
                            DepartmentName = departmentInfo.Name,
                            AccessFailedCount = user.AccessFailedCount,
                            Address = user.Address,
                            Avatar = user.Avatar,
                            BirthDay = user.BirthDay,
                            Email = user.Email,
                            EmailConfirmed = user.EmailConfirmed,
                            Gender = user.Gender,
                            LockoutEnabled = user.LockoutEnabled,
                            LockoutEndDateUtc = user.LockoutEndDateUtc,
                            PhoneNumber = user.PhoneNumber,
                            PhoneNumberConfirmed = user.PhoneNumberConfirmed,
                            TypeAccount = user.TypeAccount,
                            TypeOrganization = user.TypeOrganization,
                            OrganizationId = user.OrganizationId,
                            ListRoles = (from userRole in userRoleDbSet.Where(x => x.UserId == user.Id)
                                         join roletbl in RoleDbSet on userRole.RoleId equals roletbl.Id
                                         select roletbl).ToList(),
                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.UserNameFilter))
                {
                    query = query.Where(x => x.UserName.Contains(searchModel.UserNameFilter));
                }

                if (!string.IsNullOrEmpty(searchModel.FullNameFilter))
                {
                    query = query.Where(x => x.FullName.Contains(searchModel.FullNameFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.EmailFilter))
                {
                    query = query.Where(x => x.Email.Contains(searchModel.EmailFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.AddressFilter))
                {
                    query = query.Where(x => x.Address.Contains(searchModel.AddressFilter));
                }

                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<UserDto>();


            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            //foreach (var item in resultmodel.ListItem)
            //{
            //    item.ListRoles= (from userRole in _userRoleRepository.GetAllAsQueryable().Where(x => x.UserId == item.Id)
            //           join roletbl in _roleRepository.GetAllAsQueryable() on userRole.RoleId equals roletbl.Id
            //           select roletbl).ToList();
            //}

            var CurrentDateTime = DateTime.Now;
            foreach (var item in resultmodel.ListItem)
            {
                if (item.LockoutEndDateUtc != null && item.LockoutEndDateUtc > CurrentDateTime && item.LockoutEnabled == true)
                {
                    item.IsLock = true;
                }
                else
                {
                    item.IsLock = false;
                }
            }
            return resultmodel;
        }

        public AppUser GetById(long id)
        {
            return _appUserRepository.GetById(id);
        }

        public AppUser GetByCreatedBy(string CreatedBy)
        {
            return _appUserRepository.GetAllAsQueryable().Where(x => x.UserName == CreatedBy).FirstOrDefault();
        }

        public List<UserDto> GetAllByMaTinhAndDepartment(string MaTinh, long? IdDepartment, int CurrentUserId)
        {
            var userRoleDbSet = _userRoleRepository.DBSet();
            var RoleDbSet = _roleRepository.DBSet();
            var query = from user in _appUserRepository.GetAllAsQueryable().Where(x => x.TypeAccount == AccountTypeConstant.BussinessUser && x.Id != CurrentUserId && x.IdDepartment == IdDepartment && x.ProvinceManagement.Contains(MaTinh))
                        join departmentTbl in _departmentRepository.GetAllAsQueryable() on user.IdDepartment equals departmentTbl.Id into jDepartment
                        from departmentInfo in jDepartment.DefaultIfEmpty()

                        join DuLieuDanhMuctbl in _dM_DulieuDanhmucRepository.GetAllAsQueryable()
                        on user.IdChucVu equals DuLieuDanhMuctbl.Code into DulieuDanhMucTable
                        from DuLieuDanhMuctbl2 in DulieuDanhMucTable.DefaultIfEmpty()
                        select new UserDto
                        {
                            ProvinceManagement = user.ProvinceManagement,
                            IdChucVuName = DuLieuDanhMuctbl2.Name,
                            IdChucVu = user.IdChucVu,
                            UserName = user.UserName,
                            FullName = user.FullName,
                            Id = user.Id,
                            IdDepartment = user.IdDepartment,
                            DepartmentName = departmentInfo.Name,
                            AccessFailedCount = user.AccessFailedCount,
                            Address = user.Address,
                            Avatar = user.Avatar,
                            BirthDay = user.BirthDay,
                            Email = user.Email,
                            EmailConfirmed = user.EmailConfirmed,
                            Gender = user.Gender,
                            LockoutEnabled = user.LockoutEnabled,
                            LockoutEndDateUtc = user.LockoutEndDateUtc,
                            PhoneNumber = user.PhoneNumber,
                            PhoneNumberConfirmed = user.PhoneNumberConfirmed,
                            TypeAccount = user.TypeAccount,
                            TypeOrganization = user.TypeOrganization,
                            OrganizationId = user.OrganizationId,
                            ListRoles = (from userRole in userRoleDbSet.Where(x => x.UserId == user.Id)
                                         join roletbl in RoleDbSet on userRole.RoleId equals roletbl.Id
                                         select roletbl).ToList()
                        };
            return query.OrderByDescending(x => x.Id).ToList();
        }

        public Department GetByIdDepartment(long id)
        {
            return _departmentRepository.GetById(id);
        }
        /// <summary>
        /// Lấy thông tin dto của user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserDto GetDtoById(long id)
        {
            var userRoleDbSet = _userRoleRepository.DBSet();
            var RoleDbSet = _roleRepository.DBSet();
            var query = (from user in _appUserRepository.GetAllAsQueryable().Where(x => x.Id == id)
                         join departmentTbl in _departmentRepository.GetAllAsQueryable() on user.IdDepartment equals departmentTbl.Id into jDepartment
                         from departmentInfo in jDepartment.DefaultIfEmpty()

                         join DuLieuDanhMuctbl in _dM_DulieuDanhmucRepository.GetAllAsQueryable()
                         on user.IdChucVu equals DuLieuDanhMuctbl.Code into DulieuDanhMucTable
                         from DuLieuDanhMuctbl2 in DulieuDanhMucTable.DefaultIfEmpty()
                         select new UserDto
                         {
                             ProvinceManagement = user.ProvinceManagement,
                             IdChucVuName = DuLieuDanhMuctbl2.Name,
                             IdChucVu = user.IdChucVu,
                             UserName = user.UserName,
                             FullName = user.FullName,
                             Id = user.Id,
                             IdDepartment = user.IdDepartment,
                             DepartmentName = departmentInfo.Name,
                             AccessFailedCount = user.AccessFailedCount,
                             Address = user.Address,
                             Avatar = user.Avatar,
                             BirthDay = user.BirthDay,
                             Email = user.Email,
                             EmailConfirmed = user.EmailConfirmed,
                             Gender = user.Gender,
                             LockoutEnabled = user.LockoutEnabled,
                             LockoutEndDateUtc = user.LockoutEndDateUtc,
                             PhoneNumber = user.PhoneNumber,
                             PhoneNumberConfirmed = user.PhoneNumberConfirmed,
                             TypeAccount = user.TypeAccount,
                             TypeOrganization = user.TypeOrganization,
                             OrganizationId = user.OrganizationId,
                             ListRoles = (from userRole in userRoleDbSet.Where(x => x.UserId == user.Id)
                                          join roletbl in RoleDbSet on userRole.RoleId equals roletbl.Id
                                          select roletbl).ToList()
                         }).FirstOrDefault();
            query.ListActions = _operationService.GetListOperationOfUser(query.Id);

            query.ListOperations = new List<Operation>();
            if (query.ListActions!=null)
            {
                foreach (var item in query.ListActions)
                {
                    if (item.ListOperation!=null && item.ListOperation.Any())
                    {
                        query.ListOperations.AddRange(item.ListOperation);
                    }
                }
            }
            var deparmentProvince = _departmentRepository.GetAllAsQueryable().Where(x => x.Id == query.IdDepartment).Select(x => x.ProvinceManagement).FirstOrDefault();
            query.ListAllProvince = query.ProvinceManagement;
            if (string.IsNullOrEmpty(query.ListAllProvince))
            {
                query.ListAllProvince = deparmentProvince;
            }
            else if (!string.IsNullOrEmpty(deparmentProvince))
            {
                query.ListAllProvince += "," + deparmentProvince;
            }

           
            var KhuVuc = !string.IsNullOrEmpty(query.ProvinceManagement) ? query.ProvinceManagement.Split(',').ToList() : new List<string>();
            var KhuVucDepartment = !string.IsNullOrEmpty(deparmentProvince) ? deparmentProvince.Split(',').ToList() : new List<string>();
            query.ProvinceManagementNameEndUser = _TINHRepository.GetAllAsQueryable().Where(x => KhuVuc.Contains(x.MaTinh)).Select(x => x.TenTinh).ToList();
            query.ProvinceManagementNameDepartment = _TINHRepository.GetAllAsQueryable().Where(x => KhuVucDepartment.Contains(x.MaTinh)).Select(x => x.TenTinh).ToList();

            return query;
        }

        public UserDto GetUserDataByIdOfOrganizationIdAndTypeOrganization(long id, string typeOrganization)
        {
            var userRoleDbSet = _userRoleRepository.DBSet();
            var RoleDbSet = _roleRepository.DBSet();
            var query = (from user in _appUserRepository.GetAllAsQueryable().Where(x => x.OrganizationId == id && x.TypeOrganization == typeOrganization)
                         join departmentTbl in _departmentRepository.GetAllAsQueryable() on user.IdDepartment equals departmentTbl.Id into jDepartment
                         from departmentInfo in jDepartment.DefaultIfEmpty()

                         join DuLieuDanhMuctbl in _dM_DulieuDanhmucRepository.GetAllAsQueryable()
                         on user.IdChucVu equals DuLieuDanhMuctbl.Code into DulieuDanhMucTable
                         from DuLieuDanhMuctbl2 in DulieuDanhMucTable.DefaultIfEmpty()
                         select new UserDto
                         {
                             ProvinceManagement = user.ProvinceManagement,
                             IdChucVuName = DuLieuDanhMuctbl2.Name,
                             IdChucVu = user.IdChucVu,
                             UserName = user.UserName,
                             FullName = user.FullName,
                             Id = user.Id,
                             IdDepartment = user.IdDepartment,
                             DepartmentName = departmentInfo.Name,
                             AccessFailedCount = user.AccessFailedCount,
                             Address = user.Address,
                             Avatar = user.Avatar,
                             BirthDay = user.BirthDay,
                             Email = user.Email,
                             EmailConfirmed = user.EmailConfirmed,
                             Gender = user.Gender,
                             LockoutEnabled = user.LockoutEnabled,
                             LockoutEndDateUtc = user.LockoutEndDateUtc,
                             PhoneNumber = user.PhoneNumber,
                             PhoneNumberConfirmed = user.PhoneNumberConfirmed,
                             TypeAccount = user.TypeAccount,
                             TypeOrganization = user.TypeOrganization,
                             OrganizationId = user.OrganizationId,
                             ListRoles = (from userRole in userRoleDbSet.Where(x => x.UserId == user.Id)
                                          join roletbl in RoleDbSet on userRole.RoleId equals roletbl.Id
                                          select roletbl).ToList()
                         }).FirstOrDefault();

            query.ListActions = _operationService.GetListOperationOfUser(query.Id);

            var deparmentProvince = _departmentRepository.GetAllAsQueryable().Where(x => x.Id == query.IdDepartment).Select(x => x.ProvinceManagement).FirstOrDefault();
            query.ListAllProvince = query.ProvinceManagement;
            if (string.IsNullOrEmpty(query.ListAllProvince))
            {
                query.ListAllProvince = deparmentProvince;
            }
            else if (!string.IsNullOrEmpty(deparmentProvince))
            {
                query.ListAllProvince += "," + deparmentProvince;
            }

            return query;
        }

        /// <summary>
        /// Kiểm tra sự tồn tại của Tài khoản trên hệ thống
        /// </summary>
        /// <returns>
        /// true: Tồn tại
        /// 
        /// </returns>
        public bool CheckExistUserName(string userName, long? id = null)
        {
            return _appUserRepository.GetAll().Where(x => x.UserName != null && x.UserName.Equals(userName) && (id.HasValue ? x.Id != id : true)).Any();
        }



        /// <summary>
        /// Kiểm tra sự tồn tại của Email trên hệ thống
        /// </summary>
        /// <returns>
        /// true: Tồn tại
        /// 
        /// </returns>
        public bool CheckExistEmail(string email, long? id = null)
        {
            return _appUserRepository.GetAll().Where(x => x.Email != null && x.Email.ToLower().Equals(email.ToLower()) && (id.HasValue ? x.Id != id : true)).Any();
        }
        

        public List<long> GetUserIdByOrganizationId(long? id, string OrganizationType)
        {
            var listUserId = _appUserRepository.GetAllAsQueryable().Where(x => x.TypeAccount == AccountTypeConstant.EndUser && OrganizationType == x.TypeOrganization && id == x.OrganizationId).Select(x => x.Id).ToList();
            return listUserId;
        }

      

    }
}
