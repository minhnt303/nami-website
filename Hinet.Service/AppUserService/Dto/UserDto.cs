﻿using Nami.Model.Entities;
using Nami.Model.IdentityEntities;
using Nami.Service.ModuleService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.AppUserService.Dto
{
    public class UserDto : AppUser
    {
        public string DepartmentName { get; set; }
        public List<ModuleMenuDTO> ListActions { get; set; }
        public List<Operation> ListOperations { get; set; }
        public List<Role> ListRoles { get; set; }
        public string IdChucVuName { get; internal set; }
        public string ListAllProvince { get; set; }
        public string OrganizationName { get; set; }
        public List<string> ProvinceManagementNameEndUser { get; set; }
        public List<string> ProvinceManagementNameDepartment { get; set; }
        public bool IsLock { get; set; }
    }
}
