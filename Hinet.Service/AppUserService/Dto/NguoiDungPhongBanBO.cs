﻿using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.AppUserService.Dto
{
    public class NguoiDungPhongBanBO
    {
        public Department PhongBan { get; set; }
        public List<UserDto> LstNguoiDung { get; set; }
    }
}
