using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Nami.Service.AppUserService.Dto
{
    public class AppUserExportDto
    {
		[DisplayName("Giới tính")]
public int Gender { get; set; }
		[DisplayName("Id Chức vụ")]
public int? ChucVuId { get; set; }
		[DisplayName("Số lần truy cập thất bại")]
public int AccessFailedCount { get; set; }
		[DisplayName("Loại màn hình DashBoard")]
public int TypeDashboard { get; set; }
		[DisplayName("Ngày sinh")]
public DateTime? BirthDay { get; set; }
		[DisplayName("Thời gian khóa tải khoản kết thúc")]
public DateTime? LockoutEndDateUtc { get; set; }
		[DisplayName("Thời gian xóa")]
public DateTime? DeleteTime { get; set; }
		[DisplayName("Lần cuối đăng nhập")]
public DateTime? LastLogin { get; set; }
		[DisplayName("Email xác nhận")]
public bool EmailConfirmed { get; set; }
		[DisplayName("Số điện thoại xác nhận")]
public bool PhoneNumberConfirmed { get; set; }
		[DisplayName("TwoFactorEnabled")]
public bool TwoFactorEnabled { get; set; }
		[DisplayName("Khóa tài khoản")]
public bool LockoutEnabled { get; set; }
		[DisplayName("Đã xóa hay chưa")]
public bool? IsDelete { get; set; }
		[DisplayName("Chặn")]
public bool? Block { get; set; }
		[DisplayName("IsUpdateNewPass")]
public bool IsUpdateNewPass { get; set; }
		[DisplayName("IsAllProvine")]
public bool IsAllProvine { get; set; }
		[DisplayName("DeleteId")]
public long? DeleteId { get; set; }
		[DisplayName("OldSysId")]
public long? OldSysId { get; set; }
		[DisplayName("Tên đăng nhập")]
public string UserName { get; set; }
		[DisplayName("Email")]
public string Email { get; set; }
		[DisplayName("Số điện thoại")]
public string PhoneNumber { get; set; }
		[DisplayName("Địa chỉ")]
public string Address { get; set; }
		[DisplayName("Tên")]
public string FullName { get; set; }
		[DisplayName("Avatar")]
public string Avatar { get; set; }
		[DisplayName("Detail")]
public string Detail { get; set; }
		[DisplayName("Mobile")]
public string Mobile { get; set; }
		[DisplayName("Yahoo")]
public string Yahoo { get; set; }
		[DisplayName("Skype")]
public string Skype { get; set; }
		[DisplayName("Facebook")]
public string Facebook { get; set; }
		[DisplayName("TypeAccount")]
public string TypeAccount { get; set; }
		[DisplayName("TypeOrganization")]
public string TypeOrganization { get; set; }
		[DisplayName("IdChucVu")]
public string IdChucVu { get; set; }
		[DisplayName("ProvinceManagement")]
public string ProvinceManagement { get; set; }
		[DisplayName("PasswordHash")]
public string PasswordHash { get; set; }
		[DisplayName("SecurityStamp")]
public string SecurityStamp { get; set; }
		[DisplayName("IdDepartment")]
public long? IdDepartment { get; set; }
		[DisplayName("OrganizationId")]
public long? OrganizationId { get; set; }

    }
}