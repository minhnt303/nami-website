using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Nami.Service.AppUserService.Dto
{
    public class AppUserImportDto
    {
        [Required]
        [DisplayName("Id cục quản lý thị trường")]
        public long? IdDepartment { get; set; }
        [Required]
        [DisplayName("Tên tài khoản")]
        public string UserName { get; set; }
        [Required]
        [DisplayName("Họ tên")]
        public string FullName { get; set; }
        [DisplayName("Id Chức vụ")]
        public string IdChucVu { get; set; }
        [DisplayName("Sđt")]
        public string PhoneNumber { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Địa chỉ")]
        public string Address { get; set; }
        [Required]
        [DisplayName("List Id Role")]
        public string IdRole { get; set; }
        [Required]
        [DisplayName("Tỉnh/Thành phố")]
        public string ProvinceManagement { get; set; }

        public bool IsExist { get; set; }

    }
}