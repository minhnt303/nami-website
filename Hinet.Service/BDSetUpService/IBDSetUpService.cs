using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.BDSetUpService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BDSetUpService
{
    public interface IBDSetUpService:IEntityService<BDSetUp>
    {
        PageListResultBO<BDSetUpDto> GetDaTaByPage(BDSetUpSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        BDSetUp GetById(long id);
    }
}
