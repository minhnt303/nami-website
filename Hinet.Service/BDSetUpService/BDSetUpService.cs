using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.BDSetUpRepository;
using Nami.Service.BDSetUpService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;




namespace Nami.Service.BDSetUpService
{
    public class BDSetUpService : EntityService<BDSetUp>, IBDSetUpService
    {
        IUnitOfWork _unitOfWork;
        IBDSetUpRepository _BDSetUpRepository;
	ILog _loger;
        IMapper _mapper;


        
        public BDSetUpService(IUnitOfWork unitOfWork, 
		IBDSetUpRepository BDSetUpRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, BDSetUpRepository)
        {
            _unitOfWork = unitOfWork;
            _BDSetUpRepository = BDSetUpRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<BDSetUpDto> GetDaTaByPage(BDSetUpSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from BDSetUptbl in _BDSetUpRepository.GetAllAsQueryable()

                        select new BDSetUpDto
                        {
							IdBieuDo = BDSetUptbl.IdBieuDo,
							CategoryFeild = BDSetUptbl.CategoryFeild,
							StartDuration = BDSetUptbl.StartDuration,
							Rotate = BDSetUptbl.Rotate,
							TextColor = BDSetUptbl.TextColor,
							Theme = BDSetUptbl.Theme,
							FontSize = BDSetUptbl.FontSize,
							FontFamily = BDSetUptbl.FontFamily,
							IsDelete = BDSetUptbl.IsDelete,
							DeleteTime = BDSetUptbl.DeleteTime,
							DeleteId = BDSetUptbl.DeleteId,
							Id = BDSetUptbl.Id,
							CreatedDate = BDSetUptbl.CreatedDate,
							CreatedBy = BDSetUptbl.CreatedBy,
							CreatedID = BDSetUptbl.CreatedID,
							UpdatedDate = BDSetUptbl.UpdatedDate,
							UpdatedBy = BDSetUptbl.UpdatedBy,
							UpdatedID = BDSetUptbl.UpdatedID
                            
                        };

            if (searchModel != null)
            {
		if (searchModel.IdBieuDoFilter!=null)
		{
			query = query.Where(x => x.IdBieuDo==searchModel.IdBieuDoFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.CategoryFeildFilter))
		{
			query = query.Where(x => x.CategoryFeild.Contains(searchModel.CategoryFeildFilter));
		}
		if (searchModel.StartDurationFilter!=null)
		{
			query = query.Where(x => x.StartDuration==searchModel.StartDurationFilter);
		}
		if (searchModel.RotateFilter!=null)
		{
			query = query.Where(x => x.Rotate==searchModel.RotateFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.TextColorFilter))
		{
			query = query.Where(x => x.TextColor.Contains(searchModel.TextColorFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.ThemeFilter))
		{
			query = query.Where(x => x.Theme.Contains(searchModel.ThemeFilter));
		}
		if (searchModel.FontSizeFilter!=null)
		{
			query = query.Where(x => x.FontSize==searchModel.FontSizeFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.FontFamilyFilter))
		{
			query = query.Where(x => x.FontFamily.Contains(searchModel.FontFamilyFilter));
		}
		if (searchModel.IsDeleteFilter!=null)
		{
			query = query.Where(x => x.IsDelete==searchModel.IsDeleteFilter);
		}
		if (searchModel.DeleteTimeFilter!=null)
		{
			query = query.Where(x => x.DeleteTime==searchModel.DeleteTimeFilter);
		}
		if (searchModel.DeleteIdFilter!=null)
		{
			query = query.Where(x => x.DeleteId==searchModel.DeleteIdFilter);
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<BDSetUpDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public BDSetUp GetById(long id)
        {
            return _BDSetUpRepository.GetById(id);
        }
    

    }
}
