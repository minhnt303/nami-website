using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BDSetUpService.Dto
{
    public class BDSetUpSearchDto : SearchBase
    {
		public long? IdBieuDoFilter { get; set; }
		public string CategoryFeildFilter { get; set; }
		public int? StartDurationFilter { get; set; }
		public bool RotateFilter { get; set; }
		public string TextColorFilter { get; set; }
		public string ThemeFilter { get; set; }
		public int FontSizeFilter { get; set; }
		public string FontFamilyFilter { get; set; }
		public bool? IsDeleteFilter { get; set; }
		public DateTime? DeleteTimeFilter { get; set; }
		public long? DeleteIdFilter { get; set; }


    }
}