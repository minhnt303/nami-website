using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Nami.Service.BDSetUpService.Dto
{
    public class BDSetUpImportDto
    {
		[DisplayName("")]
public long? IdBieuDo { get; set; }
		[DisplayName("")]
public string CategoryFeild { get; set; }
		[DisplayName("")]
public int? StartDuration { get; set; }
		[Required]
[DisplayName("")]
public bool Rotate { get; set; }
		[DisplayName("")]
public string TextColor { get; set; }
		[DisplayName("")]
public string Theme { get; set; }
		[Required]
[DisplayName("")]
public int FontSize { get; set; }
		[DisplayName("")]
public string FontFamily { get; set; }
		[DisplayName("")]
public bool? IsDelete { get; set; }
		[DisplayName("")]
public DateTime? DeleteTime { get; set; }
		[DisplayName("")]
public long? DeleteId { get; set; }

    }
}