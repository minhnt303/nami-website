using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.UserOperationService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.UserOperationService
{
    public interface IUserOperationService:IEntityService<UserOperation>
    {
        PageListResultBO<UserOperationDto> GetDaTaByPage(UserOperationSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        UserOperation GetById(long id);
        List<ModuleService.DTO.ModuleDTO> GetConfigureOperation(long userID);
    }
}
