using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.CongThucRepository;
using Nami.Service.CongThucService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;



namespace Nami.Service.CongThucService
{
    public class CongThucService : EntityService<CongThuc>, ICongThucService
    {
        IUnitOfWork _unitOfWork;
        ICongThucRepository _CongThucRepository;
	ILog _loger;
        IMapper _mapper;

        
        public CongThucService(IUnitOfWork unitOfWork, 
		ICongThucRepository CongThucRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, CongThucRepository)
        {
            _unitOfWork = unitOfWork;
            _CongThucRepository = CongThucRepository;
            _loger = loger;
            _mapper = mapper;

        }

        public PageListResultBO<CongThucDto> GetDaTaByPage(CongThucSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from CongThuctbl in _CongThucRepository.GetAllAsQueryable()

                        select new CongThucDto
                        {
							Id = CongThuctbl.Id,
							CongThucName = CongThuctbl.CongThucName,
							DichVuCode = CongThuctbl.DichVuCode,
							Description = CongThuctbl.Description,
							CreatedDate = CongThuctbl.CreatedDate,
							CreatedBy = CongThuctbl.CreatedBy,
							UpdatedDate = CongThuctbl.UpdatedDate,
							UpdatedBy = CongThuctbl.UpdatedBy
                            
                        };

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<CongThucDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public CongThuc GetById(long id)
        {
            return _CongThucRepository.GetById(id);
        }
    

    }
}
