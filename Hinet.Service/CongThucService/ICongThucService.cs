using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.CongThucService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.CongThucService
{
    public interface ICongThucService:IEntityService<CongThuc>
    {
        PageListResultBO<CongThucDto> GetDaTaByPage(CongThucSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        CongThuc GetById(long id);
    }
}
