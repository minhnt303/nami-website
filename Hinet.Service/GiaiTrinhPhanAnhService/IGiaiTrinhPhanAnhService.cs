using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.GiaiTrinhPhanAnhService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.GiaiTrinhPhanAnhService
{
    public interface IGiaiTrinhPhanAnhService:IEntityService<GiaiTrinhPhanAnh>
    {
        PageListResultBO<GiaiTrinhPhanAnhDto> GetDaTaByPage(GiaiTrinhPhanAnhSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        GiaiTrinhPhanAnh GetById(long id);
        List<GiaiTrinhPhanAnhDto> GetListGiaTrinhByPhanAnhIdAndPhanAnhType(long? PhanAnhId, string PhanAnhType);
        int CountGiaiTrinhByUserId(long id);
    }
}
