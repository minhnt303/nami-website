using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.GiaiTrinhPhanAnhRepository;
using Nami.Service.GiaiTrinhPhanAnhService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Repository.GiaiTrinhPhanAnhFileAttackRepository;
using Nami.Repository.AppUserRepository;
using Nami.Service.AppUserService;

namespace Nami.Service.GiaiTrinhPhanAnhService
{
    public class GiaiTrinhPhanAnhService : EntityService<GiaiTrinhPhanAnh>, IGiaiTrinhPhanAnhService
    {
        IUnitOfWork _unitOfWork;
        IGiaiTrinhPhanAnhRepository _GiaiTrinhPhanAnhRepository;
        IGiaiTrinhPhanAnhFileAttackRepository _giaiTrinhPhanAnhFileAttackRepository;
        ILog _loger;
        IMapper _mapper;
        IAppUserRepository _appUserRepository;
        IAppUserService _appUserService;

        public GiaiTrinhPhanAnhService(IUnitOfWork unitOfWork,
        IGiaiTrinhPhanAnhRepository GiaiTrinhPhanAnhRepository,
        IGiaiTrinhPhanAnhFileAttackRepository giaiTrinhPhanAnhFileAttackRepository,
        IAppUserRepository appUserRepository,
        IAppUserService appUserService, ILog loger,
                IMapper mapper
            )
            : base(unitOfWork, GiaiTrinhPhanAnhRepository)
        {
            _appUserService = appUserService;
            _unitOfWork = unitOfWork;
            _GiaiTrinhPhanAnhRepository = GiaiTrinhPhanAnhRepository;
            _giaiTrinhPhanAnhFileAttackRepository = giaiTrinhPhanAnhFileAttackRepository;
            _loger = loger;
            _mapper = mapper;
            _appUserRepository = appUserRepository;

        }

        public PageListResultBO<GiaiTrinhPhanAnhDto> GetDaTaByPage(GiaiTrinhPhanAnhSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from GiaiTrinhPhanAnhtbl in _GiaiTrinhPhanAnhRepository.GetAllAsQueryable()

                        select new GiaiTrinhPhanAnhDto
                        {
                            NgayGiaiTrinh = GiaiTrinhPhanAnhtbl.NgayGiaiTrinh,
                            CreatedDate = GiaiTrinhPhanAnhtbl.CreatedDate,
                            UpdatedDate = GiaiTrinhPhanAnhtbl.UpdatedDate,
                            IsDelete = GiaiTrinhPhanAnhtbl.IsDelete,
                            Id = GiaiTrinhPhanAnhtbl.Id,
                            PhanAnhId = GiaiTrinhPhanAnhtbl.PhanAnhId,
                            IdHoSoGiaiTrinh = GiaiTrinhPhanAnhtbl.IdHoSoGiaiTrinh,
                            GiaiTrinhComment = GiaiTrinhPhanAnhtbl.GiaiTrinhComment,
                            PhanAnhType = GiaiTrinhPhanAnhtbl.PhanAnhType,
                            TypeHoSoGiaiTrinh = GiaiTrinhPhanAnhtbl.TypeHoSoGiaiTrinh,
                            CreatedBy = GiaiTrinhPhanAnhtbl.CreatedBy,
                            UpdatedBy = GiaiTrinhPhanAnhtbl.UpdatedBy

                        };

            if (searchModel != null)
            {
                if (searchModel.NgayGiaiTrinhFilter != null)
                {
                    query = query.Where(x => x.NgayGiaiTrinh == searchModel.NgayGiaiTrinhFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.PhanAnhTypeFilter))
                {
                    query = query.Where(x => x.PhanAnhType.Contains(searchModel.PhanAnhTypeFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.TypeHoSoGiaiTrinhFilter))
                {
                    query = query.Where(x => x.TypeHoSoGiaiTrinh.Contains(searchModel.TypeHoSoGiaiTrinhFilter));
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<GiaiTrinhPhanAnhDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public GiaiTrinhPhanAnh GetById(long id)
        {
            return _GiaiTrinhPhanAnhRepository.GetById(id);
        }

        public List<GiaiTrinhPhanAnhDto> GetListGiaTrinhByPhanAnhIdAndPhanAnhType(long? PhanAnhId, string PhanAnhType)
        {
            var query = (from GiaiTrinhPhanAnhtbl in _GiaiTrinhPhanAnhRepository.GetAllAsQueryable().Where(x => x.PhanAnhId == PhanAnhId & x.PhanAnhType == PhanAnhType)

                         join AppUsertbl in _appUserRepository.GetAllAsQueryable()
                         on GiaiTrinhPhanAnhtbl.IdHoSoGiaiTrinh equals AppUsertbl.Id into AppUsertable
                         from AppUsertbl2 in AppUsertable.DefaultIfEmpty()

                         select new GiaiTrinhPhanAnhDto
                         {
                             NgayGiaiTrinh = GiaiTrinhPhanAnhtbl.NgayGiaiTrinh,
                             CreatedDate = GiaiTrinhPhanAnhtbl.CreatedDate,
                             UpdatedDate = GiaiTrinhPhanAnhtbl.UpdatedDate,
                             IsDelete = GiaiTrinhPhanAnhtbl.IsDelete,
                             Id = GiaiTrinhPhanAnhtbl.Id,
                             PhanAnhId = GiaiTrinhPhanAnhtbl.PhanAnhId,
                             LoaiGiaiTrinh = GiaiTrinhPhanAnhtbl.LoaiGiaiTrinh,
                             IdHoSoGiaiTrinh = GiaiTrinhPhanAnhtbl.IdHoSoGiaiTrinh,
                             GiaiTrinhComment = GiaiTrinhPhanAnhtbl.GiaiTrinhComment,
                             PhanAnhType = GiaiTrinhPhanAnhtbl.PhanAnhType,
                             TypeHoSoGiaiTrinh = GiaiTrinhPhanAnhtbl.TypeHoSoGiaiTrinh,
                             CreatedBy = GiaiTrinhPhanAnhtbl.CreatedBy,
                             UpdatedBy = GiaiTrinhPhanAnhtbl.UpdatedBy,
                             NameHoSoGiaiTrinh = AppUsertbl2.FullName,
                             CreatedID = GiaiTrinhPhanAnhtbl.CreatedID
                         }).OrderByDescending(x => x.Id).ToList();
            foreach (var item in query)
            {
                item.ListFileAttackPhanAnh = _giaiTrinhPhanAnhFileAttackRepository.GetAllAsQueryable().Where(x => x.IdGiaiTrinhPhanAnh == item.Id).ToList();
                item.UserCreateInfo = _appUserRepository.GetAllAsQueryable().Where(x => x.Id == item.CreatedID).FirstOrDefault();
            }
            return query;
        }

        public int CountGiaiTrinhByUserId(long id)
        {
            int result = 0;

            //var objUser = _appUserService.GetShortById(id);
            //var listPhanAnh = _phanAnhInfoService.getListIdByOrganization(objUser.OrganizationId, objUser.TypeOrganization);
            //foreach (var item in listPhanAnh)
            //{
            //    var count = (_GiaiTrinhPhanAnhRepository.GetAllAsQueryable().Where(x => x.PhanAnhId == item.Id && x.LoaiGiaiTrinh == giaitrinh)).Count();
            //    result += count;
            //}

            return result;
        }
    }
}
