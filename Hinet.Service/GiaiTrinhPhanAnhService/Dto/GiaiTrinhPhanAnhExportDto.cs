using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Nami.Service.GiaiTrinhPhanAnhService.Dto
{
    public class GiaiTrinhPhanAnhExportDto
    {
		[DisplayName("")]
public string LoaiGiaiTrinh { get; set; }
		[DisplayName("")]
public string GiaiTrinhComment { get; set; }
		[DisplayName("")]
public string PhanAnhType { get; set; }
		[DisplayName("")]
public long? PhanAnhId { get; set; }
		[DisplayName("")]
public long? IdHoSoGiaiTrinh { get; set; }
		[DisplayName("")]
public string TypeHoSoGiaiTrinh { get; set; }
		[DisplayName("")]
public DateTime? NgayGiaiTrinh { get; set; }
		[DisplayName("")]
public bool? IsDelete { get; set; }
		[DisplayName("")]
public DateTime? DeleteTime { get; set; }
		[DisplayName("")]
public long? DeleteId { get; set; }

    }
}