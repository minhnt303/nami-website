using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.GiaiTrinhPhanAnhService.Dto
{
    public class GiaiTrinhPhanAnhSearchDto : SearchBase
    {
		public string LoaiGiaiTrinhFilter { get; set; }
		public string GiaiTrinhCommentFilter { get; set; }
		public string PhanAnhTypeFilter { get; set; }
		public long? PhanAnhIdFilter { get; set; }
		public long? IdHoSoGiaiTrinhFilter { get; set; }
		public string TypeHoSoGiaiTrinhFilter { get; set; }
		public DateTime? NgayGiaiTrinhFilter { get; set; }
		public bool? IsDeleteFilter { get; set; }
		public DateTime? DeleteTimeFilter { get; set; }
		public long? DeleteIdFilter { get; set; }


    }
}