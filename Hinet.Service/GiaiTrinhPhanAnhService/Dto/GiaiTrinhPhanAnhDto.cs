using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.GiaiTrinhPhanAnhService.Dto
{
    public class GiaiTrinhPhanAnhDto : GiaiTrinhPhanAnh
    {
        public string NameHoSoGiaiTrinh { get; set; }
        public List<GiaiTrinhPhanAnhFileAttack> ListFileAttackPhanAnh { get; set; }
        public AppUser UserCreateInfo { get; set; }
    }
}