using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.CauHinhCongThucRepository;
using Nami.Service.CauHinhCongThucService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;



namespace Nami.Service.CauHinhCongThucService
{
    public class CauHinhCongThucService : EntityService<CauHinhCongThuc>, ICauHinhCongThucService
    {
        IUnitOfWork _unitOfWork;
        ICauHinhCongThucRepository _CauHinhCongThucRepository;
	ILog _loger;
        IMapper _mapper;

        
        public CauHinhCongThucService(IUnitOfWork unitOfWork, 
		ICauHinhCongThucRepository CauHinhCongThucRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, CauHinhCongThucRepository)
        {
            _unitOfWork = unitOfWork;
            _CauHinhCongThucRepository = CauHinhCongThucRepository;
            _loger = loger;
            _mapper = mapper;

        }

        public PageListResultBO<CauHinhCongThucDto> GetDaTaByPage(CauHinhCongThucSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from CauHinhCongThuctbl in _CauHinhCongThucRepository.GetAllAsQueryable()

                        select new CauHinhCongThucDto
                        {
							Id = CauHinhCongThuctbl.Id,
							CongThucId = CauHinhCongThuctbl.CongThucId,
							DinhMucName = CauHinhCongThuctbl.DinhMucName,
							SoDau = CauHinhCongThuctbl.SoDau,
							SoCuoi = CauHinhCongThuctbl.SoCuoi,
							Price = CauHinhCongThuctbl.Price,
							Description = CauHinhCongThuctbl.Description,
							CreatedDate = CauHinhCongThuctbl.CreatedDate,
							CreatedBy = CauHinhCongThuctbl.CreatedBy,
							UpdatedDate = CauHinhCongThuctbl.UpdatedDate,
							UpdatedBy = CauHinhCongThuctbl.UpdatedBy
                            
                        };

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<CauHinhCongThucDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public CauHinhCongThuc GetById(long id)
        {
            return _CauHinhCongThucRepository.GetById(id);
        }
    

    }
}
