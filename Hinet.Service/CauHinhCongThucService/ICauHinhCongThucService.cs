using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.CauHinhCongThucService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.CauHinhCongThucService
{
    public interface ICauHinhCongThucService:IEntityService<CauHinhCongThuc>
    {
        PageListResultBO<CauHinhCongThucDto> GetDaTaByPage(CauHinhCongThucSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        CauHinhCongThuc GetById(long id);
    }
}
