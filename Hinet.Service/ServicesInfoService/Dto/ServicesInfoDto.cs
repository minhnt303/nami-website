using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.ServicesInfoService.Dto
{
    public class ServicesInfoDto : ServicesInfo
    {
        public string UnitsCodeName { get; set; }
    }
}