using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.ServicesInfoRepository;
using Nami.Service.ServicesInfoService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Repository.DanhmucRepository;
using Nami.Service.Constant;


namespace Nami.Service.ServicesInfoService
{
    public class ServicesInfoService : EntityService<ServicesInfo>, IServicesInfoService
    {
        IUnitOfWork _unitOfWork;
        IServicesInfoRepository _ServicesInfoRepository;
	ILog _loger;
        IDM_DulieuDanhmucRepository _dM_DulieuDanhmucRepository;
        IDM_NhomDanhmucRepository _nhomDanhmucRepository;
        IMapper _mapper;


        public ServicesInfoService(IUnitOfWork unitOfWork,
        IServicesInfoRepository ServicesInfoRepository,
        ILog loger,
        IDM_DulieuDanhmucRepository dM_DulieuDanhmucRepository,
        IDM_NhomDanhmucRepository nhomDanhmucRepository,
            	IMapper mapper	
            )
            : base(unitOfWork, ServicesInfoRepository)
        {
            _unitOfWork = unitOfWork;
            _ServicesInfoRepository = ServicesInfoRepository;
            _dM_DulieuDanhmucRepository = dM_DulieuDanhmucRepository;
            _nhomDanhmucRepository = nhomDanhmucRepository;
            _loger = loger;
            _mapper = mapper;

        }

        public PageListResultBO<ServicesInfoDto> GetDaTaByPage(ServicesInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var unitsGroupCodeId = _nhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.DON_VI).Select(x => x.Id).FirstOrDefault();
            var query = from ServicesInfotbl in _ServicesInfoRepository.GetAllAsQueryable()

                        select new ServicesInfoDto
                        {
							Id = ServicesInfotbl.Id,
							ServicesName = ServicesInfotbl.ServicesName,
							Price = ServicesInfotbl.Price,
							UnitsCode = ServicesInfotbl.UnitsCode,
							Description = ServicesInfotbl.Description,
							CreatedDate = ServicesInfotbl.CreatedDate,
							CreatedBy = ServicesInfotbl.CreatedBy,
							UpdatedDate = ServicesInfotbl.UpdatedDate,
							UpdatedBy = ServicesInfotbl.UpdatedBy
                            
                        };

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<ServicesInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            foreach (var item in resultmodel.ListItem)
            {
                item.UnitsCodeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == unitsGroupCodeId & x.Code == item.UnitsCode).Select(x => x.Name).FirstOrDefault();
            }
            return resultmodel;
        }

        public ServicesInfo GetById(long id)
        {
            return _ServicesInfoRepository.GetById(id);
        }
    

    }
}
