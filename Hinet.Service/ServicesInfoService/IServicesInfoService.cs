using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.ServicesInfoService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.ServicesInfoService
{
    public interface IServicesInfoService:IEntityService<ServicesInfo>
    {
        PageListResultBO<ServicesInfoDto> GetDaTaByPage(ServicesInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        ServicesInfo GetById(long id);
    }
}
