using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.BannerQuangCaoService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BannerQuangCaoService
{
    public interface IBannerQuangCaoService:IEntityService<BannerQuangCao>
    {
        PageListResultBO<BannerQuangCaoDto> GetDaTaByPage(BannerQuangCaoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        BannerQuangCao GetById(long id);
        BannerQuangCaoDto GetDtoById(long id);
        List<BannerQuangCaoDto> GetListBannerQuangCaoIsActive();
    }
}
