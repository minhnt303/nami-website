using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.BannerQuangCaoRepository;
using Nami.Service.BannerQuangCaoService.Dto;
using Nami.Service.Common;
using Nami.Service.Constant;
using log4net;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;



namespace Nami.Service.BannerQuangCaoService
{
    public class BannerQuangCaoService : EntityService<BannerQuangCao>, IBannerQuangCaoService
    {
        IUnitOfWork _unitOfWork;
        IBannerQuangCaoRepository _BannerQuangCaoRepository;
        ILog _loger;
        IMapper _mapper;



        public BannerQuangCaoService(IUnitOfWork unitOfWork,
        IBannerQuangCaoRepository BannerQuangCaoRepository,
        ILog loger,

                IMapper mapper
            )
            : base(unitOfWork, BannerQuangCaoRepository)
        {
            _unitOfWork = unitOfWork;
            _BannerQuangCaoRepository = BannerQuangCaoRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<BannerQuangCaoDto> GetDaTaByPage(BannerQuangCaoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from BannerQuangCaotbl in _BannerQuangCaoRepository.GetAllAsQueryable()

                        select new BannerQuangCaoDto
                        {
                            LinkUrl = BannerQuangCaotbl.LinkUrl,
                            ImageLink = BannerQuangCaotbl.ImageLink,
                            Status = BannerQuangCaotbl.Status,
                            Name = BannerQuangCaotbl.Name,
                            Description = BannerQuangCaotbl.Description,
                            ViTriHienThi = BannerQuangCaotbl.ViTriHienThi,
                            IsDelete = BannerQuangCaotbl.IsDelete,
                            DeleteTime = BannerQuangCaotbl.DeleteTime,
                            DeleteId = BannerQuangCaotbl.DeleteId,
                            Id = BannerQuangCaotbl.Id,
                            CreatedDate = BannerQuangCaotbl.CreatedDate,
                            CreatedBy = BannerQuangCaotbl.CreatedBy,
                            CreatedID = BannerQuangCaotbl.CreatedID,
                            UpdatedDate = BannerQuangCaotbl.UpdatedDate,
                            UpdatedBy = BannerQuangCaotbl.UpdatedBy,
                            UpdatedID = BannerQuangCaotbl.UpdatedID
                        };

            if (searchModel != null)
            {
                if (searchModel.StatusFilter != null)
                {
                    query = query.Where(x => x.Status == searchModel.StatusFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.NameFilter))
                {
                    query = query.Where(x => x.Name.Contains(searchModel.NameFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.DescriptionFilter))
                {
                    query = query.Where(x => x.Description.Contains(searchModel.DescriptionFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.ViTriHienThiFilter))
                {
                    query = query.Where(x => x.ViTriHienThi.Contains(searchModel.ViTriHienThiFilter));
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<BannerQuangCaoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            foreach (var item in resultmodel.ListItem)
            {
                if (!string.IsNullOrEmpty(item.ViTriHienThi))
                {
                    var ViTriHienThiName = ConstantExtension.GetName<ViTriHienThiQuangCaoConstant>(item.ViTriHienThi);
                    if (!string.IsNullOrEmpty(ViTriHienThiName))
                    {
                        item.ViTriHienThiName = ViTriHienThiName;
                    }
                    var ViTriHienThiBGColor = ConstantExtension.GetBackgroundColor<ViTriHienThiQuangCaoConstant>(item.ViTriHienThi);
                    if (!string.IsNullOrEmpty(ViTriHienThiBGColor))
                    {
                        item.ViTriHienThiBGColor = ViTriHienThiBGColor;
                    }
                }
            }
            return resultmodel;
        }

        public BannerQuangCao GetById(long id)
        {
            return _BannerQuangCaoRepository.GetById(id);
        }


        public BannerQuangCaoDto GetDtoById(long id)
        {
            var item = (from BannerQuangCaotbl in _BannerQuangCaoRepository.GetAllAsQueryable().Where(x => x.Id == id)

                        select new BannerQuangCaoDto
                        {
                            LinkUrl = BannerQuangCaotbl.LinkUrl,
                            ImageLink = BannerQuangCaotbl.ImageLink,
                            Status = BannerQuangCaotbl.Status,
                            Name = BannerQuangCaotbl.Name,
                            Description = BannerQuangCaotbl.Description,
                            ViTriHienThi = BannerQuangCaotbl.ViTriHienThi,
                            IsDelete = BannerQuangCaotbl.IsDelete,
                            DeleteTime = BannerQuangCaotbl.DeleteTime,
                            DeleteId = BannerQuangCaotbl.DeleteId,
                            Id = BannerQuangCaotbl.Id,
                            CreatedDate = BannerQuangCaotbl.CreatedDate,
                            CreatedBy = BannerQuangCaotbl.CreatedBy,
                            CreatedID = BannerQuangCaotbl.CreatedID,
                            UpdatedDate = BannerQuangCaotbl.UpdatedDate,
                            UpdatedBy = BannerQuangCaotbl.UpdatedBy,
                            UpdatedID = BannerQuangCaotbl.UpdatedID
                        }).FirstOrDefault();
            if (!string.IsNullOrEmpty(item.ViTriHienThi))
            {
                var ViTriHienThiName = ConstantExtension.GetName<ViTriHienThiQuangCaoConstant>(item.ViTriHienThi);
                if (!string.IsNullOrEmpty(ViTriHienThiName))
                {
                    item.ViTriHienThiName = ViTriHienThiName;
                }
                var ViTriHienThiBGColor = ConstantExtension.GetBackgroundColor<ViTriHienThiQuangCaoConstant>(item.ViTriHienThi);
                if (!string.IsNullOrEmpty(ViTriHienThiBGColor))
                {
                    item.ViTriHienThiBGColor = ViTriHienThiBGColor;
                }
            }
            return item;
        }

        public List<BannerQuangCaoDto> GetListBannerQuangCaoIsActive() {
            var query = (from BannerQuangCaotbl in _BannerQuangCaoRepository.GetAllAsQueryable().Where(x=>x.Status == true)
                        select new BannerQuangCaoDto
                        {
                            LinkUrl = BannerQuangCaotbl.LinkUrl,
                            ImageLink = BannerQuangCaotbl.ImageLink,
                            Status = BannerQuangCaotbl.Status,
                            Name = BannerQuangCaotbl.Name,
                            Description = BannerQuangCaotbl.Description,
                            ViTriHienThi = BannerQuangCaotbl.ViTriHienThi,
                            IsDelete = BannerQuangCaotbl.IsDelete,
                            DeleteTime = BannerQuangCaotbl.DeleteTime,
                            DeleteId = BannerQuangCaotbl.DeleteId,
                            Id = BannerQuangCaotbl.Id,
                            CreatedDate = BannerQuangCaotbl.CreatedDate,
                            CreatedBy = BannerQuangCaotbl.CreatedBy,
                            CreatedID = BannerQuangCaotbl.CreatedID,
                            UpdatedDate = BannerQuangCaotbl.UpdatedDate,
                            UpdatedBy = BannerQuangCaotbl.UpdatedBy,
                            UpdatedID = BannerQuangCaotbl.UpdatedID
                        }).ToList();

            foreach (var item in query)
            {
                if (!string.IsNullOrEmpty(item.ViTriHienThi))
                {
                    var ViTriHienThiName = ConstantExtension.GetName<ViTriHienThiQuangCaoConstant>(item.ViTriHienThi);
                    if (!string.IsNullOrEmpty(ViTriHienThiName))
                    {
                        item.ViTriHienThiName = ViTriHienThiName;
                    }
                    var ViTriHienThiBGColor = ConstantExtension.GetBackgroundColor<ViTriHienThiQuangCaoConstant>(item.ViTriHienThi);
                    if (!string.IsNullOrEmpty(ViTriHienThiBGColor))
                    {
                        item.ViTriHienThiBGColor = ViTriHienThiBGColor;
                    }
                }
            }
            return query;
        }
    }
}
