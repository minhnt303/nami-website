using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BannerQuangCaoService.Dto
{
    public class BannerQuangCaoSearchDto : SearchBase
    {
		public bool StatusFilter { get; set; }
		public string NameFilter { get; set; }
		public string DescriptionFilter { get; set; }
		public string ViTriHienThiFilter { get; set; }


    }
}