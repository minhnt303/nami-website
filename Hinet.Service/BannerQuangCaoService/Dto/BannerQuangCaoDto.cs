using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BannerQuangCaoService.Dto
{
    public class BannerQuangCaoDto : BannerQuangCao
    {
        public string ViTriHienThiName { get; set; }
        public string ViTriHienThiBGColor { get; set; }
    }
}