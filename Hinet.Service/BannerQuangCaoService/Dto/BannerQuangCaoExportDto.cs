using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Nami.Service.BannerQuangCaoService.Dto
{
    public class BannerQuangCaoExportDto
    {
		[DisplayName("Đường dẫn")]
public string LinkUrl { get; set; }
		[DisplayName("Đường dẫn ảnh")]
public string ImageLink { get; set; }
		[DisplayName("Trạng thái")]
public bool Status { get; set; }
		[DisplayName("Tên ")]
public string Name { get; set; }
		[DisplayName("Mô tả")]
public string Description { get; set; }
		[DisplayName("Vị trí hiển thị")]
public string ViTriHienThi { get; set; }

    }
}