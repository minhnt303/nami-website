using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.FileDinhKemService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.FileDinhKemService
{
    public interface IFileDinhKemService:IEntityService<FileDinhKem>
    {
        PageListResultBO<FileDinhKemDto> GetDaTaByPage(FileDinhKemSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        FileDinhKem GetById(long id);
        List<FileDinhKem> GetListImageFile(long IdItem, string TypeItem);
    }
}
