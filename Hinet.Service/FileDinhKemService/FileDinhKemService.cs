using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.FileDinhKemRepository;
using Nami.Service.Common;
using Nami.Service.FileDinhKemService.Dto;
using log4net;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;



namespace Nami.Service.FileDinhKemService
{
    public class FileDinhKemService : EntityService<FileDinhKem>, IFileDinhKemService
    {
        IUnitOfWork _unitOfWork;
        IFileDinhKemRepository _FileDinhKemRepository;
        ILog _loger;
        IMapper _mapper;


        public FileDinhKemService(IUnitOfWork unitOfWork,
        IFileDinhKemRepository FileDinhKemRepository,
        ILog loger,

                IMapper mapper
            )
            : base(unitOfWork, FileDinhKemRepository)
        {
            _unitOfWork = unitOfWork;
            _FileDinhKemRepository = FileDinhKemRepository;
            _loger = loger;
            _mapper = mapper;

        }

        public PageListResultBO<FileDinhKemDto> GetDaTaByPage(FileDinhKemSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from FileDinhKemtbl in _FileDinhKemRepository.GetAllAsQueryable()

                        select new FileDinhKemDto
                        {
                            Id = FileDinhKemtbl.Id,
                            IdItem = FileDinhKemtbl.IdItem,
                            TypeItem = FileDinhKemtbl.TypeItem,
                            ImageUrl = FileDinhKemtbl.ImageUrl,
                            CreatedDate = FileDinhKemtbl.CreatedDate,
                            CreatedBy = FileDinhKemtbl.CreatedBy,
                            UpdatedDate = FileDinhKemtbl.UpdatedDate,
                            UpdatedBy = FileDinhKemtbl.UpdatedBy

                        };

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<FileDinhKemDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public FileDinhKem GetById(long id)
        {
            return _FileDinhKemRepository.GetById(id);
        }

        public List<FileDinhKem> GetListImageFile(long IdItem, string TypeItem)
        {
            return _FileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == IdItem && x.TypeItem == TypeItem).OrderByDescending(x=>x.Id).ToList();
        }
    }
}
