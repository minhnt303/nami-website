using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.HUYENService.Dto
{
    public class HUYENSearchDto : SearchBase
    {
		public string MaHuyenFilter { get; set; }
		public string TenHuyenFilter { get; set; }
		public string LoaiFilter { get; set; }


    }
}