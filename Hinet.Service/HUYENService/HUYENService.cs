using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.HUYENRepository;
using Nami.Service.HUYENService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using System.Web.Mvc;

namespace Nami.Service.HUYENService
{
    public class HUYENService : EntityService<HUYEN>, IHUYENService
    {
        IUnitOfWork _unitOfWork;
        IHUYENRepository _HUYENRepository;
        ILog _loger;
        IMapper _mapper;


        public HUYENService(IUnitOfWork unitOfWork,
        IHUYENRepository HUYENRepository,
        ILog loger,

                IMapper mapper
            )
            : base(unitOfWork, HUYENRepository)
        {
            _unitOfWork = unitOfWork;
            _HUYENRepository = HUYENRepository;
            _loger = loger;
            _mapper = mapper;

        }

        public List<SelectListItem> GetDropdownOfTinh(string id, string valueSelected)
        {
            var model = _HUYENRepository.GetAllAsQueryable().Where(x => x.TinhId == id)
                .Select(x => new SelectListItem()
                {
                    Text = x.TenHuyen,
                    Value = x.MaHuyen.ToString(),
                    Selected = x.MaHuyen == valueSelected
                })
                .ToList();
            return model;
        }

        public PageListResultBO<HUYENDto> GetDaTaByPage(string tinhId, HUYENSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from HUYENtbl in _HUYENRepository.GetAllAsQueryable().Where(x => x.TinhId == tinhId)

                        select new HUYENDto
                        {
                            MaHuyen = HUYENtbl.MaHuyen,
                            TenHuyen = HUYENtbl.TenHuyen,
                            Loai = HUYENtbl.Loai,
                            Location = HUYENtbl.Location,
                            TinhId = HUYENtbl.TinhId,
                            Id = HUYENtbl.Id

                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.MaHuyenFilter))
                {
                    query = query.Where(x => x.MaHuyen.Contains(searchModel.MaHuyenFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.TenHuyenFilter))
                {
                    query = query.Where(x => x.TenHuyen.Contains(searchModel.TenHuyenFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.LoaiFilter))
                {
                    query = query.Where(x => x.Loai.Contains(searchModel.LoaiFilter));
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<HUYENDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public HUYEN GetById(long id)
        {
            return _HUYENRepository.GetById(id);
        }

        public List<HUYEN> GetDaTaAllHuyen()
        {
            var query = (from HUYENtbl in _HUYENRepository.GetAllAsQueryable()

                        select HUYENtbl).ToList();
            return query;
        }
    }
}
