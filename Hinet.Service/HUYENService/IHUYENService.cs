using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.HUYENService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nami.Service.HUYENService
{
    public interface IHUYENService : IEntityService<HUYEN>
    {
        PageListResultBO<HUYENDto> GetDaTaByPage(string tinhId, HUYENSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        List<HUYEN> GetDaTaAllHuyen();
        List<SelectListItem> GetDropdownOfTinh(string id, string valueSelected);
        HUYEN GetById(long id);
    }
}
