using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QuanLyDuAnService.Dto
{
    public class QuanLyDuAnSearchDto : SearchBase
    {
		public string TenDuAnFilter { get; set; }
		public string DuAnIdFilter { get; set; }
		public string TinhIdFilter { get; set; }
		public string HuyenIdFilter { get; set; }
		public string LocationFilter { get; set; }
		public string LongitudeFilter { get; set; }
		public string LatitudeFilter { get; set; }


    }
}