using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.QuanLyDuAnService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nami.Service.QuanLyDuAnService
{
    public interface IQuanLyDuAnService:IEntityService<QuanLyDuAn>
    {
        PageListResultBO<QuanLyDuAnDto> GetDaTaByPage(QuanLyDuAnSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        QuanLyDuAn GetById(long id);
        List<SelectListItem> GetDropdownOfDuAnByHuyen(string id, string valueSelected);
        List<QuanLyDuAnDto> GetListDuAnHasImage(int Count);
    }
}
