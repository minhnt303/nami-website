using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.QuanLyDuAnRepository;
using Nami.Service.Common;
using Nami.Service.QuanLyDuAnService.Dto;
using log4net;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace Nami.Service.QuanLyDuAnService
{
    public class QuanLyDuAnService : EntityService<QuanLyDuAn>, IQuanLyDuAnService
    {
        IUnitOfWork _unitOfWork;
        IQuanLyDuAnRepository _QuanLyDuAnRepository;
        ILog _loger;
        IMapper _mapper;



        public QuanLyDuAnService(IUnitOfWork unitOfWork,
        IQuanLyDuAnRepository QuanLyDuAnRepository,
        ILog loger,

                IMapper mapper
            )
            : base(unitOfWork, QuanLyDuAnRepository)
        {
            _unitOfWork = unitOfWork;
            _QuanLyDuAnRepository = QuanLyDuAnRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<QuanLyDuAnDto> GetDaTaByPage(QuanLyDuAnSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from QuanLyDuAntbl in _QuanLyDuAnRepository.GetAllAsQueryable()

                        select new QuanLyDuAnDto
                        {
                            TenDuAn = QuanLyDuAntbl.TenDuAn,
                            DuAnId = QuanLyDuAntbl.DuAnId,
                            TinhId = QuanLyDuAntbl.TinhId,
                            HuyenId = QuanLyDuAntbl.HuyenId,
                            Location = QuanLyDuAntbl.Location,
                            Longitude = QuanLyDuAntbl.Longitude,
                            Latitude = QuanLyDuAntbl.Latitude,
                            IsDelete = QuanLyDuAntbl.IsDelete,
                            DeleteTime = QuanLyDuAntbl.DeleteTime,
                            DeleteId = QuanLyDuAntbl.DeleteId,
                            Id = QuanLyDuAntbl.Id,
                            CreatedDate = QuanLyDuAntbl.CreatedDate,
                            CreatedBy = QuanLyDuAntbl.CreatedBy,
                            CreatedID = QuanLyDuAntbl.CreatedID,
                            UpdatedDate = QuanLyDuAntbl.UpdatedDate,
                            UpdatedBy = QuanLyDuAntbl.UpdatedBy,
                            UpdatedID = QuanLyDuAntbl.UpdatedID,
                            ImageUrl = QuanLyDuAntbl.ImageUrl,
                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.TenDuAnFilter))
                {
                    query = query.Where(x => x.TenDuAn.Contains(searchModel.TenDuAnFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.DuAnIdFilter))
                {
                    query = query.Where(x => x.DuAnId.Contains(searchModel.DuAnIdFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.TinhIdFilter))
                {
                    query = query.Where(x => x.TinhId.Contains(searchModel.TinhIdFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.HuyenIdFilter))
                {
                    query = query.Where(x => x.HuyenId.Contains(searchModel.HuyenIdFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.LocationFilter))
                {
                    query = query.Where(x => x.Location.Contains(searchModel.LocationFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.LongitudeFilter))
                {
                    query = query.Where(x => x.Longitude.Contains(searchModel.LongitudeFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.LatitudeFilter))
                {
                    query = query.Where(x => x.Latitude.Contains(searchModel.LatitudeFilter));
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<QuanLyDuAnDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public QuanLyDuAn GetById(long id)
        {
            return _QuanLyDuAnRepository.GetById(id);
        }

        public List<SelectListItem> GetDropdownOfDuAnByHuyen(string id, string valueSelected)
        {
            var model = _QuanLyDuAnRepository.GetAllAsQueryable().Where(x => x.HuyenId == id)
                .Select(x => new SelectListItem()
                {
                    Text = x.TenDuAn,
                    Value = x.Id.ToString(),
                    Selected = x.DuAnId == valueSelected
                })
                .ToList();
            return model;
        }

        public List<QuanLyDuAnDto> GetListDuAnHasImage(int Count)
        {
            var query = (from QuanLyDuAntbl in _QuanLyDuAnRepository.GetAllAsQueryable().Where(x => !string.IsNullOrEmpty(x.ImageUrl))

                         select new QuanLyDuAnDto
                         {
                             TenDuAn = QuanLyDuAntbl.TenDuAn,
                             DuAnId = QuanLyDuAntbl.DuAnId,
                             TinhId = QuanLyDuAntbl.TinhId,
                             HuyenId = QuanLyDuAntbl.HuyenId,
                             Location = QuanLyDuAntbl.Location,
                             Longitude = QuanLyDuAntbl.Longitude,
                             Latitude = QuanLyDuAntbl.Latitude,
                             IsDelete = QuanLyDuAntbl.IsDelete,
                             DeleteTime = QuanLyDuAntbl.DeleteTime,
                             DeleteId = QuanLyDuAntbl.DeleteId,
                             Id = QuanLyDuAntbl.Id,
                             CreatedDate = QuanLyDuAntbl.CreatedDate,
                             CreatedBy = QuanLyDuAntbl.CreatedBy,
                             CreatedID = QuanLyDuAntbl.CreatedID,
                             UpdatedDate = QuanLyDuAntbl.UpdatedDate,
                             UpdatedBy = QuanLyDuAntbl.UpdatedBy,
                             UpdatedID = QuanLyDuAntbl.UpdatedID,
                             ImageUrl = QuanLyDuAntbl.ImageUrl,
                         }).OrderByDescending(x => x.UpdatedDate).Take(Count).ToList();
            return query;
        }
    }
}
