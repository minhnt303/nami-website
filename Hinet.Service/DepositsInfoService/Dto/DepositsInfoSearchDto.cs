using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.DepositsInfoService.Dto
{
    public class DepositsInfoSearchDto : SearchBase
    {
		public DateTime? DepositDateFilter { get; set; }
		public DateTime ExpirationDateFilter { get; set; }
		public int? DepositsMoneyFilter { get; set; }


    }
}