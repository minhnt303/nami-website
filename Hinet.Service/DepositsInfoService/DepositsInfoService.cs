using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.DepositsInfoRepository;
using Nami.Service.DepositsInfoService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;



namespace Nami.Service.DepositsInfoService
{
    public class DepositsInfoService : EntityService<DepositsInfo>, IDepositsInfoService
    {
        IUnitOfWork _unitOfWork;
        IDepositsInfoRepository _DepositsInfoRepository;
	ILog _loger;
        IMapper _mapper;

        
        public DepositsInfoService(IUnitOfWork unitOfWork, 
		IDepositsInfoRepository DepositsInfoRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, DepositsInfoRepository)
        {
            _unitOfWork = unitOfWork;
            _DepositsInfoRepository = DepositsInfoRepository;
            _loger = loger;
            _mapper = mapper;

        }

        public PageListResultBO<DepositsInfoDto> GetDaTaByPage(DepositsInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from DepositsInfotbl in _DepositsInfoRepository.GetAllAsQueryable()

                        select new DepositsInfoDto
                        {
							Id = DepositsInfotbl.Id,
							BuildingsId = DepositsInfotbl.BuildingsId,
							RoomsId = DepositsInfotbl.RoomsId,
							RentersId = DepositsInfotbl.RentersId,
							DepositDate = DepositsInfotbl.DepositDate,
							ExpirationDate = DepositsInfotbl.ExpirationDate,
							DepositsMoney = DepositsInfotbl.DepositsMoney,
							Note = DepositsInfotbl.Note,
							CreatedDate = DepositsInfotbl.CreatedDate,
							CreatedBy = DepositsInfotbl.CreatedBy,
							UpdatedDate = DepositsInfotbl.UpdatedDate,
							UpdatedBy = DepositsInfotbl.UpdatedBy
                            
                        };

            if (searchModel != null)
            {
		if (searchModel.DepositDateFilter!=null)
		{
			query = query.Where(x => x.DepositDate==searchModel.DepositDateFilter);
		}
		if (searchModel.ExpirationDateFilter!=null)
		{
			query = query.Where(x => x.ExpirationDate==searchModel.ExpirationDateFilter);
		}
		if (searchModel.DepositsMoneyFilter!=null)
		{
			query = query.Where(x => x.DepositsMoney==searchModel.DepositsMoneyFilter);
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<DepositsInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public DepositsInfo GetById(long id)
        {
            return _DepositsInfoRepository.GetById(id);
        }
    

    }
}
