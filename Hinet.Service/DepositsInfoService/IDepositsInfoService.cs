using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.DepositsInfoService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.DepositsInfoService
{
    public interface IDepositsInfoService:IEntityService<DepositsInfo>
    {
        PageListResultBO<DepositsInfoDto> GetDaTaByPage(DepositsInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        DepositsInfo GetById(long id);
    }
}
