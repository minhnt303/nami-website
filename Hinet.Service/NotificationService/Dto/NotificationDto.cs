using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.NotificationService.Dto
{
    public class NotificationDto : Notification
    {
        public AppUser FromUserInfo { get; set; }
        public string FromUserName { get; internal set; }
    }
}