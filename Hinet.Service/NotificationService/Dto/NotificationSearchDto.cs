using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.NotificationService.Dto
{
    public class NotificationSearchDto : SearchBase
    {
		public bool IsReadFilter { get; set; }
		public long? FromUserFilter { get; set; }
		public long? ToUserFilter { get; set; }
		public string MessageFilter { get; set; }
		public string TypeFilter { get; set; }


    }
}