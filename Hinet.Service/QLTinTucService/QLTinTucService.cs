using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.QLTinTucRepository;
using Nami.Service.Common;
using Nami.Service.QLTinTucService.Dto;
using log4net;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Nami.Repository.DanhmucRepository;
using Nami.Service.Constant;
using Nami;
using System;

namespace Nami.Service.QLTinTucService
{
    public class QLTinTucService : EntityService<QLTinTuc>, IQLTinTucService
    {
        IUnitOfWork _unitOfWork;
        IQLTinTucRepository _QLTinTucRepository;
        ILog _loger;
        IMapper _mapper;
        IDM_DulieuDanhmucRepository _dM_DulieuDanhmucRepository;
        IDM_NhomDanhmucRepository _dM_NhomDanhmucRepository;

        public QLTinTucService(IUnitOfWork unitOfWork,
        IQLTinTucRepository QLTinTucRepository,
        ILog loger,
        IDM_DulieuDanhmucRepository dM_DulieuDanhmucRepository,
        IDM_NhomDanhmucRepository dM_NhomDanhmucRepository,
                IMapper mapper
            )
            : base(unitOfWork, QLTinTucRepository)
        {
            _unitOfWork = unitOfWork;
            _QLTinTucRepository = QLTinTucRepository;
            _loger = loger;
            _mapper = mapper;
            _dM_DulieuDanhmucRepository = dM_DulieuDanhmucRepository;
            _dM_NhomDanhmucRepository = dM_NhomDanhmucRepository;
        }

        public PageListResultBO<QLTinTucDto> GetDaTaByPage(QLTinTucSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var GroupDanhMucId = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.CHUDETINTUC).Select(x => x.Id).FirstOrDefault();
            var query = from QLTinTuctbl in _QLTinTucRepository.GetAllAsQueryable()

                        select new QLTinTucDto
                        {
                            ImageLink = QLTinTuctbl.ImageLink,
                            Name = QLTinTuctbl.Name,
                            NamePhu = QLTinTuctbl.NamePhu,
                            Description = QLTinTuctbl.Description,
                            Status = QLTinTuctbl.Status,
                            Author = QLTinTuctbl.Author,
                            ChuDeId = QLTinTuctbl.ChuDeId,
                            IsDelete = QLTinTuctbl.IsDelete,
                            DeleteTime = QLTinTuctbl.DeleteTime,
                            DeleteId = QLTinTuctbl.DeleteId,
                            Id = QLTinTuctbl.Id,
                            CreatedDate = QLTinTuctbl.CreatedDate,
                            CreatedBy = QLTinTuctbl.CreatedBy,
                            CreatedID = QLTinTuctbl.CreatedID,
                            UpdatedDate = QLTinTuctbl.UpdatedDate,
                            UpdatedBy = QLTinTuctbl.UpdatedBy,
                            UpdatedID = QLTinTuctbl.UpdatedID,
                            IsHot = QLTinTuctbl.IsHot,
                            SlugTitle = QLTinTuctbl.SlugTitle
                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.ImageLinkFilter))
                {
                    query = query.Where(x => x.ImageLink.Contains(searchModel.ImageLinkFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.NameFilter))
                {
                    query = query.Where(x => x.Name.Contains(searchModel.NameFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.NamePhuFilter))
                {
                    query = query.Where(x => x.NamePhu.Contains(searchModel.NamePhuFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.DescriptionFilter))
                {
                    query = query.Where(x => x.Description.Contains(searchModel.DescriptionFilter));
                }
                if (searchModel.StatusFilter != null)
                {
                    query = query.Where(x => x.Status == searchModel.StatusFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.AuthorFilter))
                {
                    query = query.Where(x => x.Author.Contains(searchModel.AuthorFilter));
                }
                if (searchModel.ChuDeIdFilter != null)
                {
                    query = query.Where(x => x.ChuDeId == searchModel.ChuDeIdFilter);
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.UpdatedDate);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.UpdatedDate);
            }
            var resultmodel = new PageListResultBO<QLTinTucDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            foreach (var item in resultmodel.ListItem)
            {
                if (item.ChuDeId != 0 && GroupDanhMucId != 0) {
                    var ChuDeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == GroupDanhMucId && x.Id == item.ChuDeId).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(ChuDeName)) {
                        item.ChuDeName = ChuDeName;
                    }
                }
            }
            return resultmodel;
        }

        public QLTinTuc GetById(long id)
        {
            return _QLTinTucRepository.GetById(id);
        }


        public QLTinTucDto GetBySlugName(string slugName)
        {
            var GroupDanhMucId = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.CHUDETINTUC).Select(x => x.Id).FirstOrDefault();
            var item = (from QLTinTuctbl in _QLTinTucRepository.GetAllAsQueryable().Where(x => x.SlugTitle == slugName && x.Status == true)
                          select new QLTinTucDto
                          {
                              ImageLink = QLTinTuctbl.ImageLink,
                              Name = QLTinTuctbl.Name,
                              NamePhu = QLTinTuctbl.NamePhu,
                              Description = QLTinTuctbl.Description,
                              Status = QLTinTuctbl.Status,
                              Author = QLTinTuctbl.Author,
                              ChuDeId = QLTinTuctbl.ChuDeId,
                              IsDelete = QLTinTuctbl.IsDelete,
                              DeleteTime = QLTinTuctbl.DeleteTime,
                              DeleteId = QLTinTuctbl.DeleteId,
                              Id = QLTinTuctbl.Id,
                              CreatedDate = QLTinTuctbl.CreatedDate,
                              CreatedBy = QLTinTuctbl.CreatedBy,
                              CreatedID = QLTinTuctbl.CreatedID,
                              UpdatedDate = QLTinTuctbl.UpdatedDate,
                              UpdatedBy = QLTinTuctbl.UpdatedBy,
                              UpdatedID = QLTinTuctbl.UpdatedID,
                              IsHot = QLTinTuctbl.IsHot,
                              SlugTitle = QLTinTuctbl.SlugTitle
                          }).FirstOrDefault();
            if (item.ChuDeId != 0 && GroupDanhMucId != 0)
            {
                var ChuDeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == GroupDanhMucId && x.Id == item.ChuDeId).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ChuDeName))
                {
                    item.ChuDeName = ChuDeName;
                }
            }
            return item;
        }

        public QLTinTucDto GetDtoById(long id)
        {
            var GroupDanhMucId = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.CHUDETINTUC).Select(x => x.Id).FirstOrDefault();
            var item = (from QLTinTuctbl in _QLTinTucRepository.GetAllAsQueryable().Where(x => x.Id == id)

                         select new QLTinTucDto
                         {
                             ImageLink = QLTinTuctbl.ImageLink,
                             Name = QLTinTuctbl.Name,
                             NamePhu = QLTinTuctbl.NamePhu,
                             Description = QLTinTuctbl.Description,
                             Status = QLTinTuctbl.Status,
                             Author = QLTinTuctbl.Author,
                             ChuDeId = QLTinTuctbl.ChuDeId,
                             IsDelete = QLTinTuctbl.IsDelete,
                             DeleteTime = QLTinTuctbl.DeleteTime,
                             DeleteId = QLTinTuctbl.DeleteId,
                             Id = QLTinTuctbl.Id,
                             CreatedDate = QLTinTuctbl.CreatedDate,
                             CreatedBy = QLTinTuctbl.CreatedBy,
                             CreatedID = QLTinTuctbl.CreatedID,
                             UpdatedDate = QLTinTuctbl.UpdatedDate,
                             UpdatedBy = QLTinTuctbl.UpdatedBy,
                             UpdatedID = QLTinTuctbl.UpdatedID,
                             IsHot = QLTinTuctbl.IsHot,
                             SlugTitle = QLTinTuctbl.SlugTitle
                         }).FirstOrDefault();

            if (item.ChuDeId != 0 && GroupDanhMucId != 0)
            {
                var ChuDeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == GroupDanhMucId && x.Id == item.ChuDeId).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ChuDeName))
                {
                    item.ChuDeName = ChuDeName;
                }
            }
            return item;
        }

        public List<QLTinTucDto> GetListTinTuc(bool IsHot)
        {
            var GroupDanhMucId = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.CHUDETINTUC).Select(x => x.Id).FirstOrDefault();
            var query = (from QLTinTuctbl in _QLTinTucRepository.GetAllAsQueryable().Where(x => x.IsHot == IsHot && x.Status == true)

                         select new QLTinTucDto
                         {
                             ImageLink = QLTinTuctbl.ImageLink,
                             Name = QLTinTuctbl.Name,
                             NamePhu = QLTinTuctbl.NamePhu,
                             Description = QLTinTuctbl.Description,
                             Status = QLTinTuctbl.Status,
                             Author = QLTinTuctbl.Author,
                             ChuDeId = QLTinTuctbl.ChuDeId,
                             IsDelete = QLTinTuctbl.IsDelete,
                             DeleteTime = QLTinTuctbl.DeleteTime,
                             DeleteId = QLTinTuctbl.DeleteId,
                             Id = QLTinTuctbl.Id,
                             CreatedDate = QLTinTuctbl.CreatedDate,
                             CreatedBy = QLTinTuctbl.CreatedBy,
                             CreatedID = QLTinTuctbl.CreatedID,
                             UpdatedDate = QLTinTuctbl.UpdatedDate,
                             UpdatedBy = QLTinTuctbl.UpdatedBy,
                             UpdatedID = QLTinTuctbl.UpdatedID,
                             IsHot = QLTinTuctbl.IsHot,
                             SlugTitle = QLTinTuctbl.SlugTitle
                         }).OrderByDescending(x => x.UpdatedDate).ToList();
            foreach (var item in query)
            {
                if (item.ChuDeId != 0 && GroupDanhMucId != 0)
                {
                    var ChuDeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == GroupDanhMucId && x.Id == item.ChuDeId).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(ChuDeName))
                    {
                        item.ChuDeName = ChuDeName;
                    }
                }
            }
            return query;
        }

        public List<QLTinTucDto> GetListTinTucByIdChuDe(bool IsHot, long IdChuDe, int amount, long CurrentTinTucId)
        {
            var GroupDanhMucId = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.CHUDETINTUC).Select(x => x.Id).FirstOrDefault();
            var query = (from QLTinTuctbl in _QLTinTucRepository.GetAllAsQueryable().Where(x => x.IsHot == IsHot && x.ChuDeId == IdChuDe && x.Id != CurrentTinTucId && x.Status == true)

                         select new QLTinTucDto
                         {
                             ImageLink = QLTinTuctbl.ImageLink,
                             Name = QLTinTuctbl.Name,
                             NamePhu = QLTinTuctbl.NamePhu,
                             Description = QLTinTuctbl.Description,
                             Status = QLTinTuctbl.Status,
                             Author = QLTinTuctbl.Author,
                             ChuDeId = QLTinTuctbl.ChuDeId,
                             IsDelete = QLTinTuctbl.IsDelete,
                             DeleteTime = QLTinTuctbl.DeleteTime,
                             DeleteId = QLTinTuctbl.DeleteId,
                             Id = QLTinTuctbl.Id,
                             CreatedDate = QLTinTuctbl.CreatedDate,
                             CreatedBy = QLTinTuctbl.CreatedBy,
                             CreatedID = QLTinTuctbl.CreatedID,
                             UpdatedDate = QLTinTuctbl.UpdatedDate,
                             UpdatedBy = QLTinTuctbl.UpdatedBy,
                             UpdatedID = QLTinTuctbl.UpdatedID,
                             IsHot = QLTinTuctbl.IsHot,
                             SlugTitle = QLTinTuctbl.SlugTitle
                         }).OrderByDescending(x => x.UpdatedDate).Take(amount).ToList();
            foreach (var item in query)
            {
                if (item.ChuDeId != 0 && GroupDanhMucId != 0)
                {
                    var ChuDeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == GroupDanhMucId && x.Id == item.ChuDeId).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(ChuDeName))
                    {
                        item.ChuDeName = ChuDeName;
                    }
                }
            }
            return query;
        }

        public PageListResultBO<QLTinTucDto> GetDaTaShowByPage(string KeySearch,bool IsHot, int pageIndex = 1, int pageSize = 20)
        {
            var currentDate = DateTime.Now.Date;
            var GroupDanhMucId = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.CHUDETINTUC).Select(x => x.Id).FirstOrDefault();
            var query = from QLTinTuctbl in _QLTinTucRepository.GetAllAsQueryable().Where(x=>x.UpdatedDate <= currentDate && x.IsHot == IsHot && x.Status == true)

                        select new QLTinTucDto
                        {
                            ImageLink = QLTinTuctbl.ImageLink,
                            Name = QLTinTuctbl.Name,
                            NamePhu = QLTinTuctbl.NamePhu,
                            Description = QLTinTuctbl.Description,
                            Status = QLTinTuctbl.Status,
                            Author = QLTinTuctbl.Author,
                            ChuDeId = QLTinTuctbl.ChuDeId,
                            IsDelete = QLTinTuctbl.IsDelete,
                            DeleteTime = QLTinTuctbl.DeleteTime,
                            DeleteId = QLTinTuctbl.DeleteId,
                            Id = QLTinTuctbl.Id,
                            CreatedDate = QLTinTuctbl.CreatedDate,
                            CreatedBy = QLTinTuctbl.CreatedBy,
                            CreatedID = QLTinTuctbl.CreatedID,
                            UpdatedDate = QLTinTuctbl.UpdatedDate,
                            UpdatedBy = QLTinTuctbl.UpdatedBy,
                            UpdatedID = QLTinTuctbl.UpdatedID,
                            IsHot = QLTinTuctbl.IsHot,
                            SlugTitle = QLTinTuctbl.SlugTitle
                        };

            if (!string.IsNullOrEmpty(KeySearch))
            {
                query = query.Where(x => x.Name.Contains(KeySearch) || x.CreatedBy.Contains(KeySearch));
            }
            query = query.OrderByDescending(x => x.UpdatedDate);
            var resultmodel = new PageListResultBO<QLTinTucDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
                resultmodel.CurrentPage = 1;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
                resultmodel.CurrentPage = pageIndex;
            }
            foreach (var item in resultmodel.ListItem)
            {
                if (item.ChuDeId != 0 && GroupDanhMucId != 0)
                {
                    var ChuDeName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == GroupDanhMucId && x.Id == item.ChuDeId).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(ChuDeName))
                    {
                        item.ChuDeName = ChuDeName;
                    }
                }
            }
            return resultmodel;
        }
    }
}
