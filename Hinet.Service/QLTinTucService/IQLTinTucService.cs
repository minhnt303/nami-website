using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.QLTinTucService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLTinTucService
{
    public interface IQLTinTucService:IEntityService<QLTinTuc>
    {
        PageListResultBO<QLTinTucDto> GetDaTaByPage(QLTinTucSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        QLTinTuc GetById(long id);
        List<QLTinTucDto> GetListTinTuc(bool IsHot);
        QLTinTucDto GetDtoById(long id);
        QLTinTucDto GetBySlugName(string slugName);
        List<QLTinTucDto> GetListTinTucByIdChuDe(bool IsHot, long IdChuDe, int amount, long CurrentTinTucId);
        PageListResultBO<QLTinTucDto> GetDaTaShowByPage(string KeySearch, bool IsHot, int pageIndex = 1, int pageSize = 20);
    }
}
