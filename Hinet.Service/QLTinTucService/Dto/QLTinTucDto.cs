using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLTinTucService.Dto
{
    public class QLTinTucDto : QLTinTuc
    {
        public string ChuDeName { get; set; }
    }
}