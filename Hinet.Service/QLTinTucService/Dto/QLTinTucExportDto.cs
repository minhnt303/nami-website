using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Nami.Service.QLTinTucService.Dto
{
    public class QLTinTucExportDto
    {
		[DisplayName("Đường dẫn ảnh")]
public string ImageLink { get; set; }
		[DisplayName("Tiêu đề")]
public string Name { get; set; }
		[DisplayName("Tiêu đề")]
public string NamePhu { get; set; }
		[DisplayName("Nội dung")]
public string Description { get; set; }
		[DisplayName("Trạng thái")]
public bool Status { get; set; }
		[DisplayName("Tác giả")]
public string Author { get; set; }
		[DisplayName("Id chủ đề")]
public long ChuDeId { get; set; }

    }
}