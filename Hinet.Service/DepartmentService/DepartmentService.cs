﻿using log4net;
using PagedList;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.DepartmentRepository;
using Nami.Service.Common;
using Nami.Service.DepartmentService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nami.Repository.TINHRepository;
using Nami.Repository.RoleRepository;

namespace Nami.Service.DepartmentService
{
    public class DepartmentService : EntityService<Department>, IDepartmentService
    {
        IUnitOfWork _unitOfWork;
        IDepartmentRepository _departmentRepository;
        ITINHRepository _tINHRepository;
        IRoleRepository _roleRepository;
        ILog _loger;

        public DepartmentService(IUnitOfWork unitOfWork,
            ITINHRepository tINHRepository,
            IRoleRepository roleRepository,
            IDepartmentRepository departmentRepository, ILog loger) 
            : base(unitOfWork, departmentRepository)
        {
            _roleRepository = roleRepository;
            _unitOfWork = unitOfWork;
            _departmentRepository = departmentRepository;
            _loger = loger;
            _tINHRepository = tINHRepository;
        }

        public bool CheckCodeExisted(string code)
        {
            return this._departmentRepository.GetAllAsQueryable().Where(x => x.Code.Equals(code)).Any();
        }

        public DepartmentDTO GetTree(long departmentId = 0)
        {
            // the original taken from db
            List<DepartmentDTO> rawList = _GetAllItems();

            DepartmentDTO root = new DepartmentDTO();
            if (departmentId > 0)
            {
                root = rawList.Where(x => x.Id == departmentId).FirstOrDefault();
                if (root != null)
                {
                    _GetChildOfTree(ref root, rawList);
                }
            }
            else
            {
                root = rawList.Where(x => x.ParentId.HasValue == false).FirstOrDefault();
                if (root != null)
                {
                    _GetChildOfTree(ref root, rawList);
                }
            }

            return root;
        }
        public void _GetChildOfTree(ref DepartmentDTO node, List<DepartmentDTO> rawList)
        {
            long id = node.Id;
            var lstChild = rawList.Where(x => x.ParentId == id).ToList();
            if (lstChild.Count > 0)
            {
                node.Child = new List<DepartmentDTO>();
                node.Child.AddRange(lstChild);
                for (int i = 0; i < lstChild.Count; i++)
                {
                    var item = node.Child[i];
                    _GetChildOfTree(ref item, rawList);
                    node.Child[i] = item;
                }
            }
        }
        public List<DepartmentDTO> _GetAllItems()
        {
            var tinhDbSet = _tINHRepository.DBSet();
            
            return (from tbl in this._departmentRepository.GetAllAsQueryable()
                    select new DepartmentDTO
                    {
                        Id = tbl.Id,
                        ParentId = tbl.ParentId,
                        Name = tbl.Name,
                        Code = tbl.Code,
                        Level = tbl.Level,
                        Priority = tbl.Priority,
                        DefaultRole = tbl.DefaultRole,
                        //role = _roleRepository.GetById(tbl.DefaultRole),
                        tINHs= tbl.ProvinceManagement!=null? tinhDbSet.Where(x=>tbl.ProvinceManagement.Contains(x.MaTinh)).ToList():new List<TINH>()
                    }).OrderBy(x=>x.Priority).ToList();
        }

        /// <summary>
        /// Lấy danh sách phòng ban đổ ra dropdownlist
        /// </summary>
        /// <param name="currentId"></param>
        /// <param name="codeselected"></param>
        /// <returns></returns>
        public List<SelectListItem> GetTreeDropdownList(long? currentId, string codeselected)
        {
            List<SelectListItem> sList = new List<SelectListItem>();
            var QueueParrent = new Queue<long>();
            //NamiDataContext eDataContext = new NamiDataContext();
            var listAll = _departmentRepository.GetAll().ToList();
            if (currentId != null)
            {
                listAll = listAll.Where(x => x.ParentId != currentId).ToList();
            }
            var currentItem = listAll.Where(x => x.Id == currentId).FirstOrDefault();

            //Lấy danh sách cấp 1
            var listLevel1 = listAll.Where(x => x.ParentId == null).OrderBy(x => x.ParentId).ThenByDescending(x => x.Id).ToList();
            if (listLevel1 != null)
            {
                foreach (var item1 in listLevel1)
                {
                    string levelStr = "-- ";
                    QueueParrent.Enqueue(item1.Id);
                    sList.Add(new SelectListItem()
                    {
                        Text = levelStr + item1.Name,
                        Value = item1.Id.ToString(),
                        Selected = currentId != null ? item1.Id == currentId.Value : false,
                    });

                    var listChilditem = CheckChilds(ref QueueParrent, currentId, listAll, levelStr + "-- ");
                    if (listChilditem != null)
                    {
                        sList.AddRange(listChilditem);
                    }

                }
            }

            return sList;

        }

        private static List<SelectListItem> CheckChilds(ref Queue<long> queue, long? currentId, List<Department> listAll, string levelStr)
        {
            List<SelectListItem> sList = new List<SelectListItem>();
            while (queue.Count() > 0)
            {
                var parentid = queue.Dequeue();
                var listChild = listAll.Where(x => x.ParentId == parentid).OrderBy(x => x.Priority).ToList();
                if (listChild != null)
                {

                    foreach (var itemChild in listChild)
                    {
                        queue.Enqueue(itemChild.Id);
                        sList.Add(new SelectListItem()
                        {
                            Text = levelStr + itemChild.Name,
                            Value = itemChild.Id.ToString(),
                            Selected = currentId != null ? itemChild.Id == currentId.Value : false,
                        });
                        var listChilditem = CheckChilds(ref queue, currentId, listAll, levelStr + " -- ");
                        if (listChilditem != null)
                        {
                            sList.AddRange(listChilditem);
                        }
                    }
                }

            }

            return sList;
        }

   
    }

}
