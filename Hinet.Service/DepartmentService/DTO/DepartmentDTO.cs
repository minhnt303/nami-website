﻿using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.DepartmentService.DTO
{
    public class DepartmentDTO : Department
    {
        public List<DepartmentDTO> Child { get; set; }
        public List<TINH> tINHs { get; set; }
        public string TinhName { get; set; }
        public Role role { get; set; }
    }

    public class DepartmentCM
    {
        public int id { get; set; }
        public List<DepartmentCM> children { get; set; }
    }
}
