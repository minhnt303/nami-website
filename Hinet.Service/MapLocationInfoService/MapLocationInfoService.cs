using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.MapLocationInfoRepository;
using Nami.Service.MapLocationInfoService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;



namespace Nami.Service.MapLocationInfoService
{
    public class MapLocationInfoService : EntityService<MapLocationInfo>, IMapLocationInfoService
    {
        IUnitOfWork _unitOfWork;
        IMapLocationInfoRepository _MapLocationInfoRepository;
	ILog _loger;
        IMapper _mapper;

        
        public MapLocationInfoService(IUnitOfWork unitOfWork, 
		IMapLocationInfoRepository MapLocationInfoRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, MapLocationInfoRepository)
        {
            _unitOfWork = unitOfWork;
            _MapLocationInfoRepository = MapLocationInfoRepository;
            _loger = loger;
            _mapper = mapper;

        }

        public PageListResultBO<MapLocationInfoDto> GetDaTaByPage(MapLocationInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from MapLocationInfotbl in _MapLocationInfoRepository.GetAllAsQueryable()

                        select new MapLocationInfoDto
                        {
							Id = MapLocationInfotbl.Id,
							BuildingsId = MapLocationInfotbl.BuildingsId,
							RoomsId = MapLocationInfotbl.RoomsId,
							ItemTypeCode = MapLocationInfotbl.ItemTypeCode,
							Xlocation = MapLocationInfotbl.Xlocation,
							Ylocation = MapLocationInfotbl.Ylocation,
							CreatedDate = MapLocationInfotbl.CreatedDate,
							CreatedBy = MapLocationInfotbl.CreatedBy,
							UpdatedDate = MapLocationInfotbl.UpdatedDate,
							UpdatedBy = MapLocationInfotbl.UpdatedBy
                            
                        };

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<MapLocationInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public MapLocationInfo GetById(long id)
        {
            return _MapLocationInfoRepository.GetById(id);
        }
    

    }
}
