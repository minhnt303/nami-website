using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.QuanLyDuongPhoService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nami.Service.QuanLyDuongPhoService
{
    public interface IQuanLyDuongPhoService:IEntityService<QuanLyDuongPho>
    {
        PageListResultBO<QuanLyDuongPhoDto> GetDaTaByPage(QuanLyDuongPhoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        QuanLyDuongPho GetById(long id);
        List<SelectListItem> GetDropdownOfStreetByHuyen(string id, string valueSelected);
    }
}
