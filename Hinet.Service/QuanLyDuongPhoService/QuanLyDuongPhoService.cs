﻿using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.QuanLyDuongPhoRepository;
using Nami.Service.QuanLyDuongPhoService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;
using System.Web.Mvc;

namespace Nami.Service.QuanLyDuongPhoService
{
    public class QuanLyDuongPhoService : EntityService<QuanLyDuongPho>, IQuanLyDuongPhoService
    {
        IUnitOfWork _unitOfWork;
        IQuanLyDuongPhoRepository _QuanLyDuongPhoRepository;
	ILog _loger;
        IMapper _mapper;


        
        public QuanLyDuongPhoService(IUnitOfWork unitOfWork, 
		IQuanLyDuongPhoRepository QuanLyDuongPhoRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, QuanLyDuongPhoRepository)
        {
            _unitOfWork = unitOfWork;
            _QuanLyDuongPhoRepository = QuanLyDuongPhoRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<QuanLyDuongPhoDto> GetDaTaByPage(QuanLyDuongPhoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from QuanLyDuongPhotbl in _QuanLyDuongPhoRepository.GetAllAsQueryable()

                        select new QuanLyDuongPhoDto
                        {
							MaDuong = QuanLyDuongPhotbl.MaDuong,
							TenDuong = QuanLyDuongPhotbl.TenDuong,
							DuongId = QuanLyDuongPhotbl.DuongId,
							TinhId = QuanLyDuongPhotbl.TinhId,
							HuyenId = QuanLyDuongPhotbl.HuyenId,
							Loai = QuanLyDuongPhotbl.Loai,
							Location = QuanLyDuongPhotbl.Location,
							Longitude = QuanLyDuongPhotbl.Longitude,
							Latitude = QuanLyDuongPhotbl.Latitude,
							IsDelete = QuanLyDuongPhotbl.IsDelete,
							DeleteTime = QuanLyDuongPhotbl.DeleteTime,
							DeleteId = QuanLyDuongPhotbl.DeleteId,
							Id = QuanLyDuongPhotbl.Id,
							CreatedDate = QuanLyDuongPhotbl.CreatedDate,
							CreatedBy = QuanLyDuongPhotbl.CreatedBy,
							CreatedID = QuanLyDuongPhotbl.CreatedID,
							UpdatedDate = QuanLyDuongPhotbl.UpdatedDate,
							UpdatedBy = QuanLyDuongPhotbl.UpdatedBy,
							UpdatedID = QuanLyDuongPhotbl.UpdatedID
                            
                        };

            if (searchModel != null)
            {
		if (!string.IsNullOrEmpty(searchModel.MaDuongFilter))
		{
			query = query.Where(x => x.MaDuong.Contains(searchModel.MaDuongFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.TenDuongFilter))
		{
			query = query.Where(x => x.TenDuong.Contains(searchModel.TenDuongFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.DuongIdFilter))
		{
			query = query.Where(x => x.DuongId.Contains(searchModel.DuongIdFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.TinhIdFilter))
		{
			query = query.Where(x => x.TinhId.Contains(searchModel.TinhIdFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.HuyenIdFilter))
		{
			query = query.Where(x => x.HuyenId.Contains(searchModel.HuyenIdFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.LoaiFilter))
		{
			query = query.Where(x => x.Loai.Contains(searchModel.LoaiFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.LocationFilter))
		{
			query = query.Where(x => x.Location.Contains(searchModel.LocationFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.LongitudeFilter))
		{
			query = query.Where(x => x.Longitude.Contains(searchModel.LongitudeFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.LatitudeFilter))
		{
			query = query.Where(x => x.Latitude.Contains(searchModel.LatitudeFilter));
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<QuanLyDuongPhoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public QuanLyDuongPho GetById(long id)
        {
            return _QuanLyDuongPhoRepository.GetById(id);
        }

        public List<SelectListItem> GetDropdownOfStreetByHuyen(string id, string valueSelected)
        {
            var model = _QuanLyDuongPhoRepository.GetAllAsQueryable().Where(x => x.HuyenId == id)
                .Select(x => new SelectListItem()
                {
                    Text = "Đường " + x.TenDuong,
                    Value = x.Id.ToString(),
                    Selected = x.Id.ToString() == valueSelected
                })
                .ToList();
            return model;
        }
    }
}
