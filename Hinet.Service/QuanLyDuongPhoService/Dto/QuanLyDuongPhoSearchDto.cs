using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QuanLyDuongPhoService.Dto
{
    public class QuanLyDuongPhoSearchDto : SearchBase
    {
		public string MaDuongFilter { get; set; }
		public string TenDuongFilter { get; set; }
		public string DuongIdFilter { get; set; }
		public string TinhIdFilter { get; set; }
		public string HuyenIdFilter { get; set; }
		public string LoaiFilter { get; set; }
		public string LocationFilter { get; set; }
		public string LongitudeFilter { get; set; }
		public string LatitudeFilter { get; set; }


    }
}