using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Nami.Service.BDValueAxesService.Dto
{
    public class BDValueAxesExportDto
    {
		[DisplayName("")]
public long? IdBieuDo { get; set; }
		[DisplayName("")]
public string IdValue { get; set; }
		[DisplayName("")]
public string StackType { get; set; }
		[DisplayName("")]
public string Title { get; set; }
		[DisplayName("")]
public bool? IsDelete { get; set; }
		[DisplayName("")]
public DateTime? DeleteTime { get; set; }
		[DisplayName("")]
public long? DeleteId { get; set; }

    }
}