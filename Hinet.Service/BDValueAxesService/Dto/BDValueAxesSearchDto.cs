using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BDValueAxesService.Dto
{
    public class BDValueAxesSearchDto : SearchBase
    {
		public long? IdBieuDoFilter { get; set; }
		public string IdValueFilter { get; set; }
		public string StackTypeFilter { get; set; }
		public string TitleFilter { get; set; }
		public bool? IsDeleteFilter { get; set; }
		public DateTime? DeleteTimeFilter { get; set; }
		public long? DeleteIdFilter { get; set; }


    }
}