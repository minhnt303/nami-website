using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.BDValueAxesService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BDValueAxesService
{
    public interface IBDValueAxesService:IEntityService<BDValueAxes>
    {
        PageListResultBO<BDValueAxesDto> GetDaTaByPage(BDValueAxesSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        BDValueAxes GetById(long id);
    }
}
