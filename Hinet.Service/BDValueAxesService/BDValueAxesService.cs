using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.BDValueAxesRepository;
using Nami.Service.BDValueAxesService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;




namespace Nami.Service.BDValueAxesService
{
    public class BDValueAxesService : EntityService<BDValueAxes>, IBDValueAxesService
    {
        IUnitOfWork _unitOfWork;
        IBDValueAxesRepository _BDValueAxesRepository;
	ILog _loger;
        IMapper _mapper;


        
        public BDValueAxesService(IUnitOfWork unitOfWork, 
		IBDValueAxesRepository BDValueAxesRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, BDValueAxesRepository)
        {
            _unitOfWork = unitOfWork;
            _BDValueAxesRepository = BDValueAxesRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<BDValueAxesDto> GetDaTaByPage(BDValueAxesSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from BDValueAxestbl in _BDValueAxesRepository.GetAllAsQueryable()

                        select new BDValueAxesDto
                        {
							IdBieuDo = BDValueAxestbl.IdBieuDo,
							IdValue = BDValueAxestbl.IdValue,
							StackType = BDValueAxestbl.StackType,
							Title = BDValueAxestbl.Title,
							IsDelete = BDValueAxestbl.IsDelete,
							DeleteTime = BDValueAxestbl.DeleteTime,
							DeleteId = BDValueAxestbl.DeleteId,
							Id = BDValueAxestbl.Id,
							CreatedDate = BDValueAxestbl.CreatedDate,
							CreatedBy = BDValueAxestbl.CreatedBy,
							CreatedID = BDValueAxestbl.CreatedID,
							UpdatedDate = BDValueAxestbl.UpdatedDate,
							UpdatedBy = BDValueAxestbl.UpdatedBy,
							UpdatedID = BDValueAxestbl.UpdatedID
                            
                        };

            if (searchModel != null)
            {
		if (searchModel.IdBieuDoFilter!=null)
		{
			query = query.Where(x => x.IdBieuDo==searchModel.IdBieuDoFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.IdValueFilter))
		{
			query = query.Where(x => x.IdValue.Contains(searchModel.IdValueFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.StackTypeFilter))
		{
			query = query.Where(x => x.StackType.Contains(searchModel.StackTypeFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.TitleFilter))
		{
			query = query.Where(x => x.Title.Contains(searchModel.TitleFilter));
		}
		if (searchModel.IsDeleteFilter!=null)
		{
			query = query.Where(x => x.IsDelete==searchModel.IsDeleteFilter);
		}
		if (searchModel.DeleteTimeFilter!=null)
		{
			query = query.Where(x => x.DeleteTime==searchModel.DeleteTimeFilter);
		}
		if (searchModel.DeleteIdFilter!=null)
		{
			query = query.Where(x => x.DeleteId==searchModel.DeleteIdFilter);
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<BDValueAxesDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public BDValueAxes GetById(long id)
        {
            return _BDValueAxesRepository.GetById(id);
        }
    

    }
}
