﻿using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.BuildingsInfoRepository;
using Nami.Repository.DanhmucRepository;
using Nami.Repository.FileDinhKemRepository;
using Nami.Repository.HUYENRepository;
using Nami.Repository.QuanLyDuAnRepository;
using Nami.Repository.QuanLyDuongPhoRepository;
using Nami.Repository.RoomInfoRepository;
using Nami.Repository.TINHRepository;
using Nami.Repository.XARepository;
using Nami.Service.Common;
using Nami.Service.Constant;
using Nami.Service.RoomInfoService.Dto;
using log4net;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace Nami.Service.RoomInfoService
{
    public class RoomInfoService : EntityService<RoomInfo>, IRoomInfoService
    {
        IUnitOfWork _unitOfWork;
        IRoomInfoRepository _RoomInfoRepository;
        ILog _loger;
        IMapper _mapper;
        IBuildingsInfoRepository _buildingsInfoRepository;
        ITINHRepository _tINHRepository;
        IHUYENRepository _hUYENRepository;
        IXARepository _xARepository;
        IFileDinhKemRepository _fileDinhKemRepository;
        IDM_DulieuDanhmucRepository _dM_DulieuDanhmucRepository;
        IDM_NhomDanhmucRepository _dM_NhomDanhmucRepository;
        IQuanLyDuongPhoRepository _quanLyDuongPhoRepository;
        IQuanLyDuAnRepository _quanLyDuAnRepository;


        public RoomInfoService(IUnitOfWork unitOfWork,
        IRoomInfoRepository RoomInfoRepository,
        IBuildingsInfoRepository buildingsInfoRepository,
        ITINHRepository tINHRepository,
        IHUYENRepository hUYENRepository,
        IDM_DulieuDanhmucRepository dM_DulieuDanhmucRepository,
        IDM_NhomDanhmucRepository dM_NhomDanhmucRepository,
        IXARepository xARepository,
        IFileDinhKemRepository fileDinhKemRepository,
        IQuanLyDuongPhoRepository quanLyDuongPhoRepository,
        IQuanLyDuAnRepository quanLyDuAnRepository,
        ILog loger,

                IMapper mapper
            )
            : base(unitOfWork, RoomInfoRepository)
        {
            _unitOfWork = unitOfWork;
            _RoomInfoRepository = RoomInfoRepository;
            _loger = loger;
            _mapper = mapper;
            _buildingsInfoRepository = buildingsInfoRepository;
            _tINHRepository = tINHRepository;
            _hUYENRepository = hUYENRepository;
            _xARepository = xARepository;
            _fileDinhKemRepository = fileDinhKemRepository;
            _dM_NhomDanhmucRepository = dM_NhomDanhmucRepository;
            _dM_DulieuDanhmucRepository = dM_DulieuDanhmucRepository;
            _quanLyDuongPhoRepository = quanLyDuongPhoRepository;
            _quanLyDuAnRepository = quanLyDuAnRepository;
        }

        public PageListResultBO<RoomInfoDto> GetDaTaByPage(RoomInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from RoomInfotbl in _RoomInfoRepository.GetAllAsQueryable()

                        select new RoomInfoDto
                        {
                            Id = RoomInfotbl.Id,
                            BuildingsId = RoomInfotbl.BuildingsId,
                            RoomName = RoomInfotbl.RoomName,
                            Floor = RoomInfotbl.Floor,
                            NumberOfPersonPerRoom = RoomInfotbl.NumberOfPersonPerRoom,
                            Price = RoomInfotbl.Price,
                            Description = RoomInfotbl.Description,
                            TinhCode = RoomInfotbl.TinhCode,
                            XaCode = RoomInfotbl.XaCode,
                            HuyenCode = RoomInfotbl.HuyenCode,
                            QuocGiaCode = RoomInfotbl.QuocGiaCode,
                            Address = RoomInfotbl.Address,
                            ServicesCode = RoomInfotbl.ServicesCode,
                            InputWaterElectricFrom = RoomInfotbl.InputWaterElectricFrom,
                            InputWaterElectricTo = RoomInfotbl.InputWaterElectricTo,
                            BillsFrom = RoomInfotbl.BillsFrom,
                            BillsTo = RoomInfotbl.BillsTo,
                            CollectionMoneyFrom = RoomInfotbl.CollectionMoneyFrom,
                            CollectionMoneyTo = RoomInfotbl.CollectionMoneyTo,
                            EndOfConstractFrom = RoomInfotbl.EndOfConstractFrom,
                            EndOfConstractTo = RoomInfotbl.EndOfConstractTo,
                            CreatedDate = RoomInfotbl.CreatedDate,
                            CreatedBy = RoomInfotbl.CreatedBy,
                            UpdatedDate = RoomInfotbl.UpdatedDate,
                            UpdatedBy = RoomInfotbl.UpdatedBy,
                            //ChieuDaiRoom = RoomInfotbl.ChieuDaiRoom,
                            //ChieuRongRoom = RoomInfotbl.ChieuRongRoom,
                            DienTich = RoomInfotbl.DienTich,
                            NumberOfPeoperPerRoomAdult = RoomInfotbl.NumberOfPeoperPerRoomAdult,
                            NumberOfPeoperPerRoomKid = RoomInfotbl.NumberOfPeoperPerRoomKid,
                            Latitude = RoomInfotbl.Latitude,
                            Longitude = RoomInfotbl.Longitude
                        };

            if (searchModel != null)
            {


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<RoomInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            foreach (var item in resultmodel.ListItem)
            {
                var BuildingsIdName = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.BuildingsId).Select(x => x.BuildingsName).FirstOrDefault();
                if (!string.IsNullOrEmpty(BuildingsIdName))
                {
                    item.BuildingsIdName = BuildingsIdName;
                }
            }
            return resultmodel;
        }

        public PageListResultBO<RoomInfoDto> GetDaTaByPageByCreatedBy(string CreatedBy, RoomInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from RoomInfotbl in _RoomInfoRepository.GetAllAsQueryable().Where(x => x.CreatedBy == CreatedBy)

                        select new RoomInfoDto
                        {
                            Id = RoomInfotbl.Id,
                            BuildingsId = RoomInfotbl.BuildingsId,
                            RoomName = RoomInfotbl.RoomName,
                            Floor = RoomInfotbl.Floor,
                            NumberOfPersonPerRoom = RoomInfotbl.NumberOfPersonPerRoom,
                            Price = RoomInfotbl.Price,
                            Description = RoomInfotbl.Description,
                            TinhCode = RoomInfotbl.TinhCode,
                            XaCode = RoomInfotbl.XaCode,
                            HuyenCode = RoomInfotbl.HuyenCode,
                            QuocGiaCode = RoomInfotbl.QuocGiaCode,
                            Address = RoomInfotbl.Address,
                            ServicesCode = RoomInfotbl.ServicesCode,
                            InputWaterElectricFrom = RoomInfotbl.InputWaterElectricFrom,
                            InputWaterElectricTo = RoomInfotbl.InputWaterElectricTo,
                            BillsFrom = RoomInfotbl.BillsFrom,
                            BillsTo = RoomInfotbl.BillsTo,
                            CollectionMoneyFrom = RoomInfotbl.CollectionMoneyFrom,
                            CollectionMoneyTo = RoomInfotbl.CollectionMoneyTo,
                            EndOfConstractFrom = RoomInfotbl.EndOfConstractFrom,
                            EndOfConstractTo = RoomInfotbl.EndOfConstractTo,
                            CreatedDate = RoomInfotbl.CreatedDate,
                            CreatedBy = RoomInfotbl.CreatedBy,
                            UpdatedDate = RoomInfotbl.UpdatedDate,
                            UpdatedBy = RoomInfotbl.UpdatedBy,
                            //ChieuDaiRoom = RoomInfotbl.ChieuDaiRoom,
                            //ChieuRongRoom = RoomInfotbl.ChieuRongRoom,
                            DienTich = RoomInfotbl.DienTich,
                            NumberOfPeoperPerRoomAdult = RoomInfotbl.NumberOfPeoperPerRoomAdult,
                            NumberOfPeoperPerRoomKid = RoomInfotbl.NumberOfPeoperPerRoomKid,
                            Latitude = RoomInfotbl.Latitude,
                            Longitude = RoomInfotbl.Longitude
                        };

            if (searchModel != null)
            {

                if (searchModel.BuildingsNameFilter != 0 && searchModel.BuildingsNameFilter != -1 && searchModel.BuildingsNameFilter != -2)
                {
                    query = query.Where(x => x.BuildingsId == searchModel.BuildingsNameFilter);
                }
                if (searchModel.BuildingsNameFilter == -2)
                {
                    query = query.Where(x => (x.BuildingsId == null || x.BuildingsId == 0));
                }

                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<RoomInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            foreach (var item in resultmodel.ListItem)
            {
                var BuildingsIdName = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.BuildingsId).Select(x => x.BuildingsName).FirstOrDefault();
                if (!string.IsNullOrEmpty(BuildingsIdName))
                {
                    item.BuildingsIdName = BuildingsIdName;
                }
            }
            return resultmodel;
        }

        public RoomInfo GetById(long id)
        {
            return _RoomInfoRepository.GetById(id);
        }

        public List<RoomInfo> GetListRoomByCurrentCreateAndBuildingId(string CurrentCreateBy, long BuildingId)
        {
            return _RoomInfoRepository.GetAllAsQueryable().Where(x => x.CreatedBy == CurrentCreateBy && x.BuildingsId == BuildingId).OrderByDescending(x => x.Id).ToList();
        }
        public List<SelectListItem> GetDropDownListRoom(string CreatedBy)
        {
            var query = (from RoomInfotbl in _RoomInfoRepository.GetAllAsQueryable().Where(x => x.CreatedBy == CreatedBy)

                         join BuildingsInfoybl in _buildingsInfoRepository.GetAllAsQueryable()
                         on RoomInfotbl.BuildingsId equals BuildingsInfoybl.Id into BuildingsInfotable
                         from BuildingsInfoybl2 in BuildingsInfotable.DefaultIfEmpty()

                         select new SelectListItem
                         {
                             Value = RoomInfotbl.Id.ToString(),
                             Text = RoomInfotbl.RoomName + (!string.IsNullOrEmpty(BuildingsInfoybl2.BuildingsName) ? ("(Nhà trọ/ khu trọ: " + BuildingsInfoybl2.BuildingsName + ")") : ""),
                         }).ToList();
            return query;
        }
        public List<SelectListItem> GetDropDownListRoomForAggreement(long? CreatedId, string SelectedValue)
        {
            var query = (from RoomInfotbl in _RoomInfoRepository.GetAllAsQueryable().Where(x => x.CreatedID == CreatedId && x.Status == ItemStatusConstant.DuocPheDuyet)

                         join BuildingsInfoybl in _buildingsInfoRepository.GetAllAsQueryable()
                         on RoomInfotbl.BuildingsId equals BuildingsInfoybl.Id into BuildingsInfotable
                         from BuildingsInfoybl2 in BuildingsInfotable.DefaultIfEmpty()

                         select new SelectListItem
                         {
                             Value = RoomInfotbl.Id.ToString(),
                             Text = RoomInfotbl.RoomName + (!string.IsNullOrEmpty(BuildingsInfoybl2.BuildingsName) ? (" (Nhà trọ/ khu trọ: " + BuildingsInfoybl2.BuildingsName + ")") : ""),
                             Selected = SelectedValue == RoomInfotbl.Id.ToString()
                         }).ToList();
            return query;
        }

        public RoomInfoDto GetDtoById(long id)
        {
            var CurrentDate = DateTime.Now;
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var query = from RoomInfotbl in _RoomInfoRepository.GetAllAsQueryable().Where(x => x.Id == id)

                        select new RoomInfoDto
                        {
                            Id = RoomInfotbl.Id,
                            BuildingsId = RoomInfotbl.BuildingsId,
                            RoomName = RoomInfotbl.RoomName,
                            Floor = RoomInfotbl.Floor,
                            NumberOfPersonPerRoom = RoomInfotbl.NumberOfPersonPerRoom,
                            Price = RoomInfotbl.Price,
                            Description = RoomInfotbl.Description,
                            TinhCode = RoomInfotbl.TinhCode,
                            XaCode = RoomInfotbl.XaCode,
                            HuyenCode = RoomInfotbl.HuyenCode,
                            QuocGiaCode = RoomInfotbl.QuocGiaCode,
                            Address = RoomInfotbl.Address,
                            ServicesCode = RoomInfotbl.ServicesCode,
                            InputWaterElectricFrom = RoomInfotbl.InputWaterElectricFrom,
                            InputWaterElectricTo = RoomInfotbl.InputWaterElectricTo,
                            BillsFrom = RoomInfotbl.BillsFrom,
                            BillsTo = RoomInfotbl.BillsTo,
                            CollectionMoneyFrom = RoomInfotbl.CollectionMoneyFrom,
                            CollectionMoneyTo = RoomInfotbl.CollectionMoneyTo,
                            EndOfConstractFrom = RoomInfotbl.EndOfConstractFrom,
                            EndOfConstractTo = RoomInfotbl.EndOfConstractTo,
                            CreatedDate = RoomInfotbl.CreatedDate,
                            CreatedBy = RoomInfotbl.CreatedBy,
                            UpdatedDate = RoomInfotbl.UpdatedDate,
                            UpdatedBy = RoomInfotbl.UpdatedBy,
                            IsDelete = RoomInfotbl.IsDelete,
                            DeleteTime = RoomInfotbl.DeleteTime,
                            DeleteId = RoomInfotbl.DeleteId,
                            DienTich = RoomInfotbl.DienTich,
                            NumberOfPeoperPerRoomAdult = RoomInfotbl.NumberOfPeoperPerRoomAdult,
                            NumberOfPeoperPerRoomKid = RoomInfotbl.NumberOfPeoperPerRoomKid,
                            Latitude = RoomInfotbl.Latitude,
                            Longitude = RoomInfotbl.Longitude,
                            StreetCode = RoomInfotbl.StreetCode,
                            DuAnId = RoomInfotbl.DuAnId,
                            MucDichSuDung = RoomInfotbl.MucDichSuDung,
                            DonVi = RoomInfotbl.DonVi,
                            HuongNha = RoomInfotbl.HuongNha,
                            HuongBanCong = RoomInfotbl.HuongBanCong,
                            SoPhongNgu = RoomInfotbl.SoPhongNgu,
                            SoToiLet = RoomInfotbl.SoToiLet,
                            NoiThat = RoomInfotbl.NoiThat,
                            ThongTinPhapLy = RoomInfotbl.ThongTinPhapLy,
                            TenLienHe = RoomInfotbl.TenLienHe,
                            DiaChiLienHe = RoomInfotbl.DiaChiLienHe,
                            DienThoai = RoomInfotbl.DienThoai,
                            DiDong = RoomInfotbl.DiDong,
                            Email = RoomInfotbl.Email,
                            Status = RoomInfotbl.Status,
                            CreatedID = RoomInfotbl.CreatedID,
                            UpdatedID = RoomInfotbl.UpdatedID,
                            StatusTin = RoomInfotbl.StatusTin,
                            StartDate = RoomInfotbl.StartDate,
                            EndDate = RoomInfotbl.EndDate,
                            IsSignedOfContact = RoomInfotbl.IsSignedOfContact,
                        };
            var item = query.OrderByDescending(x => x.Id).FirstOrDefault();
            if (item.BuildingsId != null)
            {
                var BuildingsIdName = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.BuildingsId).Select(x => x.BuildingsName).FirstOrDefault();
                if (!string.IsNullOrEmpty(BuildingsIdName))
                {
                    item.BuildingsIdName = BuildingsIdName;
                }
            }
            if (!string.IsNullOrEmpty(item.HuongNha))
            {
                var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuongNhaName))
                {
                    item.HuongNhaName = HuongNhaName;
                }
            }
            if (!string.IsNullOrEmpty(item.HuongBanCong))
            {
                var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuongBanCongName))
                {
                    item.HuongBanCongName = HuongBanCongName;
                }
            }
            if (!string.IsNullOrEmpty(item.MucDichSuDung))
            {
                var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(MucDichSuDungName))
                {
                    item.MucDichSuDungName = MucDichSuDungName;
                }
            }
            var listFileAnh = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Room).ToList();
            if (listFileAnh != null && listFileAnh.Any())
            {
                item.listFileAnh = listFileAnh;
            }
            var TinhName = _tINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == item.TinhCode).Select(x => x.TenTinh).FirstOrDefault();
            if (!string.IsNullOrEmpty(TinhName))
            {
                item.TinhName = TinhName;
            }
            var HuyenName = _hUYENRepository.GetAllAsQueryable().Where(x => x.MaHuyen == item.HuyenCode).Select(x => x.TenHuyen).FirstOrDefault();
            if (!string.IsNullOrEmpty(HuyenName))
            {
                item.HuyenName = HuyenName;
            }
            var XaName = _xARepository.GetAllAsQueryable().Where(x => x.MaXa == item.XaCode).Select(x => x.TenXa).FirstOrDefault();
            if (!string.IsNullOrEmpty(XaName))
            {
                item.XaName = XaName;
            }

            var StreetName = _quanLyDuongPhoRepository.GetAllAsQueryable().Where(x => x.Id.ToString() == item.StreetCode).Select(x => x.TenDuong).FirstOrDefault();
            if (!string.IsNullOrEmpty(StreetName))
            {
                item.StreetName = StreetName;
            }
            var ProjectName = _quanLyDuAnRepository.GetAllAsQueryable().Where(x => x.Id == item.DuAnId).Select(x => x.TenDuAn).FirstOrDefault();
            if (!string.IsNullOrEmpty(ProjectName))
            {
                item.ProjectName = ProjectName;
            }
            return item;
        }

        public List<RoomInfoDto> GetListSearch(string TinhId, string HuyenId, string XaId, decimal? MoneyFrom, decimal? MoneyTo, bool? IsInsideTinh, bool? IsInsideHuyen, bool? IsInsideXa, string TrongSoTinh, string TrongSoHuyen, string TrongSoXa, string TrongSoGia)
        {
            var TinhInfo = _tINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == TinhId).FirstOrDefault();
            var HuyenInfo = _hUYENRepository.GetAllAsQueryable().Where(x => x.MaHuyen == HuyenId).FirstOrDefault();
            var HuyenLongtitude = ConvertLocation(HuyenInfo.Location).Item1;
            var HuyenLattitude = ConvertLocation(HuyenInfo.Location).Item2;
            var XaInfo = _xARepository.GetAllAsQueryable().Where(x => x.MaXa == XaId).FirstOrDefault();
            var XaLongtitude = ConvertLocation(XaInfo.Location).Item1;
            var XaLattitude = ConvertLocation(XaInfo.Location).Item2;
            var TongBinhPhuongTinh = new double();
            var TongBinhPhuongHuyen = new double();
            var TongBinhPhuongXa = new double();
            var TongBinhPhuongGia = new double();
            var MinATinh = new double();
            var MinAHuyen = new double();
            var MinAXa = new double();
            var MinAGia = new double();
            var TinhArr = new List<double>();
            var HuyenArr = new List<double>();
            var XaArr = new List<double>();
            var GiaArr = new List<double>();
            var query = (from RoomInfotbl in _RoomInfoRepository.GetAllAsQueryable().Where(x => ((MoneyFrom != null && MoneyFrom != 0) ? x.Price >= MoneyFrom : true) && ((MoneyTo != null && MoneyTo != 0) ? x.Price <= MoneyTo : true) && x.IsSignedOfContact != true && ((IsInsideTinh == true) ? (x.TinhCode == TinhId) : true) && ((IsInsideHuyen == true) ? (x.HuyenCode == HuyenId) : true) && ((IsInsideXa == true) ? (x.XaCode == XaId) : true) && x.IsSignedOfContact != true)

                         join Tinhtbl in _tINHRepository.GetAllAsQueryable()
                         on RoomInfotbl.TinhCode equals Tinhtbl.MaTinh into Tinhtable2
                         from Tinhtbl2 in Tinhtable2.DefaultIfEmpty()

                         join Huyentbl in _hUYENRepository.GetAllAsQueryable()
                         on RoomInfotbl.HuyenCode equals Huyentbl.MaHuyen into Huyentable2
                         from Huyentbl2 in Huyentable2.DefaultIfEmpty()

                         join Xatbl in _xARepository.GetAllAsQueryable()
                         on RoomInfotbl.XaCode equals Xatbl.MaXa into Xatable2
                         from Xatbl2 in Xatable2.DefaultIfEmpty()

                         select new RoomInfoDto
                         {
                             Id = RoomInfotbl.Id,
                             BuildingsId = RoomInfotbl.BuildingsId,
                             RoomName = RoomInfotbl.RoomName,
                             Floor = RoomInfotbl.Floor,
                             NumberOfPersonPerRoom = RoomInfotbl.NumberOfPersonPerRoom,
                             Price = RoomInfotbl.Price,
                             Description = RoomInfotbl.Description,
                             TinhCode = RoomInfotbl.TinhCode,
                             XaCode = RoomInfotbl.XaCode,
                             HuyenCode = RoomInfotbl.HuyenCode,
                             QuocGiaCode = RoomInfotbl.QuocGiaCode,
                             Address = RoomInfotbl.Address,
                             ServicesCode = RoomInfotbl.ServicesCode,
                             InputWaterElectricFrom = RoomInfotbl.InputWaterElectricFrom,
                             InputWaterElectricTo = RoomInfotbl.InputWaterElectricTo,
                             BillsFrom = RoomInfotbl.BillsFrom,
                             BillsTo = RoomInfotbl.BillsTo,
                             CollectionMoneyFrom = RoomInfotbl.CollectionMoneyFrom,
                             CollectionMoneyTo = RoomInfotbl.CollectionMoneyTo,
                             EndOfConstractFrom = RoomInfotbl.EndOfConstractFrom,
                             EndOfConstractTo = RoomInfotbl.EndOfConstractTo,
                             CreatedDate = RoomInfotbl.CreatedDate,
                             CreatedBy = RoomInfotbl.CreatedBy,
                             UpdatedDate = RoomInfotbl.UpdatedDate,
                             UpdatedBy = RoomInfotbl.UpdatedBy,
                             //ChieuDaiRoom = RoomInfotbl.ChieuDaiRoom,
                             //ChieuRongRoom = RoomInfotbl.ChieuRongRoom,
                             DienTich = RoomInfotbl.DienTich,
                             NumberOfPeoperPerRoomAdult = RoomInfotbl.NumberOfPeoperPerRoomAdult,
                             NumberOfPeoperPerRoomKid = RoomInfotbl.NumberOfPeoperPerRoomKid,
                             TinhItemInfo = Tinhtbl2,
                             HuyenItemInfo = Huyentbl2,
                             XaItemInfo = Xatbl2,
                             Latitude = RoomInfotbl.Latitude,
                             Longitude = RoomInfotbl.Longitude
                         }).OrderByDescending(x => x.Id).ToList();

            foreach (var item in query)
            {
                if (TinhInfo != null && HuyenInfo != null && XaInfo != null && !string.IsNullOrEmpty(HuyenLattitude) && !string.IsNullOrEmpty(HuyenLongtitude) && !string.IsNullOrEmpty(XaLattitude) && !string.IsNullOrEmpty(XaLongtitude))
                {
                    item.DistanceTinh = Distance(TinhInfo.Latitude, TinhInfo.Longitude, item.Latitude, item.Longitude);
                    item.DistanceHuyen = Distance(HuyenLattitude, HuyenLongtitude, item.Latitude, item.Longitude);
                    item.DistanceXa = Distance(XaLattitude, XaLongtitude, item.Latitude, item.Longitude);
                    TongBinhPhuongTinh += Math.Pow(double.Parse(item.DistanceTinh.Replace('.', ',')), 2);
                    TongBinhPhuongHuyen += Math.Pow(double.Parse(item.DistanceHuyen.Replace('.', ',')), 2);
                    TongBinhPhuongXa += Math.Pow(double.Parse(item.DistanceXa.Replace('.', ',')), 2);
                    TongBinhPhuongGia += Math.Pow(Convert.ToDouble((item.Price != null && item.Price != 0) ? item.Price : 0), 2);
                }

                var ListFileActtackment = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Building).ToList();
                if (ListFileActtackment != null && ListFileActtackment.Any())
                {
                    item.ListFileActtackment = ListFileActtackment;
                }
            }
            foreach (var item in query)
            {
                item.ChuanHoaTinhBangHai = double.Parse(item.DistanceTinh.Replace('.', ',')) / TongBinhPhuongTinh;
                item.ChuanHoaHuyenBangHai = double.Parse(item.DistanceHuyen.Replace('.', ',')) / TongBinhPhuongHuyen;
                item.ChuanHoaXaBangHai = double.Parse(item.DistanceXa.Replace('.', ',')) / TongBinhPhuongXa;
                item.ChuanHoaGiaBangHai = (Convert.ToDouble((item.Price != null && item.Price != 0) ? item.Price : 0)) / TongBinhPhuongGia;
                item.ChuanHoaTinhCoTrongSo = item.ChuanHoaTinhBangHai * (!string.IsNullOrEmpty(TrongSoTinh) ? double.Parse(TrongSoTinh.Replace('.', ',')) : 0.5);
                item.ChuanHoaHuyenCoTrongSo = item.ChuanHoaHuyenBangHai * (!string.IsNullOrEmpty(TrongSoHuyen) ? double.Parse(TrongSoHuyen.Replace('.', ',')) : 0.1);
                item.ChuanHoaXaCoTrongSo = item.ChuanHoaXaBangHai * (!string.IsNullOrEmpty(TrongSoXa) ? double.Parse(TrongSoXa.Replace('.', ',')) : 0.1);
                item.ChuanHoaGiaCoTrongSo = item.ChuanHoaGiaBangHai * (!string.IsNullOrEmpty(TrongSoGia) ? double.Parse(TrongSoGia.Replace('.', ',')) : 0.3);
                TinhArr.Add(item.ChuanHoaTinhCoTrongSo);
                HuyenArr.Add(item.ChuanHoaHuyenCoTrongSo);
                XaArr.Add(item.ChuanHoaXaCoTrongSo);
                GiaArr.Add(item.ChuanHoaGiaCoTrongSo);
            }
            MinATinh = (TinhArr != null && TinhArr.Any()) ? TinhArr.Min() : 0;
            MinAHuyen = (HuyenArr != null && HuyenArr.Any()) ? HuyenArr.Min() : 0;
            MinAXa = (XaArr != null && XaArr.Any()) ? XaArr.Min() : 0;
            MinAGia = (GiaArr != null && GiaArr.Any()) ? GiaArr.Min() : 0;
            foreach (var item in query)
            {
                item.SDistance = Math.Sqrt(Math.Pow((MinATinh - item.ChuanHoaTinhCoTrongSo), 2) + Math.Pow((MinAHuyen - item.ChuanHoaHuyenCoTrongSo), 2) + Math.Pow((MinAXa - item.ChuanHoaXaCoTrongSo), 2) + Math.Pow((MinAGia - item.ChuanHoaGiaCoTrongSo), 2));
            }
            return query.OrderBy(x => x.SDistance).ToList();
        }

        public (string, string) ConvertLocation(string location)
        {
            var longtitude = "";
            var lattitude = "";
            if (!string.IsNullOrEmpty(location))
            {
                var parts = location.Split(',').ToList();

                var longtitudepart = parts[0].Split(' ').ToList();
                if (longtitudepart[2].Contains('N'))
                {
                    longtitudepart[2] = longtitudepart[2].Remove(longtitudepart[2].IndexOf("N"));
                    longtitude = (decimal.Parse(longtitudepart[0]) + decimal.Parse(longtitudepart[1]) / 60 + decimal.Parse(longtitudepart[2]) / (60 * 60)).ToString();
                }
                var lattitudepart = parts[1].Split(' ').ToList();
                if (lattitudepart[3].Contains('E'))
                {
                    lattitudepart[3] = lattitudepart[3].Remove(lattitudepart[3].IndexOf("E"));
                    lattitude = (decimal.Parse(lattitudepart[1]) + decimal.Parse(lattitudepart[2]) / 60 + decimal.Parse(lattitudepart[3]) / (60 * 60)).ToString();
                }
            }
            return (longtitude, lattitude);
        }

        public string Distance(string latitude1, string longtitude1, string latitude2, string longtitude2)
        {
            var distance = "";
            if (!string.IsNullOrEmpty(latitude1) && !string.IsNullOrEmpty(longtitude1) && !string.IsNullOrEmpty(latitude2) && !string.IsNullOrEmpty(longtitude2))
            {
                //var radlat1 = Math.PI * double.Parse(latitude1) / 180;
                //var radlat2 = Math.PI * double.Parse(latitude2) / 180;
                //var theta = double.Parse(longtitude1) - double.Parse(longtitude2);
                //var radtheta = Math.PI * theta / 180;
                //var dist = Math.Sin(radlat1) * Math.Sin(radlat2) + Math.Cos(radlat1) * Math.Cos(radlat2) * Math.Cos(radtheta);
                //dist = Math.Acos(dist);
                //dist = dist * 180 / Math.PI;
                //dist = dist * 60 * 1.1515;
                //distance = dist.ToString();
                //radians
                var lat1 = double.Parse(latitude1.Replace('.', ','));
                var lon1 = double.Parse(longtitude1.Replace('.', ','));
                var lat2 = double.Parse(latitude2.Replace('.', ','));
                var lon2 = double.Parse(longtitude2.Replace('.', ','));

                var R = 6371; // km (change this constant to get miles)
                var dLat = (lat2 - lat1) * Math.PI / 180;
                var dLon = (lon2 - lon1) * Math.PI / 180;
                var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                    Math.Cos(lat1 * Math.PI / 180) * Math.Cos(lat2 * Math.PI / 180) *
                    Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
                var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
                var d = R * c;
                //if (d > 1) distance = Math.Round(d) + " km";
                //else if (d <= 1) distance = Math.Round(d * 1000) + " m";
                distance = d.ToString();
            }
            return distance;
        }

        public List<RoomInfoDto> GetListRoomByCreatedID(long? CreatedID)
        {

            var query = (from RoomInfotbl in _RoomInfoRepository.GetAllAsQueryable().Where(x => x.CreatedID == CreatedID)

                         select new RoomInfoDto
                         {
                             Id = RoomInfotbl.Id,
                             BuildingsId = RoomInfotbl.BuildingsId,
                             RoomName = RoomInfotbl.RoomName,
                             Floor = RoomInfotbl.Floor,
                             NumberOfPersonPerRoom = RoomInfotbl.NumberOfPersonPerRoom,
                             Price = RoomInfotbl.Price,
                             Description = RoomInfotbl.Description,
                             TinhCode = RoomInfotbl.TinhCode,
                             XaCode = RoomInfotbl.XaCode,
                             HuyenCode = RoomInfotbl.HuyenCode,
                             QuocGiaCode = RoomInfotbl.QuocGiaCode,
                             Address = RoomInfotbl.Address,
                             ServicesCode = RoomInfotbl.ServicesCode,
                             InputWaterElectricFrom = RoomInfotbl.InputWaterElectricFrom,
                             InputWaterElectricTo = RoomInfotbl.InputWaterElectricTo,
                             BillsFrom = RoomInfotbl.BillsFrom,
                             BillsTo = RoomInfotbl.BillsTo,
                             CollectionMoneyFrom = RoomInfotbl.CollectionMoneyFrom,
                             CollectionMoneyTo = RoomInfotbl.CollectionMoneyTo,
                             EndOfConstractFrom = RoomInfotbl.EndOfConstractFrom,
                             EndOfConstractTo = RoomInfotbl.EndOfConstractTo,
                             CreatedDate = RoomInfotbl.CreatedDate,
                             CreatedBy = RoomInfotbl.CreatedBy,
                             UpdatedDate = RoomInfotbl.UpdatedDate,
                             UpdatedBy = RoomInfotbl.UpdatedBy,
                             //ChieuDaiRoom = RoomInfotbl.ChieuDaiRoom,
                             //ChieuRongRoom = RoomInfotbl.ChieuRongRoom,
                             DienTich = RoomInfotbl.DienTich,
                             NumberOfPeoperPerRoomAdult = RoomInfotbl.NumberOfPeoperPerRoomAdult,
                             NumberOfPeoperPerRoomKid = RoomInfotbl.NumberOfPeoperPerRoomKid,
                             Latitude = RoomInfotbl.Latitude,
                             Longitude = RoomInfotbl.Longitude
                         }).OrderByDescending(x => x.UpdatedDate).ToList();
            foreach (var item in query)
            {
                var BuildingsIdName = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.BuildingsId).Select(x => x.BuildingsName).FirstOrDefault();
                if (!string.IsNullOrEmpty(BuildingsIdName))
                {
                    item.BuildingsIdName = BuildingsIdName;
                }
            }
            return query;
        }


        public List<RoomInfoDto> GetListRoomByTrangChu(int Count)
        {
            var CurrentDate = DateTime.Now;
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var query = (from RoomInfotbl in _RoomInfoRepository.GetAllAsQueryable().Where(x => x.StatusTin == true && x.EndDate >= CurrentDate && x.IsSignedOfContact != true)

                         select new RoomInfoDto
                         {
                             Id = RoomInfotbl.Id,
                             BuildingsId = RoomInfotbl.BuildingsId,
                             RoomName = RoomInfotbl.RoomName,
                             Floor = RoomInfotbl.Floor,
                             NumberOfPersonPerRoom = RoomInfotbl.NumberOfPersonPerRoom,
                             Price = RoomInfotbl.Price,
                             Description = RoomInfotbl.Description,
                             TinhCode = RoomInfotbl.TinhCode,
                             XaCode = RoomInfotbl.XaCode,
                             HuyenCode = RoomInfotbl.HuyenCode,
                             QuocGiaCode = RoomInfotbl.QuocGiaCode,
                             Address = RoomInfotbl.Address,
                             ServicesCode = RoomInfotbl.ServicesCode,
                             InputWaterElectricFrom = RoomInfotbl.InputWaterElectricFrom,
                             InputWaterElectricTo = RoomInfotbl.InputWaterElectricTo,
                             BillsFrom = RoomInfotbl.BillsFrom,
                             BillsTo = RoomInfotbl.BillsTo,
                             CollectionMoneyFrom = RoomInfotbl.CollectionMoneyFrom,
                             CollectionMoneyTo = RoomInfotbl.CollectionMoneyTo,
                             EndOfConstractFrom = RoomInfotbl.EndOfConstractFrom,
                             EndOfConstractTo = RoomInfotbl.EndOfConstractTo,
                             CreatedDate = RoomInfotbl.CreatedDate,
                             CreatedBy = RoomInfotbl.CreatedBy,
                             UpdatedDate = RoomInfotbl.UpdatedDate,
                             UpdatedBy = RoomInfotbl.UpdatedBy,
                             IsDelete = RoomInfotbl.IsDelete,
                             DeleteTime = RoomInfotbl.DeleteTime,
                             DeleteId = RoomInfotbl.DeleteId,
                             DienTich = RoomInfotbl.DienTich,
                             NumberOfPeoperPerRoomAdult = RoomInfotbl.NumberOfPeoperPerRoomAdult,
                             NumberOfPeoperPerRoomKid = RoomInfotbl.NumberOfPeoperPerRoomKid,
                             Latitude = RoomInfotbl.Latitude,
                             Longitude = RoomInfotbl.Longitude,
                             StreetCode = RoomInfotbl.StreetCode,
                             DuAnId = RoomInfotbl.DuAnId,
                             MucDichSuDung = RoomInfotbl.MucDichSuDung,
                             DonVi = RoomInfotbl.DonVi,
                             HuongNha = RoomInfotbl.HuongNha,
                             HuongBanCong = RoomInfotbl.HuongBanCong,
                             SoPhongNgu = RoomInfotbl.SoPhongNgu,
                             SoToiLet = RoomInfotbl.SoToiLet,
                             NoiThat = RoomInfotbl.NoiThat,
                             ThongTinPhapLy = RoomInfotbl.ThongTinPhapLy,
                             TenLienHe = RoomInfotbl.TenLienHe,
                             DiaChiLienHe = RoomInfotbl.DiaChiLienHe,
                             DienThoai = RoomInfotbl.DienThoai,
                             DiDong = RoomInfotbl.DiDong,
                             Email = RoomInfotbl.Email,
                             Status = RoomInfotbl.Status,
                             CreatedID = RoomInfotbl.CreatedID,
                             UpdatedID = RoomInfotbl.UpdatedID,
                             StatusTin = RoomInfotbl.StatusTin,
                             StartDate = RoomInfotbl.StartDate,
                             EndDate = RoomInfotbl.EndDate,
                             IsSignedOfContact = RoomInfotbl.IsSignedOfContact,
                         }).OrderByDescending(x => x.UpdatedDate).ToList();
            foreach (var item in query)
            {
                if (item.BuildingsId != null)
                {
                    var BuildingsIdName = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.BuildingsId).Select(x => x.BuildingsName).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuildingsIdName))
                    {
                        item.BuildingsIdName = BuildingsIdName;
                    }
                }
                if (!string.IsNullOrEmpty(item.HuongNha))
                {
                    var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongNhaName))
                    {
                        item.HuongNhaName = HuongNhaName;
                    }
                }
                if (!string.IsNullOrEmpty(item.HuongBanCong))
                {
                    var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongBanCongName))
                    {
                        item.HuongBanCongName = HuongBanCongName;
                    }
                }
                if (!string.IsNullOrEmpty(item.MucDichSuDung))
                {
                    var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(MucDichSuDungName))
                    {
                        item.MucDichSuDungName = MucDichSuDungName;
                    }
                }
                var listFileAnh = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Room).ToList();
                if (listFileAnh != null && listFileAnh.Any())
                {
                    item.listFileAnh = listFileAnh;
                }
                var TinhName = _tINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == item.TinhCode).Select(x => x.TenTinh).FirstOrDefault();
                if (!string.IsNullOrEmpty(TinhName))
                {
                    item.TinhName = TinhName;
                }
                var HuyenName = _hUYENRepository.GetAllAsQueryable().Where(x => x.MaHuyen == item.HuyenCode).Select(x => x.TenHuyen).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuyenName))
                {
                    item.HuyenName = HuyenName;
                }
                var XaName = _xARepository.GetAllAsQueryable().Where(x => x.MaXa == item.XaCode).Select(x => x.TenXa).FirstOrDefault();
                if (!string.IsNullOrEmpty(XaName))
                {
                    item.XaName = XaName;
                }

                var StreetName = _quanLyDuongPhoRepository.GetAllAsQueryable().Where(x => x.Id.ToString() == item.StreetCode).Select(x => x.TenDuong).FirstOrDefault();
                if (!string.IsNullOrEmpty(StreetName))
                {
                    item.StreetName = StreetName;
                }
                var ProjectName = _quanLyDuAnRepository.GetAllAsQueryable().Where(x => x.Id == item.DuAnId).Select(x => x.TenDuAn).FirstOrDefault();
                if (!string.IsNullOrEmpty(ProjectName))
                {
                    item.ProjectName = ProjectName;
                }
            }
            return query;
        }


        public List<RoomInfoDto> GetListRoomSearch(string TinhId, string HuyenId, string XaId, decimal? MoneyFrom, decimal? MoneyTo)
        {
            var CurrentDate = DateTime.Now;
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var query = (from RoomInfotbl in _RoomInfoRepository.GetAllAsQueryable().Where(x => x.StatusTin == true && x.EndDate >= CurrentDate && x.IsSignedOfContact != true && x.TinhCode == TinhId && (!string.IsNullOrEmpty(HuyenId) ? x.HuyenCode == HuyenId : true) && (!string.IsNullOrEmpty(XaId) ? x.XaCode == XaId : true) && (MoneyFrom != null ? x.Price >= MoneyFrom : true) && (MoneyTo != null ? x.Price <= MoneyTo : true))

                         select new RoomInfoDto
                         {
                             Id = RoomInfotbl.Id,
                             BuildingsId = RoomInfotbl.BuildingsId,
                             RoomName = RoomInfotbl.RoomName,
                             Floor = RoomInfotbl.Floor,
                             NumberOfPersonPerRoom = RoomInfotbl.NumberOfPersonPerRoom,
                             Price = RoomInfotbl.Price,
                             Description = RoomInfotbl.Description,
                             TinhCode = RoomInfotbl.TinhCode,
                             XaCode = RoomInfotbl.XaCode,
                             HuyenCode = RoomInfotbl.HuyenCode,
                             QuocGiaCode = RoomInfotbl.QuocGiaCode,
                             Address = RoomInfotbl.Address,
                             ServicesCode = RoomInfotbl.ServicesCode,
                             InputWaterElectricFrom = RoomInfotbl.InputWaterElectricFrom,
                             InputWaterElectricTo = RoomInfotbl.InputWaterElectricTo,
                             BillsFrom = RoomInfotbl.BillsFrom,
                             BillsTo = RoomInfotbl.BillsTo,
                             CollectionMoneyFrom = RoomInfotbl.CollectionMoneyFrom,
                             CollectionMoneyTo = RoomInfotbl.CollectionMoneyTo,
                             EndOfConstractFrom = RoomInfotbl.EndOfConstractFrom,
                             EndOfConstractTo = RoomInfotbl.EndOfConstractTo,
                             CreatedDate = RoomInfotbl.CreatedDate,
                             CreatedBy = RoomInfotbl.CreatedBy,
                             UpdatedDate = RoomInfotbl.UpdatedDate,
                             UpdatedBy = RoomInfotbl.UpdatedBy,
                             IsDelete = RoomInfotbl.IsDelete,
                             DeleteTime = RoomInfotbl.DeleteTime,
                             DeleteId = RoomInfotbl.DeleteId,
                             DienTich = RoomInfotbl.DienTich,
                             NumberOfPeoperPerRoomAdult = RoomInfotbl.NumberOfPeoperPerRoomAdult,
                             NumberOfPeoperPerRoomKid = RoomInfotbl.NumberOfPeoperPerRoomKid,
                             Latitude = RoomInfotbl.Latitude,
                             Longitude = RoomInfotbl.Longitude,
                             StreetCode = RoomInfotbl.StreetCode,
                             DuAnId = RoomInfotbl.DuAnId,
                             MucDichSuDung = RoomInfotbl.MucDichSuDung,
                             DonVi = RoomInfotbl.DonVi,
                             HuongNha = RoomInfotbl.HuongNha,
                             HuongBanCong = RoomInfotbl.HuongBanCong,
                             SoPhongNgu = RoomInfotbl.SoPhongNgu,
                             SoToiLet = RoomInfotbl.SoToiLet,
                             NoiThat = RoomInfotbl.NoiThat,
                             ThongTinPhapLy = RoomInfotbl.ThongTinPhapLy,
                             TenLienHe = RoomInfotbl.TenLienHe,
                             DiaChiLienHe = RoomInfotbl.DiaChiLienHe,
                             DienThoai = RoomInfotbl.DienThoai,
                             DiDong = RoomInfotbl.DiDong,
                             Email = RoomInfotbl.Email,
                             Status = RoomInfotbl.Status,
                             CreatedID = RoomInfotbl.CreatedID,
                             UpdatedID = RoomInfotbl.UpdatedID,
                             StatusTin = RoomInfotbl.StatusTin,
                             StartDate = RoomInfotbl.StartDate,
                             EndDate = RoomInfotbl.EndDate,
                             IsSignedOfContact = RoomInfotbl.IsSignedOfContact,
                         }).OrderByDescending(x => x.UpdatedDate).ToList();
            foreach (var item in query)
            {
                if (item.BuildingsId != null)
                {
                    var BuildingsIdName = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.BuildingsId).Select(x => x.BuildingsName).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuildingsIdName))
                    {
                        item.BuildingsIdName = BuildingsIdName;
                    }
                }
                if (!string.IsNullOrEmpty(item.HuongNha))
                {
                    var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongNhaName))
                    {
                        item.HuongNhaName = HuongNhaName;
                    }
                }
                if (!string.IsNullOrEmpty(item.HuongBanCong))
                {
                    var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongBanCongName))
                    {
                        item.HuongBanCongName = HuongBanCongName;
                    }
                }
                if (!string.IsNullOrEmpty(item.MucDichSuDung))
                {
                    var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(MucDichSuDungName))
                    {
                        item.MucDichSuDungName = MucDichSuDungName;
                    }
                }
                var listFileAnh = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Room).ToList();
                if (listFileAnh != null && listFileAnh.Any())
                {
                    item.listFileAnh = listFileAnh;
                }
                var TinhName = _tINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == item.TinhCode).Select(x => x.TenTinh).FirstOrDefault();
                if (!string.IsNullOrEmpty(TinhName))
                {
                    item.TinhName = TinhName;
                }
                var HuyenName = _hUYENRepository.GetAllAsQueryable().Where(x => x.MaHuyen == item.HuyenCode).Select(x => x.TenHuyen).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuyenName))
                {
                    item.HuyenName = HuyenName;
                }
                var XaName = _xARepository.GetAllAsQueryable().Where(x => x.MaXa == item.XaCode).Select(x => x.TenXa).FirstOrDefault();
                if (!string.IsNullOrEmpty(XaName))
                {
                    item.XaName = XaName;
                }

                var StreetName = _quanLyDuongPhoRepository.GetAllAsQueryable().Where(x => x.Id.ToString() == item.StreetCode).Select(x => x.TenDuong).FirstOrDefault();
                if (!string.IsNullOrEmpty(StreetName))
                {
                    item.StreetName = StreetName;
                }
                var ProjectName = _quanLyDuAnRepository.GetAllAsQueryable().Where(x => x.Id == item.DuAnId).Select(x => x.TenDuAn).FirstOrDefault();
                if (!string.IsNullOrEmpty(ProjectName))
                {
                    item.ProjectName = ProjectName;
                }
            }
            return query;
        }



        public PageListResultBO<RoomInfoDto> GetDaTaByPageByTinhCode(string tinhCode, string KeySearch, int pageIndex = 1, int pageSize = 10)
        {
            var CurrentDate = DateTime.Now;
            var query = from RoomInfotbl in _RoomInfoRepository.GetAllAsQueryable().Where(x => x.TinhCode == tinhCode && x.StatusTin == true && x.EndDate >= CurrentDate && x.IsSignedOfContact != true)

                        select new RoomInfoDto
                        {
                            Id = RoomInfotbl.Id,
                            BuildingsId = RoomInfotbl.BuildingsId,
                            RoomName = RoomInfotbl.RoomName,
                            Floor = RoomInfotbl.Floor,
                            NumberOfPersonPerRoom = RoomInfotbl.NumberOfPersonPerRoom,
                            Price = RoomInfotbl.Price,
                            Description = RoomInfotbl.Description,
                            TinhCode = RoomInfotbl.TinhCode,
                            XaCode = RoomInfotbl.XaCode,
                            HuyenCode = RoomInfotbl.HuyenCode,
                            QuocGiaCode = RoomInfotbl.QuocGiaCode,
                            Address = RoomInfotbl.Address,
                            ServicesCode = RoomInfotbl.ServicesCode,
                            InputWaterElectricFrom = RoomInfotbl.InputWaterElectricFrom,
                            InputWaterElectricTo = RoomInfotbl.InputWaterElectricTo,
                            BillsFrom = RoomInfotbl.BillsFrom,
                            BillsTo = RoomInfotbl.BillsTo,
                            CollectionMoneyFrom = RoomInfotbl.CollectionMoneyFrom,
                            CollectionMoneyTo = RoomInfotbl.CollectionMoneyTo,
                            EndOfConstractFrom = RoomInfotbl.EndOfConstractFrom,
                            EndOfConstractTo = RoomInfotbl.EndOfConstractTo,
                            CreatedDate = RoomInfotbl.CreatedDate,
                            CreatedBy = RoomInfotbl.CreatedBy,
                            UpdatedDate = RoomInfotbl.UpdatedDate,
                            UpdatedBy = RoomInfotbl.UpdatedBy,
                            IsDelete = RoomInfotbl.IsDelete,
                            DeleteTime = RoomInfotbl.DeleteTime,
                            DeleteId = RoomInfotbl.DeleteId,
                            DienTich = RoomInfotbl.DienTich,
                            NumberOfPeoperPerRoomAdult = RoomInfotbl.NumberOfPeoperPerRoomAdult,
                            NumberOfPeoperPerRoomKid = RoomInfotbl.NumberOfPeoperPerRoomKid,
                            Latitude = RoomInfotbl.Latitude,
                            Longitude = RoomInfotbl.Longitude,
                            StreetCode = RoomInfotbl.StreetCode,
                            DuAnId = RoomInfotbl.DuAnId,
                            MucDichSuDung = RoomInfotbl.MucDichSuDung,
                            DonVi = RoomInfotbl.DonVi,
                            HuongNha = RoomInfotbl.HuongNha,
                            HuongBanCong = RoomInfotbl.HuongBanCong,
                            SoPhongNgu = RoomInfotbl.SoPhongNgu,
                            SoToiLet = RoomInfotbl.SoToiLet,
                            NoiThat = RoomInfotbl.NoiThat,
                            ThongTinPhapLy = RoomInfotbl.ThongTinPhapLy,
                            TenLienHe = RoomInfotbl.TenLienHe,
                            DiaChiLienHe = RoomInfotbl.DiaChiLienHe,
                            DienThoai = RoomInfotbl.DienThoai,
                            DiDong = RoomInfotbl.DiDong,
                            Email = RoomInfotbl.Email,
                            Status = RoomInfotbl.Status,
                            CreatedID = RoomInfotbl.CreatedID,
                            UpdatedID = RoomInfotbl.UpdatedID,
                            StatusTin = RoomInfotbl.StatusTin,
                            StartDate = RoomInfotbl.StartDate,
                            EndDate = RoomInfotbl.EndDate,
                            IsSignedOfContact = RoomInfotbl.IsSignedOfContact,
                        };

            if (!string.IsNullOrEmpty(KeySearch))
            {
                query = query.Where(x => x.RoomName.Contains(KeySearch) || x.CreatedBy.Contains(KeySearch));
            }
            query = query.OrderByDescending(x => x.UpdatedDate);

            var resultmodel = new PageListResultBO<RoomInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
                resultmodel.CurrentPage = 1;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
                resultmodel.CurrentPage = pageIndex;
            }
            foreach (var item in resultmodel.ListItem)
            {
                var BuildingsIdName = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.BuildingsId).Select(x => x.BuildingsName).FirstOrDefault();
                if (!string.IsNullOrEmpty(BuildingsIdName))
                {
                    item.BuildingsIdName = BuildingsIdName;
                }
                var listFileAnh = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Room).ToList();
                if (listFileAnh != null && listFileAnh.Any())
                {
                    item.listFileAnh = listFileAnh;
                }
                var TinhName = _tINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == item.TinhCode).Select(x => x.TenTinh).FirstOrDefault();
                if (!string.IsNullOrEmpty(TinhName))
                {
                    item.TinhName = TinhName;
                }
                var HuyenName = _hUYENRepository.GetAllAsQueryable().Where(x => x.MaHuyen == item.HuyenCode).Select(x => x.TenHuyen).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuyenName))
                {
                    item.HuyenName = HuyenName;
                }
                var XaName = _xARepository.GetAllAsQueryable().Where(x => x.MaXa == item.XaCode).Select(x => x.TenXa).FirstOrDefault();
                if (!string.IsNullOrEmpty(XaName))
                {
                    item.XaName = XaName;
                }

                var StreetName = _quanLyDuongPhoRepository.GetAllAsQueryable().Where(x => x.Id.ToString() == item.StreetCode).Select(x => x.TenDuong).FirstOrDefault();
                if (!string.IsNullOrEmpty(StreetName))
                {
                    item.StreetName = StreetName;
                }
                var ProjectName = _quanLyDuAnRepository.GetAllAsQueryable().Where(x => x.Id == item.DuAnId).Select(x => x.TenDuAn).FirstOrDefault();
                if (!string.IsNullOrEmpty(ProjectName))
                {
                    item.ProjectName = ProjectName;
                }
            }
            return resultmodel;
        }
        public PageListResultBO<RoomInfoDto> GetDaTaByPageByDuAnId(long? duanId, string KeySearch, int pageIndex = 1, int pageSize = 10)
        {
            var CurrentDate = DateTime.Now;
            var query = from RoomInfotbl in _RoomInfoRepository.GetAllAsQueryable().Where(x => x.DuAnId == duanId && x.StatusTin == true && x.EndDate >= CurrentDate && x.IsSignedOfContact != true)

                        select new RoomInfoDto
                        {
                            Id = RoomInfotbl.Id,
                            BuildingsId = RoomInfotbl.BuildingsId,
                            RoomName = RoomInfotbl.RoomName,
                            Floor = RoomInfotbl.Floor,
                            NumberOfPersonPerRoom = RoomInfotbl.NumberOfPersonPerRoom,
                            Price = RoomInfotbl.Price,
                            Description = RoomInfotbl.Description,
                            TinhCode = RoomInfotbl.TinhCode,
                            XaCode = RoomInfotbl.XaCode,
                            HuyenCode = RoomInfotbl.HuyenCode,
                            QuocGiaCode = RoomInfotbl.QuocGiaCode,
                            Address = RoomInfotbl.Address,
                            ServicesCode = RoomInfotbl.ServicesCode,
                            InputWaterElectricFrom = RoomInfotbl.InputWaterElectricFrom,
                            InputWaterElectricTo = RoomInfotbl.InputWaterElectricTo,
                            BillsFrom = RoomInfotbl.BillsFrom,
                            BillsTo = RoomInfotbl.BillsTo,
                            CollectionMoneyFrom = RoomInfotbl.CollectionMoneyFrom,
                            CollectionMoneyTo = RoomInfotbl.CollectionMoneyTo,
                            EndOfConstractFrom = RoomInfotbl.EndOfConstractFrom,
                            EndOfConstractTo = RoomInfotbl.EndOfConstractTo,
                            CreatedDate = RoomInfotbl.CreatedDate,
                            CreatedBy = RoomInfotbl.CreatedBy,
                            UpdatedDate = RoomInfotbl.UpdatedDate,
                            UpdatedBy = RoomInfotbl.UpdatedBy,
                            IsDelete = RoomInfotbl.IsDelete,
                            DeleteTime = RoomInfotbl.DeleteTime,
                            DeleteId = RoomInfotbl.DeleteId,
                            DienTich = RoomInfotbl.DienTich,
                            NumberOfPeoperPerRoomAdult = RoomInfotbl.NumberOfPeoperPerRoomAdult,
                            NumberOfPeoperPerRoomKid = RoomInfotbl.NumberOfPeoperPerRoomKid,
                            Latitude = RoomInfotbl.Latitude,
                            Longitude = RoomInfotbl.Longitude,
                            StreetCode = RoomInfotbl.StreetCode,
                            DuAnId = RoomInfotbl.DuAnId,
                            MucDichSuDung = RoomInfotbl.MucDichSuDung,
                            DonVi = RoomInfotbl.DonVi,
                            HuongNha = RoomInfotbl.HuongNha,
                            HuongBanCong = RoomInfotbl.HuongBanCong,
                            SoPhongNgu = RoomInfotbl.SoPhongNgu,
                            SoToiLet = RoomInfotbl.SoToiLet,
                            NoiThat = RoomInfotbl.NoiThat,
                            ThongTinPhapLy = RoomInfotbl.ThongTinPhapLy,
                            TenLienHe = RoomInfotbl.TenLienHe,
                            DiaChiLienHe = RoomInfotbl.DiaChiLienHe,
                            DienThoai = RoomInfotbl.DienThoai,
                            DiDong = RoomInfotbl.DiDong,
                            Email = RoomInfotbl.Email,
                            Status = RoomInfotbl.Status,
                            CreatedID = RoomInfotbl.CreatedID,
                            UpdatedID = RoomInfotbl.UpdatedID,
                            StatusTin = RoomInfotbl.StatusTin,
                            StartDate = RoomInfotbl.StartDate,
                            EndDate = RoomInfotbl.EndDate,
                            IsSignedOfContact = RoomInfotbl.IsSignedOfContact,
                        };

            if (!string.IsNullOrEmpty(KeySearch))
            {
                query = query.Where(x => x.RoomName.Contains(KeySearch) || x.CreatedBy.Contains(KeySearch));
            }
            query = query.OrderByDescending(x => x.UpdatedDate);

            var resultmodel = new PageListResultBO<RoomInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
                resultmodel.CurrentPage = 1;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
                resultmodel.CurrentPage = pageIndex;
            }
            foreach (var item in resultmodel.ListItem)
            {
                var BuildingsIdName = _buildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == item.BuildingsId).Select(x => x.BuildingsName).FirstOrDefault();
                if (!string.IsNullOrEmpty(BuildingsIdName))
                {
                    item.BuildingsIdName = BuildingsIdName;
                }
                var listFileAnh = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Room).ToList();
                if (listFileAnh != null && listFileAnh.Any())
                {
                    item.listFileAnh = listFileAnh;
                }
                var TinhName = _tINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == item.TinhCode).Select(x => x.TenTinh).FirstOrDefault();
                if (!string.IsNullOrEmpty(TinhName))
                {
                    item.TinhName = TinhName;
                }
                var HuyenName = _hUYENRepository.GetAllAsQueryable().Where(x => x.MaHuyen == item.HuyenCode).Select(x => x.TenHuyen).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuyenName))
                {
                    item.HuyenName = HuyenName;
                }
                var XaName = _xARepository.GetAllAsQueryable().Where(x => x.MaXa == item.XaCode).Select(x => x.TenXa).FirstOrDefault();
                if (!string.IsNullOrEmpty(XaName))
                {
                    item.XaName = XaName;
                }

                var StreetName = _quanLyDuongPhoRepository.GetAllAsQueryable().Where(x => x.Id.ToString() == item.StreetCode).Select(x => x.TenDuong).FirstOrDefault();
                if (!string.IsNullOrEmpty(StreetName))
                {
                    item.StreetName = StreetName;
                }
                var ProjectName = _quanLyDuAnRepository.GetAllAsQueryable().Where(x => x.Id == item.DuAnId).Select(x => x.TenDuAn).FirstOrDefault();
                if (!string.IsNullOrEmpty(ProjectName))
                {
                    item.ProjectName = ProjectName;
                }
            }
            return resultmodel;
        }
    }
}
