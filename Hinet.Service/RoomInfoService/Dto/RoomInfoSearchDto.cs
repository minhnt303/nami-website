using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.RoomInfoService.Dto
{
    public class RoomInfoSearchDto : SearchBase
    {
        public long? BuildingsNameFilter { get; set; }
    }
}