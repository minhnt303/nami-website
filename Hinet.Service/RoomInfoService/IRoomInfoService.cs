using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.RoomInfoService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nami.Service.RoomInfoService
{
    public interface IRoomInfoService:IEntityService<RoomInfo>
    {
        PageListResultBO<RoomInfoDto> GetDaTaByPage(RoomInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        RoomInfo GetById(long id);
        List<RoomInfo> GetListRoomByCurrentCreateAndBuildingId(string CurrentCreateBy, long BuildingId);
        PageListResultBO<RoomInfoDto> GetDaTaByPageByCreatedBy(string CreatedBy, RoomInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        List<SelectListItem> GetDropDownListRoom(string CreatedBy);
        List<SelectListItem> GetDropDownListRoomForAggreement(long? CreatedId, string SelectedValue);
        RoomInfoDto GetDtoById(long id);
        List<RoomInfoDto> GetListRoomByTrangChu(int Count);
        List<RoomInfoDto> GetListRoomByCreatedID(long? CreatedID);
        List<RoomInfoDto> GetListSearch(string TinhId, string HuyenId, string XaId, decimal? MoneyFrom, decimal? MoneyTo, bool? IsInsideTinh, bool? IsInsideHuyen, bool? IsInsideXa, string TrongSoTinh, string TrongSoHuyen, string TrongSoXa, string TrongSoGia);
        List<RoomInfoDto> GetListRoomSearch(string TinhId, string HuyenId, string XaId, decimal? MoneyFrom, decimal? MoneyTo);
        PageListResultBO<RoomInfoDto> GetDaTaByPageByTinhCode(string tinhCode, string KeySearch, int pageIndex = 1, int pageSize = 10);
        PageListResultBO<RoomInfoDto> GetDaTaByPageByDuAnId(long? duanId, string KeySearch, int pageIndex = 1, int pageSize = 10);
    }
}
