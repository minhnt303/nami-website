using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.BuildingsInfoRepository;
using Nami.Repository.DanhmucRepository;
using Nami.Repository.FileDinhKemRepository;
using Nami.Repository.HUYENRepository;
using Nami.Repository.QLHopDongDangKyRepository;
using Nami.Repository.QuanLyDuAnRepository;
using Nami.Repository.QuanLyDuongPhoRepository;
using Nami.Repository.RoomInfoRepository;
using Nami.Repository.TINHRepository;
using Nami.Repository.XARepository;
using Nami.Service.BuildingsInfoService.Dto;
using Nami.Service.Common;
using Nami.Service.Constant;
using log4net;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;
namespace Nami.Service.BuildingsInfoService
{
    public class BuildingsInfoService : EntityService<BuildingsInfo>, IBuildingsInfoService
    {
        IUnitOfWork _unitOfWork;
        IBuildingsInfoRepository _BuildingsInfoRepository;
        ILog _loger;
        IMapper _mapper;
        IRoomInfoRepository _roomInfoRepository;
        ITINHRepository _tINHRepository;
        IHUYENRepository _hUYENRepository;
        IXARepository _xARepository;
        IFileDinhKemRepository _fileDinhKemRepository;
        IDM_DulieuDanhmucRepository _dM_DulieuDanhmucRepository;
        IDM_NhomDanhmucRepository _dM_NhomDanhmucRepository;
        IQuanLyDuongPhoRepository _quanLyDuongPhoRepository;
        IQuanLyDuAnRepository _quanLyDuAnRepository;
        IQLHopDongDangKyRepository _QLHopDongDangKyRepository;
        //ISTREETRepository _sTREETRepository;
        //IPROJECTRepository _pROJECTRepository;

        public BuildingsInfoService(IUnitOfWork unitOfWork,
        IBuildingsInfoRepository BuildingsInfoRepository,
        ILog loger,
        IRoomInfoRepository roomInfoRepository,
        ITINHRepository tINHRepository,
        IHUYENRepository hUYENRepository,
        IXARepository xARepository,
        IFileDinhKemRepository fileDinhKemRepository,
        IDM_DulieuDanhmucRepository dM_DulieuDanhmucRepository,
        IDM_NhomDanhmucRepository dM_NhomDanhmucRepository,
        IQuanLyDuongPhoRepository quanLyDuongPhoRepository,
        IQuanLyDuAnRepository quanLyDuAnRepository,
        IQLHopDongDangKyRepository QLHopDongDangKyRepository,
        //ISTREETRepository sTREETRepository,
        //IPROJECTRepository pROJECTRepository,
                IMapper mapper
            )
            : base(unitOfWork, BuildingsInfoRepository)
        {
            //_sTREETRepository = sTREETRepository;
            _dM_NhomDanhmucRepository = dM_NhomDanhmucRepository;
            _dM_DulieuDanhmucRepository = dM_DulieuDanhmucRepository;
            _unitOfWork = unitOfWork;
            _BuildingsInfoRepository = BuildingsInfoRepository;
            _loger = loger;
            _mapper = mapper;
            _roomInfoRepository = roomInfoRepository;
            _tINHRepository = tINHRepository;
            _hUYENRepository = hUYENRepository;
            _xARepository = xARepository;
            _fileDinhKemRepository = fileDinhKemRepository;
            _quanLyDuongPhoRepository = quanLyDuongPhoRepository;
            _quanLyDuAnRepository = quanLyDuAnRepository;
            _QLHopDongDangKyRepository = QLHopDongDangKyRepository;
            //_pROJECTRepository = pROJECTRepository;
        }

        public PageListResultBO<BuildingsInfoDto> GetDaTaByPage(BuildingsInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var query = from BuildingsInfotbl in _BuildingsInfoRepository.GetAllAsQueryable()

                        select new BuildingsInfoDto
                        {
                            //ChieuDaiRoom = BuildingsInfotbl.ChieuDaiRoom,
                            //ChieuRongRoom = BuildingsInfotbl.ChieuRongRoom,
                            Id = BuildingsInfotbl.Id,
                            BuildingsName = BuildingsInfotbl.BuildingsName,
                            Address = BuildingsInfotbl.Address,
                            Description = BuildingsInfotbl.Description,
                            ServicesCode = BuildingsInfotbl.ServicesCode,
                            InputWaterElectricFrom = BuildingsInfotbl.InputWaterElectricFrom,
                            InputWaterElectricTo = BuildingsInfotbl.InputWaterElectricTo,
                            BillsFrom = BuildingsInfotbl.BillsFrom,
                            BillsTo = BuildingsInfotbl.BillsTo,
                            CollectionMoneyFrom = BuildingsInfotbl.CollectionMoneyFrom,
                            CollectionMoneyTo = BuildingsInfotbl.CollectionMoneyTo,
                            EndOfConstractFrom = BuildingsInfotbl.EndOfConstractFrom,
                            EndOfConstractTo = BuildingsInfotbl.EndOfConstractTo,
                            TinhCode = BuildingsInfotbl.TinhCode,
                            XaCode = BuildingsInfotbl.XaCode,
                            HuyenCode = BuildingsInfotbl.HuyenCode,
                            QuocGiaCode = BuildingsInfotbl.QuocGiaCode,
                            CreatedDate = BuildingsInfotbl.CreatedDate,
                            CreatedBy = BuildingsInfotbl.CreatedBy,
                            UpdatedDate = BuildingsInfotbl.UpdatedDate,
                            UpdatedBy = BuildingsInfotbl.UpdatedBy,
                            Floor = BuildingsInfotbl.Floor,
                            NumberOfRoom = BuildingsInfotbl.NumberOfRoom,
                            NumberOfPeoperPerRoomAdult = BuildingsInfotbl.NumberOfPeoperPerRoomAdult,
                            NumberOfPeoperPerRoomKid = BuildingsInfotbl.NumberOfPeoperPerRoomKid,
                            Longitude = BuildingsInfotbl.Longitude,
                            Latitude = BuildingsInfotbl.Latitude,
                            Price = BuildingsInfotbl.Price,
                            IsSignedOfContact = BuildingsInfotbl.IsSignedOfContact,
                            IsDelete = BuildingsInfotbl.IsDelete,
                            DeleteTime = BuildingsInfotbl.DeleteTime,
                            DeleteId = BuildingsInfotbl.DeleteId,
                            StreetCode = BuildingsInfotbl.StreetCode,
                            DuAnId = BuildingsInfotbl.DuAnId,
                            MucDichSuDung = BuildingsInfotbl.MucDichSuDung,
                            DonVi = BuildingsInfotbl.DonVi,
                            HuongNha = BuildingsInfotbl.HuongNha,
                            HuongBanCong = BuildingsInfotbl.HuongBanCong,
                            SoPhongNgu = BuildingsInfotbl.SoPhongNgu,
                            SoToiLet = BuildingsInfotbl.SoToiLet,
                            NoiThat = BuildingsInfotbl.NoiThat,
                            ThongTinPhapLy = BuildingsInfotbl.ThongTinPhapLy,
                            TenLienHe = BuildingsInfotbl.TenLienHe,
                            DiaChiLienHe = BuildingsInfotbl.DiaChiLienHe,
                            DienThoai = BuildingsInfotbl.DienThoai,
                            DiDong = BuildingsInfotbl.DiDong,
                            Email = BuildingsInfotbl.Email,
                            Status = BuildingsInfotbl.Status,
                            DienTich = BuildingsInfotbl.DienTich,
                            CreatedID = BuildingsInfotbl.CreatedID,
                            UpdatedID = BuildingsInfotbl.UpdatedID,
                            StatusTin = BuildingsInfotbl.StatusTin,
                            IsCreateRoom = BuildingsInfotbl.IsCreateRoom,
                            StartDate = BuildingsInfotbl.StartDate,
                            EndDate = BuildingsInfotbl.EndDate,
                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.BuildingsNameFilter))
                {
                    query = query.Where(x => x.BuildingsName.Contains(searchModel.BuildingsNameFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.AddressFilter))
                {
                    query = query.Where(x => x.Address.Contains(searchModel.AddressFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.ServicesCodeFilter))
                {
                    query = query.Where(x => x.ServicesCode.Contains(searchModel.ServicesCodeFilter));
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<BuildingsInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            foreach (var item in resultmodel.ListItem)
            {
                if (!string.IsNullOrEmpty(item.HuongNha))
                {
                    var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongNhaName))
                    {
                        item.HuongNhaName = HuongNhaName;
                    }
                }
                if (!string.IsNullOrEmpty(item.HuongBanCong))
                {
                    var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongBanCongName))
                    {
                        item.HuongBanCongName = HuongBanCongName;
                    }
                }
                if (!string.IsNullOrEmpty(item.MucDichSuDung))
                {
                    var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(MucDichSuDungName))
                    {
                        item.MucDichSuDungName = MucDichSuDungName;
                    }
                }
            }
            return resultmodel;
        }

        public PageListResultBO<BuildingsInfoDto> GetDaTaByPageByCreatedId(bool IsViewAll, long? CreatedID, BuildingsInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var CurrentDate = DateTime.Now;
            var query = from BuildingsInfotbl in _BuildingsInfoRepository.GetAllAsQueryable().Where(x => (IsViewAll != true ? x.CreatedID == CreatedID : x.Status != ItemStatusConstant.MoiTao))

                        select new BuildingsInfoDto
                        {
                            //ChieuDaiRoom = BuildingsInfotbl.ChieuDaiRoom,
                            //ChieuRongRoom = BuildingsInfotbl.ChieuRongRoom,
                            Id = BuildingsInfotbl.Id,
                            BuildingsName = BuildingsInfotbl.BuildingsName,
                            Address = BuildingsInfotbl.Address,
                            Description = BuildingsInfotbl.Description,
                            ServicesCode = BuildingsInfotbl.ServicesCode,
                            InputWaterElectricFrom = BuildingsInfotbl.InputWaterElectricFrom,
                            InputWaterElectricTo = BuildingsInfotbl.InputWaterElectricTo,
                            BillsFrom = BuildingsInfotbl.BillsFrom,
                            BillsTo = BuildingsInfotbl.BillsTo,
                            CollectionMoneyFrom = BuildingsInfotbl.CollectionMoneyFrom,
                            CollectionMoneyTo = BuildingsInfotbl.CollectionMoneyTo,
                            EndOfConstractFrom = BuildingsInfotbl.EndOfConstractFrom,
                            EndOfConstractTo = BuildingsInfotbl.EndOfConstractTo,
                            TinhCode = BuildingsInfotbl.TinhCode,
                            XaCode = BuildingsInfotbl.XaCode,
                            HuyenCode = BuildingsInfotbl.HuyenCode,
                            QuocGiaCode = BuildingsInfotbl.QuocGiaCode,
                            CreatedDate = BuildingsInfotbl.CreatedDate,
                            CreatedBy = BuildingsInfotbl.CreatedBy,
                            UpdatedDate = BuildingsInfotbl.UpdatedDate,
                            UpdatedBy = BuildingsInfotbl.UpdatedBy,
                            Floor = BuildingsInfotbl.Floor,
                            NumberOfRoom = BuildingsInfotbl.NumberOfRoom,
                            NumberOfPeoperPerRoomAdult = BuildingsInfotbl.NumberOfPeoperPerRoomAdult,
                            NumberOfPeoperPerRoomKid = BuildingsInfotbl.NumberOfPeoperPerRoomKid,
                            Longitude = BuildingsInfotbl.Longitude,
                            Latitude = BuildingsInfotbl.Latitude,
                            Price = BuildingsInfotbl.Price,
                            IsSignedOfContact = BuildingsInfotbl.IsSignedOfContact,
                            IsDelete = BuildingsInfotbl.IsDelete,
                            DeleteTime = BuildingsInfotbl.DeleteTime,
                            DeleteId = BuildingsInfotbl.DeleteId,
                            StreetCode = BuildingsInfotbl.StreetCode,
                            DuAnId = BuildingsInfotbl.DuAnId,
                            MucDichSuDung = BuildingsInfotbl.MucDichSuDung,
                            DonVi = BuildingsInfotbl.DonVi,
                            HuongNha = BuildingsInfotbl.HuongNha,
                            HuongBanCong = BuildingsInfotbl.HuongBanCong,
                            SoPhongNgu = BuildingsInfotbl.SoPhongNgu,
                            SoToiLet = BuildingsInfotbl.SoToiLet,
                            NoiThat = BuildingsInfotbl.NoiThat,
                            ThongTinPhapLy = BuildingsInfotbl.ThongTinPhapLy,
                            TenLienHe = BuildingsInfotbl.TenLienHe,
                            DiaChiLienHe = BuildingsInfotbl.DiaChiLienHe,
                            DienThoai = BuildingsInfotbl.DienThoai,
                            DiDong = BuildingsInfotbl.DiDong,
                            Email = BuildingsInfotbl.Email,
                            Status = BuildingsInfotbl.Status,
                            DienTich = BuildingsInfotbl.DienTich,
                            CreatedID = BuildingsInfotbl.CreatedID,
                            UpdatedID = BuildingsInfotbl.UpdatedID,
                            StatusTin = BuildingsInfotbl.StatusTin,
                            IsCreateRoom = BuildingsInfotbl.IsCreateRoom,
                            StartDate = BuildingsInfotbl.StartDate,
                            EndDate = BuildingsInfotbl.EndDate,
                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.BuildingsNameFilter))
                {
                    query = query.Where(x => x.BuildingsName.Contains(searchModel.BuildingsNameFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.AddressFilter))
                {
                    query = query.Where(x => x.Address.Contains(searchModel.AddressFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.AddressFilter))
                {
                    query = query.Where(x => x.Address.Contains(searchModel.AddressFilter));
                }
                if (searchModel.DienTichFilterFrom != null)
                {
                    query = query.Where(x => x.DienTich >= searchModel.DienTichFilterFrom);
                }
                if (searchModel.DienTichFilterTo != null)
                {
                    query = query.Where(x => x.DienTich <= searchModel.DienTichFilterTo);
                }

                if (searchModel.NumberOfPeoperPerRoomAdultFilterFrom != null)
                {
                    query = query.Where(x => x.NumberOfPeoperPerRoomAdult >= searchModel.NumberOfPeoperPerRoomAdultFilterFrom);
                }
                if (searchModel.NumberOfPeoperPerRoomAdultFilterTo != null)
                {
                    query = query.Where(x => x.NumberOfPeoperPerRoomAdult <= searchModel.NumberOfPeoperPerRoomAdultFilterTo);
                }

                if (searchModel.NumberOfPeoperPerRoomKidFilterFrom != null)
                {
                    query = query.Where(x => x.NumberOfPeoperPerRoomKid >= searchModel.NumberOfPeoperPerRoomKidFilterFrom);
                }
                if (searchModel.NumberOfPeoperPerRoomKidFilterTo != null)
                {
                    query = query.Where(x => x.NumberOfPeoperPerRoomKid <= searchModel.NumberOfPeoperPerRoomKidFilterTo);
                }

                if (searchModel.PriceFilterFrom != null)
                {
                    query = query.Where(x => x.Price >= searchModel.PriceFilterFrom);
                }
                if (searchModel.PriceFilterTo != null)
                {
                    query = query.Where(x => x.Price <= searchModel.PriceFilterTo);
                }
                if (!string.IsNullOrEmpty(searchModel.StatusFilter))
                {
                    query = query.Where(x => x.Status == searchModel.StatusFilter);
                }

                if (searchModel.TrangThaiDangTinFilter != "all")
                {
                    if (searchModel.TrangThaiDangTinFilter == "notdang")
                    {
                        query = query.Where(x => x.StatusTin == null);
                    }
                    else if (searchModel.TrangThaiDangTinFilter == "true")
                    {
                        query = query.Where(x => x.StatusTin == true);
                    }
                    else if (searchModel.TrangThaiDangTinFilter == "false")
                    {
                        query = query.Where(x => x.StatusTin == false);
                    }
                }


                if (searchModel.CreatedDateFromFilter != null)
                {
                    query = query.Where(x => DbFunctions.TruncateTime(x.CreatedDate) >= searchModel.CreatedDateFromFilter);
                }

                if (searchModel.CreatedDateToFilter != null)
                {
                    query = query.Where(x => DbFunctions.TruncateTime(x.CreatedDate) <= searchModel.CreatedDateToFilter);
                }
                if (searchModel.UpdatedDateFromFilter != null)
                {
                    query = query.Where(x => DbFunctions.TruncateTime(x.UpdatedDate) >= searchModel.UpdatedDateFromFilter);
                }

                if (searchModel.UpdatedDateToFilter != null)
                {
                    query = query.Where(x => DbFunctions.TruncateTime(x.UpdatedDate) <= searchModel.UpdatedDateToFilter);
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<BuildingsInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            foreach (var item in resultmodel.ListItem)
            {
                if (!string.IsNullOrEmpty(item.HuongNha))
                {
                    var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongNhaName))
                    {
                        item.HuongNhaName = HuongNhaName;
                    }
                }
                if (!string.IsNullOrEmpty(item.HuongBanCong))
                {
                    var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongBanCongName))
                    {
                        item.HuongBanCongName = HuongBanCongName;
                    }
                }
                if (!string.IsNullOrEmpty(item.MucDichSuDung))
                {
                    var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(MucDichSuDungName))
                    {
                        item.MucDichSuDungName = MucDichSuDungName;
                    }
                }
                if (!string.IsNullOrEmpty(item.Status))
                {
                    var StatusName = ConstantExtension.GetName<ItemStatusConstant>(item.Status);
                    if (!string.IsNullOrEmpty(StatusName))
                    {
                        item.StatusName = StatusName;
                    }
                    var StatusBGColor = ConstantExtension.GetBackgroundColor<ItemStatusConstant>(item.Status);
                    if (!string.IsNullOrEmpty(StatusBGColor))
                    {
                        item.StatusBGColor = StatusBGColor;
                    }
                }
                if (item.StatusTin == true && item.EndDate != null && item.EndDate <= CurrentDate && !string.IsNullOrEmpty(item.Status) && item.Status == ItemStatusConstant.DuocPheDuyet)
                {
                    item.IsHetHanDangTin = true;
                }
            }
            return resultmodel;
        }

        public BuildingsInfo GetById(long id)
        {
            return _BuildingsInfoRepository.GetById(id);
        }

        public BuildingsInfoDto GetFirstBulidingData(long? CurrentUserId)
        {
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var item = (from BuildingsInfotbl in _BuildingsInfoRepository.GetAllAsQueryable()
                        select new BuildingsInfoDto
                        {
                            //ChieuDaiRoom = BuildingsInfotbl.ChieuDaiRoom,
                            //ChieuRongRoom = BuildingsInfotbl.ChieuRongRoom,
                            Id = BuildingsInfotbl.Id,
                            BuildingsName = BuildingsInfotbl.BuildingsName,
                            Address = BuildingsInfotbl.Address,
                            Description = BuildingsInfotbl.Description,
                            ServicesCode = BuildingsInfotbl.ServicesCode,
                            InputWaterElectricFrom = BuildingsInfotbl.InputWaterElectricFrom,
                            InputWaterElectricTo = BuildingsInfotbl.InputWaterElectricTo,
                            BillsFrom = BuildingsInfotbl.BillsFrom,
                            BillsTo = BuildingsInfotbl.BillsTo,
                            CollectionMoneyFrom = BuildingsInfotbl.CollectionMoneyFrom,
                            CollectionMoneyTo = BuildingsInfotbl.CollectionMoneyTo,
                            EndOfConstractFrom = BuildingsInfotbl.EndOfConstractFrom,
                            EndOfConstractTo = BuildingsInfotbl.EndOfConstractTo,
                            TinhCode = BuildingsInfotbl.TinhCode,
                            XaCode = BuildingsInfotbl.XaCode,
                            HuyenCode = BuildingsInfotbl.HuyenCode,
                            QuocGiaCode = BuildingsInfotbl.QuocGiaCode,
                            CreatedDate = BuildingsInfotbl.CreatedDate,
                            CreatedBy = BuildingsInfotbl.CreatedBy,
                            UpdatedDate = BuildingsInfotbl.UpdatedDate,
                            UpdatedBy = BuildingsInfotbl.UpdatedBy,
                            Floor = BuildingsInfotbl.Floor,
                            NumberOfRoom = BuildingsInfotbl.NumberOfRoom,
                            NumberOfPeoperPerRoomAdult = BuildingsInfotbl.NumberOfPeoperPerRoomAdult,
                            NumberOfPeoperPerRoomKid = BuildingsInfotbl.NumberOfPeoperPerRoomKid,
                            Longitude = BuildingsInfotbl.Longitude,
                            Latitude = BuildingsInfotbl.Latitude,
                            Price = BuildingsInfotbl.Price,
                            IsSignedOfContact = BuildingsInfotbl.IsSignedOfContact,
                            IsDelete = BuildingsInfotbl.IsDelete,
                            DeleteTime = BuildingsInfotbl.DeleteTime,
                            DeleteId = BuildingsInfotbl.DeleteId,
                            StreetCode = BuildingsInfotbl.StreetCode,
                            DuAnId = BuildingsInfotbl.DuAnId,
                            MucDichSuDung = BuildingsInfotbl.MucDichSuDung,
                            DonVi = BuildingsInfotbl.DonVi,
                            HuongNha = BuildingsInfotbl.HuongNha,
                            HuongBanCong = BuildingsInfotbl.HuongBanCong,
                            SoPhongNgu = BuildingsInfotbl.SoPhongNgu,
                            SoToiLet = BuildingsInfotbl.SoToiLet,
                            NoiThat = BuildingsInfotbl.NoiThat,
                            ThongTinPhapLy = BuildingsInfotbl.ThongTinPhapLy,
                            TenLienHe = BuildingsInfotbl.TenLienHe,
                            DiaChiLienHe = BuildingsInfotbl.DiaChiLienHe,
                            DienThoai = BuildingsInfotbl.DienThoai,
                            DiDong = BuildingsInfotbl.DiDong,
                            Email = BuildingsInfotbl.Email,
                            Status = BuildingsInfotbl.Status,
                            DienTich = BuildingsInfotbl.DienTich,
                            CreatedID = BuildingsInfotbl.CreatedID,
                            UpdatedID = BuildingsInfotbl.UpdatedID,
                            StatusTin = BuildingsInfotbl.StatusTin,
                            IsCreateRoom = BuildingsInfotbl.IsCreateRoom,
                            StartDate = BuildingsInfotbl.StartDate,
                            EndDate = BuildingsInfotbl.EndDate,
                        }).OrderByDescending(x => x.Id).FirstOrDefault();
            item.ListRoomOfBuilding = _roomInfoRepository.GetAllAsQueryable().Where(x => x.BuildingsId == item.Id).ToList();

            if (!string.IsNullOrEmpty(item.HuongNha))
            {
                var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuongNhaName))
                {
                    item.HuongNhaName = HuongNhaName;
                }
            }
            if (!string.IsNullOrEmpty(item.HuongBanCong))
            {
                var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuongBanCongName))
                {
                    item.HuongBanCongName = HuongBanCongName;
                }
            }
            if (!string.IsNullOrEmpty(item.MucDichSuDung))
            {
                var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(MucDichSuDungName))
                {
                    item.MucDichSuDungName = MucDichSuDungName;
                }
            }
            return item;
        }

        public List<SelectListItem> GetDropDownListBulding(string CreatedBy)
        {
            var query = (from BuildingsInfotbl in _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.CreatedBy == CreatedBy)
                         select new SelectListItem
                         {
                             Value = BuildingsInfotbl.Id.ToString(),
                             Text = BuildingsInfotbl.BuildingsName,
                         }).ToList();
            return query;
        }


        public List<SelectListItem> GetDropDownListBuldingForAggrement(long? CreatedId, string SelectedValue)
        {
            var query = (from BuildingsInfotbl in _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.CreatedID == CreatedId && x.Status == ItemStatusConstant.DuocPheDuyet)
                         select new SelectListItem
                         {
                             Value = BuildingsInfotbl.Id.ToString(),
                             Text = BuildingsInfotbl.BuildingsName,
                             Selected = SelectedValue == BuildingsInfotbl.Id.ToString()
                         }).ToList();
            return query;
        }

        public List<BuildingsInfoDto> GetListSearch(string TinhId, string HuyenId, string XaId, decimal? MoneyFrom, decimal? MoneyTo, bool? IsInsideTinh, bool? IsInsideHuyen, bool? IsInsideXa, string TrongSoTinh, string TrongSoHuyen, string TrongSoXa, string TrongSoGia)
        {
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var TinhInfo = _tINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == TinhId).FirstOrDefault();
            var HuyenInfo = _hUYENRepository.GetAllAsQueryable().Where(x => x.MaHuyen == HuyenId).FirstOrDefault();
            var HuyenLongtitude = string.Empty;
            var HuyenLattitude = string.Empty;
            if (HuyenInfo != null && !string.IsNullOrEmpty(HuyenInfo.Location)) {
                HuyenLongtitude = ConvertLocation(HuyenInfo.Location).Item1;
                HuyenLattitude = ConvertLocation(HuyenInfo.Location).Item2;
            }
            var XaInfo = _xARepository.GetAllAsQueryable().Where(x => x.MaXa == XaId).FirstOrDefault();
            var XaLongtitude = string.Empty;
            var XaLattitude = string.Empty;
            if (HuyenInfo != null && !string.IsNullOrEmpty(HuyenInfo.Location))
            {
                XaLongtitude = ConvertLocation(XaInfo.Location).Item1;
                XaLattitude = ConvertLocation(XaInfo.Location).Item2;
            }
            var TongBinhPhuongTinh = new double();
            var TongBinhPhuongHuyen = new double();
            var TongBinhPhuongXa = new double();
            var TongBinhPhuongGia = new double();
            var MinATinh = new double();
            var MinAHuyen = new double();
            var MinAXa = new double();
            var MinAGia = new double();
            var TinhArr = new List<double>();
            var HuyenArr = new List<double>();
            var XaArr = new List<double>();
            var GiaArr = new List<double>();
            var query = (from BuildingsInfotbl in _BuildingsInfoRepository.GetAllAsQueryable().Where(x => ((MoneyFrom != null && MoneyFrom != 0) ? x.Price >= MoneyFrom : true) && ((MoneyTo != null && MoneyTo != 0) ? x.Price <= MoneyTo : true) && x.IsSignedOfContact != true && ((IsInsideTinh == true) ? (x.TinhCode == TinhId) : true) && ((IsInsideHuyen == true) ? (x.HuyenCode == HuyenId) : true) && ((IsInsideXa == true) ? (x.XaCode == XaId) : true) && x.IsSignedOfContact != true)

                         join Tinhtbl in _tINHRepository.GetAllAsQueryable()
                         on BuildingsInfotbl.TinhCode equals Tinhtbl.MaTinh into Tinhtable2
                         from Tinhtbl2 in Tinhtable2.DefaultIfEmpty()

                         join Huyentbl in _hUYENRepository.GetAllAsQueryable()
                         on BuildingsInfotbl.HuyenCode equals Huyentbl.MaHuyen into Huyentable2
                         from Huyentbl2 in Huyentable2.DefaultIfEmpty()

                         join Xatbl in _xARepository.GetAllAsQueryable()
                         on BuildingsInfotbl.XaCode equals Xatbl.MaXa into Xatable2
                         from Xatbl2 in Xatable2.DefaultIfEmpty()

                         select new BuildingsInfoDto
                         {
                             //ChieuDaiRoom = BuildingsInfotbl.ChieuDaiRoom,
                             //ChieuRongRoom = BuildingsInfotbl.ChieuRongRoom,
                             Id = BuildingsInfotbl.Id,
                             BuildingsName = BuildingsInfotbl.BuildingsName,
                             Address = BuildingsInfotbl.Address,
                             Description = BuildingsInfotbl.Description,
                             ServicesCode = BuildingsInfotbl.ServicesCode,
                             InputWaterElectricFrom = BuildingsInfotbl.InputWaterElectricFrom,
                             InputWaterElectricTo = BuildingsInfotbl.InputWaterElectricTo,
                             BillsFrom = BuildingsInfotbl.BillsFrom,
                             BillsTo = BuildingsInfotbl.BillsTo,
                             CollectionMoneyFrom = BuildingsInfotbl.CollectionMoneyFrom,
                             CollectionMoneyTo = BuildingsInfotbl.CollectionMoneyTo,
                             EndOfConstractFrom = BuildingsInfotbl.EndOfConstractFrom,
                             EndOfConstractTo = BuildingsInfotbl.EndOfConstractTo,
                             TinhCode = BuildingsInfotbl.TinhCode,
                             XaCode = BuildingsInfotbl.XaCode,
                             HuyenCode = BuildingsInfotbl.HuyenCode,
                             QuocGiaCode = BuildingsInfotbl.QuocGiaCode,
                             CreatedDate = BuildingsInfotbl.CreatedDate,
                             CreatedBy = BuildingsInfotbl.CreatedBy,
                             UpdatedDate = BuildingsInfotbl.UpdatedDate,
                             UpdatedBy = BuildingsInfotbl.UpdatedBy,
                             Floor = BuildingsInfotbl.Floor,
                             NumberOfRoom = BuildingsInfotbl.NumberOfRoom,
                             NumberOfPeoperPerRoomAdult = BuildingsInfotbl.NumberOfPeoperPerRoomAdult,
                             NumberOfPeoperPerRoomKid = BuildingsInfotbl.NumberOfPeoperPerRoomKid,
                             Longitude = BuildingsInfotbl.Longitude,
                             Latitude = BuildingsInfotbl.Latitude,
                             Price = BuildingsInfotbl.Price,
                             IsSignedOfContact = BuildingsInfotbl.IsSignedOfContact,
                             IsDelete = BuildingsInfotbl.IsDelete,
                             DeleteTime = BuildingsInfotbl.DeleteTime,
                             DeleteId = BuildingsInfotbl.DeleteId,
                             StreetCode = BuildingsInfotbl.StreetCode,
                             DuAnId = BuildingsInfotbl.DuAnId,
                             MucDichSuDung = BuildingsInfotbl.MucDichSuDung,
                             DonVi = BuildingsInfotbl.DonVi,
                             HuongNha = BuildingsInfotbl.HuongNha,
                             HuongBanCong = BuildingsInfotbl.HuongBanCong,
                             SoPhongNgu = BuildingsInfotbl.SoPhongNgu,
                             SoToiLet = BuildingsInfotbl.SoToiLet,
                             NoiThat = BuildingsInfotbl.NoiThat,
                             ThongTinPhapLy = BuildingsInfotbl.ThongTinPhapLy,
                             TenLienHe = BuildingsInfotbl.TenLienHe,
                             DiaChiLienHe = BuildingsInfotbl.DiaChiLienHe,
                             DienThoai = BuildingsInfotbl.DienThoai,
                             DiDong = BuildingsInfotbl.DiDong,
                             Email = BuildingsInfotbl.Email,
                             Status = BuildingsInfotbl.Status,
                             DienTich = BuildingsInfotbl.DienTich,
                             CreatedID = BuildingsInfotbl.CreatedID,
                             UpdatedID = BuildingsInfotbl.UpdatedID,
                             StatusTin = BuildingsInfotbl.StatusTin,
                             IsCreateRoom = BuildingsInfotbl.IsCreateRoom,
                             StartDate = BuildingsInfotbl.StartDate,
                             EndDate = BuildingsInfotbl.EndDate,
                         }).OrderByDescending(x => x.Id).ToList();

            foreach (var item in query)
            {
                item.ListRoomOfBuilding = _roomInfoRepository.GetAllAsQueryable().Where(x => x.BuildingsId == item.Id).ToList();
                if (TinhInfo != null && HuyenInfo != null && XaInfo != null && !string.IsNullOrEmpty(HuyenLattitude) && !string.IsNullOrEmpty(HuyenLongtitude) && !string.IsNullOrEmpty(XaLattitude) && !string.IsNullOrEmpty(XaLongtitude))
                {
                    item.DistanceTinh = Distance(TinhInfo.Latitude, TinhInfo.Longitude, item.Latitude, item.Longitude);
                    item.DistanceHuyen = Distance(HuyenLattitude, HuyenLongtitude, item.Latitude, item.Longitude);
                    item.DistanceXa = Distance(XaLattitude, XaLongtitude, item.Latitude, item.Longitude);
                    TongBinhPhuongTinh += Math.Pow(double.Parse(item.DistanceTinh.Replace('.', ',')), 2);
                    TongBinhPhuongHuyen += Math.Pow(double.Parse(item.DistanceHuyen.Replace('.', ',')), 2);
                    TongBinhPhuongXa += Math.Pow(double.Parse(item.DistanceXa.Replace('.', ',')), 2);
                    TongBinhPhuongGia += Math.Pow(Convert.ToDouble((item.Price != null && item.Price != 0) ? item.Price : 0), 2);
                }

                var ListFileActtackment = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Building).ToList();
                if (ListFileActtackment != null && ListFileActtackment.Any())
                {
                    item.ListFileActtackment = ListFileActtackment;
                }

                if (!string.IsNullOrEmpty(item.HuongNha))
                {
                    var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongNhaName))
                    {
                        item.HuongNhaName = HuongNhaName;
                    }
                }
                if (!string.IsNullOrEmpty(item.HuongBanCong))
                {
                    var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongBanCongName))
                    {
                        item.HuongBanCongName = HuongBanCongName;
                    }
                }
                if (!string.IsNullOrEmpty(item.MucDichSuDung))
                {
                    var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(MucDichSuDungName))
                    {
                        item.MucDichSuDungName = MucDichSuDungName;
                    }
                }

            }
            foreach (var item in query)
            {
                item.ChuanHoaTinhBangHai = double.Parse(item.DistanceTinh.Replace('.', ',')) / TongBinhPhuongTinh;
                item.ChuanHoaHuyenBangHai = double.Parse(item.DistanceHuyen.Replace('.', ',')) / TongBinhPhuongHuyen;
                item.ChuanHoaXaBangHai = double.Parse(item.DistanceXa.Replace('.', ',')) / TongBinhPhuongXa;
                item.ChuanHoaGiaBangHai = (Convert.ToDouble((item.Price != null && item.Price != 0) ? item.Price : 0)) / TongBinhPhuongGia;
                item.ChuanHoaTinhCoTrongSo = item.ChuanHoaTinhBangHai * (!string.IsNullOrEmpty(TrongSoTinh) ? double.Parse(TrongSoTinh.Replace('.', ',')) : 0.5);
                item.ChuanHoaHuyenCoTrongSo = item.ChuanHoaHuyenBangHai * (!string.IsNullOrEmpty(TrongSoHuyen) ? double.Parse(TrongSoHuyen.Replace('.', ',')) : 0.1);
                item.ChuanHoaXaCoTrongSo = item.ChuanHoaXaBangHai * (!string.IsNullOrEmpty(TrongSoXa) ? double.Parse(TrongSoXa.Replace('.', ',')) : 0.1);
                item.ChuanHoaGiaCoTrongSo = item.ChuanHoaGiaBangHai * (!string.IsNullOrEmpty(TrongSoGia) ? double.Parse(TrongSoGia.Replace('.', ',')) : 0.3);
                TinhArr.Add(item.ChuanHoaTinhCoTrongSo);
                HuyenArr.Add(item.ChuanHoaHuyenCoTrongSo);
                XaArr.Add(item.ChuanHoaXaCoTrongSo);
                GiaArr.Add(item.ChuanHoaGiaCoTrongSo);
            }
            MinATinh = (TinhArr != null && TinhArr.Any()) ? TinhArr.Min() : 0;
            MinAHuyen = (HuyenArr != null && HuyenArr.Any()) ? HuyenArr.Min() : 0;
            MinAXa = (XaArr != null && XaArr.Any()) ? XaArr.Min() : 0;
            MinAGia = (GiaArr != null && GiaArr.Any()) ? GiaArr.Min() : 0;
            foreach (var item in query)
            {
                item.SDistance = Math.Sqrt(Math.Pow((MinATinh - item.ChuanHoaTinhCoTrongSo), 2) + Math.Pow((MinAHuyen - item.ChuanHoaHuyenCoTrongSo), 2) + Math.Pow((MinAXa - item.ChuanHoaXaCoTrongSo), 2) + Math.Pow((MinAGia - item.ChuanHoaGiaCoTrongSo), 2));
            }
            return query.OrderBy(x => x.SDistance).ToList();
        }

        public (string, string) ConvertLocation(string location)
        {
            var longtitude = "";
            var lattitude = "";
            if (!string.IsNullOrEmpty(location))
            {
                var parts = location.Split(',').ToList();

                var longtitudepart = parts[0].Split(' ').ToList();
                if (longtitudepart[2].Contains('N'))
                {
                    longtitudepart[2] = longtitudepart[2].Remove(longtitudepart[2].IndexOf("N"));
                    longtitude = (decimal.Parse(longtitudepart[0]) + decimal.Parse(longtitudepart[1]) / 60 + decimal.Parse(longtitudepart[2]) / (60 * 60)).ToString();
                }
                var lattitudepart = parts[1].Split(' ').ToList();
                if (lattitudepart[3].Contains('E'))
                {
                    lattitudepart[3] = lattitudepart[3].Remove(lattitudepart[3].IndexOf("E"));
                    lattitude = (decimal.Parse(lattitudepart[1]) + decimal.Parse(lattitudepart[2]) / 60 + decimal.Parse(lattitudepart[3]) / (60 * 60)).ToString();
                }
            }
            return (longtitude, lattitude);
        }

        public string Distance(string latitude1, string longtitude1, string latitude2, string longtitude2)
        {
            var distance = "";
            if (!string.IsNullOrEmpty(latitude1) && !string.IsNullOrEmpty(longtitude1) && !string.IsNullOrEmpty(latitude2) && !string.IsNullOrEmpty(longtitude2))
            {
                var lat1 = double.Parse(latitude1.Replace('.', ','));
                var lon1 = double.Parse(longtitude1.Replace('.', ','));
                var lat2 = double.Parse(latitude2.Replace('.', ','));
                var lon2 = double.Parse(longtitude2.Replace('.', ','));

                var R = 6371; // km (change this constant to get miles)
                var dLat = (lat2 - lat1) * Math.PI / 180;
                var dLon = (lon2 - lon1) * Math.PI / 180;
                var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                    Math.Cos(lat1 * Math.PI / 180) * Math.Cos(lat2 * Math.PI / 180) *
                    Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
                var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
                var d = R * c;
                distance = d.ToString();
            }
            return distance;
        }

        public List<BuildingsInfoDto> GetListBuildingByCreatedId(long? CreatedID)
        {
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var query = (from BuildingsInfotbl in _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.CreatedID == CreatedID)

                         select new BuildingsInfoDto
                         {
                             //ChieuDaiRoom = BuildingsInfotbl.ChieuDaiRoom,
                             //ChieuRongRoom = BuildingsInfotbl.ChieuRongRoom,
                             Id = BuildingsInfotbl.Id,
                             BuildingsName = BuildingsInfotbl.BuildingsName,
                             Address = BuildingsInfotbl.Address,
                             Description = BuildingsInfotbl.Description,
                             ServicesCode = BuildingsInfotbl.ServicesCode,
                             InputWaterElectricFrom = BuildingsInfotbl.InputWaterElectricFrom,
                             InputWaterElectricTo = BuildingsInfotbl.InputWaterElectricTo,
                             BillsFrom = BuildingsInfotbl.BillsFrom,
                             BillsTo = BuildingsInfotbl.BillsTo,
                             CollectionMoneyFrom = BuildingsInfotbl.CollectionMoneyFrom,
                             CollectionMoneyTo = BuildingsInfotbl.CollectionMoneyTo,
                             EndOfConstractFrom = BuildingsInfotbl.EndOfConstractFrom,
                             EndOfConstractTo = BuildingsInfotbl.EndOfConstractTo,
                             TinhCode = BuildingsInfotbl.TinhCode,
                             XaCode = BuildingsInfotbl.XaCode,
                             HuyenCode = BuildingsInfotbl.HuyenCode,
                             QuocGiaCode = BuildingsInfotbl.QuocGiaCode,
                             CreatedDate = BuildingsInfotbl.CreatedDate,
                             CreatedBy = BuildingsInfotbl.CreatedBy,
                             UpdatedDate = BuildingsInfotbl.UpdatedDate,
                             UpdatedBy = BuildingsInfotbl.UpdatedBy,
                             Floor = BuildingsInfotbl.Floor,
                             NumberOfRoom = BuildingsInfotbl.NumberOfRoom,
                             NumberOfPeoperPerRoomAdult = BuildingsInfotbl.NumberOfPeoperPerRoomAdult,
                             NumberOfPeoperPerRoomKid = BuildingsInfotbl.NumberOfPeoperPerRoomKid,
                             Longitude = BuildingsInfotbl.Longitude,
                             Latitude = BuildingsInfotbl.Latitude,
                             Price = BuildingsInfotbl.Price,
                             IsSignedOfContact = BuildingsInfotbl.IsSignedOfContact,
                             IsDelete = BuildingsInfotbl.IsDelete,
                             DeleteTime = BuildingsInfotbl.DeleteTime,
                             DeleteId = BuildingsInfotbl.DeleteId,
                             StreetCode = BuildingsInfotbl.StreetCode,
                             DuAnId = BuildingsInfotbl.DuAnId,
                             MucDichSuDung = BuildingsInfotbl.MucDichSuDung,
                             DonVi = BuildingsInfotbl.DonVi,
                             HuongNha = BuildingsInfotbl.HuongNha,
                             HuongBanCong = BuildingsInfotbl.HuongBanCong,
                             SoPhongNgu = BuildingsInfotbl.SoPhongNgu,
                             SoToiLet = BuildingsInfotbl.SoToiLet,
                             NoiThat = BuildingsInfotbl.NoiThat,
                             ThongTinPhapLy = BuildingsInfotbl.ThongTinPhapLy,
                             TenLienHe = BuildingsInfotbl.TenLienHe,
                             DiaChiLienHe = BuildingsInfotbl.DiaChiLienHe,
                             DienThoai = BuildingsInfotbl.DienThoai,
                             DiDong = BuildingsInfotbl.DiDong,
                             Email = BuildingsInfotbl.Email,
                             Status = BuildingsInfotbl.Status,
                             DienTich = BuildingsInfotbl.DienTich,
                             CreatedID = BuildingsInfotbl.CreatedID,
                             UpdatedID = BuildingsInfotbl.UpdatedID,
                             StatusTin = BuildingsInfotbl.StatusTin,
                             IsCreateRoom = BuildingsInfotbl.IsCreateRoom,
                             StartDate = BuildingsInfotbl.StartDate,
                             EndDate = BuildingsInfotbl.EndDate,
                         }).OrderByDescending(x => x.UpdatedDate).ToList();
            foreach (var item in query)
            {

                if (!string.IsNullOrEmpty(item.HuongNha))
                {
                    var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongNhaName))
                    {
                        item.HuongNhaName = HuongNhaName;
                    }
                }
                if (!string.IsNullOrEmpty(item.HuongBanCong))
                {
                    var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongBanCongName))
                    {
                        item.HuongBanCongName = HuongBanCongName;
                    }
                }
                if (!string.IsNullOrEmpty(item.MucDichSuDung))
                {
                    var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(MucDichSuDungName))
                    {
                        item.MucDichSuDungName = MucDichSuDungName;
                    }
                }
            }
            return query;
        }

        public BuildingsInfoDto GetDtoById(long? Id)
        {
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var item = (from BuildingsInfotbl in _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.Id == Id)

                        select new BuildingsInfoDto
                        {
                            //ChieuDaiRoom = BuildingsInfotbl.ChieuDaiRoom,
                            //ChieuRongRoom = BuildingsInfotbl.ChieuRongRoom,
                            Id = BuildingsInfotbl.Id,
                            BuildingsName = BuildingsInfotbl.BuildingsName,
                            Address = BuildingsInfotbl.Address,
                            Description = BuildingsInfotbl.Description,
                            ServicesCode = BuildingsInfotbl.ServicesCode,
                            InputWaterElectricFrom = BuildingsInfotbl.InputWaterElectricFrom,
                            InputWaterElectricTo = BuildingsInfotbl.InputWaterElectricTo,
                            BillsFrom = BuildingsInfotbl.BillsFrom,
                            BillsTo = BuildingsInfotbl.BillsTo,
                            CollectionMoneyFrom = BuildingsInfotbl.CollectionMoneyFrom,
                            CollectionMoneyTo = BuildingsInfotbl.CollectionMoneyTo,
                            EndOfConstractFrom = BuildingsInfotbl.EndOfConstractFrom,
                            EndOfConstractTo = BuildingsInfotbl.EndOfConstractTo,
                            TinhCode = BuildingsInfotbl.TinhCode,
                            XaCode = BuildingsInfotbl.XaCode,
                            HuyenCode = BuildingsInfotbl.HuyenCode,
                            QuocGiaCode = BuildingsInfotbl.QuocGiaCode,
                            CreatedDate = BuildingsInfotbl.CreatedDate,
                            CreatedBy = BuildingsInfotbl.CreatedBy,
                            UpdatedDate = BuildingsInfotbl.UpdatedDate,
                            UpdatedBy = BuildingsInfotbl.UpdatedBy,
                            Floor = BuildingsInfotbl.Floor,
                            NumberOfRoom = BuildingsInfotbl.NumberOfRoom,
                            NumberOfPeoperPerRoomAdult = BuildingsInfotbl.NumberOfPeoperPerRoomAdult,
                            NumberOfPeoperPerRoomKid = BuildingsInfotbl.NumberOfPeoperPerRoomKid,
                            Longitude = BuildingsInfotbl.Longitude,
                            Latitude = BuildingsInfotbl.Latitude,
                            Price = BuildingsInfotbl.Price,
                            IsSignedOfContact = BuildingsInfotbl.IsSignedOfContact,
                            IsDelete = BuildingsInfotbl.IsDelete,
                            DeleteTime = BuildingsInfotbl.DeleteTime,
                            DeleteId = BuildingsInfotbl.DeleteId,
                            StreetCode = BuildingsInfotbl.StreetCode,
                            DuAnId = BuildingsInfotbl.DuAnId,
                            MucDichSuDung = BuildingsInfotbl.MucDichSuDung,
                            DonVi = BuildingsInfotbl.DonVi,
                            HuongNha = BuildingsInfotbl.HuongNha,
                            HuongBanCong = BuildingsInfotbl.HuongBanCong,
                            SoPhongNgu = BuildingsInfotbl.SoPhongNgu,
                            SoToiLet = BuildingsInfotbl.SoToiLet,
                            NoiThat = BuildingsInfotbl.NoiThat,
                            ThongTinPhapLy = BuildingsInfotbl.ThongTinPhapLy,
                            TenLienHe = BuildingsInfotbl.TenLienHe,
                            DiaChiLienHe = BuildingsInfotbl.DiaChiLienHe,
                            DienThoai = BuildingsInfotbl.DienThoai,
                            DiDong = BuildingsInfotbl.DiDong,
                            Email = BuildingsInfotbl.Email,
                            Status = BuildingsInfotbl.Status,
                            DienTich = BuildingsInfotbl.DienTich,
                            CreatedID = BuildingsInfotbl.CreatedID,
                            UpdatedID = BuildingsInfotbl.UpdatedID,
                            StatusTin = BuildingsInfotbl.StatusTin,
                            IsCreateRoom = BuildingsInfotbl.IsCreateRoom,
                            StartDate = BuildingsInfotbl.StartDate,
                            EndDate = BuildingsInfotbl.EndDate,
                        }).FirstOrDefault();
            if (!string.IsNullOrEmpty(item.HuongNha))
            {
                var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuongNhaName))
                {
                    item.HuongNhaName = HuongNhaName;
                }
            }
            if (!string.IsNullOrEmpty(item.HuongBanCong))
            {
                var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuongBanCongName))
                {
                    item.HuongBanCongName = HuongBanCongName;
                }
            }
            if (!string.IsNullOrEmpty(item.MucDichSuDung))
            {
                var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(MucDichSuDungName))
                {
                    item.MucDichSuDungName = MucDichSuDungName;
                }
            }
            var TinhName = _tINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == item.TinhCode).Select(x => x.TenTinh).FirstOrDefault();
            if (!string.IsNullOrEmpty(TinhName))
            {
                item.TinhName = TinhName;
            }
            var HuyenName = _hUYENRepository.GetAllAsQueryable().Where(x => x.MaHuyen == item.HuyenCode).Select(x => x.TenHuyen).FirstOrDefault();
            if (!string.IsNullOrEmpty(HuyenName))
            {
                item.HuyenName = HuyenName;
            }
            var XaName = _xARepository.GetAllAsQueryable().Where(x => x.MaXa == item.XaCode).Select(x => x.TenXa).FirstOrDefault();
            if (!string.IsNullOrEmpty(XaName))
            {
                item.XaName = XaName;
            }

            var StreetName = _quanLyDuongPhoRepository.GetAllAsQueryable().Where(x => x.Id.ToString() == item.StreetCode).Select(x => x.TenDuong).FirstOrDefault();
            if (!string.IsNullOrEmpty(StreetName))
            {
                item.StreetName = StreetName;
            }
            var ProjectName = _quanLyDuAnRepository.GetAllAsQueryable().Where(x => x.Id == item.DuAnId).Select(x => x.TenDuAn).FirstOrDefault();
            if (!string.IsNullOrEmpty(ProjectName))
            {
                item.ProjectName = ProjectName;
            }
            var listFileAnh = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Building).ToList();
            if (listFileAnh != null && listFileAnh.Any())
            {
                item.listFileAnh = listFileAnh;
            }
            return item;
        }

        public decimal TongThuNhapThangHienTai(long? CurrentUserId)
        {
            var query = _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.IsDelete != true && x.CreatedID == CurrentUserId).ToList();
            var price = new decimal();
            foreach (var item in query)
            {
                if (item.Price != null)
                {
                    if (item.NumberOfRoom != null)
                    {
                        price += Convert.ToDecimal(item.NumberOfRoom.Value) * item.Price.Value;
                    }
                    else
                    {
                        price += item.Price.Value;
                    }
                }
            }
            var listRoom = _roomInfoRepository.GetAllAsQueryable().Where(x => x.IsDelete != true && x.CreatedID == CurrentUserId).ToList();
            foreach (var item in listRoom)
            {
                if (item.BuildingsId == null)
                {
                    if (item.Price != null)
                    {
                        price += item.Price.Value;
                    }
                }
            }
            return price;
        }


        public List<BuildingsInfoDto> GetListBuildingForTrangChu(int Count)
        {
            var CurrentDate = DateTime.Now;
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var query = (from BuildingsInfotbl in _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.StatusTin == true && x.EndDate >= CurrentDate && x.IsSignedOfContact != true)

                         select new BuildingsInfoDto
                         {
                             //ChieuDaiRoom = BuildingsInfotbl.ChieuDaiRoom,
                             //ChieuRongRoom = BuildingsInfotbl.ChieuRongRoom,
                             Id = BuildingsInfotbl.Id,
                             BuildingsName = BuildingsInfotbl.BuildingsName,
                             Address = BuildingsInfotbl.Address,
                             Description = BuildingsInfotbl.Description,
                             ServicesCode = BuildingsInfotbl.ServicesCode,
                             InputWaterElectricFrom = BuildingsInfotbl.InputWaterElectricFrom,
                             InputWaterElectricTo = BuildingsInfotbl.InputWaterElectricTo,
                             BillsFrom = BuildingsInfotbl.BillsFrom,
                             BillsTo = BuildingsInfotbl.BillsTo,
                             CollectionMoneyFrom = BuildingsInfotbl.CollectionMoneyFrom,
                             CollectionMoneyTo = BuildingsInfotbl.CollectionMoneyTo,
                             EndOfConstractFrom = BuildingsInfotbl.EndOfConstractFrom,
                             EndOfConstractTo = BuildingsInfotbl.EndOfConstractTo,
                             TinhCode = BuildingsInfotbl.TinhCode,
                             XaCode = BuildingsInfotbl.XaCode,
                             HuyenCode = BuildingsInfotbl.HuyenCode,
                             QuocGiaCode = BuildingsInfotbl.QuocGiaCode,
                             CreatedDate = BuildingsInfotbl.CreatedDate,
                             CreatedBy = BuildingsInfotbl.CreatedBy,
                             UpdatedDate = BuildingsInfotbl.UpdatedDate,
                             UpdatedBy = BuildingsInfotbl.UpdatedBy,
                             Floor = BuildingsInfotbl.Floor,
                             NumberOfRoom = BuildingsInfotbl.NumberOfRoom,
                             NumberOfPeoperPerRoomAdult = BuildingsInfotbl.NumberOfPeoperPerRoomAdult,
                             NumberOfPeoperPerRoomKid = BuildingsInfotbl.NumberOfPeoperPerRoomKid,
                             Longitude = BuildingsInfotbl.Longitude,
                             Latitude = BuildingsInfotbl.Latitude,
                             Price = BuildingsInfotbl.Price,
                             IsSignedOfContact = BuildingsInfotbl.IsSignedOfContact,
                             IsDelete = BuildingsInfotbl.IsDelete,
                             DeleteTime = BuildingsInfotbl.DeleteTime,
                             DeleteId = BuildingsInfotbl.DeleteId,
                             StreetCode = BuildingsInfotbl.StreetCode,
                             DuAnId = BuildingsInfotbl.DuAnId,
                             MucDichSuDung = BuildingsInfotbl.MucDichSuDung,
                             DonVi = BuildingsInfotbl.DonVi,
                             HuongNha = BuildingsInfotbl.HuongNha,
                             HuongBanCong = BuildingsInfotbl.HuongBanCong,
                             SoPhongNgu = BuildingsInfotbl.SoPhongNgu,
                             SoToiLet = BuildingsInfotbl.SoToiLet,
                             NoiThat = BuildingsInfotbl.NoiThat,
                             ThongTinPhapLy = BuildingsInfotbl.ThongTinPhapLy,
                             TenLienHe = BuildingsInfotbl.TenLienHe,
                             DiaChiLienHe = BuildingsInfotbl.DiaChiLienHe,
                             DienThoai = BuildingsInfotbl.DienThoai,
                             DiDong = BuildingsInfotbl.DiDong,
                             Email = BuildingsInfotbl.Email,
                             Status = BuildingsInfotbl.Status,
                             DienTich = BuildingsInfotbl.DienTich,
                             CreatedID = BuildingsInfotbl.CreatedID,
                             UpdatedID = BuildingsInfotbl.UpdatedID,
                             StatusTin = BuildingsInfotbl.StatusTin,
                             IsCreateRoom = BuildingsInfotbl.IsCreateRoom,
                             StartDate = BuildingsInfotbl.StartDate,
                             EndDate = BuildingsInfotbl.EndDate,
                         }).OrderByDescending(x => x.UpdatedDate).Take(Count).ToList();
            foreach (var item in query)
            {
                if (!string.IsNullOrEmpty(item.HuongNha))
                {
                    var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongNhaName))
                    {
                        item.HuongNhaName = HuongNhaName;
                    }
                }
                if (!string.IsNullOrEmpty(item.HuongBanCong))
                {
                    var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongBanCongName))
                    {
                        item.HuongBanCongName = HuongBanCongName;
                    }
                }
                if (!string.IsNullOrEmpty(item.MucDichSuDung))
                {
                    var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(MucDichSuDungName))
                    {
                        item.MucDichSuDungName = MucDichSuDungName;
                    }
                }
                var listFileAnh = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Building).ToList();
                if (listFileAnh != null && listFileAnh.Any())
                {
                    item.listFileAnh = listFileAnh;
                }
                var TinhName = _tINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == item.TinhCode).Select(x => x.TenTinh).FirstOrDefault();
                if (!string.IsNullOrEmpty(TinhName))
                {
                    item.TinhName = TinhName;
                }
                var HuyenName = _hUYENRepository.GetAllAsQueryable().Where(x => x.MaHuyen == item.HuyenCode).Select(x => x.TenHuyen).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuyenName))
                {
                    item.HuyenName = HuyenName;
                }
                var XaName = _xARepository.GetAllAsQueryable().Where(x => x.MaXa == item.XaCode).Select(x => x.TenXa).FirstOrDefault();
                if (!string.IsNullOrEmpty(XaName))
                {
                    item.XaName = XaName;
                }

                var StreetName = _quanLyDuongPhoRepository.GetAllAsQueryable().Where(x => x.Id.ToString() == item.StreetCode).Select(x => x.TenDuong).FirstOrDefault();
                if (!string.IsNullOrEmpty(StreetName))
                {
                    item.StreetName = StreetName;
                }
                var ProjectName = _quanLyDuAnRepository.GetAllAsQueryable().Where(x => x.Id == item.DuAnId).Select(x => x.TenDuAn).FirstOrDefault();
                if (!string.IsNullOrEmpty(ProjectName))
                {
                    item.ProjectName = ProjectName;
                }
            }
            return query;
        }


        public List<BuildingsInfoDto> GetListBuildingSearch(string TinhId, string HuyenId, string XaId, decimal? MoneyFrom, decimal? MoneyTo)
        {
            var CurrentDate = DateTime.Now;
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var query = (from BuildingsInfotbl in _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.StatusTin == true && x.EndDate >= CurrentDate && x.IsSignedOfContact != true && x.TinhCode == TinhId && (!string.IsNullOrEmpty(HuyenId) ? x.HuyenCode == HuyenId : true) && (!string.IsNullOrEmpty(XaId) ? x.XaCode == XaId : true) && (MoneyFrom != null ? x.Price >= MoneyFrom : true) && (MoneyTo != null ? x.Price <= MoneyTo : true))

                         select new BuildingsInfoDto
                         {
                             //ChieuDaiRoom = BuildingsInfotbl.ChieuDaiRoom,
                             //ChieuRongRoom = BuildingsInfotbl.ChieuRongRoom,
                             Id = BuildingsInfotbl.Id,
                             BuildingsName = BuildingsInfotbl.BuildingsName,
                             Address = BuildingsInfotbl.Address,
                             Description = BuildingsInfotbl.Description,
                             ServicesCode = BuildingsInfotbl.ServicesCode,
                             InputWaterElectricFrom = BuildingsInfotbl.InputWaterElectricFrom,
                             InputWaterElectricTo = BuildingsInfotbl.InputWaterElectricTo,
                             BillsFrom = BuildingsInfotbl.BillsFrom,
                             BillsTo = BuildingsInfotbl.BillsTo,
                             CollectionMoneyFrom = BuildingsInfotbl.CollectionMoneyFrom,
                             CollectionMoneyTo = BuildingsInfotbl.CollectionMoneyTo,
                             EndOfConstractFrom = BuildingsInfotbl.EndOfConstractFrom,
                             EndOfConstractTo = BuildingsInfotbl.EndOfConstractTo,
                             TinhCode = BuildingsInfotbl.TinhCode,
                             XaCode = BuildingsInfotbl.XaCode,
                             HuyenCode = BuildingsInfotbl.HuyenCode,
                             QuocGiaCode = BuildingsInfotbl.QuocGiaCode,
                             CreatedDate = BuildingsInfotbl.CreatedDate,
                             CreatedBy = BuildingsInfotbl.CreatedBy,
                             UpdatedDate = BuildingsInfotbl.UpdatedDate,
                             UpdatedBy = BuildingsInfotbl.UpdatedBy,
                             Floor = BuildingsInfotbl.Floor,
                             NumberOfRoom = BuildingsInfotbl.NumberOfRoom,
                             NumberOfPeoperPerRoomAdult = BuildingsInfotbl.NumberOfPeoperPerRoomAdult,
                             NumberOfPeoperPerRoomKid = BuildingsInfotbl.NumberOfPeoperPerRoomKid,
                             Longitude = BuildingsInfotbl.Longitude,
                             Latitude = BuildingsInfotbl.Latitude,
                             Price = BuildingsInfotbl.Price,
                             IsSignedOfContact = BuildingsInfotbl.IsSignedOfContact,
                             IsDelete = BuildingsInfotbl.IsDelete,
                             DeleteTime = BuildingsInfotbl.DeleteTime,
                             DeleteId = BuildingsInfotbl.DeleteId,
                             StreetCode = BuildingsInfotbl.StreetCode,
                             DuAnId = BuildingsInfotbl.DuAnId,
                             MucDichSuDung = BuildingsInfotbl.MucDichSuDung,
                             DonVi = BuildingsInfotbl.DonVi,
                             HuongNha = BuildingsInfotbl.HuongNha,
                             HuongBanCong = BuildingsInfotbl.HuongBanCong,
                             SoPhongNgu = BuildingsInfotbl.SoPhongNgu,
                             SoToiLet = BuildingsInfotbl.SoToiLet,
                             NoiThat = BuildingsInfotbl.NoiThat,
                             ThongTinPhapLy = BuildingsInfotbl.ThongTinPhapLy,
                             TenLienHe = BuildingsInfotbl.TenLienHe,
                             DiaChiLienHe = BuildingsInfotbl.DiaChiLienHe,
                             DienThoai = BuildingsInfotbl.DienThoai,
                             DiDong = BuildingsInfotbl.DiDong,
                             Email = BuildingsInfotbl.Email,
                             Status = BuildingsInfotbl.Status,
                             DienTich = BuildingsInfotbl.DienTich,
                             CreatedID = BuildingsInfotbl.CreatedID,
                             UpdatedID = BuildingsInfotbl.UpdatedID,
                             StatusTin = BuildingsInfotbl.StatusTin,
                             IsCreateRoom = BuildingsInfotbl.IsCreateRoom,
                             StartDate = BuildingsInfotbl.StartDate,
                             EndDate = BuildingsInfotbl.EndDate,
                         }).OrderByDescending(x => x.UpdatedDate).ToList();
            foreach (var item in query)
            {
                if (!string.IsNullOrEmpty(item.HuongNha))
                {
                    var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongNhaName))
                    {
                        item.HuongNhaName = HuongNhaName;
                    }
                }
                if (!string.IsNullOrEmpty(item.HuongBanCong))
                {
                    var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongBanCongName))
                    {
                        item.HuongBanCongName = HuongBanCongName;
                    }
                }
                if (!string.IsNullOrEmpty(item.MucDichSuDung))
                {
                    var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(MucDichSuDungName))
                    {
                        item.MucDichSuDungName = MucDichSuDungName;
                    }
                }
                var listFileAnh = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Building).ToList();
                if (listFileAnh != null && listFileAnh.Any())
                {
                    item.listFileAnh = listFileAnh;
                }
                var TinhName = _tINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == item.TinhCode).Select(x => x.TenTinh).FirstOrDefault();
                if (!string.IsNullOrEmpty(TinhName))
                {
                    item.TinhName = TinhName;
                }
                var HuyenName = _hUYENRepository.GetAllAsQueryable().Where(x => x.MaHuyen == item.HuyenCode).Select(x => x.TenHuyen).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuyenName))
                {
                    item.HuyenName = HuyenName;
                }
                var XaName = _xARepository.GetAllAsQueryable().Where(x => x.MaXa == item.XaCode).Select(x => x.TenXa).FirstOrDefault();
                if (!string.IsNullOrEmpty(XaName))
                {
                    item.XaName = XaName;
                }

                var StreetName = _quanLyDuongPhoRepository.GetAllAsQueryable().Where(x => x.Id.ToString() == item.StreetCode).Select(x => x.TenDuong).FirstOrDefault();
                if (!string.IsNullOrEmpty(StreetName))
                {
                    item.StreetName = StreetName;
                }
                var ProjectName = _quanLyDuAnRepository.GetAllAsQueryable().Where(x => x.Id == item.DuAnId).Select(x => x.TenDuAn).FirstOrDefault();
                if (!string.IsNullOrEmpty(ProjectName))
                {
                    item.ProjectName = ProjectName;
                }
                var listUserIdDangKy = _QLHopDongDangKyRepository.GetAllAsQueryable().Where(x => x.ItemType == ItemTypeConstant.Building && x.ItemId == item.Id).Select(x => x.UserIdDangKy).Distinct().ToList();
                if (listUserIdDangKy != null && listUserIdDangKy.Any())
                {
                    item.listUserIdDangKy = listUserIdDangKy;
                }
            }
            return query;
        }


        public PageListResultBO<BuildingsInfoDto> GetDaTaByPageByTinhCode(string tinhCode, string KeySearch, int pageIndex = 1, int pageSize = 10)
        {
            var CurrentDate = DateTime.Now;
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var query = from BuildingsInfotbl in _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.TinhCode == tinhCode && x.StatusTin == true && x.EndDate >= CurrentDate && x.IsSignedOfContact != true)

                        select new BuildingsInfoDto
                        {
                            //ChieuDaiRoom = BuildingsInfotbl.ChieuDaiRoom,
                            //ChieuRongRoom = BuildingsInfotbl.ChieuRongRoom,
                            Id = BuildingsInfotbl.Id,
                            BuildingsName = BuildingsInfotbl.BuildingsName,
                            Address = BuildingsInfotbl.Address,
                            Description = BuildingsInfotbl.Description,
                            ServicesCode = BuildingsInfotbl.ServicesCode,
                            InputWaterElectricFrom = BuildingsInfotbl.InputWaterElectricFrom,
                            InputWaterElectricTo = BuildingsInfotbl.InputWaterElectricTo,
                            BillsFrom = BuildingsInfotbl.BillsFrom,
                            BillsTo = BuildingsInfotbl.BillsTo,
                            CollectionMoneyFrom = BuildingsInfotbl.CollectionMoneyFrom,
                            CollectionMoneyTo = BuildingsInfotbl.CollectionMoneyTo,
                            EndOfConstractFrom = BuildingsInfotbl.EndOfConstractFrom,
                            EndOfConstractTo = BuildingsInfotbl.EndOfConstractTo,
                            TinhCode = BuildingsInfotbl.TinhCode,
                            XaCode = BuildingsInfotbl.XaCode,
                            HuyenCode = BuildingsInfotbl.HuyenCode,
                            QuocGiaCode = BuildingsInfotbl.QuocGiaCode,
                            CreatedDate = BuildingsInfotbl.CreatedDate,
                            CreatedBy = BuildingsInfotbl.CreatedBy,
                            UpdatedDate = BuildingsInfotbl.UpdatedDate,
                            UpdatedBy = BuildingsInfotbl.UpdatedBy,
                            Floor = BuildingsInfotbl.Floor,
                            NumberOfRoom = BuildingsInfotbl.NumberOfRoom,
                            NumberOfPeoperPerRoomAdult = BuildingsInfotbl.NumberOfPeoperPerRoomAdult,
                            NumberOfPeoperPerRoomKid = BuildingsInfotbl.NumberOfPeoperPerRoomKid,
                            Longitude = BuildingsInfotbl.Longitude,
                            Latitude = BuildingsInfotbl.Latitude,
                            Price = BuildingsInfotbl.Price,
                            IsSignedOfContact = BuildingsInfotbl.IsSignedOfContact,
                            IsDelete = BuildingsInfotbl.IsDelete,
                            DeleteTime = BuildingsInfotbl.DeleteTime,
                            DeleteId = BuildingsInfotbl.DeleteId,
                            StreetCode = BuildingsInfotbl.StreetCode,
                            DuAnId = BuildingsInfotbl.DuAnId,
                            MucDichSuDung = BuildingsInfotbl.MucDichSuDung,
                            DonVi = BuildingsInfotbl.DonVi,
                            HuongNha = BuildingsInfotbl.HuongNha,
                            HuongBanCong = BuildingsInfotbl.HuongBanCong,
                            SoPhongNgu = BuildingsInfotbl.SoPhongNgu,
                            SoToiLet = BuildingsInfotbl.SoToiLet,
                            NoiThat = BuildingsInfotbl.NoiThat,
                            ThongTinPhapLy = BuildingsInfotbl.ThongTinPhapLy,
                            TenLienHe = BuildingsInfotbl.TenLienHe,
                            DiaChiLienHe = BuildingsInfotbl.DiaChiLienHe,
                            DienThoai = BuildingsInfotbl.DienThoai,
                            DiDong = BuildingsInfotbl.DiDong,
                            Email = BuildingsInfotbl.Email,
                            Status = BuildingsInfotbl.Status,
                            DienTich = BuildingsInfotbl.DienTich,
                            CreatedID = BuildingsInfotbl.CreatedID,
                            UpdatedID = BuildingsInfotbl.UpdatedID,
                            StatusTin = BuildingsInfotbl.StatusTin,
                            IsCreateRoom = BuildingsInfotbl.IsCreateRoom,
                            StartDate = BuildingsInfotbl.StartDate,
                            EndDate = BuildingsInfotbl.EndDate,
                        };

            if (!string.IsNullOrEmpty(KeySearch))
            {
                query = query.Where(x => x.BuildingsName.Contains(KeySearch) || x.CreatedBy.Contains(KeySearch));
            }
            query = query.OrderByDescending(x => x.UpdatedDate);

            var resultmodel = new PageListResultBO<BuildingsInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
                resultmodel.CurrentPage = 1;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
                resultmodel.CurrentPage = pageIndex;
            }
            foreach (var item in resultmodel.ListItem)
            {
                if (!string.IsNullOrEmpty(item.HuongNha))
                {
                    var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongNhaName))
                    {
                        item.HuongNhaName = HuongNhaName;
                    }
                }
                if (!string.IsNullOrEmpty(item.HuongBanCong))
                {
                    var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongBanCongName))
                    {
                        item.HuongBanCongName = HuongBanCongName;
                    }
                }
                if (!string.IsNullOrEmpty(item.MucDichSuDung))
                {
                    var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(MucDichSuDungName))
                    {
                        item.MucDichSuDungName = MucDichSuDungName;
                    }
                }
                var TinhName = _tINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == item.TinhCode).Select(x => x.TenTinh).FirstOrDefault();
                if (!string.IsNullOrEmpty(TinhName))
                {
                    item.TinhName = TinhName;
                }
                var HuyenName = _hUYENRepository.GetAllAsQueryable().Where(x => x.MaHuyen == item.HuyenCode).Select(x => x.TenHuyen).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuyenName))
                {
                    item.HuyenName = HuyenName;
                }
                var XaName = _xARepository.GetAllAsQueryable().Where(x => x.MaXa == item.XaCode).Select(x => x.TenXa).FirstOrDefault();
                if (!string.IsNullOrEmpty(XaName))
                {
                    item.XaName = XaName;
                }

                var StreetName = _quanLyDuongPhoRepository.GetAllAsQueryable().Where(x => x.Id.ToString() == item.StreetCode).Select(x => x.TenDuong).FirstOrDefault();
                if (!string.IsNullOrEmpty(StreetName))
                {
                    item.StreetName = StreetName;
                }
                var ProjectName = _quanLyDuAnRepository.GetAllAsQueryable().Where(x => x.Id == item.DuAnId).Select(x => x.TenDuAn).FirstOrDefault();
                if (!string.IsNullOrEmpty(ProjectName))
                {
                    item.ProjectName = ProjectName;
                }
                var listFileAnh = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Building).ToList();
                if (listFileAnh != null && listFileAnh.Any())
                {
                    item.listFileAnh = listFileAnh;
                }
            }
            return resultmodel;
        }


        public PageListResultBO<BuildingsInfoDto> GetDaTaByPageByDuAnId(long? duAnId, string KeySearch, int pageIndex = 1, int pageSize = 10)
        {
            var CurrentDate = DateTime.Now;
            var IdGroupMucDichSuDung = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.MUCDICHSUDUNG).Select(x => x.Id).FirstOrDefault();
            var IdGroupHuongNha = _dM_NhomDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupCode == DanhMucConstant.HUONGNHA).Select(x => x.Id).FirstOrDefault();
            var query = from BuildingsInfotbl in _BuildingsInfoRepository.GetAllAsQueryable().Where(x => x.DuAnId == duAnId && x.StatusTin == true && x.EndDate >= CurrentDate && x.IsSignedOfContact != true)

                        select new BuildingsInfoDto
                        {
                            //ChieuDaiRoom = BuildingsInfotbl.ChieuDaiRoom,
                            //ChieuRongRoom = BuildingsInfotbl.ChieuRongRoom,
                            Id = BuildingsInfotbl.Id,
                            BuildingsName = BuildingsInfotbl.BuildingsName,
                            Address = BuildingsInfotbl.Address,
                            Description = BuildingsInfotbl.Description,
                            ServicesCode = BuildingsInfotbl.ServicesCode,
                            InputWaterElectricFrom = BuildingsInfotbl.InputWaterElectricFrom,
                            InputWaterElectricTo = BuildingsInfotbl.InputWaterElectricTo,
                            BillsFrom = BuildingsInfotbl.BillsFrom,
                            BillsTo = BuildingsInfotbl.BillsTo,
                            CollectionMoneyFrom = BuildingsInfotbl.CollectionMoneyFrom,
                            CollectionMoneyTo = BuildingsInfotbl.CollectionMoneyTo,
                            EndOfConstractFrom = BuildingsInfotbl.EndOfConstractFrom,
                            EndOfConstractTo = BuildingsInfotbl.EndOfConstractTo,
                            TinhCode = BuildingsInfotbl.TinhCode,
                            XaCode = BuildingsInfotbl.XaCode,
                            HuyenCode = BuildingsInfotbl.HuyenCode,
                            QuocGiaCode = BuildingsInfotbl.QuocGiaCode,
                            CreatedDate = BuildingsInfotbl.CreatedDate,
                            CreatedBy = BuildingsInfotbl.CreatedBy,
                            UpdatedDate = BuildingsInfotbl.UpdatedDate,
                            UpdatedBy = BuildingsInfotbl.UpdatedBy,
                            Floor = BuildingsInfotbl.Floor,
                            NumberOfRoom = BuildingsInfotbl.NumberOfRoom,
                            NumberOfPeoperPerRoomAdult = BuildingsInfotbl.NumberOfPeoperPerRoomAdult,
                            NumberOfPeoperPerRoomKid = BuildingsInfotbl.NumberOfPeoperPerRoomKid,
                            Longitude = BuildingsInfotbl.Longitude,
                            Latitude = BuildingsInfotbl.Latitude,
                            Price = BuildingsInfotbl.Price,
                            IsSignedOfContact = BuildingsInfotbl.IsSignedOfContact,
                            IsDelete = BuildingsInfotbl.IsDelete,
                            DeleteTime = BuildingsInfotbl.DeleteTime,
                            DeleteId = BuildingsInfotbl.DeleteId,
                            StreetCode = BuildingsInfotbl.StreetCode,
                            DuAnId = BuildingsInfotbl.DuAnId,
                            MucDichSuDung = BuildingsInfotbl.MucDichSuDung,
                            DonVi = BuildingsInfotbl.DonVi,
                            HuongNha = BuildingsInfotbl.HuongNha,
                            HuongBanCong = BuildingsInfotbl.HuongBanCong,
                            SoPhongNgu = BuildingsInfotbl.SoPhongNgu,
                            SoToiLet = BuildingsInfotbl.SoToiLet,
                            NoiThat = BuildingsInfotbl.NoiThat,
                            ThongTinPhapLy = BuildingsInfotbl.ThongTinPhapLy,
                            TenLienHe = BuildingsInfotbl.TenLienHe,
                            DiaChiLienHe = BuildingsInfotbl.DiaChiLienHe,
                            DienThoai = BuildingsInfotbl.DienThoai,
                            DiDong = BuildingsInfotbl.DiDong,
                            Email = BuildingsInfotbl.Email,
                            Status = BuildingsInfotbl.Status,
                            DienTich = BuildingsInfotbl.DienTich,
                            CreatedID = BuildingsInfotbl.CreatedID,
                            UpdatedID = BuildingsInfotbl.UpdatedID,
                            StatusTin = BuildingsInfotbl.StatusTin,
                            IsCreateRoom = BuildingsInfotbl.IsCreateRoom,
                            StartDate = BuildingsInfotbl.StartDate,
                            EndDate = BuildingsInfotbl.EndDate,
                        };

            if (!string.IsNullOrEmpty(KeySearch))
            {
                query = query.Where(x => x.BuildingsName.Contains(KeySearch) || x.CreatedBy.Contains(KeySearch));
            }
            query = query.OrderByDescending(x => x.UpdatedDate);

            var resultmodel = new PageListResultBO<BuildingsInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
                resultmodel.CurrentPage = 1;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
                resultmodel.CurrentPage = pageIndex;
            }
            foreach (var item in resultmodel.ListItem)
            {
                if (!string.IsNullOrEmpty(item.HuongNha))
                {
                    var HuongNhaName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongNha).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongNhaName))
                    {
                        item.HuongNhaName = HuongNhaName;
                    }
                }
                if (!string.IsNullOrEmpty(item.HuongBanCong))
                {
                    var HuongBanCongName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupHuongNha && x.Code == item.HuongBanCong).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(HuongBanCongName))
                    {
                        item.HuongBanCongName = HuongBanCongName;
                    }
                }
                if (!string.IsNullOrEmpty(item.MucDichSuDung))
                {
                    var MucDichSuDungName = _dM_DulieuDanhmucRepository.GetAllAsQueryable().Where(x => x.GroupId == IdGroupMucDichSuDung && x.Code == item.MucDichSuDung).Select(x => x.Name).FirstOrDefault();
                    if (!string.IsNullOrEmpty(MucDichSuDungName))
                    {
                        item.MucDichSuDungName = MucDichSuDungName;
                    }
                }
                var TinhName = _tINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == item.TinhCode).Select(x => x.TenTinh).FirstOrDefault();
                if (!string.IsNullOrEmpty(TinhName))
                {
                    item.TinhName = TinhName;
                }
                var HuyenName = _hUYENRepository.GetAllAsQueryable().Where(x => x.MaHuyen == item.HuyenCode).Select(x => x.TenHuyen).FirstOrDefault();
                if (!string.IsNullOrEmpty(HuyenName))
                {
                    item.HuyenName = HuyenName;
                }
                var XaName = _xARepository.GetAllAsQueryable().Where(x => x.MaXa == item.XaCode).Select(x => x.TenXa).FirstOrDefault();
                if (!string.IsNullOrEmpty(XaName))
                {
                    item.XaName = XaName;
                }

                var StreetName = _quanLyDuongPhoRepository.GetAllAsQueryable().Where(x => x.Id.ToString() == item.StreetCode).Select(x => x.TenDuong).FirstOrDefault();
                if (!string.IsNullOrEmpty(StreetName))
                {
                    item.StreetName = StreetName;
                }
                var ProjectName = _quanLyDuAnRepository.GetAllAsQueryable().Where(x => x.Id == item.DuAnId).Select(x => x.TenDuAn).FirstOrDefault();
                if (!string.IsNullOrEmpty(ProjectName))
                {
                    item.ProjectName = ProjectName;
                }
                var listFileAnh = _fileDinhKemRepository.GetAllAsQueryable().Where(x => x.IdItem == item.Id && x.TypeItem == ItemTypeConstant.Building).ToList();
                if (listFileAnh != null && listFileAnh.Any())
                {
                    item.listFileAnh = listFileAnh;
                }
            }
            return resultmodel;
        }
    }
}
