﻿using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.BuildingsInfoService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nami.Service.BuildingsInfoService
{
    public interface IBuildingsInfoService:IEntityService<BuildingsInfo>
    {
        PageListResultBO<BuildingsInfoDto> GetDaTaByPage(BuildingsInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        BuildingsInfo GetById(long id);
        /// <summary>
        /// Lầy hồ sơ nhà đầu tiên của người dùng hiện tại
        /// </summary>
        /// <param name="CurrentUserId"></param>
        /// <returns></returns>
        BuildingsInfoDto GetFirstBulidingData(long? CurrentUserId);
        List<SelectListItem> GetDropDownListBulding(string CreatedBy);
        PageListResultBO<BuildingsInfoDto> GetDaTaByPageByCreatedId(bool IsViewAll, long? CreatedID, BuildingsInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        List<BuildingsInfoDto> GetListSearch(string TinhId, string HuyenId, string XaId, decimal? MoneyFrom, decimal? MoneyTo, bool? IsInsideTinh, bool? IsInsideHuyen, bool? IsInsideXa, string TrongSoTinh, string TrongSoHuyen, string TrongSoXa, string TrongSoGia);
        List<BuildingsInfoDto> GetListBuildingByCreatedId(long? CreatedID);
        List<BuildingsInfoDto> GetListBuildingForTrangChu(int Count);
        BuildingsInfoDto GetDtoById(long? Id);
        decimal TongThuNhapThangHienTai(long? CurrentUserId);
        List<SelectListItem> GetDropDownListBuldingForAggrement(long? CreatedId, string SelectedValue);
        List<BuildingsInfoDto> GetListBuildingSearch(string TinhId, string HuyenId, string XaId, decimal? MoneyFrom, decimal? MoneyTo);
        PageListResultBO<BuildingsInfoDto> GetDaTaByPageByTinhCode(string tinhCode, string KeySearch, int pageIndex = 1, int pageSize = 20);
        PageListResultBO<BuildingsInfoDto> GetDaTaByPageByDuAnId(long? duAnId, string KeySearch, int pageIndex = 1, int pageSize = 10);
    }
}
