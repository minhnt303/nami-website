using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BuildingsInfoService.Dto
{
    public class BuildingsInfoSearchDto : SearchBase
    {
		public string BuildingsNameFilter { get; set; }
		public string AddressFilter { get; set; }
		public string ServicesCodeFilter { get; set; }
        public int? FloorFilter { get; set; }
        public int? NumberOfRoomFilter { get; set; }
        public int? NumberOfPeoperPerRoomAdultFilter { get; set; }
        public int? NumberOfPeoperPerRoomAdultFilterFrom { get; set; }
        public int? NumberOfPeoperPerRoomAdultFilterTo { get; set; }
        public int? NumberOfPeoperPerRoomKidFilter { get; set; }
        public int? NumberOfPeoperPerRoomKidFilterFrom { get; set; }
        public int? NumberOfPeoperPerRoomKidFilterTo { get; set; }
        public string LongitudeFilter { get; set; }
        public string LatitudeFilter { get; set; }
        public decimal? PriceFilter { get; set; }
        public decimal? PriceFilterFrom { get; set; }
        public decimal? PriceFilterTo { get; set; }
        public bool? IsSignedOfContactFilter { get; set; }
        public string StreetCodeFilter { get; set; }
        public long? DuAnIdFilter { get; set; }
        public string MucDichSuDungFilter { get; set; }
        public string DonViFilter { get; set; }
        public string HuongNhaFilter { get; set; }
        public string HuongBanCongFilter { get; set; }
        public int SoPhongNguFilter { get; set; }
        public int SoToiLetFilter { get; set; }
        public string NoiThatFilter { get; set; }
        public string ThongTinPhapLyFilter { get; set; }
        public string TenLienHeFilter { get; set; }
        public string DiaChiLienHeFilter { get; set; }
        public string DienThoaiFilter { get; set; }
        public string DiDongFilter { get; set; }
        public string EmailFilter { get; set; }
        public string StatusFilter { get; set; }
        public decimal? DienTichFilter { get; set; }
        public decimal? DienTichFilterFrom { get; set; }
        public decimal? DienTichFilterTo { get; set; }
        public string TrangThaiDangTinFilter { get; set; }
        public DateTime? CreatedDateFromFilter { get; set; }
        public DateTime? CreatedDateToFilter { get; set; }
        public DateTime? UpdatedDateFromFilter { get; set; }
        public DateTime? UpdatedDateToFilter { get; set; }
    }
}