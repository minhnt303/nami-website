using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BuildingsInfoService.Dto
{
    public class BuildingsInfoDto : BuildingsInfo
    {
        public List<long?> listUserIdDangKy { get; set; }
        public List<FileDinhKem> listFileAnh { get; set; }
        public List<RoomInfo> ListRoomOfBuilding { get; set; }
        public TINH TinhItemInfo { get; set; }
        public HUYEN HuyenItemInfo { get; set; }
        public XA XaItemInfo { get; set; }
        public string DistanceTinh { get; set; }
        public string DistanceHuyen { get; set; }
        public string DistanceXa { get; set; }
        public List<FileDinhKem> ListFileActtackment { get; set; }
        public double ChuanHoaTinhBangHai { get; set; }
        public double ChuanHoaHuyenBangHai { get; set; }
        public double ChuanHoaXaBangHai { get; set; }
        public double ChuanHoaGiaBangHai { get; set; }
        public double ChuanHoaTinhCoTrongSo { get; set; }
        public double ChuanHoaHuyenCoTrongSo { get; set; }
        public double ChuanHoaXaCoTrongSo { get; set; }
        public double ChuanHoaGiaCoTrongSo { get; set; }
        public double SDistance { get; set; }
        public string HuongNhaName { get; set; }
        public string HuongBanCongName { get; set; }
        public string MucDichSuDungName { get; set; }
        public string StatusName { get; set; }
        public string StatusBGColor { get; set; }
        public bool IsHetHanDangTin { get; set; }
        public string TinhName { get; set; }
        public string HuyenName { get; set; }
        public string XaName { get; set; }
        public string StreetName { get; set; }
        public string ProjectName { get; set; }
    }
}