using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.RentersInfoService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.RentersInfoService
{
    public interface IRentersInfoService:IEntityService<RentersInfo>
    {
        PageListResultBO<RentersInfoDto> GetDaTaByPage(RentersInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        RentersInfo GetById(long id);
        PageListResultBO<RentersInfoDto> GetDaTaByPageByCreatedID(long? CreatedId, RentersInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
    }
}
