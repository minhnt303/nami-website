using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.RentersInfoRepository;
using Nami.Service.RentersInfoService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Repository.BuildingsInfoRepository;
using Nami.Repository.RoomInfoRepository;



namespace Nami.Service.RentersInfoService
{
    public class RentersInfoService : EntityService<RentersInfo>, IRentersInfoService
    {
        IUnitOfWork _unitOfWork;
        IRentersInfoRepository _RentersInfoRepository;
	ILog _loger;
        IMapper _mapper;
        IBuildingsInfoRepository _buildingsInfoRepository;
        IRoomInfoRepository _roomInfoRepository;
        
        public RentersInfoService(IUnitOfWork unitOfWork, 
		IRentersInfoRepository RentersInfoRepository, 
		ILog loger,
        IRoomInfoRepository roomInfoRepository,
        IBuildingsInfoRepository buildingsInfoRepository,

                IMapper mapper	
            )
            : base(unitOfWork, RentersInfoRepository)
        {
            _unitOfWork = unitOfWork;
            _RentersInfoRepository = RentersInfoRepository;
            _loger = loger;
            _mapper = mapper;
            _roomInfoRepository = roomInfoRepository;
            _buildingsInfoRepository = buildingsInfoRepository;
        }

        public PageListResultBO<RentersInfoDto> GetDaTaByPage(RentersInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from RentersInfotbl in _RentersInfoRepository.GetAllAsQueryable()

                        select new RentersInfoDto
                        {
							Id = RentersInfotbl.Id,
							BuildingsId = RentersInfotbl.BuildingsId,
                            RoomsId = RentersInfotbl.RoomsId,
                            RentersName = RentersInfotbl.RentersName,
							Phone = RentersInfotbl.Phone,
							Email = RentersInfotbl.Email,
							DateOfBirth = RentersInfotbl.DateOfBirth,
							SexCode = RentersInfotbl.SexCode,
							NumberId = RentersInfotbl.NumberId,
							NumberDate = RentersInfotbl.NumberDate,
							NumberAddress = RentersInfotbl.NumberAddress,
							HoKhau = RentersInfotbl.HoKhau,
							JobCode = RentersInfotbl.JobCode,
							JobAddress = RentersInfotbl.JobAddress,
							DangKyTamTru = RentersInfotbl.DangKyTamTru,
							Note = RentersInfotbl.Note,
							ParentName = RentersInfotbl.ParentName,
							ParentPhone = RentersInfotbl.ParentPhone,
							CreatedDate = RentersInfotbl.CreatedDate,
							CreatedBy = RentersInfotbl.CreatedBy,
							UpdatedDate = RentersInfotbl.UpdatedDate,
							UpdatedBy = RentersInfotbl.UpdatedBy
                        };

            if (searchModel != null)
            {
		if (!string.IsNullOrEmpty(searchModel.RentersNameFilter))
		{
			query = query.Where(x => x.RentersName.Contains(searchModel.RentersNameFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.SexCodeFilter))
		{
			query = query.Where(x => x.SexCode.Contains(searchModel.SexCodeFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.JobCodeFilter))
		{
			query = query.Where(x => x.JobCode.Contains(searchModel.JobCodeFilter));
		}
		if (searchModel.DangKyTamTruFilter!=null)
		{
			query = query.Where(x => x.DangKyTamTru==searchModel.DangKyTamTruFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.ParentNameFilter))
		{
			query = query.Where(x => x.ParentName.Contains(searchModel.ParentNameFilter));
		}
		if (searchModel.ParentPhoneFilter!=null)
		{
			query = query.Where(x => x.ParentPhone==searchModel.ParentPhoneFilter);
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<RentersInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public PageListResultBO<RentersInfoDto> GetDaTaByPageByCreatedID(long? CreatedId,RentersInfoSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from RentersInfotbl in _RentersInfoRepository.GetAllAsQueryable().Where(x =>x.BuildingsId != null)

                        select new RentersInfoDto
                        {
                            Id = RentersInfotbl.Id,
                            BuildingsId = RentersInfotbl.BuildingsId,
                            RentersName = RentersInfotbl.RentersName,
                            Phone = RentersInfotbl.Phone,
                            Email = RentersInfotbl.Email,
                            DateOfBirth = RentersInfotbl.DateOfBirth,
                            SexCode = RentersInfotbl.SexCode,
                            NumberId = RentersInfotbl.NumberId,
                            NumberDate = RentersInfotbl.NumberDate,
                            NumberAddress = RentersInfotbl.NumberAddress,
                            HoKhau = RentersInfotbl.HoKhau,
                            JobCode = RentersInfotbl.JobCode,
                            JobAddress = RentersInfotbl.JobAddress,
                            DangKyTamTru = RentersInfotbl.DangKyTamTru,
                            Note = RentersInfotbl.Note,
                            ParentName = RentersInfotbl.ParentName,
                            ParentPhone = RentersInfotbl.ParentPhone,
                            CreatedDate = RentersInfotbl.CreatedDate,
                            CreatedBy = RentersInfotbl.CreatedBy,
                            UpdatedDate = RentersInfotbl.UpdatedDate,
                            UpdatedBy = RentersInfotbl.UpdatedBy

                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.RentersNameFilter))
                {
                    query = query.Where(x => x.RentersName.Contains(searchModel.RentersNameFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.SexCodeFilter))
                {
                    query = query.Where(x => x.SexCode.Contains(searchModel.SexCodeFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.JobCodeFilter))
                {
                    query = query.Where(x => x.JobCode.Contains(searchModel.JobCodeFilter));
                }
                if (searchModel.DangKyTamTruFilter != null)
                {
                    query = query.Where(x => x.DangKyTamTru == searchModel.DangKyTamTruFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.ParentNameFilter))
                {
                    query = query.Where(x => x.ParentName.Contains(searchModel.ParentNameFilter));
                }
                if (searchModel.ParentPhoneFilter != null)
                {
                    query = query.Where(x => x.ParentPhone == searchModel.ParentPhoneFilter);
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<RentersInfoDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public RentersInfo GetById(long id)
        {
            return _RentersInfoRepository.GetById(id);
        }
    

    }
}
