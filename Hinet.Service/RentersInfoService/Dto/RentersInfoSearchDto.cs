using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.RentersInfoService.Dto
{
    public class RentersInfoSearchDto : SearchBase
    {
		public string RentersNameFilter { get; set; }
		public string SexCodeFilter { get; set; }
		public string JobCodeFilter { get; set; }
		public bool? DangKyTamTruFilter { get; set; }
		public string ParentNameFilter { get; set; }
		public int? ParentPhoneFilter { get; set; }


    }
}