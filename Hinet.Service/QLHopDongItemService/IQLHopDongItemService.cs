using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.QLHopDongItemService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLHopDongItemService
{
    public interface IQLHopDongItemService:IEntityService<QLHopDongItem>
    {
        PageListResultBO<QLHopDongItemDto> GetDaTaByPage(QLHopDongItemSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        QLHopDongItem GetById(long id);
        List<QLHopDongItemDto> GetListQLHopDongItemByIdHopDong(long? IdHopDong);
    }
}
