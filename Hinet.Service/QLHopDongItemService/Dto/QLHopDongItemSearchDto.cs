using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.QLHopDongItemService.Dto
{
    public class QLHopDongItemSearchDto : SearchBase
    {
		public long? IdQLHopDongFilter { get; set; }
		public int? ThangFilter { get; set; }
		public DateTime? StartDateFilter { get; set; }
		public DateTime? EndDateFilter { get; set; }
		public int? DepositsMoneyFilter { get; set; }
		public int? TienPhaiTraHangThangFilter { get; set; }
		public int? TienThangNayFilter { get; set; }
		public DateTime? PayDateFilter { get; set; }
		public string NoteFilter { get; set; }


    }
}