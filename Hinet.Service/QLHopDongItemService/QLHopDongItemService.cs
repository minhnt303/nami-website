using AutoMapper;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.QLHopDongItemRepository;
using Nami.Service.Common;
using Nami.Service.QLHopDongItemService.Dto;
using log4net;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;




namespace Nami.Service.QLHopDongItemService
{
    public class QLHopDongItemService : EntityService<QLHopDongItem>, IQLHopDongItemService
    {
        IUnitOfWork _unitOfWork;
        IQLHopDongItemRepository _QLHopDongItemRepository;
        ILog _loger;
        IMapper _mapper;



        public QLHopDongItemService(IUnitOfWork unitOfWork,
        IQLHopDongItemRepository QLHopDongItemRepository,
        ILog loger,

                IMapper mapper
            )
            : base(unitOfWork, QLHopDongItemRepository)
        {
            _unitOfWork = unitOfWork;
            _QLHopDongItemRepository = QLHopDongItemRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<QLHopDongItemDto> GetDaTaByPage(QLHopDongItemSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from QLHopDongItemtbl in _QLHopDongItemRepository.GetAllAsQueryable()

                        select new QLHopDongItemDto
                        {
                            IdQLHopDong = QLHopDongItemtbl.IdQLHopDong,
                            Thang = QLHopDongItemtbl.Thang,
                            StartDate = QLHopDongItemtbl.StartDate,
                            EndDate = QLHopDongItemtbl.EndDate,
                            DepositsMoney = QLHopDongItemtbl.DepositsMoney,
                            TienPhaiTraHangThang = QLHopDongItemtbl.TienPhaiTraHangThang,
                            TienThangNay = QLHopDongItemtbl.TienThangNay,
                            PayDate = QLHopDongItemtbl.PayDate,
                            Note = QLHopDongItemtbl.Note,
                            IsDelete = QLHopDongItemtbl.IsDelete,
                            DeleteTime = QLHopDongItemtbl.DeleteTime,
                            DeleteId = QLHopDongItemtbl.DeleteId,
                            Id = QLHopDongItemtbl.Id,
                            CreatedDate = QLHopDongItemtbl.CreatedDate,
                            CreatedBy = QLHopDongItemtbl.CreatedBy,
                            CreatedID = QLHopDongItemtbl.CreatedID,
                            UpdatedDate = QLHopDongItemtbl.UpdatedDate,
                            UpdatedBy = QLHopDongItemtbl.UpdatedBy,
                            UpdatedID = QLHopDongItemtbl.UpdatedID,
                            Status = QLHopDongItemtbl.Status,
                        };

            if (searchModel != null)
            {
                if (searchModel.IdQLHopDongFilter != null)
                {
                    query = query.Where(x => x.IdQLHopDong == searchModel.IdQLHopDongFilter);
                }
                if (searchModel.ThangFilter != null)
                {
                    query = query.Where(x => x.Thang == searchModel.ThangFilter);
                }
                if (searchModel.StartDateFilter != null)
                {
                    query = query.Where(x => x.StartDate == searchModel.StartDateFilter);
                }
                if (searchModel.EndDateFilter != null)
                {
                    query = query.Where(x => x.EndDate == searchModel.EndDateFilter);
                }
                if (searchModel.DepositsMoneyFilter != null)
                {
                    query = query.Where(x => x.DepositsMoney == searchModel.DepositsMoneyFilter);
                }
                if (searchModel.TienPhaiTraHangThangFilter != null)
                {
                    query = query.Where(x => x.TienPhaiTraHangThang == searchModel.TienPhaiTraHangThangFilter);
                }
                if (searchModel.TienThangNayFilter != null)
                {
                    query = query.Where(x => x.TienThangNay == searchModel.TienThangNayFilter);
                }
                if (searchModel.PayDateFilter != null)
                {
                    query = query.Where(x => x.PayDate == searchModel.PayDateFilter);
                }
                if (!string.IsNullOrEmpty(searchModel.NoteFilter))
                {
                    query = query.Where(x => x.Note.Contains(searchModel.NoteFilter));
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<QLHopDongItemDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public QLHopDongItem GetById(long id)
        {
            return _QLHopDongItemRepository.GetById(id);
        }

        public List<QLHopDongItemDto> GetListQLHopDongItemByIdHopDong(long? IdHopDong)
        {
            var query = (from QLHopDongItemtbl in _QLHopDongItemRepository.GetAllAsQueryable().Where(x => x.IdQLHopDong == IdHopDong)

                         select new QLHopDongItemDto
                         {
                             IdQLHopDong = QLHopDongItemtbl.IdQLHopDong,
                             Thang = QLHopDongItemtbl.Thang,
                             StartDate = QLHopDongItemtbl.StartDate,
                             EndDate = QLHopDongItemtbl.EndDate,
                             DepositsMoney = QLHopDongItemtbl.DepositsMoney,
                             TienPhaiTraHangThang = QLHopDongItemtbl.TienPhaiTraHangThang,
                             TienThangNay = QLHopDongItemtbl.TienThangNay,
                             PayDate = QLHopDongItemtbl.PayDate,
                             Note = QLHopDongItemtbl.Note,
                             IsDelete = QLHopDongItemtbl.IsDelete,
                             DeleteTime = QLHopDongItemtbl.DeleteTime,
                             DeleteId = QLHopDongItemtbl.DeleteId,
                             Id = QLHopDongItemtbl.Id,
                             CreatedDate = QLHopDongItemtbl.CreatedDate,
                             CreatedBy = QLHopDongItemtbl.CreatedBy,
                             CreatedID = QLHopDongItemtbl.CreatedID,
                             UpdatedDate = QLHopDongItemtbl.UpdatedDate,
                             UpdatedBy = QLHopDongItemtbl.UpdatedBy,
                             UpdatedID = QLHopDongItemtbl.UpdatedID,
                             Status = QLHopDongItemtbl.Status
                         }).OrderBy(x => x.StartDate).ToList();
            return query;
        }




        public List<QLHopDongItem> GetListHopDongItemByIdHopDong(long IdHopDong)
        {
            var listHopDong = _QLHopDongItemRepository.GetAllAsQueryable().Where(x => x.IdQLHopDong == IdHopDong).ToList();
            return listHopDong;
        }
    }
}
