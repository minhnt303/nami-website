using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.BDGraphsRepository;
using Nami.Service.BDGraphsService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;
using Nami.Service.Constant;




namespace Nami.Service.BDGraphsService
{
    public class BDGraphsService : EntityService<BDGraphs>, IBDGraphsService
    {
        IUnitOfWork _unitOfWork;
        IBDGraphsRepository _BDGraphsRepository;
	ILog _loger;
        IMapper _mapper;


        
        public BDGraphsService(IUnitOfWork unitOfWork, 
		IBDGraphsRepository BDGraphsRepository, 
		ILog loger,

            	IMapper mapper	
            )
            : base(unitOfWork, BDGraphsRepository)
        {
            _unitOfWork = unitOfWork;
            _BDGraphsRepository = BDGraphsRepository;
            _loger = loger;
            _mapper = mapper;



        }

        public PageListResultBO<BDGraphsDto> GetDaTaByPage(BDGraphsSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from BDGraphstbl in _BDGraphsRepository.GetAllAsQueryable()

                        select new BDGraphsDto
                        {
							IdBieuDo = BDGraphstbl.IdBieuDo,
							BalloonText = BDGraphstbl.BalloonText,
							IsHidden = BDGraphstbl.IsHidden,
							IdGraphs = BDGraphstbl.IdGraphs,
							Title = BDGraphstbl.Title,
							Type = BDGraphstbl.Type,
							ValueField = BDGraphstbl.ValueField,
							BalloonColor = BDGraphstbl.BalloonColor,
							ShowBalloon = BDGraphstbl.ShowBalloon,
							ColumnWidth = BDGraphstbl.ColumnWidth,
							ConnerRadius = BDGraphstbl.ConnerRadius,
							DashLength = BDGraphstbl.DashLength,
							FillColor = BDGraphstbl.FillColor,
							FixedColoumWidth = BDGraphstbl.FixedColoumWidth,
							FontSize = BDGraphstbl.FontSize,
							Bullet = BDGraphstbl.Bullet,
							BulletAlpha = BDGraphstbl.BulletAlpha,
							BulletColor = BDGraphstbl.BulletColor,
							BulletSize = BDGraphstbl.BulletSize,
							CustomBullet = BDGraphstbl.CustomBullet,
							LabelText = BDGraphstbl.LabelText,
							LabelRotation = BDGraphstbl.LabelRotation,
							LabelPosition = BDGraphstbl.LabelPosition,
							IsDelete = BDGraphstbl.IsDelete,
							DeleteTime = BDGraphstbl.DeleteTime,
							DeleteId = BDGraphstbl.DeleteId,
							Id = BDGraphstbl.Id,
							CreatedDate = BDGraphstbl.CreatedDate,
							CreatedBy = BDGraphstbl.CreatedBy,
							CreatedID = BDGraphstbl.CreatedID,
							UpdatedDate = BDGraphstbl.UpdatedDate,
							UpdatedBy = BDGraphstbl.UpdatedBy,
							UpdatedID = BDGraphstbl.UpdatedID
                            
                        };

            if (searchModel != null)
            {
		if (searchModel.IdBieuDoFilter!=null)
		{
			query = query.Where(x => x.IdBieuDo==searchModel.IdBieuDoFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.BalloonTextFilter))
		{
			query = query.Where(x => x.BalloonText.Contains(searchModel.BalloonTextFilter));
		}
		if (searchModel.IsHiddenFilter!=null)
		{
			query = query.Where(x => x.IsHidden==searchModel.IsHiddenFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.IdGraphsFilter))
		{
			query = query.Where(x => x.IdGraphs.Contains(searchModel.IdGraphsFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.TitleFilter))
		{
			query = query.Where(x => x.Title.Contains(searchModel.TitleFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.TypeFilter))
		{
			query = query.Where(x => x.Type.Contains(searchModel.TypeFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.ValueFieldFilter))
		{
			query = query.Where(x => x.ValueField.Contains(searchModel.ValueFieldFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.BalloonColorFilter))
		{
			query = query.Where(x => x.BalloonColor.Contains(searchModel.BalloonColorFilter));
		}
		if (searchModel.ShowBalloonFilter!=null)
		{
			query = query.Where(x => x.ShowBalloon==searchModel.ShowBalloonFilter);
		}
		if (searchModel.ColumnWidthFilter!=null)
		{
			query = query.Where(x => x.ColumnWidth==searchModel.ColumnWidthFilter);
		}
		if (searchModel.ConnerRadiusFilter!=null)
		{
			query = query.Where(x => x.ConnerRadius==searchModel.ConnerRadiusFilter);
		}
		if (searchModel.DashLengthFilter!=null)
		{
			query = query.Where(x => x.DashLength==searchModel.DashLengthFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.FillColorFilter))
		{
			query = query.Where(x => x.FillColor.Contains(searchModel.FillColorFilter));
		}
		if (searchModel.FixedColoumWidthFilter!=null)
		{
			query = query.Where(x => x.FixedColoumWidth==searchModel.FixedColoumWidthFilter);
		}
		if (searchModel.FontSizeFilter!=null)
		{
			query = query.Where(x => x.FontSize==searchModel.FontSizeFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.BulletFilter))
		{
			query = query.Where(x => x.Bullet.Contains(searchModel.BulletFilter));
		}
		if (searchModel.BulletAlphaFilter!=null)
		{
			query = query.Where(x => x.BulletAlpha==searchModel.BulletAlphaFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.BulletColorFilter))
		{
			query = query.Where(x => x.BulletColor.Contains(searchModel.BulletColorFilter));
		}
		if (searchModel.BulletSizeFilter!=null)
		{
			query = query.Where(x => x.BulletSize==searchModel.BulletSizeFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.CustomBulletFilter))
		{
			query = query.Where(x => x.CustomBullet.Contains(searchModel.CustomBulletFilter));
		}
		if (!string.IsNullOrEmpty(searchModel.LabelTextFilter))
		{
			query = query.Where(x => x.LabelText.Contains(searchModel.LabelTextFilter));
		}
		if (searchModel.LabelRotationFilter!=null)
		{
			query = query.Where(x => x.LabelRotation==searchModel.LabelRotationFilter);
		}
		if (!string.IsNullOrEmpty(searchModel.LabelPositionFilter))
		{
			query = query.Where(x => x.LabelPosition.Contains(searchModel.LabelPositionFilter));
		}
		if (searchModel.IsDeleteFilter!=null)
		{
			query = query.Where(x => x.IsDelete==searchModel.IsDeleteFilter);
		}
		if (searchModel.DeleteTimeFilter!=null)
		{
			query = query.Where(x => x.DeleteTime==searchModel.DeleteTimeFilter);
		}
		if (searchModel.DeleteIdFilter!=null)
		{
			query = query.Where(x => x.DeleteId==searchModel.DeleteIdFilter);
		}


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<BDGraphsDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public BDGraphs GetById(long id)
        {
            return _BDGraphsRepository.GetById(id);
        }
    

    }
}
