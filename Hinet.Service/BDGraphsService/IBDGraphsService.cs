using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.BDGraphsService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BDGraphsService
{
    public interface IBDGraphsService:IEntityService<BDGraphs>
    {
        PageListResultBO<BDGraphsDto> GetDaTaByPage(BDGraphsSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        BDGraphs GetById(long id);
    }
}
