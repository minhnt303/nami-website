using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.BDGraphsService.Dto
{
    public class BDGraphsSearchDto : SearchBase
    {
		public long? IdBieuDoFilter { get; set; }
		public string BalloonTextFilter { get; set; }
		public bool IsHiddenFilter { get; set; }
		public string IdGraphsFilter { get; set; }
		public string TitleFilter { get; set; }
		public string TypeFilter { get; set; }
		public string ValueFieldFilter { get; set; }
		public string BalloonColorFilter { get; set; }
		public bool ShowBalloonFilter { get; set; }
		public decimal ColumnWidthFilter { get; set; }
		public int ConnerRadiusFilter { get; set; }
		public int DashLengthFilter { get; set; }
		public string FillColorFilter { get; set; }
		public int FixedColoumWidthFilter { get; set; }
		public int FontSizeFilter { get; set; }
		public string BulletFilter { get; set; }
		public decimal BulletAlphaFilter { get; set; }
		public string BulletColorFilter { get; set; }
		public int BulletSizeFilter { get; set; }
		public string CustomBulletFilter { get; set; }
		public string LabelTextFilter { get; set; }
		public decimal LabelRotationFilter { get; set; }
		public string LabelPositionFilter { get; set; }
		public bool? IsDeleteFilter { get; set; }
		public DateTime? DeleteTimeFilter { get; set; }
		public long? DeleteIdFilter { get; set; }


    }
}