using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Nami.Service.BDGraphsService.Dto
{
    public class BDGraphsExportDto
    {
		[DisplayName("")]
public long? IdBieuDo { get; set; }
		[DisplayName("")]
public string BalloonText { get; set; }
		[DisplayName("")]
public bool IsHidden { get; set; }
		[DisplayName("")]
public string IdGraphs { get; set; }
		[DisplayName("")]
public string Title { get; set; }
		[DisplayName("")]
public string Type { get; set; }
		[DisplayName("")]
public string ValueField { get; set; }
		[DisplayName("")]
public string BalloonColor { get; set; }
		[DisplayName("")]
public bool ShowBalloon { get; set; }
		[DisplayName("")]
public decimal ColumnWidth { get; set; }
		[DisplayName("")]
public int ConnerRadius { get; set; }
		[DisplayName("")]
public int DashLength { get; set; }
		[DisplayName("")]
public string FillColor { get; set; }
		[DisplayName("")]
public int FixedColoumWidth { get; set; }
		[DisplayName("")]
public int FontSize { get; set; }
		[DisplayName("")]
public string Bullet { get; set; }
		[DisplayName("")]
public decimal BulletAlpha { get; set; }
		[DisplayName("")]
public string BulletColor { get; set; }
		[DisplayName("")]
public int BulletSize { get; set; }
		[DisplayName("")]
public string CustomBullet { get; set; }
		[DisplayName("")]
public string LabelText { get; set; }
		[DisplayName("")]
public decimal LabelRotation { get; set; }
		[DisplayName("")]
public string LabelPosition { get; set; }
		[DisplayName("")]
public bool? IsDelete { get; set; }
		[DisplayName("")]
public DateTime? DeleteTime { get; set; }
		[DisplayName("")]
public long? DeleteId { get; set; }

    }
}