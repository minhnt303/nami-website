﻿using Nami.Model.Entities;
using Nami.Service.OperationService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.ModuleService.DTO
{
    public class ModuleDTO : Module
    {
        public int OperationQuantity { set; get; }
        public IEnumerable<OperationDTO> GroupOperations { get; set; }
    }
}
