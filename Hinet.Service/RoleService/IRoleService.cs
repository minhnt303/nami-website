﻿using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.RoleService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.RoleService
{
    public interface IRoleService : IEntityService<Role>
    {
        PageListResultBO<RoleDTO> GetDataByPage(RoleSearchDTO searchParams, int pageIndex = 1, int pageSize = 20);
    }
}
