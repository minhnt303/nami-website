﻿using Nami.Model.Entities;
using Nami.Service.Common;
using Nami.Service.ModuleService.DTO;
using Nami.Service.OperationService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.OperationService
{
    public interface IOperationService : IEntityService<Operation>
    {
        PageListResultBO<OperationDTO> GetDataByPage(OperationSearchDTO searchParams, int pageIndex = 1, int pageSize = 20);
        List<ModuleMenuDTO> GetListOperationOfUser(long userId);
        bool CheckCode(string code, long? id = null);
        Operation getByCode(string code);
        List<MobileModule> GetListOperationOfUserForMobile(long userId);
    }
}
