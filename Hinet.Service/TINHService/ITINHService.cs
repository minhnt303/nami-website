using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Service.TINHService.Dto;
using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.TINHService
{
    public interface ITINHService:IEntityService<TINH>
    {
        PageListResultBO<TINHDto> GetDaTaByPage(TINHSearchDto searchModel, int pageIndex = 1, int pageSize = 20);
        TINH GetById(long id);
        TINH GetByIdName(string id);
        TINH GetNameByMaTinh(string MaTinh);
        string GetIdByProvinceName(string TenTinh);
    }
}
