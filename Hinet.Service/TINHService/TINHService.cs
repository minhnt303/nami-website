using log4net;
using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using Nami.Repository;
using Nami.Repository.TINHRepository;
using Nami.Service.TINHService.Dto;
using Nami.Service.Common;
using System.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
using AutoMapper;



namespace Nami.Service.TINHService
{
    public class TINHService : EntityService<TINH>, ITINHService
    {
        IUnitOfWork _unitOfWork;
        ITINHRepository _TINHRepository;
        ILog _loger;
        IMapper _mapper;


        public TINHService(IUnitOfWork unitOfWork,
        ITINHRepository TINHRepository,
        ILog loger,

                IMapper mapper
            )
            : base(unitOfWork, TINHRepository)
        {
            _unitOfWork = unitOfWork;
            _TINHRepository = TINHRepository;
            _loger = loger;
            _mapper = mapper;

        }

        public PageListResultBO<TINHDto> GetDaTaByPage(TINHSearchDto searchModel, int pageIndex = 1, int pageSize = 20)
        {
            var query = from TINHtbl in _TINHRepository.GetAllAsQueryable()

                        select new TINHDto
                        {
                            MaTinh = TINHtbl.MaTinh,
                            TenTinh = TINHtbl.TenTinh,
                            Loai = TINHtbl.Loai,
                            Id = TINHtbl.Id

                        };

            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.MaTinhFilter))
                {
                    query = query.Where(x => x.MaTinh.Contains(searchModel.MaTinhFilter));
                }
                if (!string.IsNullOrEmpty(searchModel.TenTinhFilter))
                {
                    query = query.Where(x => x.TenTinh.Contains(searchModel.TenTinhFilter));
                }


                if (!string.IsNullOrEmpty(searchModel.sortQuery))
                {
                    query = query.OrderBy(searchModel.sortQuery);
                }
                else
                {
                    query = query.OrderByDescending(x => x.Id);
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.Id);
            }
            var resultmodel = new PageListResultBO<TINHDto>();
            if (pageSize == -1)
            {
                var dataPageList = query.ToList();
                resultmodel.Count = dataPageList.Count;
                resultmodel.TotalPage = 1;
                resultmodel.ListItem = dataPageList;
            }
            else
            {
                var dataPageList = query.ToPagedList(pageIndex, pageSize);
                resultmodel.Count = dataPageList.TotalItemCount;
                resultmodel.TotalPage = dataPageList.PageCount;
                resultmodel.ListItem = dataPageList.ToList();
            }
            return resultmodel;
        }

        public TINH GetById(long id)
        {
            return _TINHRepository.GetById(id);
        }

        public TINH GetByIdName(string id)
        {
            var query = from TINHtbl in _TINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == id)

                        select new TINHDto
                        {
                            MaTinh = TINHtbl.MaTinh,
                            TenTinh = TINHtbl.TenTinh,
                            Loai = TINHtbl.Loai,
                            Id = TINHtbl.Id

                        };

            var resultmodel = query.FirstOrDefault();
            return resultmodel;
        }

        public TINH GetNameByMaTinh(string MaTinh)
        {
            var query = from TINHtbl in _TINHRepository.GetAllAsQueryable().Where(x => x.MaTinh == MaTinh)

                        select new TINHDto
                        {
                            TenTinh = TINHtbl.TenTinh,
                        };

            var resultmodel = query.FirstOrDefault();
            return resultmodel;
        }
        
        public string GetIdByProvinceName(string TenTinh)
        {
            var query = from TINHtbl in _TINHRepository.GetAllAsQueryable().Where(x => x.TenTinh.Contains(TenTinh))

                        select new TINHDto
                        {
                            Id = TINHtbl.Id
                        };
            var result = query.FirstOrDefault();
            return result.TenTinh;
        }
    }
}
