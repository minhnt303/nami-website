﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class LoaiTinhConstant
    {
        [DisplayName("Tỉnh")]
        public static string Tinh { get; set; } = "Tỉnh";
        [DisplayName("Thành phố")]

        public static string ThanhPho { get; set; } = "Thành phố";
    }
}
