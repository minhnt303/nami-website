﻿using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class ItemStatusConstant
    {
        [DisplayName("Mới tạo")]
        [Color(BgColor = "#34495e")]
        public static string MoiTao { get; set; } = "MoiTao";
        [DisplayName("Chờ duyệt")]
        [Color(BgColor = "#3498db")]
        public static string ChoDuyet { get; set; } = "ChoDuyet";
        [DisplayName("Yêu cầu bổ sung")]
        [Color(BgColor = "#f1c40f")]
        public static string YeuCauBoSung { get; set; } = "YeuCauBoSung";
        [DisplayName("Từ chối")]
        [Color(BgColor = "#e74c3c")]
        public static string TuChoi { get; set; } = "TuChoi";
        [DisplayName("Được phê duyệt")]
        [Color(BgColor = "#2ecc71")]
        public static string DuocPheDuyet { get; set; } = "DuocPheDuyet";
    }
}
