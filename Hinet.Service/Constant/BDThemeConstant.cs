﻿using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class BDThemeConstant
    {
        [DisplayName("Default")]
        [Color(BgColor = "#1abc9c")]
        public static string Default { get; set; } = "default";
        [DisplayName("Patterns")]
        [Color(BgColor = "#1abc9c")]
        public static string Patterns { get; set; } = "patterns";
        [DisplayName("Chalk")]
        [Color(BgColor = "#1abc9c")]
        public static string Chalk { get; set; } = "chalk";
        [DisplayName("Dark")]
        [Color(BgColor = "#1abc9c")]
        public static string Dark { get; set; } = "dark";
        [DisplayName("Light")]
        [Color(BgColor = "#1abc9c")]
        public static string Light { get; set; } = "light";
        [DisplayName("Black")]
        [Color(BgColor = "#1abc9c")]
        public static string Black { get; set; } = "black";
    }
}
