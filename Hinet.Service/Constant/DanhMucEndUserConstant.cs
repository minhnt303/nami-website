﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class DanhMucEndUserConstant
    {
        /// danh sách hosting
        public static string Hosting = "APP_HOSTING_PROVIDER";
        /// danh sách loại hình cung cấp dịch vụ
        public static string Service = "APP_SERVICE_PROVIDER";
        /// danh sách các tiện ích 
        public static string Utility = "APP_UTILITY";
        /// loại hàng hóa dịch vụ
        public static string Product = "APP_PRODUCT"; 
    }
}
