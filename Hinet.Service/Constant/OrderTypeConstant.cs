﻿using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class OrderTypeConstant
    {
        [DisplayName("Phí đăng tin")]
        [Color(BgColor = "#34495e")]
        public static string PhiDangTin { get; set; } = "250006";
        [DisplayName("Tiền nhà")]
        [Color(BgColor = "#3498db")]
        public static string TienNha { get; set; } = "250000";
    }
}
