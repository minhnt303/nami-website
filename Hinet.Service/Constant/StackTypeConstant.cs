﻿using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class StackTypeConstant
    {
        [DisplayName("Regular")]
        [Color(BgColor = "#1abc9c")]
        public static string Regular { get; set; } = "regular";
        [DisplayName("OneHunderedPercent")]
        [Color(BgColor = "#1abc9c")]
        public static string OneHunderedPercent { get; set; } = "100%";
        [DisplayName("ThreeD")]
        [Color(BgColor = "#1abc9c")]
        public static string ThreeD { get; set; } = "3d";
    }
}
