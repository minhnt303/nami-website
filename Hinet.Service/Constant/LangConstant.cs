﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class LangConstant
    {

        [DisplayName("Tiếng việt")]
        public static string VietNam
        {
            get
            {
                return "vi";
            }
            private set { }
        }
        [DisplayName("Tiếng anh")]
        public static string TiengAnh
        {
            get
            {
                return "en";
            }
            private set { }
        }

    }
}
