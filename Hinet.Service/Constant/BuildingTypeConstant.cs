﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class BuildingTypeConstant
    {
        [DisplayName("Tòa nhà/Khu trọ")]
        public static string Building { get; set; } = "Building";
        [DisplayName("Phòng trọ")]
        public static string Room { get; set; } = "Room";
    }
}
