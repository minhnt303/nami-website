﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class ItemTypeConstant
    {
        [DisplayName("Tòa nhà/Khu trọ")]
        public static string Building { get; set; } = "Building";
        [DisplayName("Phòng trọ")]
        public static string Room { get; set; } = "Room";
        [DisplayName("Hợp đồng")]
        public static string Agreement { get; set; } = "Agreement";
        [DisplayName("Dịch vụ")]
        public static string Service { get; set; } = "Service";
        [DisplayName("Giao dịch")]
        public static string Order { get; set; } = "Order";
    }
}
