﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami
{
    public class PermissionCodeConst
    {


        [DisplayName("Dashboard")]
        public static string Dashboard { get; set; } = "Dashboard";
        [DisplayName("Quản  lý tài khoản")]
        public static string QLTaiKhoan { get; set; } = "QLTaiKhoan";


        [DisplayName("Quản lý hệ thống")]
        public static string QLHeThong { get; set; } = "QLHeThong";

        [DisplayName("Quản lý chức năng")]
        public static string QLChucNang { get; set; } = "QLChucNang";

        [DisplayName("Quản lý vai trò")]
        public static string QLVaiTro { get; set; } = "QLVaiTro";

        [DisplayName("Quản lý danh mục")]
        public static string QLDanhMuc { get; set; } = "QLDanhMuc";
        [DisplayName("Quản lý phòng ban")]
        public static string QLPhongBan { get; set; } = "QLPhongBan";
        [DisplayName("Quản lý cấu hình chung")]
        public static string QLCauHinhChung { get; set; } = "QLCauHinhChung";


        [DisplayName("Quản lý Tỉnh/Thành phố")]
        public static string TINH_index { get; set; } = "TINH_index";
        [DisplayName("Quản lý Quận/Huyện")]
        public static string HUYEN_index { get; set; } = "HUYEN_index";
        [DisplayName("Quản lý Xã/Phường")]
        public static string XA_index { get; set; } = "XA_index";


        [DisplayName("Quản lý Khu trọ")]
        public static string QLKhuTro { get; set; } = "QLKhuTro";

        [DisplayName("Quản lý quảng cáo")]
        public static string BannerQuangCao_index { get; set; } = "BannerQuangCao_index";

        [DisplayName("Quản lý tin tức")]
        public static string QLTuVan_index { get; set; } = "QLTuVan_index";

        #region Quản lý giao dịch
        [DisplayName("Quyền xem tất cả giao dịch")]
        public static string OrderInfo_ViewAll { get; set; } = "OrderInfo_ViewAll";
        #endregion


        #region Quản lý admin
        [DisplayName("Quyền xem tất cả mọi thứ admin")]
        public static string Super_Admin { get; set; } = "Super_Admin";
        #endregion
    }
}
