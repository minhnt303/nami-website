﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class NotificationTypeConstant
    {
        [DisplayName("Hệ thống")]
        public static string System { get; set; } = "System";
        [DisplayName("Tất cả")]
        public static string Global { get; set; } = "Global";

    }
}
