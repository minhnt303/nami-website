﻿using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class ViTriHienThiQuangCaoConstant
    {
        [DisplayName("Trên cùng")]
        [Color(BgColor = "#1abc9c")]
        public static string TrenCung { get; set; } = "TrenCung";
        [DisplayName("Bên phải")]
        [Color(BgColor = "#2ecc71")]
        public static string BenPhai { get; set; } = "BenPhai";
        [DisplayName("Bên trái")]
        [Color(BgColor = "#e74c3c")]
        public static string BenTrai { get; set; } = "BenTrai";
        [DisplayName("Giữa")]
        [Color(BgColor = "#3498db")]
        public static string Giua { get; set; } = "Giua";
        [DisplayName("Cuối cùng")]
        [Color(BgColor = "#9b59b6")]
        public static string CuoiCung { get; set; } = "CuoiCung";
    }
}
