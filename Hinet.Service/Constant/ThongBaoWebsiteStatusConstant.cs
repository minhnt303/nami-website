﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class ThongBaoWebsiteStatusConstant
    {
        [DisplayName("Mới tạo")]
        public static int MoiTao { get; set; } = 0;
        [DisplayName("Chờ duyệt")]
        public static int ChoDuyet { get; set; } = 1;
    }
}
