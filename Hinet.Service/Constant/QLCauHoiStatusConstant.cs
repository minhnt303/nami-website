﻿using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class QLCauHoiStatusConstant
    {
        [DisplayName("Mới nhập")]
        [Color(BgColor = "#1abc9c")]
        public static string MoiNhap { get; set; } = "MoiNhap";
        [DisplayName("Đã tiếp nhân")]
        [Color(BgColor = "#2ecc71")]
        public static string DaTiepNhan { get; set; } = "DaTiepNhan";
        [DisplayName("Đã trả lời")]
        [Color(BgColor = "#3498db")]
        public static string DaTraLoi { get; set; } = "DaTraLoi";
        [DisplayName("Chưa xuất bản")]
        [Color(BgColor = "#f1c40f")]
        public static string ChuaXuatBan { get; set; } = "ChuaXuatBan";
        [DisplayName("Đã xuất bản")]
        [Color(BgColor = "#9b59b6")]
        public static string DaXuatBan { get; set; } = "DaXuatBan";
        [DisplayName("Hủy")]
        [Color(BgColor = "#e74c3c")]
        public static string Huy { get; set; } = "Huy";
    }
}
