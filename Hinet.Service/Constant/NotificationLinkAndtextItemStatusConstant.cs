﻿using Nami.Service.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class NotificationLinkAndtextItemStatusConstant
    {
        [DisplayName(" mới tạo thông tin nhà cho thuê")]
        [Color(BgColor = "#34495e")]
        public static string MoiTao { get; set; } = "MoiTao";
        [DisplayName(" chuyển trạng thái nhà cho thuê thành chờ duyệt")]
        [Color(BgColor = "#3498db")]
        public static string ChoDuyet { get; set; } = "ChoDuyet";
        [DisplayName(" yêu cầu bổ sung thông tin nhà cho thuê")]
        [Color(BgColor = "#f1c40f")]
        public static string YeuCauBoSung { get; set; } = "YeuCauBoSung";
        [DisplayName(" từ chối phê duyệt thông tin nhà cho thuê")]
        [Color(BgColor = "#e74c3c")]
        public static string TuChoi { get; set; } = "TuChoi";
        [DisplayName(" phê duyệt thông tin nhà cho thuê")]
        [Color(BgColor = "#2ecc71")]
        public static string DuocPheDuyet { get; set; } = "DuocPheDuyet";
    }
}
