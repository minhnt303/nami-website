﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class AccountTypeConstant
    {
        [DisplayName("Tài khoản cá nhân/ thương nhân")]
        public static string EndUser { get; set; } = "EndUser";
        [DisplayName("Tài khoản xử lý nghiệp vụ")]
        public static string BussinessUser { get; set; } = "BussinessUser";
        [DisplayName("Tài khoản người dùng")]
        public static string NguoiDung { get; set; } = "NguoiDung";
        [DisplayName("Tài khoản chủ nhà")]
        public static string ChuNha { get; set; } = "ChuNha";
        [DisplayName("Tài khoản nhân viên tòa nhà")]
        public static string NhanVien { get; set; } = "NhanVien";
    }
    public class OrganizationTypeConstant
    {
        [DisplayName("Cá nhân")]
        public static string Personal { get; set; } = "Personal";
        [DisplayName("Thương nhân")]
        public static string Company { get; set; } = "Company";
        [DisplayName("Tổ chức")]
        public static string Organization { get; set; } = "Organization";
    }
}
