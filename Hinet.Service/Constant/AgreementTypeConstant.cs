﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Service.Constant
{
    public class AgreementTypeConstant
    {
        [DisplayName("Khách vãng lai")]
        public static string VisitorOutSys { get; set; } = "VisitorOutSys";
        [DisplayName("Khách trong hệ thống")]
        public static string VisitorInSys { get; set; } = "VisitorInSys";
    }
}
