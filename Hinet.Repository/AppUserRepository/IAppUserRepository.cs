﻿using Nami.Model.IdentityEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.AppUserRepository
{
    public interface IAppUserRepository:IGenericRepository<AppUser>
    {
        AppUser GetById(long id);

    }
   
}
