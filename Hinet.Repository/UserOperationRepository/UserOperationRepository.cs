using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.UserOperationRepository
{
    public class UserOperationRepository : GenericRepository<UserOperation>, IUserOperationRepository
    {
        public UserOperationRepository(DbContext context)
            : base(context)
        {

        }
        public UserOperation GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
