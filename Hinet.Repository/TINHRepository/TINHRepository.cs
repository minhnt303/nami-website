using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.TINHRepository
{
    public class TINHRepository : GenericRepository<TINH>, ITINHRepository
    {
        public TINHRepository(DbContext context)
            : base(context)
        {

        }
        public TINH GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
