using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.RoomInfoRepository
{
    public class RoomInfoRepository : GenericRepository<RoomInfo>, IRoomInfoRepository
    {
        public RoomInfoRepository(DbContext context)
            : base(context)
        {

        }
        public RoomInfo GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<RoomInfo> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
