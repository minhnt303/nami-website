using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.BDDataProvinceRepository
{
    public class BDDataProvinceRepository : GenericRepository<BDDataProvince>, IBDDataProvinceRepository
    {
        public BDDataProvinceRepository(DbContext context)
            : base(context)
        {

        }
        public BDDataProvince GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
