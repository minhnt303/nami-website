using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.QLHopDongRepository
{
    public class QLHopDongRepository : GenericRepository<QLHopDong>, IQLHopDongRepository
    {
        public QLHopDongRepository(DbContext context)
            : base(context)
        {

        }
        public QLHopDong GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
