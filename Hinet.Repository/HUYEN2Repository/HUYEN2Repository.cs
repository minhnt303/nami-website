using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.HUYEN2Repository
{
    public class HUYEN2Repository : GenericRepository<HUYEN2>, IHUYEN2Repository
    {
        public HUYEN2Repository(DbContext context)
            : base(context)
        {

        }
        public HUYEN2 GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
