using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.HUYEN2Repository
{
    public interface IHUYEN2Repository:IGenericRepository<HUYEN2>
    {
        HUYEN2 GetById(long id);

    }
   
}
