using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.BDGraphsRepository
{
    public class BDGraphsRepository : GenericRepository<BDGraphs>, IBDGraphsRepository
    {
        public BDGraphsRepository(DbContext context)
            : base(context)
        {

        }
        public BDGraphs GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
