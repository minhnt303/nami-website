using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.DevicesInfoRepository
{
    public class DevicesInfoRepository : GenericRepository<DevicesInfo>, IDevicesInfoRepository
    {
        public DevicesInfoRepository(DbContext context)
            : base(context)
        {

        }
        public DevicesInfo GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<DevicesInfo> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
