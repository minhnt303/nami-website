using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.MapLocationInfoRepository
{
    public class MapLocationInfoRepository : GenericRepository<MapLocationInfo>, IMapLocationInfoRepository
    {
        public MapLocationInfoRepository(DbContext context)
            : base(context)
        {

        }
        public MapLocationInfo GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
