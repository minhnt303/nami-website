using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.BDValueAxesRepository
{
    public class BDValueAxesRepository : GenericRepository<BDValueAxes>, IBDValueAxesRepository
    {
        public BDValueAxesRepository(DbContext context)
            : base(context)
        {

        }
        public BDValueAxes GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
