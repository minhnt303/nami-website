using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.XARepository
{
    public class XARepository : GenericRepository<XA>, IXARepository
    {
        public XARepository(DbContext context)
            : base(context)
        {

        }
        public XA GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
