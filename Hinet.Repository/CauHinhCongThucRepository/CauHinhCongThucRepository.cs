using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.CauHinhCongThucRepository
{
    public class CauHinhCongThucRepository : GenericRepository<CauHinhCongThuc>, ICauHinhCongThucRepository
    {
        public CauHinhCongThucRepository(DbContext context)
            : base(context)
        {

        }
        public CauHinhCongThuc GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }


        public IQueryable<CauHinhCongThuc> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
