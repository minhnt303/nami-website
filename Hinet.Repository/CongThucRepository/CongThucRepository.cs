using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.CongThucRepository
{
    public class CongThucRepository : GenericRepository<CongThuc>, ICongThucRepository
    {
        public CongThucRepository(DbContext context)
            : base(context)
        {

        }
        public CongThuc GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<CongThuc> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
