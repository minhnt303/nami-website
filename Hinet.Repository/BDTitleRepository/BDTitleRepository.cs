using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.BDTitleRepository
{
    public class BDTitleRepository : GenericRepository<BDTitle>, IBDTitleRepository
    {
        public BDTitleRepository(DbContext context)
            : base(context)
        {

        }
        public BDTitle GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
