using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.TINH2Repository
{
    public interface ITINH2Repository:IGenericRepository<TINH2>
    {
        TINH2 GetById(long id);

    }
   
}
