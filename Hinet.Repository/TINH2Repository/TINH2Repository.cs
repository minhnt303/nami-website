using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.TINH2Repository
{
    public class TINH2Repository : GenericRepository<TINH2>, ITINH2Repository
    {
        public TINH2Repository(DbContext context)
            : base(context)
        {

        }
        public TINH2 GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
