using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.GiaiTrinhPhanAnhRepository
{
    public interface IGiaiTrinhPhanAnhRepository:IGenericRepository<GiaiTrinhPhanAnh>
    {
        GiaiTrinhPhanAnh GetById(long id);

    }
   
}
