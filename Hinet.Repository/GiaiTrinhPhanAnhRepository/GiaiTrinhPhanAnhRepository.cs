using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.GiaiTrinhPhanAnhRepository
{
    public class GiaiTrinhPhanAnhRepository : GenericRepository<GiaiTrinhPhanAnh>, IGiaiTrinhPhanAnhRepository
    {
        public GiaiTrinhPhanAnhRepository(DbContext context)
            : base(context)
        {

        }
        public GiaiTrinhPhanAnh GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
