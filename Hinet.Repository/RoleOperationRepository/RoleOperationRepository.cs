﻿using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.RoleOperationRepository
{
    public class RoleOperationRepository : GenericRepository<RoleOperation>, IRoleOperationRepository
    {
        public RoleOperationRepository(DbContext context) : base(context)
        {
        }
    }
}
