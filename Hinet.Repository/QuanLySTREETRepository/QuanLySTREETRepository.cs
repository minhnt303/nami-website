using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.QuanLySTREETRepository
{
    public class QuanLySTREETRepository : GenericRepository<QuanLySTREET>, IQuanLySTREETRepository
    {
        public QuanLySTREETRepository(DbContext context)
            : base(context)
        {

        }
        public QuanLySTREET GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
