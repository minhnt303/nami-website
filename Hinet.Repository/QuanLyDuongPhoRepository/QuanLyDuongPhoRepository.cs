using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.QuanLyDuongPhoRepository
{
    public class QuanLyDuongPhoRepository : GenericRepository<QuanLyDuongPho>, IQuanLyDuongPhoRepository
    {
        public QuanLyDuongPhoRepository(DbContext context)
            : base(context)
        {

        }
        public QuanLyDuongPho GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
