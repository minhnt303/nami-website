using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.QuanLyDuongPhoRepository
{
    public interface IQuanLyDuongPhoRepository:IGenericRepository<QuanLyDuongPho>
    {
        QuanLyDuongPho GetById(long id);

    }
   
}
