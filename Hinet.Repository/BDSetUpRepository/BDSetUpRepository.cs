using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.BDSetUpRepository
{
    public class BDSetUpRepository : GenericRepository<BDSetUp>, IBDSetUpRepository
    {
        public BDSetUpRepository(DbContext context)
            : base(context)
        {

        }
        public BDSetUp GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
