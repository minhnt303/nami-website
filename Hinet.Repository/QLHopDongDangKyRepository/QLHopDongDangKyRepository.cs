using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.QLHopDongDangKyRepository
{
    public class QLHopDongDangKyRepository : GenericRepository<QLHopDongDangKy>, IQLHopDongDangKyRepository
    {
        public QLHopDongDangKyRepository(DbContext context)
            : base(context)
        {

        }
        public QLHopDongDangKy GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
