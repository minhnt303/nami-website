using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.RentersInfoRepository
{
    public class RentersInfoRepository : GenericRepository<RentersInfo>, IRentersInfoRepository
    {
        public RentersInfoRepository(DbContext context)
            : base(context)
        {

        }
        public RentersInfo GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<RentersInfo> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
