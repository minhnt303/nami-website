using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.DepositsInfoRepository
{
    public class DepositsInfoRepository : GenericRepository<DepositsInfo>, IDepositsInfoRepository
    {
        public DepositsInfoRepository(DbContext context)
            : base(context)
        {

        }
        public DepositsInfo GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<DepositsInfo> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
