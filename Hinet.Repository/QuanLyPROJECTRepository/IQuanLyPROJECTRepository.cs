using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.QuanLyPROJECTRepository
{
    public interface IQuanLyPROJECTRepository:IGenericRepository<QuanLyPROJECT>
    {
        QuanLyPROJECT GetById(long id);

    }
   
}
