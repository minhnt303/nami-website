using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.QuanLyPROJECTRepository
{
    public class QuanLyPROJECTRepository : GenericRepository<QuanLyPROJECT>, IQuanLyPROJECTRepository
    {
        public QuanLyPROJECTRepository(DbContext context)
            : base(context)
        {

        }
        public QuanLyPROJECT GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
