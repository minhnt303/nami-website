using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.CommentByAccountFileAttackRepository
{
    public class CommentByAccountFileAttackRepository : GenericRepository<CommentByAccountFileAttack>, ICommentByAccountFileAttackRepository
    {
        public CommentByAccountFileAttackRepository(DbContext context)
            : base(context)
        {

        }
        public CommentByAccountFileAttack GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }


        public IQueryable<CommentByAccountFileAttack> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
