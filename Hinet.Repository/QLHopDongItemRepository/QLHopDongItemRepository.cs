using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.QLHopDongItemRepository
{
    public class QLHopDongItemRepository : GenericRepository<QLHopDongItem>, IQLHopDongItemRepository
    {
        public QLHopDongItemRepository(DbContext context)
            : base(context)
        {

        }
        public QLHopDongItem GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
