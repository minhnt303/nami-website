using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.RentersInfoImageRepository
{
    public class RentersInfoImageRepository : GenericRepository<RentersInfoImage>, IRentersInfoImageRepository
    {
        public RentersInfoImageRepository(DbContext context)
            : base(context)
        {

        }
        public RentersInfoImage GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
