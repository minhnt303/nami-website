﻿using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.CommonConfigurationRepository
{
    public interface ICommonConfigurationRepository : IGenericRepository<CommonConfiguration>
    {
    }
}
