using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.XA2Repository
{
    public interface IXA2Repository:IGenericRepository<XA2>
    {
        XA2 GetById(long id);

    }
   
}
