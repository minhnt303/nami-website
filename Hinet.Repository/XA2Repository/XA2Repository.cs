using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.XA2Repository
{
    public class XA2Repository : GenericRepository<XA2>, IXA2Repository
    {
        public XA2Repository(DbContext context)
            : base(context)
        {

        }
        public XA2 GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
