using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.CommentByAccountRepository
{
    public class CommentByAccountRepository : GenericRepository<CommentByAccount>, ICommentByAccountRepository
    {
        public CommentByAccountRepository(DbContext context)
            : base(context)
        {

        }
        public CommentByAccount GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<CommentByAccount> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
