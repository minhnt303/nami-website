using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.QLTuVanRepository
{
    public class QLTuVanRepository : GenericRepository<QLTuVan>, IQLTuVanRepository
    {
        public QLTuVanRepository(DbContext context)
            : base(context)
        {

        }
        public QLTuVan GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<QLTuVan> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
