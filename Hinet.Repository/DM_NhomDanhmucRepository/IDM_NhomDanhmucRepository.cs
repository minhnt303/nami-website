﻿using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.DanhmucRepository
{
    public interface IDM_NhomDanhmucRepository : IGenericRepository<DM_NhomDanhmuc>
    {
    }
}
