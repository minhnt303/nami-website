using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.BDBieuDoRepository
{
    public class BDBieuDoRepository : GenericRepository<BDBieuDo>, IBDBieuDoRepository
    {
        public BDBieuDoRepository(DbContext context)
            : base(context)
        {

        }
        public BDBieuDo GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
