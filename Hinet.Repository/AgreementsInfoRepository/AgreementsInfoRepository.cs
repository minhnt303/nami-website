using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.AgreementsInfoRepository
{
    public class AgreementsInfoRepository : GenericRepository<AgreementsInfo>, IAgreementsInfoRepository
    {
        public AgreementsInfoRepository(DbContext context)
            : base(context)
        {

        }
        public AgreementsInfo GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<AgreementsInfo> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
