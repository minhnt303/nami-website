using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.BuildingsInfoRepository
{
    public class BuildingsInfoRepository : GenericRepository<BuildingsInfo>, IBuildingsInfoRepository
    {
        public BuildingsInfoRepository(DbContext context)
            : base(context)
        {

        }
        public BuildingsInfo GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<BuildingsInfo> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
