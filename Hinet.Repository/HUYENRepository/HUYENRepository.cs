using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.HUYENRepository
{
    public class HUYENRepository : GenericRepository<HUYEN>, IHUYENRepository
    {
        public HUYENRepository(DbContext context)
            : base(context)
        {

        }
        public HUYEN GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
