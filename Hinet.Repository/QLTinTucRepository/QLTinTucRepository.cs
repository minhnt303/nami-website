using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.QLTinTucRepository
{
    public class QLTinTucRepository : GenericRepository<QLTinTuc>, IQLTinTucRepository
    {
        public QLTinTucRepository(DbContext context)
            : base(context)
        {

        }
        public QLTinTuc GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }


        public IQueryable<QLTinTuc> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
