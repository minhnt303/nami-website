using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.GiaiTrinhPhanAnhFileAttackRepository
{
    public class GiaiTrinhPhanAnhFileAttackRepository : GenericRepository<GiaiTrinhPhanAnhFileAttack>, IGiaiTrinhPhanAnhFileAttackRepository
    {
        public GiaiTrinhPhanAnhFileAttackRepository(DbContext context)
            : base(context)
        {

        }
        public GiaiTrinhPhanAnhFileAttack GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
