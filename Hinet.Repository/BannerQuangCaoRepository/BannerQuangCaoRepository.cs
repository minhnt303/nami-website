using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nami.Repository;

namespace Nami.Repository.BannerQuangCaoRepository
{
    public class BannerQuangCaoRepository : GenericRepository<BannerQuangCao>, IBannerQuangCaoRepository
    {
        public BannerQuangCaoRepository(DbContext context)
            : base(context)
        {

        }
        public BannerQuangCao GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<BannerQuangCao> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
