using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.ServicesInfoRepository
{
    public class ServicesInfoRepository : GenericRepository<ServicesInfo>, IServicesInfoRepository
    {
        public ServicesInfoRepository(DbContext context)
            : base(context)
        {

        }
        public ServicesInfo GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
