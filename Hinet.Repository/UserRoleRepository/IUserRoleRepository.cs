﻿using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.UserRoleRepository
{
    public interface IUserRoleRepository:IGenericRepository<UserRole>
    {
    }
}
