using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.FileDinhKemRepository
{
    public class FileDinhKemRepository : GenericRepository<FileDinhKem>, IFileDinhKemRepository
    {
        public FileDinhKemRepository(DbContext context)
            : base(context)
        {

        }
        public FileDinhKem GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
