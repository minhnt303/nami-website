using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.QLCauHoiRepository
{
    public class QLCauHoiRepository : GenericRepository<QLCauHoi>, IQLCauHoiRepository
    {
        public QLCauHoiRepository(DbContext context)
            : base(context)
        {

        }
        public QLCauHoi GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }


        public IQueryable<QLCauHoi> GetAllAsQueryable()
        {
            return base.GetAllAsQueryable().Where(x => x.IsDelete != true);
        }
    }
}
