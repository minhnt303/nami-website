using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.OrderInfoRepository
{
    public class OrderInfoRepository : GenericRepository<OrderInfo>, IOrderInfoRepository
    {
        public OrderInfoRepository(DbContext context)
            : base(context)
        {

        }
        public OrderInfo GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
