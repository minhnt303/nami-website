using Nami.Model.IdentityEntities;
using Nami.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nami.Repository.QuanLyDuAnRepository
{
    public class QuanLyDuAnRepository : GenericRepository<QuanLyDuAn>, IQuanLyDuAnRepository
    {
        public QuanLyDuAnRepository(DbContext context)
            : base(context)
        {

        }
        public QuanLyDuAn GetById(long id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
        
    }
}
